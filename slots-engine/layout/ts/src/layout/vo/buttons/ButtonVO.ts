module layout {
	export class ButtonVO extends BaseVO {
		public static CLASS_NAME:string = "b";

		public states:Array<ButtonStateVO>;
		public isToggle:boolean;

		constructor() {
			super();
		}

		public read(data:any):void {
			super.read(data);
			var statesData = data.states;
			this.isToggle = data.isToggle;
			this.states = new Array(statesData.length);
			for (var i:number = 0; i < statesData.length; i++) {
				var state:ButtonStateVO = new ButtonStateVO();
				state.read(statesData[i]);
				this.states[i] = state;
			}
		}
	}
}
