module layout {

	export class DropShadowFilterVO extends GlowFilterVO {
		public static CLASS_NAME:string = "ds";

		public distance:number;
		public angle:number;
		public hideObject:boolean;

		constructor() {
			super();
		}

		public read(data:any):void {
			super.read(data);
			this.distance = data.distance;
			this.angle = data.angle;
			this.hideObject = data.hideObject;
		}
	}
}
