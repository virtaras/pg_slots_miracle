module layout {
	import SpriteSheet = createjs.SpriteSheet;

	export class StarlingSpriteSheet {
		public static create(xml:Document, image:any):SpriteSheet {
			var tp:any = XMLUtils.getElement(xml, "TextureAtlas");
			if (!tp) {
				console.log("TextureAtlas not found");
				return null;
			}

			var textures:any = XMLUtils.getElements(tp, "SubTexture");
			var frames:Array<Array<number>> = [];
			if (textures.length == 0) {
				console.log("No textures found.");
				return null;
			}

			for (var i:number = 0; i < textures.length; i++) {
				var t = textures[i];
				var x:number = t.getAttribute("x");
				var y:number = t.getAttribute("y");
				var w:number = t.getAttribute("width");
				var h:number = t.getAttribute("height");
				var fx:number = t.getAttribute("frameX");
				var fy:number = t.getAttribute("frameY");
				frames.push([x, y, w, h, 0, fx, fy]);
			}
			var data:Object = { "frames": frames, "images": [image]};
			return new SpriteSheet(data);
		}
	}
}
