module layout {
	import LoadQueue = createjs.LoadQueue;

	export class AssetFinder {
		private static transparentFormat:string = AssetFinder.getTransparentFormat();
		private assets:Array<AssetVO>;

		constructor() {
			this.assets = [];
		}

		public find(vo:BaseVO):void {
			var imageVO:ImageVO;
			var extension:string;
			switch (vo.className) {
				case ImageVO.CLASS_NAME:
				{
					imageVO = <ImageVO>vo;
					extension = imageVO.hasAlpha ? AssetFinder.transparentFormat : FileConst.JPEGXR_EXTENSION;
					this.addAsset(new AssetVO(imageVO.fileName, imageVO.fileName + extension, LoadQueue.IMAGE));
					break;
				}
				case ButtonVO.CLASS_NAME:
				{
					var buttonVO:ButtonVO = <ButtonVO>vo;
					var states:Array<ButtonStateVO> = buttonVO.states;
					for (var i:number = 0; i < states.length; i++) {
						var buttonStateVO:ButtonStateVO = states[i];
						imageVO = buttonStateVO.imageVO;
						extension = imageVO.hasAlpha ? AssetFinder.transparentFormat : FileConst.JPEGXR_EXTENSION;
						this.addAsset(new AssetVO(imageVO.fileName, imageVO.fileName + extension, LoadQueue.IMAGE));
					}
					break;
				}
				case SimpleMovieVO.CLASS_NAME:
				{
					var movieVO:SimpleMovieVO = <SimpleMovieVO>vo;
					extension = movieVO.hasAlpha ? AssetFinder.transparentFormat : FileConst.JPEGXR_EXTENSION;
					this.addAsset(new AssetVO(movieVO.fileName, movieVO.fileName + extension, LoadQueue.IMAGE));
					this.addAsset(new AssetVO(movieVO.fileName + FileConst.XML_EXTENSION, movieVO.fileName + FileConst.XML_EXTENSION, LoadQueue.XML));
					break;
				}
				case SpriteVO.CLASS_NAME:
				{
					this.findSpriteRes(<SpriteVO>vo);
					break;
				}
			}
		}

		private static getTransparentFormat():string {
			// Detect WebP-lossless support
//			if (AssetFinder.isWebPSupported()) {
//				return FileConst.WEBP_EXTENSION;
//			}
			// Detect JPEG XR support
//			else if (AssetFinder.isJpegXRSupported()) {
//				return FileConst.JPEGXR_EXTENSION;
//			}
//			else {
				return FileConst.PNG_EXTENSION;
//			}
		}

		private static isWebPSupported():boolean {
			var webP = new Image();
			webP.src = 'data:image/webp;base64,UklGRjoAAABXRUJQVlA4IC4AAACyAgCdASoCAAIALmk0mk0iIiIiIgBoSygABc6WWgAA/veff/0PP8bA//LwYAAA';
			return webP.height == 2;
		}

		private static isJpegXRSupported():boolean {
			var jxr = new Image();
			// is a 1x1 black image
			jxr.src = "data:image/vnd.ms-photo;base64,SUm8AQgAAAAFAAG8AQAQAAAASgAAAIC8BAABAAAAAQAAAIG8BAABAAAAAQAAAMC8BAABAAAAWgAAAMG8BAABAAAAHwAAAAAAAAAkw91vA07+S7GFPXd2jckNV01QSE9UTwAZAYBxAAAAABP/gAAEb/8AAQAAAQAAAA==";
			return jxr.height == 1;
		}

		public getAssets():Array<AssetVO> {
			return this.assets;
		}

		private findSpriteRes(vo:SpriteVO):void {
			var displays:Array<BaseVO> = vo.displays;
			for (var i:number = 0; i < displays.length; i++) {
				this.find(displays[i]);
			}
		}

		private addAsset(vo:AssetVO):void {
			for (var i:number = 0; i < this.assets.length; i++) {
				if (this.assets[i].id == vo.id) {
					return;
				}
			}
			this.assets.push(vo);
		}
	}
}
