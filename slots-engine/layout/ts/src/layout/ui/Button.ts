module layout {
	import Bitmap = createjs.Bitmap;
	import Touch = createjs.Touch;

	export class Button extends Bitmap {
		private static STATE_UP:string = "up";
		private static STATE_OVER:string = "over";
		private static STATE_DOWN:string = "down";
		private static STATE_DISABLE:string = "dis";

		private textures:any;
		private state:string;

		constructor(textures:any) {
			this.state = Button.STATE_UP;
			this.textures = textures;
			var image:any = this.textures[this.state];
			super(image);

			this.on("mousedown", ()=> {
				this.onChangeState(Button.STATE_DOWN);
			});
			this.on("pressup", ()=> {
				this.onChangeState(Button.STATE_UP);
			});
			this.on("rollover", ()=> {
				this.onChangeState(Button.STATE_OVER);
				document.body.style.cursor = "pointer";
			});
			this.on("rollout", ()=> {
				this.onChangeState(Button.STATE_UP);
				document.body.style.cursor = "default";
			});
		}

		private onChangeState(state:string):void {
			if (this.state != Button.STATE_DISABLE) {
				this.changeState(state);
			}
		}

		private changeState(state:string):void {
			if (this.state != state) {
				this.state = state;
				var image:any = this.textures[state];
				if (image != null) {
					this.image = image;
				}
			}
		}

		public getEnable():boolean {
			return this.state != Button.STATE_DISABLE;
		}

		public setEnable(value:boolean):void {
			this.changeState(value ? Button.STATE_UP : Button.STATE_DISABLE);
			this.mouseEnabled = value;
		}
	}
}