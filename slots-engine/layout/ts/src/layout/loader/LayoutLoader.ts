module layout {
	import LoadQueue = createjs.LoadQueue;
	import EventDispatcher = createjs.EventDispatcher;

	export class LayoutLoader extends EventDispatcher {
		public static EVENT_ON_LOADED:string = "layout_loaded";
		public static LAYOUT_ASSET_ID:string = "layout";

		private url:string;
		private vo:BaseVO;
		private assets:LoadQueue;

		constructor(url:string) {
			super();
			this.url = url;
		}

		public load():void {
			console.log("Start load layout URL = " + this.url);
			var assetVO:AssetVO = new AssetVO(LayoutLoader.LAYOUT_ASSET_ID, this.url, LoadQueue.JSON);
			var loader:LoadQueue = new LoadQueue(false);
			loader.on("complete", () => {
				var layout:Object = loader.getResult(LayoutLoader.LAYOUT_ASSET_ID);
				this.vo = BaseVO.parse(layout);
				this.startLoadTexture();
			});
			loader.loadFile(assetVO);
		}

		public getVO():BaseVO {
			return this.vo;
		}

		public getAssets():LoadQueue {
			return this.assets;
		}

		private startLoadTexture():void {
			var assetFolderURL:string = URLUtils.getFolderUrl(this.url) + "/" + FileConst.MAIN_RES_FOLDER;
			var assetsFinder:AssetFinder = new AssetFinder();
			assetsFinder.find(this.vo);
			var assetsVO:Array<AssetVO> = assetsFinder.getAssets();

			this.assets = new LoadQueue(false);
			this.assets.on("complete", () => {
				//console.log("Layout loaded URL = " + this.url);
				this.dispatchEvent(LayoutLoader.EVENT_ON_LOADED);
			});
			this.assets.loadManifest(assetsVO, true, assetFolderURL);
		}
	}
}
