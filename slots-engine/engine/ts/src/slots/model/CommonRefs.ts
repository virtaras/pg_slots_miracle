module engine {
	import Point = createjs.Point;
	import Rectangle = createjs.Rectangle;

	export class CommonRefs {
		public config:any;
		public layouts:Object;
		public server:ServerData;
		public symbolsRect:Array<Array<Rectangle>>;
		public gameName:string;
		public dirName:string;
		public key:string;
		public login:string;
		public password:string;
		public room:string;
		public artWidth:number;
		public artHeight:number;
        public arrLinesIds:Array<number>;
		public isDemo:boolean;
		public isDebug:boolean;
		public isMobile:boolean;
		public isLayoutsLoaded:boolean = false;
		public isSoundLoaded:boolean;
		public isSoundOn:boolean;
		public originBonus:string;
		public moverType:string = ReelMoverTypes.DEFAULT;
		public restore:boolean;
		public payTableLoaded:boolean;
		public wildSymbolId:number = 2;
		public getParams:any;
		public isTurbo:boolean = false;
		public isAuto:boolean = false;

		constructor() {
		}
	}
}