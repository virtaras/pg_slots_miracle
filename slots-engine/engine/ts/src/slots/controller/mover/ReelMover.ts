module engine {
	import EventDispatcher = createjs.EventDispatcher;
	import Ticker = createjs.Ticker;

	export class ReelMover extends EventDispatcher {
		public static SYMBOLS_IN_REEL:number = 4;
		// events
		public static EVENT_REEL_STOPPED:string = "event_reel_stopped";
		public static EVENT_REEL_PREPARE_STOPPED:string = "event_reel_prepare_stopped";
		// reel strips type
		public static TYPE_REGULAR:string = "regular";
		public static TYPE_FREE_SPIN:string = "freeSpins";
		// mover states
		private static STATE_NORMAL:string = "normal";
		private static STATE_UP:string = "up";
		private static STATE_MOVING:string = "moving";
		private static STATE_DOWN:string = "end";

		private static UP_MOVE_SYMBOLS:number = 1;
		private static DOWN_MOVE_SYMBOLS:number = 1;
		private static MOVE_SPEED:number = 2;
		private static DELAY_BETWEEN_START_REEL:number = 7;
		private static REGULAR_STOP_SYMBOLS:number = 4;
		private static HARD_STOP_SYMBOLS:number = 4;
		private static TURBO_STOP_SYMBOLS:number = 1;

		private static EPSILON:number = 0.001;
		private static PREPARE_REEL_SOUND_TIME:number = 0.09;

		private reelStrips:Object;
		private reelView:ReelView;
		private reelId:number;
		// состояние вращения барабана (STATE_NORMAL, STATE_UP, STATE_MOVING, STATE_END)
		private state:string;
		// высота которая приходится на один видимый символ
		private dh:number;
		// позиция барабана при инициализации
		private startReelPos:number;
		// осталось до старта вращения барабана
		private delayStartReel:number;
		private isStarted:boolean;
		private isCanStop:boolean;
		public isTurbo:boolean = false;
		// тип ленты
		private moverType:string;
		// лента для текущего барабана
		private reelSequence:Array<number>;
		// длина лента для текущего барабана
		private sequenceLength:number;
		private upEasing:BezierEasing;
		private moveEasing:LinearEasing;
		private downEasing:BezierEasing;
		// текущее смещение слота в координатах
		private pos:number;
		// смещение на предыдущем шаге слота в координатах
		private posPrevState:number;
		// текущий индекс в ленте
		private currentIdx:number;
		// индекс старта вращения в ленте
		private startIdx:number;
		// индекс окончания вращения в ленте
		private targetIdx:number;
		private isCutSequence:boolean;
		private isHardStop:boolean;
		private isFirst:boolean = true;

		constructor(reelStrips:Object, reelView:ReelView, reelId:number) {
			super();
			this.reelStrips = reelStrips;
			this.reelView = reelView;
			this.reelId = reelId;
		}

		public init(wheel:Array<number>, sequenceType:string):void {
			this.state = ReelMover.STATE_NORMAL;
			this.dh = this.reelView.getSymbol(0).getBounds().height;
			this.startReelPos = this.reelView.getY();

			this.changeSequenceType(sequenceType);
			this.resetData();
			this.setServerResponse(wheel);
		}

		public onEnterFrame():void {
			if(this.isTurbo && this.state != ReelMover.STATE_NORMAL && this.state != ReelMover.STATE_DOWN && this.state != ReelMover.STATE_MOVING && !this.isFirst){
				this.state = ReelMover.STATE_MOVING;
			}
			if (this.state == ReelMover.STATE_NORMAL) {
				return;
			}
			if (this.delayStartReel > 0 && this.state == ReelMover.STATE_UP) {
				this.delayStartReel -= 1;
				return;
			}
			if (this.isStarted) {
				this.isStarted = false;
			}
			if (this.state == ReelMover.STATE_UP) {
				this.up();
			}
			if (this.state == ReelMover.STATE_MOVING) {
				this.move();
			}
			if (this.state == ReelMover.STATE_DOWN) {
				this.down();
			}
			this.updatePos(false);
		}

		public start():void {
			this.isCanStop = false;
			this.state = ReelMover.STATE_UP;
		}

		public expressStop():void {
			this.isCutSequence = false;
			this.isHardStop = true;
		}

		public onGetSpin(wheel:Array<number>):void {
			if (!this.isCanStop) {
				this.setServerResponse(wheel);
				this.updatePos(false);
				this.isCanStop = true;
			}
		}

		private up():void {
			if (this.upEasing.getTime() < 1) {
				this.pos = this.upEasing.getPosition();
			}
			else {
				this.posPrevState = this.pos;
				this.state = ReelMover.STATE_MOVING;
			}
		}

		private move():void {
			//console.log("this.moveEasing.getTime()="+this.moveEasing.getTime()+" this.isCanStop="+this.isCanStop);
			if (this.moveEasing.getTime() >= 1) {
				if (this.isCanStop) {
					this.cutSequence();
					if (this.currentIdx == this.correctIdx(this.targetIdx - ReelMover.DOWN_MOVE_SYMBOLS)) {
						this.posPrevState = this.pos;
						this.state = ReelMover.STATE_DOWN;
						return;
					}
				}
				this.moveEasing.reset();
				this.posPrevState = this.pos;
			}
			this.pos = this.posPrevState + this.moveEasing.getPosition();
		}

		private down():void {
			if (this.downEasing.getTime() < 1) {
				this.pos = this.posPrevState + this.downEasing.getPosition();
				if(this.downEasing.getTime()<ReelMover.PREPARE_REEL_SOUND_TIME){
					this.dispatchEvent(ReelMover.EVENT_REEL_PREPARE_STOPPED, this.reelView.getReelId());
				}
			}
			else {
				this.stop();
			}
		}

		public changeSequenceType(moverType:string):void {
			if (this.moverType != moverType) {
				this.moverType = moverType;

				var reelStrips:Array<Array<number>> = this.reelStrips[moverType];
				var reelId:number = this.reelView.getReelId();
				this.reelSequence = reelStrips[reelId].reverse();
				this.sequenceLength = this.reelSequence.length;

				this.upEasing = new BezierEasing(0, 0, 4, -3, 0);
				this.upEasing.setParameters(this.dh * ReelMover.UP_MOVE_SYMBOLS, 10);

				this.moveEasing = new LinearEasing();
				this.moveEasing.setParameters(this.dh, ReelMover.MOVE_SPEED);

				this.downEasing = new BezierEasing(0, -2, 10, -15, 8);
				this.downEasing.setParameters(this.dh * ReelMover.DOWN_MOVE_SYMBOLS, 15);

				this.resetForChangeSequence();
			}
		}

		private stop():void {
			this.isFirst = false;
			this.state = ReelMover.STATE_NORMAL;
			this.resetData();
			this.dispatchEvent(ReelMover.EVENT_REEL_STOPPED, this.reelView.getReelId());
		}

		private setServerResponse(wheel:Array<number>, is_synchro_stop:boolean = true):void {
			var symbolCount:number = ReelMover.SYMBOLS_IN_REEL;
			var targetWheels:Array<number> = new Array(symbolCount);
			for (var i:number = 0; i < symbolCount; i++) {
				targetWheels[i] = wheel[symbolCount - i + symbolCount * this.reelView.getReelId()-1];
			}
			console.log("targetWheels",targetWheels);
			if(is_synchro_stop){
				this.pasteSequenceReelsAtIdx(targetWheels);
			}
			var idx:number = this.getSequenceIdx(targetWheels);

			if (idx == -1) {
				var reverseReelSequence:Array<number> = this.reelSequence.slice(0).reverse();
				var reverseTargetWheels:Array<number> = targetWheels.slice(0).reverse();
				console.log("[ERROR]: reel id: " + this.reelView.getReelId() + " sequence type: " + this.moverType + " target wheels: " + reverseTargetWheels + " not found in sequence: " + reverseReelSequence);

				console.log("targetWheels",targetWheels);
				console.log("wheel",wheel);
				this.reelSequence = this.reelSequence.concat(targetWheels);
				this.sequenceLength = this.reelSequence.length;
				idx = this.getSequenceIdx(targetWheels);
			}
			if (this.state == ReelMover.STATE_NORMAL) {
				this.startIdx = idx;
				this.updatePos(true);
			}
			else {
				this.targetIdx = idx;
			}
		}

		private pasteSequenceReelsAtIdx(targetWheels:Array<number>):void{
			var idx:number = this.currentIdx + ReelMover.SYMBOLS_IN_REEL*2;
			//TODO: finish redefining idx for all states (should depend of state and reelid sometimes)
			switch (this.state){
				case ReelMover.STATE_NORMAL:{
					break;
				}
				case ReelMover.STATE_MOVING:{
					idx += this.reelId*ReelMover.SYMBOLS_IN_REEL;
					break;
				}
				default:{
					break;
				}
			}
			console.log(this.reelId, "this.reelid");
			for(var i:number=0; i<ReelMover.SYMBOLS_IN_REEL; ++i){
				this.reelSequence[(i+idx) % this.reelSequence.length] = targetWheels[i];
			}
			return;
		}

		private updatePos(isFist:boolean):void {
			var idx:number = this.getCurrentIdx();
			var dh:number = this.pos % this.dh - this.dh;
			this.reelView.setY(this.startReelPos + dh);

			if (this.currentIdx != idx || isFist) {
				var symbolCount:number = ReelMover.SYMBOLS_IN_REEL + 1;
				for (var i:number = 0; i < symbolCount; i++) {
					var symbolId:number = symbolCount - i - 1;
					var symbolIdx:number = this.correctIdx(idx + i);
					var frame:number = this.reelSequence[symbolIdx] - 1;
					this.reelView.setSymbolFrame(symbolId, frame);
				}
				this.currentIdx = idx;
			}
		}

		private getCurrentIdx():number {
			return Utils.float2int(this.correctIdx(this.startIdx + Math.abs(this.pos + ReelMover.EPSILON) / this.dh));
		}

		private getSequenceIdx(value:Array<number>):number {
			var i:number = 0;
			var valueLength:number = value.length;
			var j:number = 0;
			while (j < valueLength) {
				if (this.reelSequence.length == i) {
					// for set wheel
					return -1;
				}
				if (this.reelSequence[i] == value[j]) {
					j++;
					if (i == this.sequenceLength - 1) {
						i = -1;
					}
				}
				else if (j > 0) {
					i = this.correctIdx(i - j);
					j = 0;
				}
				i++;
			}
			var idx:number = i - valueLength;
			if (idx < 0) {
				idx = this.sequenceLength + idx;
			}
			return idx;
		}

		private  correctIdx(value:number):number {
			if (value >= 0 && value < this.sequenceLength) {
				return value;
			}
			return (value + this.sequenceLength) % this.sequenceLength;
		}

		private cutSequence():boolean {
			if (this.isCutSequence) {
				return false;
			}
			var maxDiv:number = this.isTurbo ? ReelMover.TURBO_STOP_SYMBOLS : this.isHardStop ? ReelMover.HARD_STOP_SYMBOLS : ReelMover.REGULAR_STOP_SYMBOLS;
			this.isHardStop = false;
			var realDivIdx:number = this.correctIdx(this.targetIdx - ReelMover.DOWN_MOVE_SYMBOLS - this.currentIdx);
			if (realDivIdx <= maxDiv) {
				return;
			}
			var symbolId:number = this.reelSequence[this.correctIdx(this.currentIdx + maxDiv)];
			if (this.isGroupSymbol(symbolId)) {
				return;
			}
			var div:number = realDivIdx - maxDiv;
			while (this.isGroupSymbol(this.reelSequence[this.correctIdx(this.currentIdx + div + maxDiv)])) {
				div--;
			}
			if (div <= 0) {
				return;
			}
			this.pos += div * this.dh;
			this.currentIdx = this.getCurrentIdx();
			this.isCutSequence = true;
		}

		// TODO: need implemented
		private isGroupSymbol(symbolId:number):boolean {
			//console.log("isGroupSymbol", "symbolId="+symbolId);
			//console.log("isGroupSymbol this.getCurrentIdx()=", this.getCurrentIdx());
			//console.log("isGroupSymbol this.correctIdx()=", this.correctIdx);
			//console.log("isGroupSymbol this.reelSequence", this.reelSequence);
			return false;
		}

		// Сброс данных после остановки барабана
		private resetData():void {
			this.delayStartReel = ReelMover.DELAY_BETWEEN_START_REEL * this.reelView.getReelId();
			this.startIdx = this.targetIdx;
			this.targetIdx = 0;
			this.isCutSequence = false;
			this.pos = 0;
			this.upEasing.reset();
			this.downEasing.reset();
			this.moveEasing.reset();
		}

		// Сброс данных после изменения ленты
		private resetForChangeSequence():void {
			this.currentIdx = 0;
			this.startIdx = 0;
		}
	}
}