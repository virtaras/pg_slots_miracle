module engine {
	import Stage = createjs.Stage;
	import Ticker = createjs.Ticker;
	import Touch = createjs.Touch;
	import Container = createjs.Container;
	import LayoutCreator = layout.LayoutCreator;
	import Shape = createjs.Shape;
	import Text = createjs.Text;

	export class GameController extends BaseController {
		public static MOBILE_TO_WEB_SCALE:number = 1;
		public static SHADOW_COLOR:string = "rgba(0,0,0,1)";
		public static EVENT_BACK_TO_LOBBY:string = "back_to_lobby";
		public static EVENT_HIDE_PRELOADER:string = "hide_preloader";
		public static TICKER_FPS:number = 30;

		private stage:Stage;
		private common:CommonRefs;
		private container:Container;
		private orientationView:RotateScreenView;
		private bgTemp:Shape;
		public loaderView:LoaderView;
		public windowParams:any = {};
		public payTablesInited:boolean = false;

		constructor() {
			super(new ControllerManager());
		}

		/*public start(gameName:string, key:string, container:Container):void {
			super.init();

			this.container = container;
			this.common = new CommonRefs();
			this.common.gameName = gameName;
			this.common.key = key;
			this.common.isMobile = Device.isMobile();

			this.initBaseControllers();
		}*/

		public startSingle(stage:Stage, artWidth:number, artHeight:number, gameName:string, mobile:boolean, isDemo:boolean, login:string, password:string, isDebug:boolean, room:string, getParams:any = []):void {
			this.getWindowParams();
			console.log(isDemo,"isDemo");
			//alert("startSingle: window.outerWidth="+window.outerWidth+" window.outerHeight="+window.outerHeight+" window.innerWidth="+this.windowParams.innerWidth+" window.innerHeight="+this.windowParams.innerHeight);
			super.init();
			this.stage = stage;

			this.common = new CommonRefs();
			this.common.isMobile = mobile;
			//this.common.isMobile = true; //always load mobile version //mobiletoweb
			this.common.gameName = this.common.isMobile ? gameName + "_mobile" : gameName;
			this.common.dirName = gameName + "/"; //ALLINONEGAMES
			//this.common.dirName = ""; //SEPARETEGAMES
			this.common.login = login;
			this.common.password = password;
			this.common.room = room;
			this.common.isDemo = isDemo;
			this.common.isDebug = isDebug;
			this.common.artWidth = artWidth;
			this.common.artHeight = artHeight;
			this.common.getParams = getParams;
			this.common.key = (getParams['token']) ? getParams['token'] : null;

			Ticker.setFPS(GameConst.GAME_FPS);

			this.container = new Container();
			if (!this.common.isMobile) {
				this.stage.enableMouseOver(Ticker.getFPS());
			}

			//this.stage.enableMouseOver(Ticker.getFPS()); //mobiletoweb

			this.stage.mouseMoveOutside = true;
			if (Touch.isSupported()) {
				Touch.enable(this.stage, true, true);
			}
			this.stage.addChild(this.container);

			Ticker.addEventListener("tick", ()=> {
				this.gameUpdate();
			});

			FullScreen.init();

			this.onResize();
			this.initBaseControllers();
		}

		public gameUpdate():void {
			this.manager.onEnterFrame();
		}

		public onEnterFrame():void {
			if (this.stage != null) {
				this.stage.update();
			}
		}

        public listNotification():Array<string> {
            var notifications:Array<string> = super.listNotification();
            notifications.push(NotificationList.SERVER_INIT);
            notifications.push(NotificationList.BACK_TO_LOBBY);
            notifications.push(NotificationList.HIDE_PRELOADER);
            notifications.push(NotificationList.LAZY_LOAD_COMP);
            //notifications.push(NotificationList.LOADER_LOADED);
            return notifications;
        }

        public handleNotification(message:string, data:any):void {
            switch (message) {
                case NotificationList.SERVER_INIT:
                {
                    this.initGameControllers();
					this.initKeyboardHandler();
                    break;
                }
                case NotificationList.BACK_TO_LOBBY:
                {
                    this.container.dispatchEvent(GameController.EVENT_BACK_TO_LOBBY);
                    break;
                }
                case NotificationList.HIDE_PRELOADER:
                {
					this.stage.dispatchEvent(GameController.EVENT_HIDE_PRELOADER);
                    break;
                }
                case NotificationList.LAZY_LOAD_COMP:
                {
                    this.initPayTables();
                    break;
                }
				//case NotificationList.LOADER_LOADED:
				//{
				//	this.loaderView.visible = false;
				//	this.container.addChild(this.loaderView);
				//	this.onResize();
				//	this.loaderView.visible = true;
				//	break;
				//}
            }
        }

		private initBaseControllers():void {
			var baseController:BaseController;

			baseController = new ServerController(this.manager, this.common);
			baseController.init();

			this.loaderView = new LoaderView();
			baseController = new LoaderController(this.manager, this.common, this.loaderView);
			baseController.init();

			this.container.addChild(this.loaderView);
		}

        private initPayTables():void {
			if(!this.payTablesInited) {
				this.payTablesInited = true;
				var baseController:BaseController;
				baseController = new PayTableController(this.manager, this.common, this.container);
				baseController.init();

				// create background
				var backgroundContainer:Container = new Container();
				baseController = new BackgroundController(this.manager, this.common, backgroundContainer);
				baseController.init();

				//if(this.bgTemp)this.container.removeChild(this.bgTemp);
				this.container.addChildAt(backgroundContainer, 0);

				this.common.payTableLoaded = true;
				this.send(NotificationList.LOAD_PAY_TABLE);
			}
        }

		private initGameControllers():void {
			console.log("this.common.config", this.common.config);
			//Ticker.setFPS(GameController.TICKER_FPS);
			var baseController:BaseController;

			this.onResize();

			// create header
			var headerContainer:Container = new Container();
			baseController = new HeaderController(this.manager, this.common, headerContainer);
			baseController.init();

			// create reels
			//var reelsView:ReelsView = this.common.layouts[ReelsView.LAYOUT_NAME];
			//baseController = new ReelController(this.manager, this.common, reelsView);
			//baseController.init();

			this.common.moverType = (this.common.config.reelMoverType) ? this.common.config.reelMoverType : ReelMoverTypes.DEFAULT;
			switch(this.common.moverType){
				case ReelMoverTypes.TAPE:{
					var reelsView:ReelsView = this.common.layouts[ReelsView.LAYOUT_NAME];
					baseController = new ReelController(this.manager, this.common, reelsView);
					break;
				}
				case ReelMoverTypes.CASCADE:{
					var reelsView:ReelsView = this.common.layouts[ReelsView.LAYOUT_NAME];
					baseController = new ReelControllerCascade(this.manager, this.common, reelsView);
					break;
				}
			}
			baseController.init();

			//prepare canvas to draw
			//var linesView:Container = new Container();
			//baseController = new DrawController(this.manager, this.common, linesView);
			//baseController.init();

			// create lines
			var linesView:LinesView = this.common.layouts[LinesView.LAYOUT_NAME];
			baseController = new LinesController(this.manager, this.common, linesView);
			baseController.init();

			// create win animation
			var winView:Container = new Container();
			baseController = new WinController(this.manager, this.common, winView);
			baseController.init();

			//create Wild Views
			baseController = new ReelWildController(this.manager, this.common, reelsView, winView);
			baseController.init();

			// create bonus
			var bonusContainer:Container = new Container();
			baseController = new BonusHolderController(this.manager, this.common, bonusContainer);
			baseController.init();

			// create game hud
			var hudView:HudView = this.common.layouts[HudView.LAYOUT_NAME];
			hudView.create();

			//if(this.common.isMobile && this.common.layouts[BorderView.LAYOUT_NAME]){
			//	var borderView:BorderView = this.common.layouts[BorderView.LAYOUT_NAME];
			//	baseController = new BorderController(this.manager, this.common, borderView);
			//	baseController.init();
			//}

			//JACKPOT
			if(this.common.layouts[JackpotView.LAYOUT_NAME]) {
				var jackpotView:JackpotView = this.common.layouts[JackpotView.LAYOUT_NAME];
				jackpotView.create();
				baseController = new JackpotController(this.manager, this.common, jackpotView);
				baseController.init();
				//headerContainer.addChild(jackpotView); //jackpot over bonus holder
				hudView.addChildAt(jackpotView, 0); //jackpot under bonus holder
			}
			if(this.common.layouts[JackpotViewPopup.LAYOUT_NAME]) {
				var jackpotViewPopup:JackpotView = this.common.layouts[JackpotViewPopup.LAYOUT_NAME];
				jackpotViewPopup.create();
			}

			// create bonus
			var modalContainer:Container = new Container();
			baseController = new ModalWindowController(this.manager, this.common, modalContainer);
			baseController.init();

			// error controller
			baseController = new ErrorController(this.manager, this.common, modalContainer);
			baseController.init();

			var autoSpinTableView:Container = <Container>hudView.getChildByName(HudView.AUTO_SPIN_TABLE);
			if (autoSpinTableView != null) {
				var autoSpinTableMask:Container = <Container>hudView.getChildByName(HudView.AUTO_SPIN_TABLE_MASK);
				baseController = new AutoSpinTableController(this.manager, this.common, autoSpinTableView, autoSpinTableMask);
				baseController.init();
			}

			baseController = new StatsController(this.manager, this.common, hudView);
			baseController.init();

			var hudController:HudController = new HudController(this.manager, this.common, hudView);
			hudController.init();

			var settingsView:Container = <Container>hudView.getChildByName(HudView.SETTINGS_MENU);
			if (settingsView != null) {
				baseController = new SettingsController(this.manager, this.common, hudController, settingsView);
				baseController.init();
			}
			baseController = new SoundController(this.manager, this.common);
			baseController.init();

			this.container.addChild(reelsView);
			this.container.addChild(linesView);
			//if(borderView)this.container.addChild(borderView); // if exist border view
			this.container.addChild(winView);
			this.container.addChild(hudView);
			if (this.common.isMobile) {
				hudView.resizeForMobile();
			}
			this.container.addChild(bonusContainer);
			this.container.addChild(headerContainer);
			this.container.addChild(modalContainer);


			//this.container.addChild(wheelView);
			//// create set wheel for debug
			//if (this.common.isDebug) {
			//	var wheelView:SetWheelView = new SetWheelView(this.common.symbolsRect.length, this.common.symbolsRect[0].length);
			//	baseController = new SetWheelController(this.manager, wheelView);
			//	baseController.init();
			//}

		}

		private initKeyboardHandler():void {
			document.onkeydown = (event)=> {
				this.send(NotificationList.KEYBOARD_CLICK, event.keyCode);
			};
		}

		private showChangeRotation():void {
			if (this.windowParams.innerHeight > this.windowParams.innerWidth) {
				if (this.orientationView == undefined){
					if (this.common.layouts != null) {
						this.orientationView = this.common.layouts[RotateScreenView.LAYOUT_NAME];
						if (this.orientationView != null) {
							this.orientationView.create();
						}
					}
				}
				if (this.orientationView != null && !this.stage.contains(this.orientationView)){
					this.orientationView.play();
					this.stage.addChild(this.orientationView);
					this.container.visible = false;
				}
			}
			else{
				if (this.orientationView != undefined && this.stage.contains(this.orientationView)){
					this.orientationView.stop();
					this.stage.removeChild(this.orientationView);
					this.container.visible = true;
				}
			}
		}

		public onResize():void {
			this.getWindowParams();
			var canvas:HTMLCanvasElement = this.stage.canvas;
			if (this.common.isMobile) {
				console.log("is full screen = " + FullScreen.isFullScreen());
				this.showChangeRotation();
				window.scrollTo(0, 1);
			}

			var maxWidth:number = this.windowParams.innerWidth;
			var maxHeight:number = this.windowParams.innerHeight;
			//alert("onResize: window.outerWidth="+window.outerWidth+" window.outerHeight="+window.outerHeight+" window.innerWidth="+this.windowParams.innerWidth+" window.innerHeight="+this.windowParams.innerHeight);

			// keep aspect ratio
			var scale:number = Math.min(maxWidth / this.common.artWidth, maxHeight / this.common.artHeight);


			if (scale > 1 && !this.common.isMobile) {
				scale = 1;
			}

			this.stage.scaleX = scale;
			this.stage.scaleY = scale;

			this.stage.x = (maxWidth - this.common.artWidth * scale) / 2;
			this.stage.y = (maxHeight - this.common.artHeight * scale) / 2;

			// adjust canvas size
			canvas.width = maxWidth;
			canvas.height = maxHeight;

			if (this.common.isMobile && this.common.layouts != null) {

				//this.bgTemp = new Shape();
				//var x:number = -this.stage.x / this.stage.scaleX;
				//var color:string = this.common.config.colorBg || "#FFD8D0";
				//this.bgTemp.graphics.beginFill(color).drawRect(x, this.container.y, maxWidth / scale, maxHeight / scale);
				//this.container.addChildAt(this.bgTemp, 0);

				var hudView:HudView = this.common.layouts[HudView.LAYOUT_NAME];
				if(hudView)hudView.resizeForMobile();
				var payView:PayTableHudView = this.common.layouts[PayTableHudView.LAYOUT_NAME];
				if(payView)payView.resizeForMobile();
			}
		}

		public dispose():void {
			this.send(NotificationList.SERVER_DISCONNECT);
		}

		public static setFieldStroke(container:Container, field:string, outline:number, color:string, offsetX:number, offsetY:number, blur:number):void{
			var text:Text = <Text>container.getChildByName(field);
			GameController.setTextStroke(text, outline, color, offsetX, offsetY, blur);
		}

		public static setTextStroke(text:Text, outline:number, color:string, offsetX:number, offsetY:number, blur:number){
			if(text) {
				text.outline = outline;
				text.shadow = new createjs.Shadow(color, offsetX, offsetY, blur);
			}
		}

		public getWindowParams():any{
			this.windowParams.innerWidth = document.getElementsByTagName('body')[0].clientWidth;
			this.windowParams.zoomLevel = document.documentElement.clientWidth / window.innerWidth;
			this.windowParams.innerHeight = window.innerHeight * this.windowParams.zoomLevel;
			return;
		}
	}
}