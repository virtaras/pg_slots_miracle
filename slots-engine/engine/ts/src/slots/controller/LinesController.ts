module engine {
	import Tween = createjs.Tween;
	import Point = createjs.Point;
	import Shape = createjs.Shape;
	import Ticker = createjs.Ticker;
	import Graphics = createjs.Graphics;
	import Rectangle = createjs.Rectangle;

	export class LinesController extends BaseController {
		public static HIGHLIGHT_LINE_HOR:number = 9;
		public static HIGHLIGHT_LINE_VER:number = 9;
		private static DELAY_REMOVE:number = 2;
		private static INVISIBLE_TIME:number = 1;

		public common:CommonRefs;
		public view:LinesView;

		constructor(manager:ControllerManager, common:CommonRefs, view:LinesView) {
			super(manager);
			this.common = common;
			this.view = view;
			//this.setViewMask();
			LinesController.DELAY_REMOVE = Utils.float2int(LinesController.DELAY_REMOVE * Ticker.getFPS());
			LinesController.INVISIBLE_TIME = Utils.float2int(LinesController.INVISIBLE_TIME * Ticker.getFPS());
			LinesController.HIGHLIGHT_LINE_HOR = this.common.config.highlight_line_hor || LinesController.HIGHLIGHT_LINE_HOR;
			LinesController.HIGHLIGHT_LINE_VER = this.common.config.highlight_line_ver || LinesController.HIGHLIGHT_LINE_VER;
		}

		public setViewMask():void{
			this.view.mask = new Shape();
			this.view.mask.graphics.beginFill("0").drawRect(50, 0, 700, 600);
			return;
		}

		public init():void {
			super.init();
			this.view.create();
			this.view.hideAllLines();
		}

		public listNotification():Array<string> {
			var notifications:Array<string> = super.listNotification();
			notifications.push(NotificationList.SHOW_LINES);
			notifications.push(NotificationList.HIDE_LINES);
			notifications.push(NotificationList.START_SPIN);
			notifications.push(NotificationList.CREATE_BONUS);
			notifications.push(NotificationList.OPEN_PAY_TABLE);
			notifications.push(NotificationList.CLOSE_PAY_TABLE);
			if (!this.common.isMobile) {
				notifications.push(NotificationList.CHANGED_LINE_COUNT);
			}
			return notifications;
		}

		public createMask(positions:Array<Point>):void {
			var symbolsRect:Array<Array<Rectangle>> = this.common.symbolsRect;
			var mask:Shape = new Shape();
			mask.graphics.beginFill("0");
			for (var x:number = 0; x < symbolsRect.length; x++) {
				var reelSymbolsRect = symbolsRect[x];
				for (var y:number = 0; y < reelSymbolsRect.length; y++) {
					if (!isExist(x, y, positions)) {
						var rect:Rectangle = this.common.symbolsRect[x][y];
						if(this.common.isMobile){
							mask.graphics.drawRect(rect.x - LinesController.HIGHLIGHT_LINE_HOR, rect.y - LinesController.HIGHLIGHT_LINE_VER, rect.width * GameController.MOBILE_TO_WEB_SCALE + LinesController.HIGHLIGHT_LINE_HOR*2, rect.height * GameController.MOBILE_TO_WEB_SCALE + LinesController.HIGHLIGHT_LINE_VER*2);
						}else{
							mask.graphics.drawRect(rect.x - LinesController.HIGHLIGHT_LINE_HOR, rect.y - LinesController.HIGHLIGHT_LINE_VER, rect.width + LinesController.HIGHLIGHT_LINE_HOR*2, rect.height + LinesController.HIGHLIGHT_LINE_VER*2);
						}
					}
				}
			}
			this.view.mask = mask;

			function isExist(x:number, y:number, positions:Array<Point>) {
				for (var i:number = 0; i < positions.length; i++) {
					var position:Point = positions[i];
					if (position.x == x && position.y == y) {
						return true;
					}
				}
				return false;
			}
		}

		public handleNotification(message:string, data:any):void {
			switch (message) {
				case NotificationList.SHOW_LINES:
				{
					this.createMask(data[0]);
					this.view.showLines(data[1]);
					break;
				}
				case NotificationList.START_SPIN:
				case NotificationList.CREATE_BONUS:
				case NotificationList.HIDE_LINES:
				{
					this.view.mask = null;
					this.view.hideAllLines();
					break;
				}
				case NotificationList.CHANGED_LINE_COUNT:
				{
					Tween.removeTweens(this.view);
					this.send(NotificationList.REMOVE_WIN_LINES);
					//this.view.mask = null;
					this.setViewMask();
					this.view.alpha = 1;
					this.view.showLinesFromTo(1, this.common.server.lineCount);
					Tween.get(this.view, {useTicks: true})
						.wait(LinesController.DELAY_REMOVE)
						.to({alpha: 0}, LinesController.INVISIBLE_TIME)
						.call(()=> {
							this.view.hideAllLines();
							this.view.alpha = 1;
						});
					break;
				}
				case NotificationList.OPEN_PAY_TABLE:
				{
					this.view.visible = false;
					break;
				}
				case NotificationList.CLOSE_PAY_TABLE:
				{
					this.view.visible = true;
					break;
				}
			}
		}
	}
}