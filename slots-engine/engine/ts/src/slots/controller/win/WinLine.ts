module engine {
	import Container = createjs.Container;
	import Point = createjs.Point;
	import EventDispatcher = createjs.EventDispatcher;
	import Tween = createjs.Tween;
	import Shape = createjs.Shape;

	export class WinLine extends EventDispatcher {
		private container:Container;
		private lineId:number;
		private winVOs:Array<WinAniVO>;
		private wildLineId:number = -1;

		constructor(container:Container, lineId:number, winVOs:Array<WinAniVO>) {
			super();
			this.container = container;
			this.lineId = lineId;
			this.winVOs = winVOs;
		}

		public create():void {
			for (var i:number = 0; i < this.winVOs.length; i++) {
				var vo:WinAniVO = this.winVOs[i];
				var winSymbolView:WinSymbolView = vo.winSymbolView;
				if (winSymbolView.parent != null) {
					winSymbolView.parent.removeChild(winSymbolView);
				}
				winSymbolView.x = vo.rect.x;
				winSymbolView.y = vo.rect.y;
				if(!winSymbolView._is_wild) {
					winSymbolView.play();
					this.container.addChild(winSymbolView);
				}else{
					this.wildLineId = i;
					this.dispatchEvent(BaseWinLines.SHOW_WILD_REEL);
				}
			}
		}

		public remove():void {
			for (var i:number = 0; i < this.winVOs.length; i++) {
				var vo:WinAniVO = this.winVOs[i];
				var winSymbolView:WinSymbolView = vo.winSymbolView;
				if(!winSymbolView._is_wild) {
					this.container.removeChild(winSymbolView);
				}
			}
		}

		public getPositions():Array<Point> {
			var positions:Array<Point> = new Array(this.winVOs.length);
			for (var i:number = 0; i < this.winVOs.length; i++) {
				positions[i] = this.winVOs[i].posIdx.clone();
			}
			return positions;
		}

		public getLineId():number {
			return this.lineId;
		}

		public processRecursionRotation(isMobile:boolean):void{
			for (var i:number = 0; i < this.winVOs.length; i++) {
				var vo:WinAniVO = this.winVOs[i];
				var mask:Shape = new Shape();
				mask.graphics.beginFill("0");
				if(isMobile){
					mask.graphics.drawRect(vo.rect.x, vo.rect.y, vo.rect.width*GameConst.MOBILE_TO_WEB_SCALE, vo.rect.height*GameConst.MOBILE_TO_WEB_SCALE);
				}else {
					mask.graphics.drawRect(vo.rect.x, vo.rect.y, vo.rect.width, vo.rect.height);
				}
				vo.winSymbolView.mask = mask;
				Tween.get(vo.winSymbolView).to({rotation: 90}, ReelMoverCascade.RECURSION_ROTATION_TIME);
			}
			return;
		}
	}
}