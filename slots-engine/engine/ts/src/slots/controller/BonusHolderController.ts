module engine {
	import Container = createjs.Container;

	export class BonusHolderController extends BaseController {
		private common:CommonRefs;
		private container:Container;
		private bonusController:BaseController;

		constructor(manager:ControllerManager, common:CommonRefs, container:Container) {
			super(manager);
			this.common = common;
			this.container = container;
		}

		public init():void {
			super.init();
		}

		public listNotification():Array<string> {
			var notifications:Array<string> = super.listNotification();
			notifications.push(NotificationList.CREATE_BONUS);
			notifications.push(NotificationList.END_BONUS);
			notifications.push(NotificationList.START_SECOND_FREE_GAME);
			return notifications;
		}

		public handleNotification(message:string, data:any):void {
			switch (message) {
				case NotificationList.CREATE_BONUS:
				{
					this.startBonus();
					break;
				}
				case NotificationList.END_BONUS:
				{
					this.removeBonus();
					break;
				}
				case NotificationList.START_SECOND_FREE_GAME:
				{
					this.removeBonus();
					this.common.server.bonus.className = 'FreeSpinsController';
					this.startBonus();
					break;
				}
			}
		}

		private startBonus():void {
			var className:string = this.common.server.bonus.className;
			var BonusClass:any = engine[className];

			this.bonusController = <BaseController>new BonusClass(this.manager, this.common, this.container);
			this.bonusController.init();
			console.log(className, "startBonus");
		}

		private removeBonus():void {
			this.bonusController.dispose();
			this.bonusController = null;
		}

		public dispose():void {
			super.dispose();
			this.removeBonus();
		}
	}
}