module engine {
	import Button = layout.Button;
	import Toggle = layout.Toggle;
	import Text = createjs.Text;
	import Sound = createjs.Sound;
	import Container = createjs.Container;

	export class HudController extends BaseController {
		private currentState:IState;
		private states:Object;

		public common:CommonRefs;
		public view:HudView;
		public autoPlayCount:Array<number>;
		public autoPlayCountIdx:number;

		constructor(manager:ControllerManager, common:CommonRefs, view:HudView) {
			super(manager);
			this.common = common;
			this.view = view;
			this.autoPlayCount = this.common.config.autoPlayCount;
			this.autoPlayCountIdx = 0;
		}

		public init() {
			super.init();

			this.initHudState();
			this.initButtons();
			this.updateLineCountText();
			this.updateTotalBetText();
			this.updateBetText();
			this.updateBetPerLineText();
			this.setTextFields();
		}

		public listNotification():Array<string> {
			var notifications:Array<string> = super.listNotification();
			notifications.push(NotificationList.KEYBOARD_CLICK);
			notifications.push(NotificationList.ON_SCREEN_CLICK);
			notifications.push(NotificationList.TRY_START_SPIN);
			notifications.push(NotificationList.START_SPIN);
			notifications.push(NotificationList.WIN_LINES_SHOWED);
			notifications.push(NotificationList.STOPPED_ALL_REELS);
			notifications.push(NotificationList.EXPRESS_STOP);
			notifications.push(NotificationList.CLOSE_PAY_TABLE);
			notifications.push(NotificationList.END_BONUS);
			notifications.push(NotificationList.TRY_START_AUTO_PLAY);
			notifications.push(NotificationList.AUTO_PLAY_COMP);
			notifications.push(NotificationList.AUTO_PLAY_CONT);
			notifications.push(NotificationList.LOAD_PAY_TABLE);
			notifications.push(NotificationList.CREATE_BONUS);
			notifications.push(NotificationList.RECURSION_PROCESSED);
			notifications.push(NotificationList.OK_BTN_ERROR_CLICKED);
			return notifications;
		}

		public handleNotification(message:string, data:any):void {
			this.currentState.handleNotification(message, data);
		}

		private initHudState():void {
			this.states = {};
			this.states[DefaultState.NAME] = new DefaultState(this);
			this.states[PayTableState.NAME] = new PayTableState(this);
			this.states[RegularSpinState.NAME] = new RegularSpinState(this);
			this.states[AutoSpinState.NAME] = new AutoSpinState(this);
			this.states[BonusState.NAME] = new BonusState(this);

			if (this.common.server.bonus == null) {
				this.changeState(DefaultState.NAME);
			} else {
				this.changeState(BonusState.NAME);
			}
		}

		private initButtons():void {
			var buttonsNames:Array<string> = this.view.buttonsNames;
			for (var i:number = 0; i < buttonsNames.length; i++) {
				var buttonName:string = buttonsNames[i];
				var button:Button = this.view.getBtn(buttonName);
				if (button == null) {
					//console.log("WARNING: Button with name = " + buttonName + " is null");
					continue;
				}
				button.on("click", (eventObj:any)=> {
					if (eventObj.nativeEvent instanceof MouseEvent) {
						this.onBtnClick(eventObj.currentTarget.name);
					}
				})
			}
		}

		public onBtnClick(buttonName:string):void {
			switch (buttonName) {
				case HudView.BACK_TO_LOBBY_BTN:
				{
					this.send(NotificationList.BACK_TO_LOBBY);
					break;
				}
				case HudView.FULL_SCREEN_BTN:
				{
					FullScreen.toggleFullScreen();
					break;
				}
				case HudView.TURBO_OFF_BTN:
				{
					this.setTurboMode(true);
					this.view.changeButtonState(HudView.TURBO_OFF_BTN, false, false);
					//this.controller.send(NotificationList.TURBO_OFF_CLICKED);
					break;
				}
				case HudView.TURBO_ON_BTN:
				{
					this.setTurboMode(false);
					this.view.changeButtonState(HudView.TURBO_OFF_BTN, true, true);
					break;
				}
				case HudView.AUTO_OFF_BTN:
				{
					this.setAutoMode(true);
					this.view.changeButtonState(HudView.AUTO_OFF_BTN, false, false);
					//this.controller.send(NotificationList.TURBO_OFF_CLICKED);
					break;
				}
				case HudView.AUTO_ON_BTN:
				{
					this.setAutoMode(false);
					this.view.changeButtonState(HudView.AUTO_OFF_BTN, true, true);
					break;
				}
			}
			HudController.playBtnSound(buttonName);
			this.currentState.onBtnClick(buttonName);
		}

		private static playBtnSound(buttonName:string):void {
			if (buttonName == HudView.START_SPIN_BTN) {
				Sound.play(SoundsList.SPIN_CLICK_BTN);
			}
			else {
				Sound.play(SoundsList.REGULAR_CLICK_BTN);
			}
		}

		public changeState(name:string, data:any = null):void {
			console.log("Change state: " + name);
			if (this.currentState != null) {
				this.currentState.end();
			}
			this.currentState = this.states[name];
			this.currentState.start(data);
		}

		public tryStartSpin():void {
			console.log("Try start spin");
			var balanceAfterSpin:number = this.common.server.getBalance() - this.common.server.getTotalBet();
			if (balanceAfterSpin >= 0) {
				if(!this.common.server.recursion.length) {
					var balance:number = this.common.server.getBalance() - this.common.server.getTotalBet();
					this.common.server.setBalance(balance);
					this.send(NotificationList.UPDATE_BALANCE_TF);
					this.send(NotificationList.TRY_START_SPIN);
				}
			}
			else {
				this.send(NotificationList.SHOW_ERRORS, [ErrorController.ERROR_NO_MONEY_STR]);
				//throw new Error("ERROR: no money");
			}
		}

		public updateTotalBetText():void {
			var totalBetText:Text = this.view.getText(HudView.TOTAL_BET_TEXT);
			if (totalBetText != null) {
				totalBetText.text = MoneyFormatter.format(this.common.server.getTotalBet(), true);
			}
		}

		public updateBetText():void {
			var betText:Text = this.view.getText(HudView.BET_TEXT);
			if (betText != null) {
				betText.text = MoneyFormatter.format(this.common.server.bet, true);
			}
		}

		public updateBetPerLineText():void {
			var betPerLineText:Text = this.view.getText(HudView.BET_PER_LINE_TEXT);
			if (betPerLineText != null) {
				betPerLineText.text = MoneyFormatter.format(this.common.server.getBetPerLine(), false);
			}
		}

		public updateLineCountText():void {
			this.view.setLineCountText(this.common.server.lineCount.toString());
			if(this.common.isMobile)this.updateTotalBetText();
		}

		public updateAutoPlayBtn():void {
			//console.log("Update auto play btn");
			this.view.changeButtonState(HudView.DEC_SPIN_COUNT_BTN, true, this.autoPlayCountIdx > 0);
			this.view.changeButtonState(HudView.INC_SPIN_COUNT_BTN, true, this.autoPlayCountIdx < this.autoPlayCount.length - 1);
		}

		public updateAutoSpinCountText():void {
			//console.log("Update auto spin count text");
			var autoSpinCountText:Text = this.view.getText(HudView.AUTO_SPINS_COUNT_TEXT);
			if (autoSpinCountText != null && this.autoPlayCountIdx > -1 && this.autoPlayCountIdx < this.autoPlayCount.length) {
				var autoPlayCount:number = this.autoPlayCount[this.autoPlayCountIdx];
				autoSpinCountText.text = autoPlayCount.toString();
			}
		}

		public setTextFields():void{
			if(this.common.config.hud_shadows){
				this.view.addFieldsShadows();
			}
		}

		public setTurboMode(mode:boolean):void{
			this.common.isTurbo = mode;
			this.send(NotificationList.UPDATE_TURBO_MODE, null);
		}

		public setAutoMode(mode:boolean):void{
			//set auto mode
			this.common.isAuto = mode;
			if(this.common.isAuto){
				this.changeState(AutoSpinState.NAME, 5);
			}else{
				this.changeState(DefaultState.NAME);
			}
		}

		public dispose():void {
			super.dispose();
			this.view.dispose();
			this.view = null;
			this.common = null;
		}
	}
}