module engine {
	import Text = createjs.Text;
	import Button = layout.Button;

	export class AutoSpinState implements IState {
		public static NAME:string = "AutoSpinState";

		private controller:HudController;
		private common:CommonRefs;
		private view:HudView;
		private autoSpinCount:number;
		private lastSpinCount:number;
		private hardStop:boolean;

		constructor(controller:HudController) {
			this.controller = controller;
			this.common = controller.common;
			this.view = controller.view;
		}

		public start(data:any):void {
			this.autoSpinCount = data;
			this.lastSpinCount = this.autoSpinCount;
			this.hardStop = false;

			this.view.changeButtonState(HudView.SETTINGS_BTN, true, false);
			this.view.changeButtonState(HudView.OPEN_AUTO_SPIN_TABLE_BTN, false, false);
			this.view.changeButtonState(HudView.START_AUTO_PLAY_BTN, false, false);
			this.view.changeButtonState(HudView.STOP_AUTO_PLAY_BTN, true, true);
			this.view.changeTextState(HudView.AUTO_SPINS_COUNT_TEXT, true);
			this.view.changeButtonState(HudView.GAMBLE_BTN, true, false);
			this.view.changeButtonState(HudView.INC_SPIN_COUNT_BTN, true, false);
			this.view.changeButtonState(HudView.DEC_SPIN_COUNT_BTN, true, false);
			this.view.changeButtonState(HudView.INC_LINE_COUNT_BTN, true, false);
			this.view.changeButtonState(HudView.DEC_LINE_COUNT_BTN, true, false);
			this.view.changeButtonState(HudView.NEXT_BET_BTN, true, false);
			this.view.changeButtonState(HudView.PREV_BET_BTN, true, false);
			this.view.changeButtonState(HudView.MAX_LINES_BTN, true, false);
			this.view.changeButtonState(HudView.MAX_BET_BTN, true, false);
			this.view.changeButtonState(HudView.START_SPIN_BTN, true, false);
			this.view.changeButtonState(HudView.START_BONUS_BTN, false, false);
			this.view.changeButtonState(HudView.STOP_SPIN_BTN, false, false);
			this.view.changeButtonState(HudView.PAY_TABLE_BTN, true, false);
			this.view.changeButtonState(HudView.BET_BTN, true, false);
			this.view.changeButtonState(HudView.LINE_COUNT_BTN, true, false);
			this.view.changeButtonState(HudView.MULTIPLY_BTN, true, false);

			this.controller.send(NotificationList.CLOSE_SETTINGS_MENU);
			this.controller.send(NotificationList.REMOVE_WIN_LINES);
			this.controller.tryStartSpin();
		}

		public handleNotification(message:string, data:any):void {
			switch (message) {
				case NotificationList.TRY_START_SPIN:
				{
					if(!this.controller.common.isAuto){
						this.autoSpinCount--;
						this.updateAutoSpinCountText();
					}
					this.controller.send(NotificationList.COLLECT_WIN_TF);
					this.controller.send(NotificationList.START_SPIN);
					this.controller.send(NotificationList.START_SPIN_AUDIO);
					this.controller.send(NotificationList.SERVER_SEND_SPIN);
					break;
				}
				case NotificationList.STOPPED_ALL_REELS:
				{
					var bonus:BonusVO = this.common.server.bonus;
					if (bonus == null || bonus.type == BonusTypes.GAMBLE) {
						this.controller.send(NotificationList.SHOW_WIN_LINES, true);
					}
					else {
						this.view.changeButtonState(HudView.STOP_AUTO_PLAY_BTN, false, false);
						this.view.changeButtonState(HudView.START_AUTO_PLAY_BTN, true, false);
						var autoPlayText:Text = this.view.getText(HudView.AUTO_SPINS_COUNT_TEXT);
						if(autoPlayText) autoPlayText.visible = this.common.config.showAllTimeSpinCount;
						this.controller.send(NotificationList.SHOW_WIN_LINES, true);
					}
					this.controller.send(NotificationList.SHOW_WIN_TF);
					break;
				}
				case NotificationList.RECURSION_PROCESSED:
				{
					var bonus:BonusVO = this.common.server.bonus;
					if(this.common.server.recursion.length){
						//console.log("processing recursion", this.common.server.recursion);
					}
					else if (bonus != null && bonus.type != BonusTypes.GAMBLE) {
						this.controller.changeState(BonusState.NAME);
					}
					else if (this.autoSpinCount == 0) {
						if(!this.hardStop){
							this.view.changeButtonState(HudView.STOP_AUTO_PLAY_BTN, false, false);
							var autoPlayText:Text = this.view.getText(HudView.AUTO_SPINS_COUNT_TEXT);
							autoPlayText.visible = this.common.config.showAllTimeSpinCount;
							this.controller.send(NotificationList.SHOW_AUTO_MODAL, this.lastSpinCount);
						}
						else {
							this.controller.send(NotificationList.END_AUTO_PLAY);
							this.controller.changeState(DefaultState.NAME);
						}
						return;
					}
					else {
						this.controller.send(NotificationList.REMOVE_WIN_LINES);
						this.controller.tryStartSpin();
					}
					break;
				}
				case NotificationList.AUTO_PLAY_COMP:{
					this.controller.send(NotificationList.END_AUTO_PLAY);
					this.controller.changeState(DefaultState.NAME);
					return;
					break;
				}
				case NotificationList.AUTO_PLAY_CONT:{
					this.start(this.lastSpinCount);
					break;
				}
				case NotificationList.OK_BTN_ERROR_CLICKED:
				{
					//TODO: handle no money for autospins
					//this.controller.send(NotificationList.END_AUTO_PLAY);
					break;
				}
			}
		}

		public onBtnClick(buttonName:string):void {
			switch (buttonName) {
				case HudView.STOP_AUTO_PLAY_BTN:
				{
					this.autoSpinCount = 0;
					this.hardStop = true;
					var autoPlayText:Text = this.view.getText(HudView.AUTO_SPINS_COUNT_TEXT);
					autoPlayText.visible = this.common.config.showAllTimeSpinCount;
					this.view.changeButtonState(HudView.STOP_AUTO_PLAY_BTN, false, false);
					this.view.changeButtonState(HudView.START_AUTO_PLAY_BTN, true, false);
					this.view.changeButtonState(HudView.OPEN_AUTO_SPIN_TABLE_BTN, true, false);
					break;
				}
			}
		}

		private updateAutoSpinCountText():void {
			var autoPlayText:Text = this.view.getText(HudView.AUTO_SPINS_COUNT_TEXT);
			if (autoPlayText != null) {
				autoPlayText.text = this.autoSpinCount.toString();
			}
		}

		public end():void {
			this.controller.common.isAuto = false;
			this.view.changeButtonState(HudView.AUTO_OFF_BTN, true, true);
		}
	}
}