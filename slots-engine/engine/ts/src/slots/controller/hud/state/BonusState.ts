module engine {
	import Button = layout.Button;

	export class BonusState implements IState {
		public static NAME:string = "BonusState";

		private controller:HudController;
		private common:CommonRefs;
		private view:HudView;

		constructor(controller:HudController) {
			this.controller = controller;
			this.common = controller.common;
			this.view = controller.view;
		}

		public start(data:any):void {
			// update buttons
			if(this.common.isMobile){
				this.view.changeButtonState(HudView.SETTINGS_BTN, false, false);
				this.view.changeButtonState(HudView.GAMBLE_BTN, false, false);
				this.view.changeButtonState(HudView.PAY_TABLE_BTN, false, false);
			}else{
				this.view.changeButtonState(HudView.SETTINGS_BTN, true, false);
				this.view.changeButtonState(HudView.GAMBLE_BTN, true, false);
				this.view.changeButtonState(HudView.PAY_TABLE_BTN, true, false);
			}

			this.view.changeButtonState(HudView.OPEN_AUTO_SPIN_TABLE_BTN, true, false);

			this.view.changeButtonState(HudView.STOP_SPIN_BTN, false, false);
			this.view.changeButtonState(HudView.START_AUTO_PLAY_BTN, true, false);
			this.view.changeButtonState(HudView.STOP_AUTO_PLAY_BTN, false, false);
			this.view.changeTextState(HudView.AUTO_SPINS_COUNT_TEXT, false);

			this.view.changeButtonState(HudView.INC_SPIN_COUNT_BTN, true, false);
			this.view.changeButtonState(HudView.DEC_SPIN_COUNT_BTN, true, false);
			this.view.changeButtonState(HudView.INC_LINE_COUNT_BTN, true, false);
			this.view.changeButtonState(HudView.DEC_LINE_COUNT_BTN, true, false);
			this.view.changeButtonState(HudView.MAX_BET_BTN, true, false);

			this.view.changeButtonState(HudView.BET_BTN, true, false);
			this.view.changeButtonState(HudView.LINE_COUNT_BTN, true, false);
			this.view.changeButtonState(HudView.MULTIPLY_BTN, true, false);
			this.view.changeButtonState(HudView.NEXT_BET_BTN, true, false);
			this.view.changeButtonState(HudView.PREV_BET_BTN, true, false);
			this.view.changeButtonState(HudView.MAX_LINES_BTN, true, false);

			if (this.common.server.bonus.type == BonusTypes.GAMBLE) {
				this.view.changeButtonState(HudView.START_SPIN_BTN, true, false);
				this.view.changeButtonState(HudView.START_BONUS_BTN, false, false);
				if(this.common.isMobile)this.containerHudCVisibility(false);
			}
			else {
				var bonusBtn:Button = this.view.getBtn(HudView.START_BONUS_BTN);
				if (bonusBtn != null) {
					this.view.changeButtonState(HudView.START_SPIN_BTN, false, false);
					this.view.changeButtonState(HudView.START_BONUS_BTN, true, true);
				}
				else{
					this.view.changeButtonState(HudView.START_SPIN_BTN, true, true);
				}
			}

			if (this.common.server.bonus.type == BonusTypes.SELECT_GAME) {
				this.view.changeButtonState(HudView.START_BONUS_BTN, false, false);
				this.view.changeButtonState(HudView.START_SPIN_BTN, true, false);
			}

			this.controller.send(NotificationList.COLLECT_WIN_TF);
			this.controller.send(NotificationList.REMOVE_WIN_LINES);
			this.controller.send(NotificationList.CREATE_BONUS);
		}

		private containerHudCVisibility(state:boolean){
			this.view.visible = state;
		}

		public handleNotification(message:string, data:any):void {
			switch (message) {
				case NotificationList.END_BONUS:
				{
					// remove bonus VO
					console.log('end bonus');
					if(this.common.isMobile){
						this.containerHudCVisibility(true);
					}
					this.common.server.bonus = null;
					this.controller.changeState(DefaultState.NAME);
					break;
				}
			}
		}

		public onBtnClick(buttonName:string):void {
			switch (buttonName) {
				case HudView.START_SPIN_BTN:
				case HudView.START_BONUS_BTN:
				{
					this.view.changeButtonState(HudView.START_SPIN_BTN, true, false);
					this.view.changeButtonState(HudView.START_BONUS_BTN, false, false);
					this.controller.send(NotificationList.START_BONUS, 'spins');
					break;
				}
			}
		}

		public end():void {
		}
	}
}