module engine {
	import Text = createjs.Text;
	import Button = layout.Button;

	export class DefaultState implements IState {
		public static NAME:string = "DefaultState";

		private controller:HudController;
		private common:CommonRefs;
		private view:HudView;

		constructor(controller:HudController) {
			this.controller = controller;
			this.common = controller.common;
			this.view = controller.view;
		}

		public start(data:any):void {
			this.view.changeButtonState(HudView.AUTO_OFF_BTN, true, true);
			this.controller.send(NotificationList.SHOW_HEADER);
			if(!this.common.server.recursion.length) {
				this.updateState();
			}
		}

		public updateState():void{
			this.view.changeButtonState(HudView.OPEN_AUTO_SPIN_TABLE_BTN, true, true);
			var autoPlayText:Text = this.view.getText(HudView.AUTO_SPINS_COUNT_TEXT);
			if(autoPlayText) autoPlayText.visible = this.common.config.showAllTimeSpinCount;
			this.view.changeButtonState(HudView.SETTINGS_BTN, true, true);
			this.view.changeButtonState(HudView.MAX_LINES_BTN, true, true);
			this.view.changeButtonState(HudView.START_AUTO_PLAY_BTN, true, true);
			this.view.changeButtonState(HudView.STOP_AUTO_PLAY_BTN, false, false);
			this.view.changeButtonState(HudView.GAMBLE_BTN, true, false);
			this.view.changeButtonState(HudView.MAX_BET_BTN, true, true);

			this.view.changeButtonState(HudView.START_SPIN_BTN, true, true);
			this.view.changeButtonState(HudView.STOP_SPIN_BTN, false, false);

			this.view.changeButtonState(HudView.START_BONUS_BTN, false, false);
			this.view.changeButtonState(HudView.PAY_TABLE_BTN, this.common.payTableLoaded, this.common.payTableLoaded);
			this.view.changeButtonState(HudView.BET_BTN, true, true);
			this.view.changeButtonState(HudView.LINE_COUNT_BTN, true, true);
			this.view.changeButtonState(HudView.MULTIPLY_BTN, true, true);

			this.controller.updateAutoPlayBtn();
			this.updateGambleBtn();
			this.controller.updateAutoSpinCountText();
			this.updateBetBtn();
			this.updateLineBtn();
		}

		public handleNotification(message:string, data:any):void {
			switch (message) {
				case NotificationList.KEYBOARD_CLICK:
				{
					if (data == Keyboard.SPACE) {
						this.controller.tryStartSpin();
					}
					break;
				}
				case NotificationList.ON_SCREEN_CLICK:
				{
					this.controller.tryStartSpin();
					break;
				}
				case NotificationList.TRY_START_SPIN:
				{
					this.controller.changeState(RegularSpinState.NAME);
					break;
				}
				case NotificationList.LOAD_PAY_TABLE:
				{
					this.view.changeButtonState(HudView.PAY_TABLE_BTN, true, true);
					break;
				}
				case NotificationList.TRY_START_AUTO_PLAY:
				{
					this.controller.send(NotificationList.TRY_START_AUTO_PLAY_AUDIO);
					var balanceAfterSpin:number = this.common.server.getBalance() - this.common.server.getTotalBet();
					if (balanceAfterSpin >= 0) {
						this.controller.changeState(AutoSpinState.NAME, data);
					}else {
						this.controller.send(NotificationList.SHOW_ERRORS, [ErrorController.ERROR_NO_MONEY_STR]);
					}
					break;
				}
				case NotificationList.RECURSION_PROCESSED:
				{
					this.updateState();
					break;
				}
			}
		}

		public onBtnClick(buttonName:string):void {
			switch (buttonName) {
				case HudView.START_SPIN_BTN:
				{
					this.controller.tryStartSpin();
					break;
				}
				case HudView.PAY_TABLE_BTN:
				{
					this.controller.changeState(PayTableState.NAME);
					break;
				}
				case HudView.LINE_COUNT_BTN:
				{
					this.common.server.setNextLineCount(true);
					this.controller.updateLineCountText();
					this.controller.updateTotalBetText();
					this.controller.send(NotificationList.CHANGED_LINE_COUNT);
					break;
				}
				case HudView.INC_LINE_COUNT_BTN:
				{
					this.common.server.setNextLineCount(false);
					this.updateLineBtn();
					this.view.changeButtonState(HudView.DEC_LINE_COUNT_BTN, true, true);
					this.controller.updateLineCountText();
					this.controller.updateTotalBetText();
					this.controller.send(NotificationList.CHANGED_LINE_COUNT);
					break;
				}
				case HudView.DEC_LINE_COUNT_BTN:
				{
					this.common.server.setPrevLineCount(false);
					this.updateLineBtn();
					this.view.changeButtonState(HudView.INC_LINE_COUNT_BTN, true, true);
					this.controller.updateLineCountText();
					this.controller.updateTotalBetText();
					this.controller.send(NotificationList.CHANGED_LINE_COUNT);
					break;
				}
				case HudView.MULTIPLY_BTN:
				{
					this.common.server.setNextMultiply();
					this.controller.updateBetPerLineText();
					this.controller.updateTotalBetText();
					break;
				}
				case HudView.BET_BTN:
				{
					this.common.server.setNextBet(true);
					this.controller.updateBetText();
					this.controller.updateBetPerLineText();
					this.controller.updateTotalBetText();
					break;
				}
				case HudView.NEXT_BET_BTN:
				{
					this.common.server.setNextBet(false);
					this.updateBetBtn();
					this.controller.updateBetText();
					this.controller.updateBetPerLineText();
					this.controller.updateTotalBetText();
					break;
				}
				case HudView.PREV_BET_BTN:
				{
					this.common.server.setPrevBet(false);
					this.updateBetBtn();
					this.controller.updateBetText();
					this.controller.updateBetPerLineText();
					this.controller.updateTotalBetText();
					break;
				}
				case HudView.MAX_BET_BTN:
				{
					this.common.server.setMaxMultiply();
					this.common.server.setMaxBet();
					this.updateBetBtn();
					this.common.server.setMaxLines();
					this.updateLineBtn();

					this.controller.updateBetPerLineText();
					this.controller.updateLineCountText();
					this.controller.updateTotalBetText();
					this.controller.updateBetText();
					this.controller.updateBetPerLineText();
					this.controller.tryStartSpin();
					break;
				}
				case HudView.MAX_LINES_BTN:
				{
					this.common.server.setMaxLines();
					this.controller.updateLineCountText();
					this.controller.updateTotalBetText();
					this.controller.tryStartSpin();
					break;
				}
				case HudView.OPEN_AUTO_SPIN_TABLE_BTN:
				{
					this.controller.send(NotificationList.OPEN_AUTO_SPIN_MENU);
					break;
				}
				case HudView.SETTINGS_BTN:
				{
					this.controller.send(NotificationList.TOGGLE_SETTINGS_MENU);
					break;
				}
				case HudView.START_AUTO_PLAY_BTN:
				{
					var autoPlayCount:number = this.controller.autoPlayCount[this.controller.autoPlayCountIdx];
					this.controller.send(NotificationList.TRY_START_AUTO_PLAY, autoPlayCount);
					break;
				}
				case HudView.INC_SPIN_COUNT_BTN:
				{
					this.controller.autoPlayCountIdx++;
					this.controller.updateAutoPlayBtn();
					this.controller.updateAutoSpinCountText();
					break;
				}
				case HudView.DEC_SPIN_COUNT_BTN:
				{
					this.controller.autoPlayCountIdx--;
					this.controller.updateAutoPlayBtn();
					this.controller.updateAutoSpinCountText();
					break;
				}
				case HudView.GAMBLE_BTN:
				{
					this.controller.changeState(BonusState.NAME);
					this.controller.send(NotificationList.START_BONUS);
					break;
				}
			}
		}

		private updateGambleBtn():void {
			console.log("Update gamble btn");
			var bonusVO:BonusVO = this.common.server.bonus;
			if (bonusVO != null && bonusVO.type == BonusTypes.GAMBLE) {
				this.view.changeButtonState(HudView.GAMBLE_BTN, true, true);
			}
			//var button:Button = this.view.getBtn(HudView.GAMBLE_BTN);
			//if (button != null){
			//	console.log("gambleBtn="+button.getEnable()+" "+button.id);
			//}
		}

		public updateBetBtn():void {
			this.view.changeButtonState(HudView.NEXT_BET_BTN, true, this.common.server.isHasNextBet());
			this.view.changeButtonState(HudView.PREV_BET_BTN, true, this.common.server.isHasPrevBet());
		}

		public updateLineBtn():void {
			this.view.changeButtonState(HudView.INC_LINE_COUNT_BTN, true, this.common.server.isHasNextLine());
			this.view.changeButtonState(HudView.DEC_LINE_COUNT_BTN, true, this.common.server.isHasPrevLine());
		}

		public end():void {
			this.view.changeButtonState(HudView.AUTO_OFF_BTN, true, false);
		}
	}
}