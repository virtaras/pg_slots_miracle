module engine {
	import Container = createjs.Container;
	import Rectangle = createjs.Rectangle;
	import Ticker = createjs.Ticker;
	import Tween = createjs.Tween;
	import Shape = createjs.Shape;
	import Text = createjs.Text;
	import Button = layout.Button;

	export class AutoSpinTableController extends BaseController {
		private static ITEM_PREFIX:string = "item";
		private static ITEM_POSTFIX:string = "Btn";
		private static ITEM_TEXT_PREFIX:string = "item_value_";

		private static OPEN_TIME:number = 0.3;
		private static CLOSE_TIME:number = 0.3;

		private common:CommonRefs;
		private view:Container;
		private maskView:Container;
		private startY:number;
		private isOpen:boolean;

		private itemsBtn:Array<Button>;
		private itemsText:Array<Text>;

		constructor(manager:ControllerManager, common:CommonRefs, view:Container, maskView:Container) {
			super(manager);
			this.common = common;
			this.view = view;
			this.maskView = maskView;
			AutoSpinTableController.OPEN_TIME = Utils.float2int(AutoSpinTableController.OPEN_TIME * Ticker.getFPS());
			AutoSpinTableController.CLOSE_TIME = Utils.float2int(AutoSpinTableController.CLOSE_TIME * Ticker.getFPS());
		}

		public init():void {
			super.init();
			this.initMask();
			this.initItems();

			this.startY = this.view.y;
			this.isOpen = true;
			this.close(false);
		}

		public listNotification():Array<string> {
			var notifications:Array<string> = super.listNotification();
			notifications.push(NotificationList.OPEN_AUTO_SPIN_MENU);
			notifications.push(NotificationList.TRY_START_AUTO_PLAY);
			notifications.push(NotificationList.TRY_START_SPIN);
			notifications.push(NotificationList.OPEN_PAY_TABLE);
			return notifications;
		}

		public handleNotification(message:string, data:any):void {
			switch (message) {
				case NotificationList.OPEN_AUTO_SPIN_MENU:
				{
					this.toggle();
					break;
				}
				case NotificationList.TRY_START_AUTO_PLAY:
				case NotificationList.TRY_START_SPIN:
				{
					this.close(true);
					break;
				}
				case NotificationList.OPEN_PAY_TABLE:
				{
					this.close(false);
					break;
				}
			}
		}

		private toggle():void {
			if (this.isOpen) {
				this.close(true);
			}
			else {
				this.open(true);
			}
		}

		private initItems():void {
			var i:number;
			var autoPlayCount:Array<number> = <Array<number>>this.common.config.autoPlayCount;

			this.itemsBtn = new Array(autoPlayCount.length);
			for (var i:number = 1; i <= this.itemsBtn.length; i++) {
				var button:Button = <Button>this.view.getChildByName(AutoSpinTableController.ITEM_PREFIX + i + AutoSpinTableController.ITEM_POSTFIX);
				var bounds:Rectangle = button.getBounds();
				var hitArea:Shape = new Shape();
				hitArea.graphics.beginFill("0").drawRect(bounds.x, bounds.y, bounds.width, bounds.height);
				button.hitArea = hitArea;
				button.on("click", (eventObj:any)=>{

					if (eventObj.nativeEvent instanceof MouseEvent) {
						var target:Button = eventObj.currentTarget;
						var id:number = parseInt(target.name.substring(AutoSpinTableController.ITEM_PREFIX.length, target.name.length - AutoSpinTableController.ITEM_POSTFIX.length));
						var count:number = parseInt(this.itemsText[id - 1].text);
						var bonus:BonusVO = this.common.server.bonus;
						if (bonus == null || bonus.type == BonusTypes.GAMBLE) {
							this.send(NotificationList.TRY_START_AUTO_PLAY, count);
						}
					}
				});
				this.itemsBtn[i - 1] = button;
			}

			this.itemsText = new Array(autoPlayCount.length);
			for (i = 1; i <= this.itemsText.length; i++) {
				var itemValue:Text = <Text>this.view.getChildByName(AutoSpinTableController.ITEM_TEXT_PREFIX + i);
				itemValue.text = autoPlayCount[autoPlayCount.length - i].toString();
				this.itemsText[i - 1] = itemValue;
				//itemValue.y += itemValue.lineHeight / 2;
			}
		}

		private initMask():void {
			this.maskView.parent.removeChild(this.maskView);
			var bounds:Rectangle = this.maskView.getBounds();
			var mask:Shape = new Shape();
			mask.graphics.beginFill("0").drawRect(this.maskView.x + bounds.x, this.maskView.y + bounds.y, bounds.width, bounds.height);
			this.view.mask = mask;
		}

		private close(animation:boolean):void {
			var endPos:number = this.startY + this.view.getBounds().height;
			if (animation) {
				var tween:Tween = Tween.get(this.view, {useTicks: true});
				tween.to({y: endPos}, AutoSpinTableController.CLOSE_TIME);
			}
			else {
				this.view.y = endPos;
			}
			this.isOpen = false;
			this.setButtonsEnable(false);
		}

		private open(animation:boolean):void {
			if (animation) {
				var tween:Tween = Tween.get(this.view, {useTicks: true});
				tween.to({y: this.startY}, AutoSpinTableController.OPEN_TIME);
			}
			else {
				this.view.y = this.startY;
			}
			this.isOpen = true;
			this.setButtonsEnable(true);
		}

		public setButtonsEnable(enable:boolean){
			for (var i:number = 1; i <= this.itemsBtn.length; i++) {
				var button:Button = <Button>this.view.getChildByName(AutoSpinTableController.ITEM_PREFIX + i + AutoSpinTableController.ITEM_POSTFIX);
				button.setEnable(enable);
			}
		}

		public dispose():void {
			super.dispose();
		}
	}
}