module engine {
	import Container = createjs.Container;
	import LayoutCreator = layout.LayoutCreator;
	import Button = layout.Button;
	import Sound = createjs.Sound;

	export class PayTableController extends BaseController {
		private static PAY_TABLE_LAYOUT_PREFIX:string = "PayTable_";

		private common:CommonRefs;
		private gameContainer:Container;
		private payTableContainer:Container;
		private pageContainer:Container;
		private hudView:PayTableHudView;
		private pageIdx:number;

		constructor(manager:ControllerManager, common:CommonRefs, gameContainer:Container) {
			super(manager);
			this.common = common;
			this.gameContainer = gameContainer;
			this.pageIdx = 1;
		}

		public init():void {
			super.init();

			this.payTableContainer = new Container();

			this.pageContainer = new Container();
			this.payTableContainer.addChild(this.pageContainer);

			this.hudView = this.common.layouts[PayTableHudView.LAYOUT_NAME];
			this.hudView.create();
			this.createPage(this.pageIdx);
			this.payTableContainer.addChild(this.hudView);

			this.initButtonsHandler();
		}

		public listNotification():Array<string> {
			var notifications:Array<string> = super.listNotification();
			notifications.push(NotificationList.OPEN_PAY_TABLE);
			notifications.push(NotificationList.CLOSE_PAY_TABLE);
			return notifications;
		}

		public handleNotification(message:string, data:any):void {
			switch (message) {
				case NotificationList.OPEN_PAY_TABLE:
				{
					this.onOpen();
					break;
				}
				case NotificationList.CLOSE_PAY_TABLE:
				{
					this.onClose();
					break;
				}
			}
		}

		private initButtonsHandler():void {
			var buttonsNames:Array<string> = this.hudView.buttonsNames;
			for (var i:number = 0; i < buttonsNames.length; i++) {
				this.hudView.getBtn(buttonsNames[i]).on("click", (eventObj:any)=> {
					if (eventObj.nativeEvent instanceof MouseEvent) {
						PayTableController.playBtnSound(eventObj.currentTarget.name);
						this.onBtnClick(eventObj.currentTarget.name);
					}
				})
			}
		}

		private static playBtnSound(buttonName:string):void {
			Sound.play(SoundsList.REGULAR_CLICK_BTN);
		}

		public onBtnClick(buttonName:string):void {
			switch (buttonName) {
				case PayTableHudView.PREV_PAGE_BTN:
				{
					this.createPage(--this.pageIdx);
					break;
				}
				case PayTableHudView.NEXT_PAGE_BTN:
				{
					this.createPage(++this.pageIdx);
					break;
				}
				case PayTableHudView.BACK_TO_GAME_BTN:
				{
					this.send(NotificationList.CLOSE_PAY_TABLE);
					break;
				}
			}
		}

		private createPage(pageIdx:number):void {
			this.clearPageContainer();

			var prevCreator:LayoutCreator = this.common.layouts[PayTableController.PAY_TABLE_LAYOUT_PREFIX + (pageIdx - 1)];
			this.hudView.getBtn(PayTableHudView.PREV_PAGE_BTN).setEnable(prevCreator != null);

			var nextCreator:LayoutCreator = this.common.layouts[PayTableController.PAY_TABLE_LAYOUT_PREFIX + (pageIdx + 1)];
			this.hudView.getBtn(PayTableHudView.NEXT_PAGE_BTN).setEnable(nextCreator != null);

			var creator:LayoutCreator = this.common.layouts[PayTableController.PAY_TABLE_LAYOUT_PREFIX + pageIdx];
			creator.create(this.pageContainer);
		}

		private onClose():void {
			this.gameContainer.removeChild(this.payTableContainer);
            this.gameContainer.removeChild(this.hudView);
		}

		private onOpen():void {
            this.gameContainer.addChild(this.payTableContainer);
            if (this.common.isMobile){
                this.hudView = this.common.layouts[PayTableHudView.LAYOUT_NAME];
                this.hudView.create();
                this.hudView.resizeForMobile();
                this.gameContainer.addChild(this.hudView);
            }
		}

		private clearPageContainer():void {
			while (this.pageContainer.getNumChildren() > 0) {
				this.pageContainer.removeChildAt(0);
			}
		}

		public dispose():void {
			super.dispose();
		}
	}
}