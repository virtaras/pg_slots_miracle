/**
 * Created by Taras on 22.12.2014.
 */

module engine {
    import Tween = createjs.Tween;
    import Point = createjs.Point;
    import Shape = createjs.Shape;
    import Ticker = createjs.Ticker;
    import Container = createjs.Container;
    import Rectangle = createjs.Rectangle;
    import DisplayObject = createjs.DisplayObject;

    export class DrawController extends BaseController {
        private static DELAY_REMOVE:number = 2;
        private static INVISIBLE_TIME:number = 1;
        public common:CommonRefs;
        public view:Container;
        public drawLineClass:DrawLine;

        constructor(manager:ControllerManager, common:CommonRefs, view:Container) {
            super(manager);
            this.common = common;
            this.view = view;
            DrawController.DELAY_REMOVE = Utils.float2int(DrawController.DELAY_REMOVE * Ticker.getFPS());
            DrawController.INVISIBLE_TIME = Utils.float2int(DrawController.INVISIBLE_TIME * Ticker.getFPS());
            this.drawLineClass = new DrawLine(this.view, this.common);
        }

        public init():void {
            super.init();
        }

        public listNotification():Array<string> {
            var notifications:Array<string> = super.listNotification();
            notifications.push(NotificationList.REMOVE_WIN_LINES);
            notifications.push(NotificationList.OPEN_PAY_TABLE);
            notifications.push(NotificationList.CLOSE_PAY_TABLE);
            notifications.push(NotificationList.GET_ALL_IDS);
            notifications.push(NotificationList.SHOW_LINES);
            if (!this.common.isMobile) {
                notifications.push(NotificationList.CHANGED_LINE_COUNT);
            }
            return notifications;
        }

        public drawLine():void {
            var newLine:Shape;
            var lines = this.common.server.arrLinesIds;

            for(var n:number = 0; n < lines.length; n++){
                newLine = this.drawLineClass.create(lines[n], 0);
                this.view.addChild(newLine);
            }
        }

        public showLinesFromTo(fromId:number, toId:number):void {
            var newLine:Shape;
            for(var n:number = fromId; n <= toId; n++){
                newLine = this.drawLineClass.create(n, 1);
                this.view.addChild(newLine);
            }
        }

        private remove():void {
            this.drawLineClass.remove();
        }

        public createMask(positions:Array<Point>):void {
            var symbolsRect:Array<Array<Rectangle>> = this.common.symbolsRect;
            var mask:Shape = new Shape();
            mask.graphics.beginFill("0");
            for (var x:number = 0; x < symbolsRect.length; x++) {
                var reelSymbolsRect = symbolsRect[x];
                for (var y:number = 0; y < reelSymbolsRect.length; y++) {
                    if (!isExist(x, y, positions)) {
                        var rect:Rectangle = this.common.symbolsRect[x][y];
                        mask.graphics.drawRect(rect.x, rect.y, rect.width, rect.height);
                    }
                }
            }
            this.view.mask = mask;

            function isExist(x:number, y:number, positions:Array<Point>) {
                for (var i:number = 0; i < positions.length; i++) {
                    var position:Point = positions[i];
                    if (position.x == x && position.y == y) {
                        return true;
                    }
                }
                return false;
            }
        }

        public handleNotification(message:string, data:any):void {
            switch (message) {
                case NotificationList.GET_ALL_IDS:
                {
                    this.drawLine();
                    this.drawLineClass.reset();
                    break;
                }
                case NotificationList.REMOVE_WIN_LINES:
                {
                    this.remove();
                    break;
                }
                case NotificationList.OPEN_PAY_TABLE:
                {
                    this.view.visible = false;
                    break;
                }
                case NotificationList.CLOSE_PAY_TABLE:
                {
                    this.view.visible = true;
                    break;
                }
                case NotificationList.SHOW_LINES:
                {
                    this.createMask(data[0]);
                    this.drawLineClass.showNextLine();
                    break;
                }
                case NotificationList.CHANGED_LINE_COUNT:
                {
                    Tween.removeTweens(this.view);
                    this.send(NotificationList.REMOVE_WIN_LINES);
                    this.view.mask = null;
                    this.view.alpha = 1;
                    this.showLinesFromTo(1, this.common.server.lineCount);
                    Tween.get(this.view, {useTicks: true})
                        .wait(DrawController.DELAY_REMOVE)
                        .to({alpha: 0}, DrawController.INVISIBLE_TIME)
                        .call(()=> {
                            this.remove();
                            this.view.alpha = 1;
                        });
                    break;
                }

            }
        }

    }
}