module engine {
	import Point = createjs.Point;
	import Container = createjs.Container;

	export class ReelWildController extends BaseController {
		public static wildSymbolsViews:Array<WildSymbolView> = [];
		public static reelsCnt:number = 5;

		private common:CommonRefs;
		private reelsView:ReelsView;
		private winView:Container;

		constructor(manager:ControllerManager, common:CommonRefs, reelsView:ReelsView, winView:Container) {
			super(manager);
			this.common = common;
			this.reelsView = reelsView;
			this.winView = winView;
			for(var i=0; i<ReelWildController.reelsCnt; ++i) {
				if(this.common.layouts[WinController.WIN_ANIMATION_PREFIX + this.common.config.wildSymbolId] && this.common.layouts[WinController.WIN_ANIMATION_PREFIX + this.common.config.wildSymbolId + 'loop']) {
					ReelWildController.wildSymbolsViews[i] = new WildSymbolView();
					ReelWildController.wildSymbolsViews[i].create(this.common.layouts[WinController.WIN_ANIMATION_PREFIX + this.common.config.wildSymbolId], null, this.common.layouts[WinController.WIN_ANIMATION_PREFIX + this.common.config.wildSymbolId + 'loop'], this.common.isMobile);
				}
			}
		}

		public init():void {
			super.init();
		}

		public listNotification():Array<string> {
			var notifications:Array<string> = super.listNotification();
			notifications.push(NotificationList.SHOW_WILD_REEL_FADEIN);
			notifications.push(NotificationList.END_BONUS);
			notifications.push(NotificationList.SHOW_WILD_REEL);
			notifications.push(NotificationList.REMOVE_WIN_LINES);
			return notifications;
		}

		public handleNotification(message:string, data:any):void {
			switch (message) {
				case NotificationList.END_BONUS:
				{
					this.removeWilds();
					break;
				}
				case NotificationList.SHOW_WILD_REEL_FADEIN:
				{
					this.showWildFadeIn(data);
					break;
				}
				case NotificationList.SHOW_WILD_REEL:
				{
					this.showWild(data);
					break;
				}
				case NotificationList.REMOVE_WIN_LINES:
				{
					this.removeWilds();
					break;
				}
			}
		}

		public showWild(reelId:number):void {
			if(ReelWildController.wildSymbolsViews[reelId] && !ReelWildController.wildSymbolsViews[reelId]._is_wild_started) {
				if(this.common.isMobile){
					ReelWildController.wildSymbolsViews[reelId].x = this.reelsView.getReel(reelId).getView().x*GameController.MOBILE_TO_WEB_SCALE;
					ReelWildController.wildSymbolsViews[reelId].y = 92;
				}else {
					ReelWildController.wildSymbolsViews[reelId].x = this.reelsView.getReel(reelId).getView().x;
					ReelWildController.wildSymbolsViews[reelId].y = 68;
				}
				this.winView.addChild(ReelWildController.wildSymbolsViews[reelId]);
				ReelWildController.wildSymbolsViews[reelId].play();
			}
		}

		public showWildFadeIn(reelId:number):void {
			if(ReelWildController.wildSymbolsViews[reelId] && !ReelWildController.wildSymbolsViews[reelId]._is_wild_started) {
				if(this.common.isMobile){
					ReelWildController.wildSymbolsViews[reelId].x = this.reelsView.getReel(reelId).getView().x*GameController.MOBILE_TO_WEB_SCALE;
					ReelWildController.wildSymbolsViews[reelId].y = 92;
				}else {
					ReelWildController.wildSymbolsViews[reelId].x = this.reelsView.getReel(reelId).getView().x;
					ReelWildController.wildSymbolsViews[reelId].y = 68;
				}
				this.winView.addChild(ReelWildController.wildSymbolsViews[reelId]);
				ReelWildController.wildSymbolsViews[reelId].playFadeIn();
			}
		}

		public removeWilds():void{
			console.log("removeWilds");
			for(var reelId=0; reelId<ReelWildController.reelsCnt; ++reelId) {
				this.removeWild(reelId);
			}
		}

		public removeWild(reelId:number):void{
			if (ReelWildController.wildSymbolsViews[reelId] && ReelWildController.wildSymbolsViews[reelId]._is_wild_started){
				this.winView.removeChild(ReelWildController.wildSymbolsViews[reelId]);
				ReelWildController.wildSymbolsViews[reelId]._is_wild_started = false;
			}
		}

		public dispose():void {
			super.dispose();
			this.reelsView.dispose();
			this.reelsView = null;
			this.common = null;
		}

		private hideSymbols(positions:Array<Point>):void {
			this.reelsView.hideSymbols(positions);
		}
	}
}