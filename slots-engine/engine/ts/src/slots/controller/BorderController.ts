/**
 * Created by Taras on 02.02.2015.
 */
module engine {

    export class BorderController extends BaseController {
        public common:CommonRefs;
        public view:BorderView;

        constructor(manager:ControllerManager, common:CommonRefs, view:BorderView) {
            super(manager);
            this.common = common;
            this.view = view;
        }

        public init():void {
            super.init();
            this.view.create();
        }
    }
}