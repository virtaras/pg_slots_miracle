module engine {
	import Sound = createjs.Sound;
	import SoundInstance = createjs.SoundInstance;

	export class SoundController extends BaseController {
		private common:CommonRefs;
		private regularSpinBg:SoundInstance;
		private autoSpinBg:SoundInstance;
		private freeSpinBg:SoundInstance;
		private gambleBg:SoundInstance;
		private payTable:SoundInstance;
		private isShowAllLines:boolean;
		private freeSpinBgStarted:boolean = false;

		constructor(manager:ControllerManager, common:CommonRefs) {
			super(manager);
			this.common = common;
		}

		public init():void {
			super.init();
		}

		public listNotification():Array<string> {
			var notifications:Array<string> = super.listNotification();
			notifications.push(NotificationList.TRY_START_AUTO_PLAY_AUDIO);
			notifications.push(NotificationList.END_AUTO_PLAY);
			notifications.push(NotificationList.START_SPIN);
			notifications.push(NotificationList.START_SPIN_AUDIO);
			notifications.push(NotificationList.STOPPED_ALL_REELS);
			notifications.push(NotificationList.SHOW_WIN_LINES);
			notifications.push(NotificationList.STOPPED_REEL_ID);
			notifications.push(NotificationList.STOPPED_REEL_ID_PREPARE);
			notifications.push(NotificationList.OPEN_PAY_TABLE);
			notifications.push(NotificationList.CLOSE_PAY_TABLE);
			notifications.push(NotificationList.SHOW_LINES);
			notifications.push(NotificationList.SOUND_TOGGLE);
			notifications.push(NotificationList.START_BONUS_AUDIO);
			notifications.push(NotificationList.END_BONUS);
			notifications.push(NotificationList.SHOW_HEADER);
			notifications.push(NotificationList.HIDE_CARD);
			notifications.push(NotificationList.SHOW_CARD);
			notifications.push(NotificationList.CREATE_BONUS);
			notifications.push(NotificationList.RECURSION_PROCESS_ROTATION);
			notifications.push(NotificationList.RECURSION_PROCESSED_FALLING);
			return notifications;
		}

		public handleNotification(message:string, data:any):void {
			switch (message) {
				case NotificationList.SOUND_TOGGLE:
				{
					this.common.isSoundOn = !this.common.isSoundOn;
					Sound.setMute(!this.common.isSoundOn);
					break;
				}
				case NotificationList.TRY_START_AUTO_PLAY_AUDIO:
				{
					this.autoSpinBg = Sound.createInstance(SoundsList.AUTO_SPIN_BG);
					this.autoSpinBg.play({loop: -1});
					break;
				}
				case NotificationList.END_AUTO_PLAY:
				{
					this.autoSpinBg.stop();
					this.autoSpinBg = null;
					break;
				}
				case NotificationList.START_SPIN:
				{
					break;
				}
				case NotificationList.START_SPIN_AUDIO:
				{
					if (this.common.server.win > 0) {
						Sound.play(SoundsList.COLLECT);
					}
					if (this.autoSpinBg == null) {
						this.regularSpinBg = Sound.createInstance(SoundsList.REGULAR_SPIN_BG);
						this.regularSpinBg.play();
					}
					break;
				}
				case NotificationList.STOPPED_ALL_REELS:
				{
					if (this.autoSpinBg == null) {
						if(this.regularSpinBg != null)this.regularSpinBg.stop();
					}
					break;
				}
				case NotificationList.SHOW_WIN_LINES:
				{
					this.isShowAllLines = <boolean>data;
					var winLinesVOs:Array<WinLineVO> = this.common.server.winLines;
					if (winLinesVOs.length > 0) {
						Sound.play(SoundsList.REGULAR_WIN);
					}
					break;
				}
				case NotificationList.RECURSION_PROCESS_ROTATION:
				{
					Sound.play(SoundsList.RECURSION_ROTATION);
					break;
				}
				case NotificationList.STOPPED_REEL_ID_PREPARE:
				case NotificationList.RECURSION_PROCESSED_FALLING:
				{
					Sound.play(SoundsList.REEL_STOP);
					break;
				}
				case NotificationList.OPEN_PAY_TABLE:
				{
					this.payTable = Sound.createInstance(SoundsList.PAY_TABLE_BG);
					this.payTable.play({loop: -1});
					break;
				}
				case NotificationList.CLOSE_PAY_TABLE:
				{
					this.payTable.stop();
					break;
				}
				case NotificationList.SHOW_LINES:
				{
					if (!this.isShowAllLines) {
						Sound.play(SoundsList.LINE_WIN_ENUM);
					}
					break;
				}
				case NotificationList.START_BONUS_AUDIO: case NotificationList.CREATE_BONUS:
				{
					//console.log(NotificationList.START_BONUS_AUDIO, "soundcontroller");
					var bonus:BonusVO = this.common.server.bonus;
					if (bonus != null) {
						//console.log("bonus.type="+bonus.type, "soundcontroller");
						if (this.autoSpinBg != null) {
							this.autoSpinBg.stop();
							this.autoSpinBg = null;
						}
						switch (bonus.type){
							case BonusTypes.FREE_SPINS: case BonusTypes.SELECT_GAME:{
								if (!(this.freeSpinBg != undefined && this.freeSpinBg != null)) this.freeSpinBg = Sound.createInstance(SoundsList.FREE_SPIN_BG);
								if (this.freeSpinBg != undefined && this.freeSpinBg != null && !this.freeSpinBgStarted && this.common.isSoundLoaded) {
									this.freeSpinBgStarted = true;
									this.freeSpinBg.stop();
									this.freeSpinBg.play({loop: -1});
								}
								break;
							}
							case BonusTypes.GAMBLE:{
								if (!(this.gambleBg != undefined && this.gambleBg != null)) this.gambleBg = Sound.createInstance(SoundsList.GAMBLE_AMBIENT);
								if (this.gambleBg != undefined && this.gambleBg != null) {
									this.gambleBg.stop();
									this.gambleBg.play({loop: -1});
								}
								break;
							}
						}
					}
					break;
				}
				case NotificationList.END_BONUS:
				{
					if (this.freeSpinBg != undefined && this.freeSpinBg != null) {
						this.freeSpinBg.stop();
						this.freeSpinBg = null;
					}
					this.freeSpinBgStarted = false;
					break;
				}
				case NotificationList.SHOW_HEADER:
				{
					if (this.gambleBg != undefined && this.gambleBg != null) {
						this.gambleBg.stop();
						this.gambleBg = null;
					}

					break;
				}
				case NotificationList.HIDE_CARD:
				{
					Sound.play(SoundsList.GAMBLE_SHUFFLE);
					break;
				}
				case NotificationList.SHOW_CARD:
				{
					Sound.play(SoundsList.GAMBLE_TURN);
					break;
				}
			}
		}

		public dispose():void {
			super.dispose();
			this.common = null;
		}
	}
}