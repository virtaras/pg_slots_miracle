module engine {
	import Container = createjs.Container;
	import Text = createjs.Text;
	import Layout = layout.Layout;
	import Tween = createjs.Tween;
	import Shape = createjs.Shape;
	import DisplayObject = createjs.DisplayObject;

	export class JackpotView extends Layout {
		public static SCROLL_TIME:number = 300;
		public static LAYOUT_NAME:string = "JackpotView";
		public static JACKPOT_SCROLL_NAME:string = "jackpot_scroll";
		public static JACKPOT_BLACKLINE_NAME:string = "jackpot_blackline";
		public static JACKPOT_TF_NAME:string = "jackpotTF";
		public static JACKPOT_TF_TEXT:string = "";
		private jackpotScroll:Container;
		private jackpotBlackline:DisplayObject;
		private jackpotTF:Text;

		constructor() {
			super();
		}

		public onInit():void {
			this.jackpotBlackline = <DisplayObject>this.getChildByName(JackpotView.JACKPOT_BLACKLINE_NAME);
			this.jackpotScroll = <Container>this.getChildByName(JackpotView.JACKPOT_SCROLL_NAME);
			this.jackpotTF = <Text>this.jackpotScroll.getChildByName(JackpotView.JACKPOT_TF_NAME);
			this.startScrolling();
			this.setScrollingMask();
		}

		public setScrollingMask():void{
			this.mask = new Shape();
			this.mask.graphics.beginFill("0").drawRect(this.jackpotBlackline.x + this.x, this.jackpotBlackline.y + this.y, this.jackpotBlackline.getBounds().width, this.jackpotBlackline.getBounds().height);
		}

		public startScrolling():void{
			Tween.get(this.jackpotScroll, {useTicks: true}).to({x: -1*this.jackpotBlackline.getBounds().width}, JackpotView.SCROLL_TIME).call(()=>{this.jackpotScroll.x=this.jackpotBlackline.getBounds().width; this.startScrolling()});
		}

		public setJackpot(jackpot:number){
			this.jackpotTF.text = JackpotView.JACKPOT_TF_TEXT + MoneyFormatter.format(jackpot, false);
		}
	}
}