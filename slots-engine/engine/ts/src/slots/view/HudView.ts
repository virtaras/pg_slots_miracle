module engine {
	import DisplayObject = createjs.DisplayObject;
	import Container = createjs.Container;
	import Stage = createjs.Stage;
	import Layout = layout.Layout;
	import Button = layout.Button;
	import Text = createjs.Text;

	import Sprite = createjs.Sprite;

	export class HudView extends Layout {
		public static LAYOUT_NAME:string = "HudView";
		//================================================================
		//------------------MENU------------------------------------------
		//================================================================
		public static AUTO_SPIN_TABLE:string = "autoSpinTable";
		public static AUTO_SPIN_TABLE_MASK:string = "autoSpinTableMask";
		public static OPEN_AUTO_SPIN_TABLE_BTN:string = "openAutoSpinTableBtn";
		public static SETTINGS_MENU:string = "settingsMenu";

		//================================================================
		//------------------BUTTONS NAMES---------------------------------
		//================================================================
		public static BACK_TO_LOBBY_BTN:string = "backToLobbyBtn";
		public static SETTINGS_BTN:string = "settingsBtn";
		public static FULL_SCREEN_BTN:string = "fullScreenBtn";

		//Запускает автоспины
		public static START_AUTO_PLAY_BTN:string = "autoSpinStartBtn";
		//Начинает спин
		public static START_SPIN_BTN:string = "spinBtn";

		//Останавлевает автоспины
		public static STOP_AUTO_PLAY_BTN:string = "autoStopBtn";
		//Ускоряет остановку барабанов
		public static STOP_SPIN_BTN:string = "stopBtn";

		//Запускает рисковую
		public static GAMBLE_BTN:string = "gambleBtn";
		//Запускает бонус
		public static START_BONUS_BTN:string = "bonusStartBtn";
		//Открывает HELP
		public static PAY_TABLE_BTN:string = "payTableBtn";

		//Устанавлевает следующее значение фриспинов из конфига autoPlayCount
		public static INC_SPIN_COUNT_BTN:string = "incSpinsBtn";
		//Устанавлевает предыдущее значение фриспинов из конфига autoPlayCount
		public static DEC_SPIN_COUNT_BTN:string = "decSpinsBtn";

		//Увеличевает количество линий на 1 до максимального
		public static INC_LINE_COUNT_BTN:string = "incLinesBtn";
		//Уменьшает количество линий на 1 до 1
		public static DEC_LINE_COUNT_BTN:string = "decLinesBtn";
		//Меняет количество линий по кругу
		public static LINE_COUNT_BTN:string = "linesBtn";
		//Устанавлевает максимальное количество линий и начинает спин
		public static MAX_LINES_BTN:string = "maxBetBtn"; //this is max_lines_btn that should be named as maxBetBtn as we don't have time to overload all games

		//Устанавлевает следующую ставку до максимальной
		public static NEXT_BET_BTN:string = "nextBetBtn";
		//Устанавлевает предыдущую ставку до первой
		public static PREV_BET_BTN:string = "prevBetBtn";
		//Устанавлевает следующую ставку по кругу
		public static BET_BTN:string = "betBtn";
		//Устанавлевает максимальный мультиплекатор и начинает спин
		public static MAX_BET_BTN:string = "maxLinesBtn";
		//Устанавлевает следующий мультиплекатор по кругу
		public static MULTIPLY_BTN:string = "multiplyBtn";

		//Ускоренный режим прокрутки барабанов
		public static TURBO_ON_BTN:string = "turboOnBtn";
		public static TURBO_OFF_BTN:string = "turboOffBtn";
		//Постоянные автоспины
		public static AUTO_ON_BTN:string = "autoOnBtn";
		public static AUTO_OFF_BTN:string = "autoOffBtn";

		//================================================================
		//------------------TEXT FIELDS NAMES-----------------------------
		//================================================================
		public static WIN_TEXT:string = "winTf";
		public static BET_TEXT:string = "betTf";
		public static TOTAL_BET_TEXT:string = "totalBetTf";
		public static PREV_BET_PER_LINE_TEXT:string = "prevLineBetTf";
		public static BET_PER_LINE_TEXT:string = "lineBetTf";
		public static NEXT_BET_PER_LINE_TEXT:string = "nextLineBetTf";
		public static PREV_LINES_COUNT_TEXT:string = "prevLinesCountTf";
		public static LINES_COUNT_TEXT:string = "linesCountTf";
		public static NEXT_LINES_COUNT_TEXT:string = "nextLinesCountTf";
		public static PREV_AUTO_SPINS_COUNT_TEXT:string = "prevSpinCountTf";
		public static AUTO_SPINS_COUNT_TEXT:string = "spinCountTf";
		public static NEXT_AUTO_SPINS_COUNT_TEXT:string = "nextSpinCountTf";
		public static BALANCE_TEXT:string = "balanceTf";

		public buttonsNames:Array<string> = [
			HudView.SETTINGS_BTN,
			HudView.BACK_TO_LOBBY_BTN,
			HudView.START_AUTO_PLAY_BTN,
			HudView.STOP_AUTO_PLAY_BTN,
			HudView.GAMBLE_BTN,
			HudView.INC_SPIN_COUNT_BTN,
			HudView.DEC_SPIN_COUNT_BTN,
			HudView.INC_LINE_COUNT_BTN,
			HudView.DEC_LINE_COUNT_BTN,
			HudView.NEXT_BET_BTN,
			HudView.PREV_BET_BTN,
			HudView.MAX_BET_BTN,
			HudView.MAX_LINES_BTN,
			HudView.START_SPIN_BTN,
			HudView.START_BONUS_BTN,
			HudView.STOP_SPIN_BTN,
			HudView.PAY_TABLE_BTN,
			HudView.BET_BTN,
			HudView.LINE_COUNT_BTN,
			HudView.MULTIPLY_BTN,
			HudView.OPEN_AUTO_SPIN_TABLE_BTN,
			HudView.TURBO_OFF_BTN,
			HudView.TURBO_ON_BTN,
			HudView.AUTO_OFF_BTN,
			HudView.AUTO_ON_BTN
		];

		// for mobile
		private leftObj:Array<string> = [
			HudView.BACK_TO_LOBBY_BTN,
			HudView.START_SPIN_BTN,
			HudView.STOP_SPIN_BTN,
			HudView.START_AUTO_PLAY_BTN,
			HudView.STOP_AUTO_PLAY_BTN,
			HudView.GAMBLE_BTN
		];

		// for mobile
		private rightObj:Array<string> = [
			HudView.SETTINGS_BTN,
			HudView.PAY_TABLE_BTN
		];

		public linesText:Array<Text>;

		public text_fileds_shadow:Array<string> = [
			HudView.BALANCE_TEXT,
			HudView.TOTAL_BET_TEXT,
			HudView.WIN_TEXT,
			HudView.BET_PER_LINE_TEXT
		]

		constructor() {
			super();
			this.linesText = [];
		}

		public onInit():void {
			//this.prepareView();
			var textField:Text;
			var i:number = 1;
			while ((textField = <Text>this.getChildByName(HudView.LINES_COUNT_TEXT + i)) != null) {
				this.linesText.push(textField);
				i++;
			}
		}

		public prepareView():void{
		}

		public addFieldsShadows():void{
			for(var i:number = 0; i<this.text_fileds_shadow.length; ++i){
				GameController.setFieldStroke(this, this.text_fileds_shadow[i], 0, GameController.SHADOW_COLOR, 1, 1, 2);
			}
			for(var i:number = 0; i<this.linesText.length; ++i){
				GameController.setTextStroke(this.linesText[i], 0, GameController.SHADOW_COLOR, 1, 1, 2);
			}
		}

		public resizeForMobile():void {
			var stage:Stage = this.getStage();
			if (stage == null) {
				return;
			}
			var i:number;
			var displayObject:DisplayObject;
			var startX:number;
			var canvas:HTMLCanvasElement = stage.canvas;
			var rightX:number = -stage.x / stage.scaleX;
			var leftX:number = (canvas.width - stage.x) / stage.scaleX;

			startX = this.getBtn(HudView.STOP_AUTO_PLAY_BTN).x;
			for (i = 0; i < this.leftObj.length; i++) {
				displayObject = this.getChildByName(this.leftObj[i]);
				if (displayObject != null) {
					displayObject.x = leftX - displayObject.getBounds().width;
				}
			}
			this.getText(HudView.AUTO_SPINS_COUNT_TEXT).x += this.getBtn(HudView.STOP_AUTO_PLAY_BTN).x - startX;

			// move settings menu
			var settingsMenu:DisplayObject = this.getChildByName(HudView.SETTINGS_MENU);
			if (settingsMenu != null) {
				displayObject = this.getChildByName(this.rightObj[0]);
				settingsMenu.x = rightX + (settingsMenu.x - displayObject.x);
			}

			for (i = 0; i < this.rightObj.length; i++) {
				displayObject = this.getChildByName(this.rightObj[i]);
				if (displayObject != null) {
					displayObject.x = rightX;
				}
			}
		}

		public changeButtonState(btnName:string, visible:boolean, enable:boolean):void {
			var button:Button = this.getBtn(btnName);
			if (button) {
				button.visible = visible;
				button.setEnable(enable);
			}
		}

		public setLineCountText(value:string):void {
			for (var i:number = 0; i < this.linesText.length; i++) {
				this.linesText[i].text = value;
			}
		}

		public dispose():void {
			this.parent.removeChild(this);
		}

		/////////////////////////////////////////
		//	GETTERS
		/////////////////////////////////////////

		public getBtn(btnName:string):Button {
			return <Button>this.getChildByName(btnName);
		}

		public getText(textFieldName:string):Text {
			return <Text>this.getChildByName(textFieldName);
		}

		public changeTextState(textFieldName:string, visible:boolean):void{
			var textField:Text = this.getText(textFieldName);
			if (textField) {
				textField.visible = visible;
			}
		}
	}
}