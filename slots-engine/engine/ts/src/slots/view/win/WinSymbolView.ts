module engine {
	import Container = createjs.Container;
	import DisplayObject = createjs.DisplayObject;
	import Sprite = createjs.Sprite;
	import LayoutCreator = layout.LayoutCreator;
	import Tween = createjs.Tween;

	export class WinSymbolView extends Container {
		public winSymbol:DisplayObject;
		private highlight:Sprite;
		public _is_wild:boolean = false;

		constructor() {
			super();
		}

		public create(winSymbolCreator:LayoutCreator, highlightCreator:LayoutCreator, isMobile:boolean):void {
			if (winSymbolCreator != null) {
				var winSymbolContainer:Container = new Container();
				winSymbolCreator.create(winSymbolContainer);
				this.winSymbol = winSymbolContainer.getChildAt(0);
				this.addChild(winSymbolContainer);
			}

			if (highlightCreator != null) {
				var highlightContainer:Container = new Container();
				highlightCreator.create(highlightContainer);
				this.highlight = <Sprite>highlightContainer.getChildAt(0);
				if(isMobile){
					this.highlight.scaleX *= GameController.MOBILE_TO_WEB_SCALE;
					this.highlight.scaleY *= GameController.MOBILE_TO_WEB_SCALE;
				}
				this.addChild(highlightContainer);
			}
		}

		public play():void {
			if (this.winSymbol != null && this.winSymbol instanceof Sprite) {
				var sprite = (<Sprite>this.winSymbol);
				sprite.gotoAndPlay(0);
			}
			if (this.highlight != null) {
				this.highlight.gotoAndPlay(0)
			}
		}

		public setY(value:number):void {
			this.y = value;
		}
		public setX(value:number):void {
			this.x = value;
		}
	}
}