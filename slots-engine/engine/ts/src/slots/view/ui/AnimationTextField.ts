module engine {
	import Text = createjs.Text;

	export class AnimationTextField {
		private textField:Text;
		private easing:BaseEasing;
		private value:number;
		private isUpdate:boolean;

		constructor(textField:Text) {
			this.textField = textField;
			this.easing = new LinearEasing();
			this.isUpdate = false;
		}

		public setValue(value:number, updateTime:number = 0):void {
			if (updateTime == 0) {
				this.value = value;
				this.setText(value);
				this.isUpdate = false;
			}
			else if (this.value != value) {
				this.isUpdate = true;
				this.easing.setParameters(value - this.value, updateTime);
			}
		}

		public update():void {
			if (this.isUpdate) {
				var pos:number = this.easing.getPosition();
				this.setText(this.value + pos);
				if (this.easing.getTime() >= 1) {
					this.isUpdate = false;
					this.value += pos;
					this.easing.reset();
				}
			}
		}

		private setText(value:number):void {
			this.textField.text = MoneyFormatter.format(value, false);
		}
	}
}