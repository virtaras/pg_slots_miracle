module engine {
	import Shape = createjs.Shape;
	import Sprite = createjs.Sprite;
	import Rectangle = createjs.Rectangle;
	import Container = createjs.Container;
	import DisplayObject = createjs.DisplayObject;
	import Layout = layout.Layout;
	import Button = layout.Button;

	export class LoaderView extends Layout {
		public static LAYOUT_NAME:string = "LoaderView";
		public static ANIMATION_CONTAINER_NAME:string = "animation";
		public static PROGRESS_BAR:string = "progressBar";
		public static PROGRESS:string = "progress";

		public static BUTTON_YES:string = "yesBtn";
		public static BUTTON_NO:string = "noBtn";

		public progressBar:Container;
		public progress:DisplayObject;
		public startPos:number;

		public buttonsNames:Array<string> = [
			LoaderView.BUTTON_YES,
			LoaderView.BUTTON_NO
		];

		constructor() {
			super();
		}

		public onInit():void {
			this.initAnimation();
			this.initProgressBar();
		}

		private initAnimation():void {
			var animation:Sprite = <Sprite>this.getChildByName(LoaderView.ANIMATION_CONTAINER_NAME);
			if (animation != null) {
				animation.play();
			}
		}

		private initProgressBar():void {
			this.progressBar = <Container>this.getChildByName(LoaderView.PROGRESS_BAR);
			this.progressBar.visible = false;

			this.progress = this.progressBar.getChildByName(LoaderView.PROGRESS);
			var bounds:Rectangle = this.progress.getBounds();
			var mask:Shape = new Shape();
			mask.graphics.beginFill("0").drawRect(this.progress.x, this.progress.y, bounds.width, bounds.height);
			this.progress.x -= bounds.width;
			this.progress.mask = mask;

			this.startPos = this.progress.x;
		}

		public hideButtons():void {
			for (var i:number = 0; i < this.buttonsNames.length; i++) {
				var buttonName:string = this.buttonsNames[i];
				var button:Button = <Button>this.getChildByName(buttonName);
				button.visible = false;
			}
		}

		public showProgressBar():void {
			this.progressBar.visible = true;
		}

		/**
		 * @param value - loading progress [0;1]
		 */
		public setProgress(value:number):void {
			if (value > 1 || value < 0) {
				throw new Error("loading progress value is not correct");
			}
			this.progress.x = this.startPos + this.progress.getBounds().width * value;
			//console.log("Loading progress: " + value);
		}

		public dispose():void {
			this.parent.removeChild(this);
		}
	}
}