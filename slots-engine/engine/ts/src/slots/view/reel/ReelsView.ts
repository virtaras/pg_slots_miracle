module engine {
	import Container = createjs.Container;
	import Shape = createjs.Shape;
	import Rectangle = createjs.Rectangle;
	import Point = createjs.Point;
	import Layout = layout.Layout;

	export class ReelsView extends Layout {
		public static LAYOUT_NAME:string = "ReelsView";
		private static REEL_PREFIX:string = "reel_";
		private reels:Array<ReelView>;
		public isMobile:boolean;

		constructor() {
			super();
		}

		public setMobileTrue(isMobile:boolean):void {
			this.isMobile = isMobile;
			this.createReelsMask();
		}

		public onInit():void {
			this.createReelsView();
		}

		private createReelsView():void {
			this.reels = [];
			var view:Container;
			var reelId:number = 1;
			while ((view = <Container>this.getChildByName(ReelsView.REEL_PREFIX + reelId)) != null) {
				var reelView:ReelView = new ReelView(view, reelId - 1);
				reelView.init();
				this.reels.push(reelView);
				reelId++;
			}
		}

		private createReelsMask():void {
			var firstReel:ReelView = this.reels[0];
			var symbolBounds:Rectangle = firstReel.getSymbol(0).getBounds();
			var lastReel:ReelView = this.reels[this.reels.length - 1];

			var mask:Shape = new Shape();
			var y:number = firstReel.getY();

			if(this.isMobile){
				mask.graphics.beginFill("0").drawRect(firstReel.getX(), y*GameController.MOBILE_TO_WEB_SCALE, this.getWidth() * GameController.MOBILE_TO_WEB_SCALE, this.getHeight() * GameController.MOBILE_TO_WEB_SCALE - symbolBounds.height * GameController.MOBILE_TO_WEB_SCALE);
			}else{
				mask.graphics.beginFill("0").drawRect(firstReel.getX(), firstReel.getY(), lastReel.getX() + symbolBounds.width, this.getHeight() - symbolBounds.height);
			}
			//mask.graphics.beginFill("0").drawRect(firstReel.getX(), firstReel.getY(), lastReel.getX() + symbolBounds.width, this.getHeight() - symbolBounds.height); //mobiletoweb

			this.mask = mask;
			this.hitArea = mask;
		}

		public showAllSymbols():void {
			for (var i:number = 0; i < this.reels.length; i++) {
				this.reels[i].showAllSymbols();
			}
		}

		public hideSymbols(positions:Array<Point>, offset:number = 0):void {
			for (var i:number = 0; i < positions.length; i++) {
				var pos:Point = positions[i];
				this.reels[pos.x].getSymbol(pos.y + offset).visible = false;
			}
		}

		public dispose():void {
			this.parent.removeChild(this);
			this.reels = null;
		}

		/////////////////////////////////////////
		//	GETTERS
		/////////////////////////////////////////

		public getReel(id:number):ReelView {
			return this.reels[id];
		}

		public getReelCount():number {
			return this.reels.length;
		}

		public getHeight():number {
			return this.getBounds().height;
		}

		public getWidth():number {
			return this.getBounds().width;
		}

		public getReelsMask():Shape{
			return this.mask;
		}
	}
}