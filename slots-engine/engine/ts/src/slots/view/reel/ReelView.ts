module engine {
	import Container = createjs.Container;
	import Sprite = createjs.Sprite;
	import Rectangle = createjs.Rectangle;
	import Tween = createjs.Tween;
	import Shape = createjs.Shape;
	import Ease = createjs.Ease;

	export class ReelView {
		private static SYMBOL_PREFIX:string = "symbol_";

		public view:Container;
		private reelId:number;
		private symbols:Array<Sprite>;

		constructor(view:Container, reelId:number) {
			this.view = view;
			this.reelId = reelId;
		}

		public init():void {
			this.symbols = [];
			var symbolId:number = 1;
			var symbol:Sprite;
			while ((symbol = <Sprite>this.view.getChildByName(ReelView.SYMBOL_PREFIX + symbolId)) != null) {
				this.symbols.push(symbol);
				symbolId++;
			}
		}

		public showAllSymbols():void {
			for (var i:number = 0; i < this.symbols.length; i++) {
				this.symbols[i].visible = true;
			}
		}

		/////////////////////////////////////////
		//	GETTERS
		/////////////////////////////////////////
		public getReelId():number {
			return this.reelId;
		}

		public getSymbol(id:number):Sprite {
			return this.symbols[id];
		}

		public getSymbolFrame(id:number):number {
			return this.symbols[id].currentFrame;
		}

		public getSymbolCount():number {
			return this.symbols.length - 1;
		}

		public getX():number {
			return this.view.x;
		}

		public getY():number {
			return this.view.y;
		}

		/////////////////////////////////////////
		//	SETTERS
		/////////////////////////////////////////

		public setX(value:number):void {
			this.view.x = value;
		}

		public setY(value:number):void {
			this.view.y = value;
		}

		public setSymbolFrame(symbolId:number, frameId:number):void {
			return this.symbols[symbolId].gotoAndStop(frameId || 0);
		}

		public getView():Container{
			return this.view;
		}

		public getSymbols():Array<Sprite>{
			return this.symbols;
		}

		public symbolsMoveDown(delay_reels:number, delay_symbols:number, callback:(...params:any[]) => any):void{
			var self = this;
			for(var i:number = 0; i<this.symbols.length; ++i){
				//Tween.get(this.symbols[i]).wait(delay).to({y:400+300*(i+1)},1000).call(function(){self.symbolsAppearFromTop();});
				Tween.get(this.symbols[i]).wait(delay_reels + delay_symbols*(this.symbols.length - i)).to({y:400+600*(i+1)},ReelMoverCascade.MOVE_DOWN_TIME, Ease.getPowIn(5.2)).call(callback);
			}
		}

		public symbolsAppearFromTop(callback:(...params:any[]) => any):void{
			var symbolHeight:number = this.symbols[0].getBounds().height;
			for(var i:number = 0; i<this.symbols.length; ++i){
				Tween.get(this.symbols[i], {override:true}).to({y:-1500 + 300*i}).to({y:symbolHeight*(i)},500).call(callback);
			}
			//Tween.get(this).call(callback);
		}

		public  recursionProcessFalling(common:any):void {
			var symbolHeight:number = this.symbols[0].getBounds().height;

			for(var symbolid:number = 0; symbolid<ReelMover.SYMBOLS_IN_REEL; ++symbolid){
				var start_y = this.symbols[symbolid].y;
				var end_y = this.symbols[symbolid].y;

				var symbolUpdates:any = this.getSymbolUpdates(symbolid, common);
				if(symbolUpdates["isNeedRefresh"]){
					var frameid = this.getUpdatedFrameId(symbolid, common);
					this.setSymbolFrame(symbolid, frameid);
					start_y = start_y - parseInt(symbolUpdates["symbolUpdates"])*symbolHeight;
					Tween.get(this.symbols[symbolid], {override:true}).to({y:start_y, rotation:0}).to({y:end_y}, ReelMoverCascade.RECURSION_SYMBOLS_FALLING_TIME);
					this.symbols[symbolid].mask = null;
				}
			}
		}

		public  recursionProcessRotation(common:any):void {
			var symbolWidth:number = this.symbols[0].getBounds().width;
			var symbolHeight:number = this.symbols[0].getBounds().height;
			for(var symbolid:number = 0; symbolid<ReelMover.SYMBOLS_IN_REEL; ++symbolid){
				var symbolUpdates:any = this.getSymbolUpdates(symbolid, common);
				if(symbolUpdates["isWin"]){
					var mask:Shape = new Shape();
					mask.graphics.beginFill("0");
					mask.graphics.drawRect(this.symbols[symbolid].x, this.symbols[symbolid].y, symbolWidth, symbolHeight);
					this.symbols[symbolid].mask = mask;
					Tween.get(this.symbols[symbolid], {override:true}).to({rotation: 90}, ReelMoverCascade.RECURSION_ROTATION_TIME);
				}
			}
		}

		public getSymbolUpdates(symbolid:number, common:any):any{
			var symbolUpdates:any = [];
			var needRefresh:number = 0;
			//for(var i:number=0; i<common.server.winLines.length; ++i){
			//	if(common.server.winLines[i].winPos[this.reelId] > symbolid){
			//		++needRefresh; //TODO: check if that symbol hasn't already played in another line
			//	}
			//}
			for(var i:number = symbolid; i<ReelMover.SYMBOLS_IN_REEL; ++i){
				for(var j:number=0; j<common.server.winLines.length; ++j){
					if(common.server.winLines[j].winPos[this.reelId] == (symbolid+1)){
						symbolUpdates["isWin"] = true;
					}
					if(common.server.winLines[j].winPos[this.reelId] == (i+1)){
						++needRefresh;
						break;
					}
				}
			}
			symbolUpdates["symbolUpdates"] = needRefresh;
			symbolUpdates["isNeedRefresh"] = (needRefresh>0);
			return symbolUpdates;
		}

		public getUpdatedFrameId(symbolid:number, common:any):number{
			var frameid:number = common.server.recursion[0].wheels[this.reelId*ReelMoverCascade.SYMBOLS_IN_REEL + symbolid] - 1;
			return frameid;
		}

		public getNumFrames():number{
			return this.symbols[0].spriteSheet.getNumFrames();
		}
	}
}