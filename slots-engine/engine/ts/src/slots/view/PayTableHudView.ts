module engine {
    import DisplayObject = createjs.DisplayObject;
	import Layout = layout.Layout;
	import Button = layout.Button;
    import Stage = createjs.Stage;

	export class PayTableHudView extends Layout {
		public static LAYOUT_NAME:string = "PayTableHudView";

		public static BACK_TO_GAME_BTN:string = "backToGameBtn";
		public static PREV_PAGE_BTN:string = "prevPageBtn";
		public static NEXT_PAGE_BTN:string = "nextPageBtn";

		public buttonsNames:Array<string> = [
			PayTableHudView.BACK_TO_GAME_BTN,
			PayTableHudView.PREV_PAGE_BTN,
			PayTableHudView.NEXT_PAGE_BTN
		];

		constructor() {
			super();
		}

		public getBtn(btnName:string):Button {
			return <Button>this.getChildByName(btnName);
		}

        public resizeForMobile():void {
            var stage:Stage = this.getStage();
            if (stage == null) {
                return;
            }
            var i:number;
            var displayObject:DisplayObject;
            var canvas:HTMLCanvasElement = stage.canvas;
            var rightX:number = (canvas.width - stage.x) / stage.scaleX;
            for (i = 0; i < this.buttonsNames.length; i++) {
                displayObject = this.getChildByName(this.buttonsNames[i]);
                if (displayObject != null) {
                    displayObject.x = rightX - displayObject.getBounds().width;
                }
            }
        }
	}
}