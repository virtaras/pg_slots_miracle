module engine {
	export class NotificationList {
		public static BACK_TO_LOBBY:string = "back_to_lobby";
		public static HIDE_PRELOADER:string = "hide_preloader";

		public static SERVER_INIT:string = "server_init";
		public static SERVER_SEND_SPIN:string = "send_spin";
		public static SERVER_GOT_SPIN:string = "got_spin";
		public static SERVER_SEND_BONUS:string = "send_bonus";
		public static SERVER_GOT_BONUS:string = "got_bonus";
		public static SERVER_SET_WHEEL:string = "set_wheel";
		public static SERVER_DISCONNECT:string = "disconnect";
		public static SERVER_NOT_RESPONSE:string = "server_not_response";
		public static SERVER_GET_BALANCE_REQUEST:string = "server_get_balance";

		public static RES_LOADED:string = "res_loaded";
		public static LOAD_SOUNDS:string = "load_sounds";
		public static SOUNDS_LOADED:string = "sounds_loaded";

		public static KEYBOARD_CLICK:string = "keyboard_click";
		public static ON_SCREEN_CLICK:string = "on_screen_click";

		public static TRY_START_AUTO_PLAY:string = "try_start_auto_play";
		public static END_AUTO_PLAY:string = "end_auto_play";
		public static TRY_START_SPIN:string = "try_start_spin";
		public static START_SPIN:string = "start_spin";
		public static START_SPIN_AUDIO:string = "start_spin_audio";
		public static EXPRESS_STOP:string = "express_stop";
		public static STOPPED_REEL_ID:string = "stopped_reel_id";
		public static STOPPED_REEL_ID_PREPARE:string = "stopped_reel_id_prepare";
		public static STOPPED_ALL_REELS:string = "stopped_all_reels";

		public static CHANGED_LINE_COUNT:string = "changed_line_count";
		public static OPEN_PAY_TABLE:string = "open_pay_table";
		public static CLOSE_PAY_TABLE:string = "close_pay_table";
		public static OPEN_AUTO_SPIN_MENU:string = "open_auto_spin_menu";
		public static TOGGLE_SETTINGS_MENU:string = "toggle_settings_menu";
		public static CLOSE_SETTINGS_MENU:string = "close_settings_menu";
		public static SOUND_TOGGLE:string = "sound_toggle";

		public static SHOW_WIN_LINES:string = "show_win_lines";
		public static WIN_LINES_SHOWED:string = "win_lines_showed";
		public static REMOVE_WIN_LINES:string = "remove_vin_lines";
		public static SHOW_LINES:string = "show_line";
		public static HIDE_LINES:string = "hide_lines";

		public static SHOW_HEADER:string = "show_header";
		public static REMOVE_HEADER:string = "remove_header";

		public static UPDATE_BALANCE_TF:string = "update_balance";
		public static SHOW_WIN_TF:string = "show_win";
		public static COLLECT_WIN_TF:string = "collect_balance";

		public static CREATE_BONUS:string = "create_bonus";
		public static START_BONUS:string = "start_bonus";
		public static END_BONUS:string = "end_bonus";
		public static START_SECOND_FREE_GAME:string = "second_game";
		public static HIDE_CARD:string = "hide_card";
		public static SHOW_CARD:string = "show_card";

		public static SHOW_ALL_SYMBOLS:string = "show_all_symbols";
		public static HIDE_SYMBOLS:string = "hide_symbols";

        public static GET_ALL_IDS:string = "get_all_ids";
        public static LAZY_LOAD:string = "lazy_load";
        public static LAZY_LOAD_COMP:string = "lazy_load_compl";

		public static AUTO_PLAY_COMP:string = "auto_play_compl";
		public static AUTO_PLAY_CONT:string = "auto_play_cont";
		public static SHOW_AUTO_MODAL:string = "show_auto_modal";

		public static LOAD_PAY_TABLE:string = "load_pay_tables";
		public static START_BONUS_AUDIO:string = "start_bonus_audio";
		public static TRY_START_AUTO_PLAY_AUDIO:string = "try_start_auto_play_audio";
		public static SHOW_WILD_REEL_FADEIN:string = "show_wild_reel_fadein";
		public static SHOW_WILD_REEL:string = "show_wild_reel";

		public static RECURSION_PROCESS_FALLING:string = "recursion_process_falling";
		public static RECURSION_PROCESSED_FALLING:string = "recursion_processed_falling";
		public static RECURSION_PROCESSED:string = "recursion_processed";
		public static RECURSION_PROCESS_ROTATION:string = "recursion_process_rotation";
		public static LOADER_LOADED:string = "loader_loaded";

		public static SHOW_ERRORS:string = "show_errors";
		public static UPDATE_JACKPOT:string = "update_jackpot";
		public static UPDATE_JACKPOT_SERVER:string = "update_jackpot_server";
		public static SHOW_JACKPOT_POPUP:string = "show_jackpot_popup";
		public static OK_BTN_ERROR_CLICKED:string = "ok_btn_error_clicked";

		public static TURBO_OFF_CLICKED:string = "turbo_off_clicked";
		public static UPDATE_TURBO_MODE:string = "update_turbo_mode";

	}
}
