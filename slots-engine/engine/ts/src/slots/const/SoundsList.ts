module engine {
	export class SoundsList {
		//-------------SOUND NAMES---------------------------------
		public static REGULAR_CLICK_BTN:string = "regular_click_btn";
		public static SPIN_CLICK_BTN:string = "spin_click_btn";

		public static REGULAR_SPIN_BG:string = "regular_spin_bg";
		public static AUTO_SPIN_BG:string = "auto_spin_bg";
		public static FREE_SPIN_BG:string = "free_spin_bg";
		public static PAY_TABLE_BG:string = "pay_table_bg";

		public static COLLECT:string = "collect";
		public static REEL_STOP:string = "reel_stop";
		public static FREE_SPIN_COLLECT:string = "free_spin_collect";
		public static REGULAR_WIN:string = "regular_win";
		public static LINE_WIN_ENUM:string = "line_win_enum";

		public static GAMBLE_BG:string = 'gamble_bg';
		public static GAMBLE_SHUFFLE:string = 'gamble_shuffle';
		public static GAMBLE_TURN:string = 'gamble_turn';

		public static GAMBLE_AMBIENT:string = 'gamble_ambient';
		public static CARD_TURN:string = 'card_turn';
		public static CARD_SHUFFLE:string = 'card_shuffle';
		public static RECURSION_ROTATION:string = 'recursion_rotation';

	}
}