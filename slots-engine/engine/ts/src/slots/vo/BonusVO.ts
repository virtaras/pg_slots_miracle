module engine {
	export class BonusVO {
		public id:number;
		public step:number;
		public key:string;
		public type:string;
		public className:string;
		public data:any;

		constructor() {
		}
	}
}