module engine {
	export class AjaxRequest {
		public static JSON:string = "json";
		public static XML:string = "xml";

		private xmlHttp:any;
		private url:string;
		private type:string;
		private isAsynchronous:boolean;
		private handler:Function;

		constructor(type:string, url:string, isAsynchronous:boolean, params:Object) {
			this.type = type;
			this.url = url + AjaxRequest.paramsToString(params);
			this.isAsynchronous = isAsynchronous;
		}

		private static paramsToString(params:any):string {
			var paramsStr:string = "";
			for (var key in params) {
				paramsStr += paramsStr.length == 0 ? "?" : "&";
				paramsStr += key + "=" + params[key];
			}
			return paramsStr;
		}

		private static getXmlHttp():any {
			if (window.hasOwnProperty("XMLHttpRequest")) {
				// code for IE7+, Firefox, Chrome, Opera, Safari
				return new XMLHttpRequest();
			} else if (window.hasOwnProperty("ActiveXObject")) {
				// code for IE6, IE5
				return new ActiveXObject("Microsoft.XMLHTTP");
			}
		}

		/**
		 * Send GET request
		 */
		public get(handler:Function = null):void {
			this.handler = handler;
			this.xmlHttp = AjaxRequest.getXmlHttp();
			this.xmlHttp.onreadystatechange = ()=> {
				this.onComplete();
			};
			this.xmlHttp.open("GET", this.url, this.isAsynchronous);
			this.xmlHttp.send(null);
		}

		public abort():void {
			this.xmlHttp.abort();
		}

		/**
		 * Request is ready
		 */
		private onComplete():void {
			if (this.xmlHttp.readyState == XMLHttpRequest.DONE) {
				// success
				if (this.xmlHttp.status == 200) {
					// Registration : in the registration process xmlRootNode return current status that user registered or not.
					var responseData;
					switch (this.type) {
						case AjaxRequest.XML:
						{
							responseData = this.xmlHttp.responseXML;
							break;
						}
						case AjaxRequest.JSON:
						{
							responseData = JSON.parse(this.xmlHttp.response);
							break;
						}
					}
					// TODO: need remove on server empty response
					/*if (responseData == null) {
						throw new Error("ERROR: Server response null");
					}*/
					if (this.handler != null) {
						this.handler(responseData);
					}
				} else {
					throw new Error("ERROR: There was a problem retrieving the data: " + this.xmlHttp.statusText);
				}
			}
		}
	}
}