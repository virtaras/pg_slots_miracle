module engine {
	export class BaseEasing {
		private static EPSILON:number = 0.000000001;

		private distance:number;
		private dt:number;
		private currentTime:number;
		private position:number;
		private speed:number;

		constructor() {
		}

		/**
		 * Установка параметров
		 * @param distance - рассояние
		 * @param updateCount - за сколько кадров нужно прйти от distance
		 */
		public setParameters(distance:number, updateCount:number):void {
			this.distance = distance;
			this.dt = 1 / updateCount;
			this.reset();
		}

		public getPosition():number {
			this.currentTime += this.dt;
			if (this.currentTime + BaseEasing.EPSILON > 1) {
				this.currentTime = 1;
			}
			var currentPos:number = this.getRatio(this.currentTime) * this.distance;
			this.speed = currentPos - this.position;
			this.position = currentPos;
			return currentPos;
		}

		public getTime():number {
			return this.currentTime;
		}

		public reset():void {
			this.currentTime = 0;
			this.position = 0;
			this.speed = 0;
		}

		public getRatio(time:number):number {
			throw new Error("ERROR: Need override method");
		}
	}
}