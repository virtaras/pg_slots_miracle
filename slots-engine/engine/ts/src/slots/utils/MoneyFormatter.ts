module engine {

	export class MoneyFormatter {

		public static format(value:number, isTrimZeros:boolean, isShowZero:boolean = true, symbol:string = ""):string {
			if (value == 0) {
				return isShowZero ? symbol + "0" : "";
			}

			var digits:Array<string> = value.toFixed(2).split(".");
			digits[0] = digits[0].split("").reverse().join("").replace(/(\d{3})(?=\d)/g, "$1,").split("").reverse().join("");

			if (isTrimZeros) {
				value = parseInt(digits[1]);
				if (value > 0) {
					digits[1] = value.toString();
				}
				else {
					return symbol + digits[0];
				}
			}
			return symbol + digits.join(".");
		}
	}
}