module engine {
    import Layout = layout.Layout;
    import Text = createjs.Text;
    import Tween = createjs.Tween;
    import Ticker = createjs.Ticker;
    import Button = layout.Button;
    import Shape = createjs.Shape;
    import Rectangle = createjs.Rectangle;

    export class ConnectionView extends Layout {
        public static LAYOUT_NAME:string = "ConnectionView";
        public static OK_BTN:string = "okBtn";

        public buttonsNames:Array<string> = [
            ConnectionView.OK_BTN
        ];

        constructor() {
            super();
        }

        public onInit():void {
        }

        public getButton(name:string):Button {
            return <Button>this.getChildByName(name);
        }

        public changeButtonState(btnName:string, visible:boolean, enable:boolean):void {
            var button:Button = this.getButton(btnName);
            if (button) {
                button.visible = visible;
                button.setEnable(enable);
            }
        }

        public setText(txt:any):void{
            return;
        }
    }
}