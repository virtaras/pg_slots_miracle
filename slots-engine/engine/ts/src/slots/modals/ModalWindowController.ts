module engine {
    import Container = createjs.Container;
    import Text = createjs.Text;
    import Button = layout.Button;
    import Rectangle = createjs.Rectangle;
    import Shape = createjs.Shape;
    import Stage = createjs.Stage;

    export class ModalWindowController extends BaseController {
        private isCreated:boolean;

        public common:CommonRefs;
        public container:Container;
        private overlay:Container;
        private view:AutoPlayView;
        private modalWindowName:string;

        constructor(manager:ControllerManager, common:CommonRefs, container:Container) {
            super(manager);
            this.common = common;
            this.container = container;
            this.isCreated = false;
        }

        public init():void {
            super.init();
            this.create();
        }

        public listNotification():Array<string> {
            var notifications:Array<string> = super.listNotification();
            notifications.push(NotificationList.SHOW_AUTO_MODAL);
            notifications.push(NotificationList.TRY_START_AUTO_PLAY);
            notifications.push(NotificationList.SERVER_NOT_RESPONSE);
            notifications.push(NotificationList.SHOW_ERRORS);
            notifications.push(NotificationList.SHOW_JACKPOT_POPUP);
            return notifications;
        }

        private initModal(info:any):void {
            this.view = <AutoPlayView>this.common.layouts[this.modalWindowName] || <AutoPlayView>this.common.layouts[ConnectionView.LAYOUT_NAME];
            this.view.create();

            this.overlay = new Container();
            var maskView:Container = new Container();

            this.overlay.addChild(maskView);
            this.overlay.addChild(this.view);
            this.container.addChild(this.overlay);

            var stage:Stage = this.container.getStage();
            var canvas:HTMLCanvasElement = stage.canvas;
            var leftX:number = -(canvas.width - stage.x) / stage.scaleX;
            var top:number = -(canvas.height - stage.y) / stage.scaleY;

            var mask:Shape = new Shape();
            var bounds:Rectangle = this.view.getBounds();
            mask.graphics.beginFill("0").drawRect(leftX, top, bounds.width  * 10, bounds.height * 5);
            mask.alpha = 0.7;
            maskView.addEventListener("click", function(){});
            maskView.addChild(mask);
            this.initHandlers();
            if(info)this.view.setText(info);
        }

        private initHandlers():void {
            var buttonsNames:Array<string> = this.view.buttonsNames;
            var buttonsCount:number = buttonsNames.length;
            for (var i:number = 0; i < buttonsCount; i++) {
                this.view.getButton(buttonsNames[i]).on("click", (eventObj:any)=> {
                    if (eventObj.nativeEvent instanceof MouseEvent) {
                        this.onBtnClick(eventObj.currentTarget.name);
                    }
                });
                this.view.changeButtonState(buttonsNames[i], true, true);
            }
        }

        private disposeModal():void {
            this.removeHandlers();
            this.view = null;
            this.overlay = null;
            this.container.removeAllChildren();
        }

        private removeHandlers():void {
            var buttonsNames:Array<string> = this.view.buttonsNames;
            var buttonsCount:number = buttonsNames.length;
            for (var i:number = 0; i < buttonsCount; i++) {
                this.view.getButton(buttonsNames[i]).removeAllEventListeners("click");
            }
        }

        private onBtnClick(buttonName:string):void {
            switch (buttonName) {
                case AutoPlayView.YES_BTN:
                {
                    this.send(NotificationList.AUTO_PLAY_CONT);
                    break;
                }
                case AutoPlayView.NO_BTN:
                {
                    this.send(NotificationList.AUTO_PLAY_COMP);
                    break;
                }
                case ConnectionView.OK_BTN:
                {
                    window.location.reload();
                    break;
                }
                case ErrorView.OK_BTN:
                {
                    //process error closing
                    this.send(NotificationList.OK_BTN_ERROR_CLICKED);
                    break;
                }
                case JackpotViewPopup.JACKPOT_CONTINUE_BTN_NAME:
                {
                    //process error closing
                    this.send(NotificationList.SERVER_GET_BALANCE_REQUEST, null);
                    break;
                }
            }

            this.disposeModal();
        }

        public handleNotification(message:string, data:any):void {
            switch (message) {
                case NotificationList.SHOW_AUTO_MODAL:
                {
                    this.modalWindowName = AutoPlayView.LAYOUT_NAME;
                    this.initModal(data);
                    break;
                }
                case NotificationList.SERVER_NOT_RESPONSE:
                {
                    this.modalWindowName = ConnectionView.LAYOUT_NAME;
                    this.initModal(data);
                    break;
                }
                case NotificationList.SHOW_ERRORS:
                {
                    this.modalWindowName = ErrorView.LAYOUT_NAME;
                    this.initModal(data);
                    break;
                }
                case NotificationList.SHOW_JACKPOT_POPUP:
                {
                    if(this.common.layouts[JackpotViewPopup.LAYOUT_NAME]) {
                        this.modalWindowName = JackpotViewPopup.LAYOUT_NAME;
                        this.initModal(data);
                    }
                    break;
                }
            }
        }

        // create modal window
        public create():void {
            this.isCreated = true;
        }

        public onGotResponse(data:any):void {
        }
    }
}