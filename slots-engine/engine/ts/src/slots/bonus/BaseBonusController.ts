module engine {
	import Container = createjs.Container;
	import Text = createjs.Text;
	import Button = layout.Button;

	export class BaseBonusController extends BaseController {
		private isCreated:boolean;

		public common:CommonRefs;
		public container:Container;
		public isAutoRequest:Boolean;

		constructor(manager:ControllerManager, common:CommonRefs, container:Container, startStep:number, isAutoRequest:boolean) {
			super(manager);
			this.common = common;
			this.container = container;
			if (this.common.server.bonus.data == null && !this.common.server.bonus.step) {
				this.common.server.bonus.step = startStep;
			}
			this.isCreated = false;
			this.isAutoRequest = isAutoRequest;
		}

		public init():void {
			super.init();
			var bonusData:any = this.common.server.bonus.data;
			//TODO: process bonus with bonusdata==null
			//console.log(this.common.server.bonus, "bonus");
			//console.log(this.common.server.bonus.key, "bonus key");
			if (bonusData == null && this.isAutoRequest) {
				this.sendRequest();
				this.isAutoRequest = false; // to check bonus with bonusData==null if it is double calling of init() (should work properly) or double creation of object (the error will remain)
			}
			else {
				this.create();
				if (bonusData != null) {
					this.onGotResponse(bonusData);
				}
			}
		}

		public listNotification():Array<string> {
			var notifications:Array<string> = super.listNotification();
			notifications.push(NotificationList.SERVER_GOT_BONUS);
			notifications.push(NotificationList.START_BONUS);
			notifications.push(NotificationList.LAZY_LOAD_COMP);
			return notifications;
		}

		public handleNotification(message:string, data:any):void {
			switch (message) {
				case NotificationList.SERVER_GOT_BONUS:
				{
					if (!this.isCreated) {
						this.create();
					}
					this.onGotResponse(this.common.server.bonus.data);
					break;
				}
				case NotificationList.LAZY_LOAD_COMP:
				{
					this.send(NotificationList.START_BONUS_AUDIO);
					break;
				}
				case NotificationList.START_BONUS:
				{
					this.send(NotificationList.START_BONUS_AUDIO);
					break;
				}
			}
		}

		// create bonus game
		public create():void {
			this.isCreated = true;
		}

		public sendRequest(param:number = 0):void {
			this.send(NotificationList.SERVER_SEND_BONUS, {"param": param, "step": ++this.common.server.bonus.step});
		}

		public onGotResponse(data:any):void {
		}
	}
}