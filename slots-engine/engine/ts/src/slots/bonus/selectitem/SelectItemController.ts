module engine {
	import Container = createjs.Container;
	import Tween = createjs.Tween;
	import Button = layout.Button;

	export class SelectItemController extends BaseBonusController {
		private view:SelectItemView;
		private isFinish:boolean;
		private itemsVO:Array<ItemVO>;
		private spinCount:number;
		private multpCount:number;
		private serverCountStep:number = 0;

		constructor(manager:ControllerManager, common:CommonRefs, container:Container) {
			super(manager, common, container, 0, false);
			common.originBonus = common.server.bonus.type;
		}

		public init():void {
			super.init();
			if(this.common.isMobile) {
				this.setHudButtons(false, false);
			}
		}

		public create():void {
			console.log("create");
			super.create();
			this.send(NotificationList.REMOVE_HEADER);
			this.view = <SelectItemView>this.common.layouts[SelectItemView.LAYOUT_NAME];
			this.view.create();
			this.view.showBonus();
			this.view.hideAllWinAnimation();
			this.container.addChild(this.view);
			this.spinCount = 8;
			this.multpCount = 2;
			var itemCount:number = this.view.getItemCount();
			this.itemsVO = new Array(itemCount);
			for (var i:number = 0; i < itemCount; i++) {
				this.itemsVO[i] = new ItemVO(i);
			}

			this.setEnableAllItem(true);
			this.initStartButton();
			this.view.updateCounters(0,this.spinCount.toString());
			this.view.updateCounters(1,this.multpCount.toString());
		}

		private initHandlers():void {
			console.log("initHandlers");
			var items:Array<Button> = this.view.getItems();
			for (var i:number = 0; i < items.length; i++) {
				var item:Button = items[i];
				item.on("click", (eventObj:any)=> {
					if (eventObj.nativeEvent instanceof MouseEvent) {
						console.log("item click");
						this.setEnableAllItem(false);
						var currentItem:Button = <Button>eventObj.currentTarget;
						var itemId = SelectItemView.getItemIdByName(currentItem.name);
						this.sendRequest(itemId);
					}
				});
			}
		}

		public startBonusGame():void{
			console.log("startBonusGame");
			this.view.startBonusGame();
			this.initHandlers();
		}

		private initStartButton():void {
			this.view.changeButtonState(SelectItemView.START_BTN, true, true);
			//if(this.common.isMobile)this.view.setupMobile();
			var startBtn:Button = this.view.getBtn(SelectItemView.START_BTN);
			if(startBtn){
				startBtn.on("click", (eventObj:any)=> {
					if (eventObj.nativeEvent instanceof MouseEvent) {
						this.startBonusGame();
					}
				});
			}
			else {
				this.startBonusGame();
			}
		}

		private removeHandlers():void {
			console.log("removeHandlers");
			var items:Array<Button> = this.view.getItems();
			for (var i:number = 0; i < items.length; i++) {
				items[i].removeAllEventListeners("click");
			}
			this.view.getCollectBtn().removeAllEventListeners("click");
			this.view.getBtn(SelectItemView.START_BTN).removeAllEventListeners("click");
			this.send(NotificationList.SHOW_HEADER);
		}

		public showWinAnimation(vo:ItemVO):void {
			this.view.showWinAnimation(vo, ()=> {
				if (this.isFinish) {
					//this.view.setTotalWin(this.calculateTotalWin());
					this.view.setTotalWin(this.spinCount, this.multpCount);
					this.view.showWinPopup();
					this.view.getCollectBtn().on("click", (eventObj:any)=> {
						if (eventObj.nativeEvent instanceof MouseEvent) {
							this.view.hideWinPopup(()=> {
								this.send(NotificationList.START_SECOND_FREE_GAME);
							})
						}
					});
				}
				else {
					this.setEnableAllItem(true);
				}
			});
		}

		public setEnableAllItem(value:boolean):void {
			var items:Array<Button> = this.view.getItems();
			for (var i:number = 0; i < this.itemsVO.length; i++) {
				if (!this.itemsVO[i].isSelected) {
					items[i].setEnable(value);
				}
			}
		}

		public calculateTotalWin():number {
			var result:number = 0;
			for (var i:number = 0; i < this.itemsVO.length; i++) {
				var itemVO:ItemVO = this.itemsVO[i];
				if (itemVO.isSelected) {
					result += itemVO.winValue;
				}
			}
			return result;
		}

		public onGotResponse(xml:any):void {
			var data:any = XMLUtils.getElement(xml, "data");
			var counter = XMLUtils.getAttributeInt(xml, "counter");
			this.serverCountStep = XMLUtils.getAttributeInt(data, "step");
			if(counter && counter != 0){
				var all:number = XMLUtils.getAttributeInt(xml, "all");
				this.common.server.bonus.step = all - counter;
				this.send(NotificationList.START_SECOND_FREE_GAME);
			}
			else {
				//var wheels:any = XMLUtils.getElement(xml, "wheels");
				var items:Array<any> = XMLUtils.getElement(data, "items");
				var item:Array<any> = XMLUtils.getElements(items, "item");
				for (var i:number = 0; i < item.length; i++) {
					var it:any = item[i];
					var id:number = XMLUtils.getAttributeInt(it, "id") - 1;

					var vo:ItemVO = this.itemsVO[id];
					vo.type = XMLUtils.getAttributeInt(it, "type");
					if (!vo.isSelected) {
						vo.isSelected = XMLUtils.getAttributeInt(it, "selected") == 1;
						vo.winValue = parseFloat(it.textContent);
						if (vo.isSelected) {
							this.showWinAnimation(vo);
							if(vo.type == 4){
								this.spinCount += vo.winValue;
								this.view.updateCounters(0,this.spinCount.toString());
							}
							else {
								this.multpCount += vo.winValue;
								this.view.updateCounters(1,this.multpCount.toString());
							}
						}
					}
				}
				this.isFinish = XMLUtils.getAttributeInt(items, "finishLevel") == 1;

				//this.serverVersion = XMLUtils.getAttributeInt(items, "version");
				//this.serverFinishBonus = XMLUtils.getAttributeInt(items, "finishBonus");
				//this.serverSpins = XMLUtils.getAttributeInt(items, "spins");
				//this.serverSpinsMultiplier = XMLUtils.getAttributeInt(items, "spinsMultiplier");
				//this.serverCoinsMultiplierLevel = XMLUtils.getAttributeInt(items, "coinsMultiplierLevel");
				//this.serverCoinsMultiplierTotal = XMLUtils.getAttributeInt(items, "coinsMultiplierTotal");
				this.serverCountStep = XMLUtils.getAttributeInt(items, "countStep");
			}
			console.log(this.serverCountStep, "serverCountStep");
			if(this.serverCountStep>0 && this.common.restore){
				switch(this.serverCountStep){
					case 1:
						this.common.server.bonus.step = this.serverCountStep;
						this.startBonusGame();
						break;
					case 2:
						//this.view.startBonusGame();
						//this.removeHandlers();
						//this.isFinish = true;
						this.send(NotificationList.START_SECOND_FREE_GAME);
						break;
				}
			}
		}

		public sendRequest(param:number = 0):void {
			super.sendRequest(param);
			this.setEnableAllItem(false);
		}

		public dispose():void {
			super.dispose();
			this.container.removeChild(this.view);
			this.removeHandlers();
			//this.view = null;
			//this.itemsVO = null;

			if(this.common.isMobile) {
				this.setHudButtons(true, false);
			}
		}

		public setHudButtons(visible, enable){
			<HudView>this.common.layouts[HudView.LAYOUT_NAME].changeButtonState(HudView.START_SPIN_BTN, visible, enable);
			//<HudView>this.common.layouts[HudView.LAYOUT_NAME].changeButtonState(HudView.GAMBLE_BTN, visible, enable);
			<HudView>this.common.layouts[HudView.LAYOUT_NAME].changeButtonState(HudView.START_AUTO_PLAY_BTN, visible, enable);
		}
	}
}