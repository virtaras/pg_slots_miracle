module engine {
	import Container = createjs.Container;
	import Button = layout.Button;
	import Ticker = createjs.Ticker;
	import Sound = createjs.Sound;
	import LayoutCreator = layout.LayoutCreator;

	export class FreeSpinsController extends BaseBonusController {
		private static UPDATE_WIN_TIME:number = 0.5;
		private static UPDATE_WIN_TIME_UP:number;
		private static START_POPUP_DELAY:number = 2;

		private view:FreeSpinsView;
		private count:number;
		private leftSpins:number;
		private multiplicator:number;
		private gameWin:number;
		private featureWin:number = 0;
		private featureWinPrev:number = 0;
		private isFist:boolean;
		private isBonusStarted:boolean;
		private isSecondGame:boolean = false;
		private addFSCount:number = 0; //show winpopup if win add spins
		private currentWildReel:number = 2;
		private multiplier:number = 0;

		constructor(manager:ControllerManager, common:CommonRefs, container:Container) {
			super(manager, common, container, 0, true);
		}

		public init():void {
			super.init();
			FreeSpinsController.UPDATE_WIN_TIME_UP = Utils.float2int(FreeSpinsController.UPDATE_WIN_TIME * Ticker.getFPS());
		}

		public create():void {
			super.create();

			this.send(NotificationList.REMOVE_HEADER);

			this.view = <FreeSpinsView>this.common.layouts[FreeSpinsView.LAYOUT_NAME];
			this.view.create();
			this.container.addChild(this.view);
		}

		public listNotification():Array<string> {
			var notifications:Array<string> = super.listNotification();
			notifications.push(NotificationList.START_BONUS);
			notifications.push(NotificationList.STOPPED_ALL_REELS);
			notifications.push(NotificationList.WIN_LINES_SHOWED);
			notifications.push(NotificationList.RECURSION_PROCESSED);
			notifications.push(NotificationList.RECURSION_PROCESS_FALLING);
			return notifications;
		}

		public handleNotification(message:string, data:any):void {
			super.handleNotification(message, data);
			switch (message) {
				case NotificationList.START_BONUS:
				{
					this.isBonusStarted = true;
					this.setLeftSpins();

					if(this.isSecondGame){
						this.isSecondGame = false;
					}
					else {
						if(data == 'spins' || this.common.originBonus == BonusTypes.SELECT_GAME){
							this.removeStartPopup();
							//this.send(NotificationList.SHOW_WILD_REEL, this.currentWildReel);
						}
					}
					console.log(NotificationList.START_BONUS+" this.leftSpins="+this.leftSpins+" this.count="+this.count, "FreeSpinsController");
					if(this.leftSpins < (this.count-1) || data == 'spins' || this.common.originBonus == BonusTypes.SELECT_GAME || this.common.restore) this.send(NotificationList.SHOW_WILD_REEL_FADEIN, this.currentWildReel);
					break;
				}
				case NotificationList.STOPPED_ALL_REELS:
				{
					this.tryShowMoreFreeSpinsPopupForTime();
					if (this.view != null) {
						this.view.setWin(this.featureWin, FreeSpinsController.UPDATE_WIN_TIME_UP);
					}

					this.send(NotificationList.SHOW_WIN_LINES, true);
					this.send(NotificationList.SHOW_WIN_TF);
					break;
				}
				case NotificationList.RECURSION_PROCESSED:
				{
					if (this.isBonusStarted) {
						if (this.leftSpins == 0) {
							this.container.removeChild(this.view);
							this.send(NotificationList.SHOW_HEADER);
							this.showResultPopup();
						}
						else {
							this.setLeftSpins();
							this.send(NotificationList.REMOVE_WIN_LINES);
							this.send(NotificationList.START_SPIN);
							//this.send(NotificationList.START_SPIN_AUDIO);
							this.sendRequest();
						}
					}
					break;
				}
				case NotificationList.RECURSION_PROCESS_FALLING:
				{
					if(this.common.server.recursion.length > 1) {
						this.showNextMultiplier();
					}
					break;
				}
			}
		}

		private setFields():void{
			this.setLeftSpins();
			this.view.setMultiplicator(this.multiplicator);
			this.view.setWin(this.featureWin);
			this.send(NotificationList.SHOW_WIN_TF);
			if(this.leftSpins == (this.count - 1)){
				this.view.setWin(this.featureWinPrev);
			}
		}

		public onGotResponse(data:any):void {
			this.parseResponse(data);
			if (this.isFist && !this.isSecondGame) {
				this.setFields();
				if(!this.common.restore)this.showStartPopup();
			}
			else {
				this.send(NotificationList.SERVER_GOT_SPIN);
			}
			this.common.restore = false;
			this.multiplier = 0;
			this.showNextMultiplier();
		}

		public onEnterFrame():void {
			if (this.view != null) {
				this.view.update();
			}
		}

		private showStartPopup():void {
			var startPopup:FreeSpinsStartView = <FreeSpinsStartView>this.common.layouts[FreeSpinsStartView.LAYOUT_NAME];
			startPopup.create();
			startPopup.alpha = 1;
			startPopup.setFreeSpinsCount(this.count);
			this.container.addChild(startPopup);
		}

		private tryShowMoreFreeSpinsPopupForTime():void{
			if(this.addFSCount>0){
				this.showMorePopupForTime();
				this.addFSCount = 0;
				//this.view.setLeftSpins(this.leftSpins);
			}
		}

		private showMorePopupForTime():void {
			var morePopup:FreeSpinsMoreView = <FreeSpinsMoreView>this.common.layouts[FreeSpinsMoreView.LAYOUT_NAME];
			morePopup.create();
			morePopup.alpha = 1;
			morePopup.setFreeSpinsCount(this.addFSCount);
			this.container.addChild(morePopup);
			morePopup.hide(()=> {this.container.removeChild(morePopup);}, FreeSpinsController.START_POPUP_DELAY);
		}

		private removeStartPopup():void {
			this.send(NotificationList.START_SPIN);
			//this.send(NotificationList.START_SPIN_AUDIO);
			this.send(NotificationList.SERVER_GOT_SPIN);
			var startPopup:FreeSpinsStartView = <FreeSpinsStartView>this.common.layouts[FreeSpinsStartView.LAYOUT_NAME];
			startPopup.hide(()=> {
					this.container.removeChild(startPopup);
				}
			)
		}

		private showResultPopup():void {
			var resultPopup:FreeSpinsResultView = <FreeSpinsResultView>this.common.layouts[FreeSpinsResultView.LAYOUT_NAME];
			resultPopup.create();
			resultPopup.alpha = 1;
			resultPopup.setTextPositions(this.common.isMobile);
			resultPopup.setGameWin(this.gameWin);
			resultPopup.setFeatureWin(this.featureWin);
			var totalWin:number = this.gameWin + this.featureWin;
			resultPopup.setTotalWin(totalWin);

			var collectBtn:Button = resultPopup.getCollectBtn();
			collectBtn.removeAllEventListeners("click");
			collectBtn.on("click", (eventObj:any)=> {
				if (eventObj.nativeEvent instanceof MouseEvent) {
					collectBtn.removeAllEventListeners("click");
					this.hideResultPopup();
				}
			});
			this.container.addChild(resultPopup);
		}

		private hideResultPopup():void{
			var resultView:FreeSpinsResultView = <FreeSpinsResultView>this.common.layouts[FreeSpinsResultView.LAYOUT_NAME];
			resultView.hide(()=> {
					Sound.play(SoundsList.COLLECT);
					this.send(NotificationList.END_BONUS);
					this.send(NotificationList.UPDATE_BALANCE_TF);
					this.container.removeChild(resultView);
				}
			)
		}

		private parseResponse(xml:any):void {
			console.log(this.common.restore, 'restore');
			try {
				var XMLbalance:number = parseFloat(XMLUtils.getElement(xml, "balance").textContent);
				if(XMLbalance){
					this.common.server.setBalance(XMLbalance);
				}
				var bonusnew:any = XMLUtils.getElement(XMLUtils.getElement(XMLUtils.getElement(xml, "data"), "spin"), "bonus");
			}catch (ex){
				//no bonusnew so sad
			}
			if(bonusnew && XMLUtils.getAttributeInt(bonusnew, "id")){
				this.addFSCount = XMLUtils.getAttributeInt(bonusnew, "addFSCount");
				if(this.common.restore) this.tryShowMoreFreeSpinsPopupForTime();
			}
			var data:any = XMLUtils.getElement(xml, "data");
			var items:any = XMLUtils.getElement(data, "items");
			if(items){
				var finishBonus:number = XMLUtils.getAttributeInt(items, "finishBonus");
				if(finishBonus && finishBonus == 1){
					var items:any = XMLUtils.getElement(data, "items");
					this.count = XMLUtils.getAttributeInt(items, "spins");
					this.leftSpins = this.count;
					this.multiplicator = XMLUtils.getAttributeInt(items, "spinsMultiplier");
					this.isFist = true;
					this.gameWin = XMLUtils.getAttributeFloat(xml, "gamewin") || 0;
					this.featureWin = 0;
					this.common.server.bonus.step = 0;
					this.isSecondGame = true;
					this.setFields();
					this.sendRequest(1);
				}
			}
			else {
				this.count = XMLUtils.getAttributeInt(xml, "all");
				this.leftSpins = XMLUtils.getAttributeInt(xml, "counter");
				this.multiplicator = XMLUtils.getAttributeInt(xml, "mp");
				this.gameWin = XMLUtils.getAttributeFloat(xml, "gamewin") || 0;
				this.isFist = (this.common.restore)? true : (this.isSecondGame) ? this.leftSpins == this.count : this.leftSpins == this.count - 1;
				this.featureWin = XMLUtils.getAttributeFloat(data, "summ") || 0;
				var serverWin:any = XMLUtils.getElement(xml, "win");
				this.featureWinPrev = this.featureWin - parseFloat(serverWin.textContent);
				this.parseSpinData(data);
				var data = null;
				if(!this.common.restore && this.leftSpins < (this.count-1)) data = 'spins';
				this.send(NotificationList.START_BONUS, data);
			}
		}

		private parseSpinData(xml:any):void {
			var spinData:any = XMLUtils.getElement(xml, "spin");
			this.parseRecursion(spinData);
			var wheelsData:any = XMLUtils.getElement(spinData, "wheels").childNodes;
			for (var i = 0; i < wheelsData.length; i++) {
				var wheelData:Array<number> = Utils.toIntArray(wheelsData[i].childNodes[0].data.split(","));
				for (var j:number = 0; j < wheelData.length; j++) {
					this.common.server.wheel[i * wheelData.length + j] = wheelData[j];
				}
				if(wheelData[0] == this.common.wildSymbolId) this.currentWildReel = i;
			}
			var winLinesData:any = XMLUtils.getElement(spinData, "winposition").childNodes;
			this.common.server.winLines = [];
			for (var i:number = 0; i < winLinesData.length; i++) {
				var winLine:any = winLinesData[i];
				var winLineVO:WinLineVO = new WinLineVO();
				winLineVO.lineId = XMLUtils.getAttributeInt(winLine, "line");
				winLineVO.win = XMLUtils.getAttributeFloat(winLine, "win");
				winLineVO.winPos = Utils.toIntArray(winLine.childNodes[0].data.split(","));
				this.common.server.winLines.push(winLineVO);
			}
			//TODO: server return 0;
			//this.common.server.setBalance(parseFloat(XMLUtils.getElement(spinData, "balance").textContent));
			this.common.server.win = parseFloat(XMLUtils.getElement(spinData, "win").textContent);
		}

		public parseRecursion(data:any):void{
			this.common.server.recursion = [];
			var recursionData:any = XMLUtils.getElement(data, "recursion");
			if(recursionData) {
				var recursionDataArr:any = recursionData.childNodes;
				for (var i:number = 0; i < recursionDataArr.length; i++) {
					var wheelData:Array<number> = eval(recursionDataArr[i].attributes[0].nodeValue);
					this.common.server.recursion[i] = [];
					this.common.server.recursion[i]["wheels"] = wheelData;
					this.common.server.recursion[i]["win"] = XMLUtils.getAttributeInt(recursionDataArr[i], "win");

					this.common.server.recursion[i]["winLines"] = [];
					this.common.server.recursion[i]["arrLinesIds"] = [];
					var winLinesData:any = XMLUtils.getElement(recursionDataArr[i], "winposition").childNodes;
					for (var j:number = 0; j < winLinesData.length; ++j) {
						var winLine:any = winLinesData[j];
						var winLineVO:WinLineVO = new WinLineVO();
						winLineVO.lineId = XMLUtils.getAttributeInt(winLine, "line");
						winLineVO.win = XMLUtils.getAttributeFloat(winLine, "win");
						winLineVO.winPos = Utils.toIntArray(winLine.childNodes[0].data.split(","));
						this.common.server.recursion[i]["winLines"].push(winLineVO);
						this.common.server.recursion[i]["arrLinesIds"].push(winLineVO.lineId);
					}
				}
			}
			this.common.server.recursion_len = this.common.server.recursion.length;
			//console.log("this.common.server.recursion parseRecursion FreeSpinsController", this.common.server.recursion);
		}

		public setLeftSpins():void{
			if(!this.addFSCount) {
				console.log("this.addFSCount="+this.addFSCount+" START_BONUS");
				this.view.setLeftSpins(this.leftSpins);
			}
		}

		public showNextMultiplier():void{
			this.view.showMultiplierAnimation(++this.multiplier);
		}
	}
}