module engine {
	import Layout = layout.Layout;
	import Text = createjs.Text;
	import Tween = createjs.Tween;
	import Ticker = createjs.Ticker;

	export class FreeSpinsStartView extends Layout {
		public static LAYOUT_NAME:string = "FreeSpinsStartView";

		public static FREE_SPINS_COUNT:string = "freeSpinsCountTf";
		private static INVISIBLE_TIME:number = 0.5;
		private static INVISIBLE_TIME_UP:number;

		constructor() {
			super();
		}

		public onInit():void {
			FreeSpinsStartView.INVISIBLE_TIME_UP = Utils.float2int(FreeSpinsStartView.INVISIBLE_TIME * Ticker.getFPS());
		}

		public setFreeSpinsCount(value:number):void {
			var tf:Text = <Text>this.getChildByName(FreeSpinsStartView.FREE_SPINS_COUNT);
			if (tf != null) {
				tf.text = value.toString();
			}
		}

		public hide(callback:(...params:any[]) => any):void {
			var tween:Tween = Tween.get(this, {useTicks: true});
			tween.to({alpha: 0}, FreeSpinsStartView.INVISIBLE_TIME_UP);
			tween.call(callback);
		}
	}
}