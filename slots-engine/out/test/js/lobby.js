var lobby;
(function (lobby) {
    var LobbyConst = (function () {
        function LobbyConst() {
        }
        LobbyConst.MAIN_RES_FOLDER = "assets/";
        LobbyConst.LOBBY_RES_FOLDER = "lobby/";
        LobbyConst.ICONS_FOLDER = "icons/";
        LobbyConst.LAYOUT_FOLDER = "layout/";
        LobbyConst.FONTS_FOLDER = "fonts/";

        LobbyConst.GAME_CONFIG_NAME = "config";

        LobbyConst.FPS = 30;
        return LobbyConst;
    })();
    lobby.LobbyConst = LobbyConst;
})(lobby || (lobby = {}));
var lobby;
(function (lobby) {
    var FileConst = (function () {
        function FileConst() {
        }
        FileConst.JSON_EXTENSION = ".json";
        FileConst.LAYOUT_EXTENSION = ".layout";
        FileConst.WOFF_EXTENSION = ".woff";
        return FileConst;
    })();
    lobby.FileConst = FileConst;
})(lobby || (lobby = {}));
var lobby;
(function (lobby) {
    var NotificationList = (function () {
        function NotificationList() {
        }
        NotificationList.RES_LOADED = "res_loaded";
        NotificationList.AUTHORIZATION = "authorization";
        NotificationList.DISCONNECT = "disconnect";
        NotificationList.AUTHORIZED = "authorized";
        NotificationList.LOGOUT = "logout";
        NotificationList.START_GAME = "start_game";
        return NotificationList;
    })();
    lobby.NotificationList = NotificationList;
})(lobby || (lobby = {}));
var lobby;
(function (lobby) {
    var CommonRefs = (function () {
        function CommonRefs() {
        }
        return CommonRefs;
    })();
    lobby.CommonRefs = CommonRefs;
})(lobby || (lobby = {}));
var lobby;
(function (lobby) {
    var AjaxRequest = (function () {
        function AjaxRequest(url, isAsynchronous, params) {
            this.url = url + AjaxRequest.paramsToString(params);
            this.isAsynchronous = isAsynchronous;
        }
        AjaxRequest.paramsToString = function (params) {
            var paramsStr = "";
            for (var key in params) {
                paramsStr += paramsStr.length == 0 ? "?" : "&";
                paramsStr += key + "=" + params[key];
            }
            return paramsStr;
        };

        AjaxRequest.getXmlHttp = function () {
            if (window.hasOwnProperty("XMLHttpRequest")) {
                // code for IE7+, Firefox, Chrome, Opera, Safari
                return new XMLHttpRequest();
            } else if (window.hasOwnProperty("ActiveXObject")) {
                // code for IE6, IE5
                return new ActiveXObject("Microsoft.XMLHTTP");
            }
        };

        /**
        * Send GET request
        */
        AjaxRequest.prototype.get = function (handler) {
            var _this = this;
            if (typeof handler === "undefined") { handler = null; }
            this.handler = handler;
            this.xmlHttp = AjaxRequest.getXmlHttp();
            this.xmlHttp.onreadystatechange = function () {
                _this.onComplete();
            };
            this.xmlHttp.open("GET", this.url, this.isAsynchronous);
            this.xmlHttp.send(null);
        };

        /**
        * Request is ready
        */
        AjaxRequest.prototype.onComplete = function () {
            if (this.xmlHttp.readyState == XMLHttpRequest.DONE) {
                // success
                if (this.xmlHttp.status == 200) {
                    // Registration : in the registration process xmlRootNode return current status that user registered or not.
                    var responseData = this.xmlHttp.responseXML;
                    if (responseData == null) {
                        throw new Error("ERROR: Server response null");
                    }
                    if (this.handler != null) {
                        this.handler(responseData);
                    }
                } else {
                    throw new Error("ERROR: There was a problem retrieving the data: " + this.xmlHttp.statusText);
                }
            }
        };
        return AjaxRequest;
    })();
    lobby.AjaxRequest = AjaxRequest;
})(lobby || (lobby = {}));
var lobby;
(function (lobby) {
    var Utils = (function () {
        function Utils() {
        }
        Utils.getClassName = function (obj) {
            var funcNameRegex = /function (.{1,})\(/;
            var results = (funcNameRegex).exec(obj["constructor"].toString());
            return (results && results.length > 1) ? results[1] : "";
        };
        return Utils;
    })();
    lobby.Utils = Utils;
})(lobby || (lobby = {}));
var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var lobby;
(function (lobby) {
    var Layout = layout.Layout;

    var LoginView = (function (_super) {
        __extends(LoginView, _super);
        function LoginView() {
            _super.call(this);
        }
        LoginView.prototype.onInit = function () {
            this.keyboard = this.getChildByName(LoginView.KEYBOARD);
            this.inputTf = this.keyboard.getChildByName(LoginView.INPUT_TEXT);
        };

        LoginView.prototype.getKeyboard = function () {
            return this.keyboard;
        };

        LoginView.prototype.getInputTf = function () {
            return this.inputTf;
        };

        LoginView.prototype.dispose = function () {
            this.parent.removeChild(this);
        };
        LoginView.LAYOUT_NAME = "LoginView";

        LoginView.KEYBOARD = "keyboard";
        LoginView.INPUT_TEXT = "inputTf";

        LoginView.ZERO_BTN = "zeroBtn";
        LoginView.ONE_BTN = "oneBtn";
        LoginView.TWO_BTN = "twoBtn";
        LoginView.THREE_BTN = "threeBtn";
        LoginView.FOUR_BTN = "fourBtn";
        LoginView.FIVE_BTN = "fiveBtn";
        LoginView.SIX_BTN = "sixBtn";
        LoginView.SEVEN_BTN = "sevenBtn";
        LoginView.EIGHT_BTN = "eightBtn";
        LoginView.NINE_BTN = "nineBtn";

        LoginView.NUM_BUTTONS = [
            LoginView.ZERO_BTN,
            LoginView.ONE_BTN,
            LoginView.TWO_BTN,
            LoginView.THREE_BTN,
            LoginView.FOUR_BTN,
            LoginView.FIVE_BTN,
            LoginView.SIX_BTN,
            LoginView.SEVEN_BTN,
            LoginView.EIGHT_BTN,
            LoginView.NINE_BTN
        ];

        LoginView.OK_BTN = "okBtn";
        LoginView.CANCEL_BTN = "cancelBtn";
        return LoginView;
    })(Layout);
    lobby.LoginView = LoginView;
})(lobby || (lobby = {}));
var lobby;
(function (lobby) {
    var Layout = layout.Layout;

    var Shape = createjs.Shape;

    var Ease = createjs.Ease;
    var Tween = createjs.Tween;
    var Bitmap = createjs.Bitmap;
    var LoadQueue = createjs.LoadQueue;

    var SelectGameView = (function (_super) {
        __extends(SelectGameView, _super);
        function SelectGameView() {
            _super.call(this);
            this.gameIcons = [];
        }
        SelectGameView.prototype.onInit = function () {
            this.content = this.getChildByName(SelectGameView.GAME_ICONS_CONTENT);
            this.createMask();
            this.initIcons();
            this.initButtons();
        };

        SelectGameView.prototype.createMask = function () {
            var bounds = this.content.getBounds();
            var mask = new Shape();

            // TODO: hack bounds.height * 2
            mask.graphics.beginFill("0").drawRect(this.content.x + bounds.x, this.content.y + bounds.y, bounds.width, bounds.height * 2);
            this.content.mask = mask;
        };

        SelectGameView.prototype.initButtons = function () {
            this.prevPageBtn = this.getChildByName(SelectGameView.PREV_PAGE_BTN);
            this.nextPageBtn = this.getChildByName(SelectGameView.NEXT_PAGE_BTN);
            this.backBtn = this.getChildByName(SelectGameView.BACK_BTN);
        };

        SelectGameView.prototype.initIcons = function () {
            var gameId = 1;
            var gameIcon;
            while ((gameIcon = this.content.getChildByName(SelectGameView.GAME_ICON_PREFIX + gameId)) != null) {
                this.gameIcons.push(gameIcon);
                gameId++;
            }
            this.iconsDiv = this.gameIcons[1].x - this.gameIcons[0].x - this.gameIcons[0].getBounds().width;
        };

        SelectGameView.prototype.tweenPage = function (isNextPage) {
            var _this = this;
            var cloneContent = this.content.clone(true);
            cloneContent.x = this.content.x;
            cloneContent.y = this.content.y;
            cloneContent.mask = this.content.mask;
            this.addChild(cloneContent);

            var div = (this.content.getBounds().width + this.iconsDiv) * (isNextPage ? -1 : 1);
            this.content.x -= div;
            Tween.get(this.content, { useTicks: true }).to({ x: this.content.x + div }, SelectGameView.CHANGE_PAGE_TIME * lobby.LobbyConst.FPS, Ease.backOut);
            Tween.get(cloneContent, { useTicks: true }).to({ x: cloneContent.x + div }, SelectGameView.CHANGE_PAGE_TIME * lobby.LobbyConst.FPS, Ease.backOut).call(function () {
                _this.removeChild(cloneContent);
            });
        };

        SelectGameView.prototype.updatePage = function (gamesParams, pageId) {
            var _this = this;
            var gameIcons = this.gameIcons;
            var gamesCount = gamesParams.length;
            var gameIconsCount = this.gameIcons.length;
            var manifest = [];
            var j = pageId * gameIconsCount;
            for (var i = 0; i < gameIconsCount; i++) {
                var gameId = j + i;
                var gameIcon = gameIcons[i];
                if (gameId < gamesCount) {
                    var gameParams = gamesParams[gameId];
                    var label = gameIcon.getChildByName(SelectGameView.GAME_ICON_LABEL);
                    label.text = gameParams.label;

                    // remove old icon
                    var icon = gameIcon.getChildByName(SelectGameView.GAME_ICON_NAME);
                    if (icon != null) {
                        gameIcon.removeChild(icon);
                    }
                    var iconUrl = lobby.LobbyConst.MAIN_RES_FOLDER + lobby.LobbyConst.LOBBY_RES_FOLDER + lobby.LobbyConst.ICONS_FOLDER + gameParams.iconUrl;
                    manifest.push({ id: gameIcon.name, src: iconUrl });

                    gameIcon.alpha = 1;
                } else {
                    gameIcon.alpha = 0;
                }
            }

            // start load new icon
            var queue = new LoadQueue(true);
            queue.on("fileload", function (eventData) {
                var iconName = eventData.item.id;
                var result = eventData.result;
                var icon = new Bitmap(result);
                var gameIcon = _this.content.getChildByName(iconName);
                var bounds = icon.getBounds();
                icon.regX = bounds.width >> 1;
                icon.regY = bounds.height >> 1;
                gameIcon.addChild(icon);
            });
            queue.loadManifest(manifest);

            this.nextPageBtn.setEnable(gamesCount > (pageId + 1) * gameIconsCount);
            this.prevPageBtn.setEnable(pageId > 0);
        };

        SelectGameView.prototype.getIcons = function () {
            return this.gameIcons;
        };

        SelectGameView.prototype.getPrevPageBtn = function () {
            return this.prevPageBtn;
        };

        SelectGameView.prototype.getNextPageBtn = function () {
            return this.nextPageBtn;
        };

        SelectGameView.prototype.getBackBtn = function () {
            return this.backBtn;
        };

        SelectGameView.prototype.dispose = function () {
            this.parent.removeChild(this);
        };
        SelectGameView.LAYOUT_NAME = "SelectGameView";

        SelectGameView.CHANGE_PAGE_TIME = 0.3;

        SelectGameView.PREV_PAGE_BTN = "prevPageBtn";
        SelectGameView.NEXT_PAGE_BTN = "nextPageBtn";
        SelectGameView.BACK_BTN = "backBtn";
        SelectGameView.GAME_ICONS_CONTENT = "content";
        SelectGameView.GAME_ICON_PREFIX = "icon_";
        SelectGameView.GAME_ICON_LABEL = "label";
        SelectGameView.GAME_ICON_NAME = "icon";
        return SelectGameView;
    })(Layout);
    lobby.SelectGameView = SelectGameView;
})(lobby || (lobby = {}));
var lobby;
(function (lobby) {
    var ControllerManager = (function () {
        function ControllerManager() {
            this.observerMap = {};
            this.controllers = [];
        }
        ControllerManager.prototype.register = function (controller) {
            var notificationsNames = controller.listNotification();
            for (var i = 0; i < notificationsNames.length; i++) {
                this.registerObserver(notificationsNames[i], controller);
            }
            this.controllers.push(controller);
        };

        ControllerManager.prototype.remove = function (controller) {
            var notificationsNames = controller.listNotification();
            for (var i = 0; i < notificationsNames.length; i++) {
                this.removeObserver(notificationsNames[i], controller);
            }
            var index = this.controllers.indexOf(controller);
            this.controllers.splice(index, 1);
        };

        ControllerManager.prototype.registerObserver = function (notificationName, controller) {
            if (this.observerMap[notificationName] != null) {
                this.observerMap[notificationName].push(controller);
            } else {
                this.observerMap[notificationName] = [controller];
            }
        };

        ControllerManager.prototype.removeObserver = function (notificationName, controller) {
            var controllers = this.observerMap[notificationName];
            for (var i = 0; i < controllers.length; i++) {
                if (controllers[i] == controller) {
                    controllers.splice(i, 1);
                }
            }
        };

        ControllerManager.prototype.send = function (notificationName, data) {
            var controllers = this.observerMap[notificationName];
            if (controllers != null) {
                for (var i = 0; i < controllers.length; i++) {
                    controllers[i].handleNotification(notificationName, data);
                }
            }
        };

        ControllerManager.prototype.onEnterFrame = function () {
            for (var i = 0; i < this.controllers.length; i++) {
                this.controllers[i].onEnterFrame();
            }
        };
        return ControllerManager;
    })();
    lobby.ControllerManager = ControllerManager;
})(lobby || (lobby = {}));
var lobby;
(function (lobby) {
    var BaseController = (function () {
        function BaseController(manager) {
            this.manager = manager;
        }
        BaseController.prototype.init = function () {
            this.manager.register(this);
        };

        BaseController.prototype.listNotification = function () {
            return [];
        };

        BaseController.prototype.handleNotification = function (message, data) {
        };

        BaseController.prototype.send = function (message, data) {
            if (typeof data === "undefined") { data = null; }
            this.manager.send(message, data);
        };

        BaseController.prototype.dispose = function () {
            console.log("Dispose controller: " + lobby.Utils.getClassName(this));
            this.manager.remove(this);
        };

        BaseController.prototype.onEnterFrame = function () {
        };
        return BaseController;
    })();
    lobby.BaseController = BaseController;
})(lobby || (lobby = {}));
var lobby;
(function (lobby) {
    var LoadQueue = createjs.LoadQueue;
    var LayoutCreator = layout.LayoutCreator;

    var LoaderController = (function (_super) {
        __extends(LoaderController, _super);
        function LoaderController(manager, common) {
            _super.call(this, manager);
            this.common = common;
        }
        LoaderController.prototype.init = function () {
            _super.prototype.init.call(this);
            this.loadConfig();
        };

        LoaderController.prototype.loadConfig = function () {
            var _this = this;
            var configLoader = new LoadQueue(false);
            var configUrl = lobby.LobbyConst.MAIN_RES_FOLDER + lobby.LobbyConst.LOBBY_RES_FOLDER + lobby.LobbyConst.GAME_CONFIG_NAME + lobby.FileConst.JSON_EXTENSION;
            configLoader.on("complete", function () {
                _this.common.config = configLoader.getResult(configUrl);
                _this.loadFonts();
            });
            configLoader.loadFile(configUrl);
        };

        LoaderController.prototype.loadFonts = function () {
            var _this = this;
            var fonts = this.common.config.fonts;
            if (fonts != null && fonts.length > 0) {
                var style = document.createElement('style');
                var manifest = [];

                for (var i = 0; i < fonts.length; i++) {
                    var fontName = fonts[i].name;
                    var fontURL = lobby.LobbyConst.MAIN_RES_FOLDER + lobby.LobbyConst.LOBBY_RES_FOLDER + lobby.LobbyConst.FONTS_FOLDER + fonts[i].fileName + lobby.FileConst.WOFF_EXTENSION;
                    style.appendChild(document.createTextNode("@font-face {font-family: '" + fontName + "'; src: url('" + fontURL + "');}"));
                    document.head.appendChild(style);
                    manifest.push({ "id": fontName, "src": fontURL });
                }

                var configLoader = new LoadQueue(false);
                configLoader.on("complete", function () {
                    _this.loadLayouts();
                });
                configLoader.loadManifest(manifest);
            } else {
                this.loadLayouts();
            }
        };

        LoaderController.prototype.loadLayouts = function () {
            var _this = this;
            var layouts = {};
            var layoutNames = this.common.config.layouts;
            for (var i = 0; i < layoutNames.length; i++) {
                var layoutName = layoutNames[i];
                var LayoutClass = lobby[layoutName];
                layouts[layoutName] = LayoutClass ? new LayoutClass() : new LayoutCreator();
            }

            var needLoad = layoutNames.length;
            var leftLoad = needLoad;
            for (var i = 0; i < layoutNames.length; i++) {
                var layoutName = layoutNames[i];
                var layoutObj = layouts[layoutName];
                layoutObj.on(LayoutCreator.EVENT_LOADED, function () {
                    leftLoad -= 1;
                    var progress = (needLoad - leftLoad) / needLoad;
                    console.log("Loading progress: " + progress);
                    if (leftLoad == 0) {
                        _super.prototype.send.call(_this, lobby.NotificationList.RES_LOADED);
                    } else {
                        // TODO: update progress bar
                    }
                });
                layoutObj.load(lobby.LobbyConst.MAIN_RES_FOLDER + lobby.LobbyConst.LOBBY_RES_FOLDER + "/" + lobby.LobbyConst.LAYOUT_FOLDER + layoutName + lobby.FileConst.LAYOUT_EXTENSION);
            }
            this.common.layouts = layouts;
        };
        return LoaderController;
    })(lobby.BaseController);
    lobby.LoaderController = LoaderController;
})(lobby || (lobby = {}));
var lobby;
(function (lobby) {
    var Stage = createjs.Stage;
    var Ticker = createjs.Ticker;
    var Container = createjs.Container;

    var LobbyController = (function (_super) {
        __extends(LobbyController, _super);
        function LobbyController(artWidth, artHeight, canvasHolderName) {
            _super.call(this, new lobby.ControllerManager());
            this.common = new lobby.CommonRefs();
            this.common.canvasHolderName = canvasHolderName;
            this.common.artWidth = artWidth;
            this.common.artHeight = artHeight;
        }
        LobbyController.prototype.start = function () {
            var _this = this;
            _super.prototype.init.call(this);

            var canvas = document.createElement("canvas");

            // if the method is not supported, i.e canvas is not supported
            if (!canvas.getContext) {
                document.write("Your browser does not support the HTML5 canvas element.");
                return;
            }
            canvas.style.position = "absolute";

            var canvasHolder = document.getElementById(this.common.canvasHolderName);
            canvasHolder.appendChild(canvas);

            this.stage = new Stage(canvas);
            this.stage.enableMouseOver(lobby.LobbyConst.FPS);

            this.lobbyContainer = new Container();
            this.stage.addChild(this.lobbyContainer);
            this.gameContainer = new Container();
            this.stage.addChild(this.gameContainer);

            screen.fontSmoothingEnabled = true;

            Ticker.setFPS(lobby.LobbyConst.FPS);
            Ticker.addEventListener("tick", function () {
                _this.manager.onEnterFrame();
            });

            this.onResize();
            window.onresize = function () {
                _this.onResize();
            };

            this.startLoading();
        };

        LobbyController.prototype.onEnterFrame = function () {
            this.stage.update();
        };

        LobbyController.prototype.listNotification = function () {
            var notifications = _super.prototype.listNotification.call(this);
            notifications.push(lobby.NotificationList.RES_LOADED);
            return notifications;
        };

        LobbyController.prototype.handleNotification = function (message, data) {
            switch (message) {
                case lobby.NotificationList.RES_LOADED: {
                    this.onLoaded();
                    break;
                }
            }
        };

        LobbyController.prototype.dispose = function () {
            this.send(lobby.NotificationList.DISCONNECT);
        };

        LobbyController.prototype.startLoading = function () {
            var serverController = new lobby.ServerController(this.manager, this.common);
            serverController.init();

            var loaderController = new lobby.LoaderController(this.manager, this.common);
            loaderController.init();
        };

        LobbyController.prototype.onLoaded = function () {
            var loginController = new lobby.LoginController(this.manager, this.common, this.lobbyContainer);
            loginController.init();

            var selectGameController = new lobby.SelectGameController(this.manager, this.common, this.lobbyContainer);
            selectGameController.init();

            var gameController = new lobby.GameController(this.manager, this.common, this.gameContainer);
            gameController.init();
        };

        LobbyController.prototype.onResize = function () {
            var maxWidth = window.innerWidth;
            var maxHeight = window.innerHeight;

            // keep aspect ratio
            var scaleLobby = Math.min(maxWidth / this.common.artWidth, maxHeight / this.common.artHeight);
            this.lobbyContainer.scaleX = scaleLobby;
            this.lobbyContainer.scaleY = scaleLobby;
            this.lobbyContainer.x = (maxWidth - this.common.artWidth * scaleLobby) / 2;
            this.lobbyContainer.y = (maxHeight - this.common.artHeight * scaleLobby) / 2;

            if (this.common.currentGame != null) {
                var scaleGame = Math.min(maxWidth / this.common.currentGame.artWidth, maxHeight / this.common.currentGame.artHeight);
                this.gameContainer.scaleX = scaleGame;
                this.gameContainer.scaleY = scaleGame;
                this.gameContainer.x = (maxWidth - this.common.currentGame.artWidth * scaleGame) / 2;
                this.gameContainer.y = (maxHeight - this.common.currentGame.artHeight * scaleGame) / 2;
            }

            // adjust canvas size
            var canvas = this.stage.canvas;
            canvas.width = maxWidth;
            canvas.height = maxHeight;
        };
        return LobbyController;
    })(lobby.BaseController);
    lobby.LobbyController = LobbyController;
})(lobby || (lobby = {}));
var lobby;
(function (lobby) {
    var LoginController = (function (_super) {
        __extends(LoginController, _super);
        function LoginController(manager, common, container) {
            _super.call(this, manager);
            this.common = common;
            this.container = container;
        }
        LoginController.prototype.init = function () {
            _super.prototype.init.call(this);
            this.view = this.common.layouts[lobby.LoginView.LAYOUT_NAME];
            this.view.create();
            this.initHandlers();
            this.show();
        };

        LoginController.prototype.listNotification = function () {
            var notifications = _super.prototype.listNotification.call(this);
            notifications.push(lobby.NotificationList.LOGOUT);
            notifications.push(lobby.NotificationList.AUTHORIZATION);
            return notifications;
        };

        LoginController.prototype.handleNotification = function (message, data) {
            switch (message) {
                case lobby.NotificationList.LOGOUT: {
                    this.show();
                    break;
                }
                case lobby.NotificationList.AUTHORIZATION: {
                    this.remove();
                    break;
                }
            }
        };

        LoginController.prototype.show = function () {
            this.state = LoginController.LOGIN_STATE;
            this.view.getInputTf().text = LoginController.LOGIN_STATE_TEXT;
            this.container.addChild(this.view);
        };

        LoginController.prototype.remove = function () {
            this.view.dispose();
        };

        LoginController.prototype.initHandlers = function () {
            var _this = this;
            for (var i = 0; i < lobby.LoginView.NUM_BUTTONS.length; i++) {
                var button = this.view.getKeyboard().getChildByName(lobby.LoginView.NUM_BUTTONS[i]);
                button.on("click", function (eventData) {
                    var value = _this.view.getInputTf().text;
                    if (value == LoginController.LOGIN_STATE_TEXT || value == LoginController.PASSWORD_STATE_TEXT) {
                        value = "";
                    }
                    var currentBtn = eventData.currentTarget;
                    var buttonName = currentBtn.name;
                    _this.view.getInputTf().text = value + lobby.LoginView.NUM_BUTTONS.indexOf(buttonName);
                });
            }

            var okBtn = this.view.getKeyboard().getChildByName(lobby.LoginView.OK_BTN);
            okBtn.on("click", function (eventData) {
                var value = _this.view.getInputTf().text;
                if (value != LoginController.LOGIN_STATE_TEXT && value != LoginController.PASSWORD_STATE_TEXT && value.length > 0) {
                    _this.onEnterClick();
                }
            });

            var cancelBtn = this.view.getKeyboard().getChildByName(lobby.LoginView.CANCEL_BTN);
            cancelBtn.on("click", function (eventData) {
                var value = _this.view.getInputTf().text;
                if (value != LoginController.LOGIN_STATE_TEXT && value != LoginController.PASSWORD_STATE_TEXT) {
                    _this.view.getInputTf().text = value.substr(0, value.length - 1);
                }
            });
        };

        LoginController.prototype.onEnterClick = function () {
            switch (this.state) {
                case LoginController.LOGIN_STATE: {
                    this.common.login = this.view.getInputTf().text;
                    this.view.getInputTf().text = LoginController.PASSWORD_STATE_TEXT;
                    this.state = LoginController.PASSWORD_STATE;
                    break;
                }
                case LoginController.PASSWORD_STATE: {
                    this.common.password = this.view.getInputTf().text;
                    _super.prototype.send.call(this, lobby.NotificationList.AUTHORIZATION);
                    break;
                }
            }
        };
        LoginController.LOGIN_STATE = 1;
        LoginController.PASSWORD_STATE = 2;

        LoginController.LOGIN_STATE_TEXT = "login";
        LoginController.PASSWORD_STATE_TEXT = "password";
        return LoginController;
    })(lobby.BaseController);
    lobby.LoginController = LoginController;
})(lobby || (lobby = {}));
var lobby;
(function (lobby) {
    var SelectGameController = (function (_super) {
        __extends(SelectGameController, _super);
        function SelectGameController(manager, common, container) {
            _super.call(this, manager);
            this.common = common;
            this.container = container;
            this.pageId = 0;
        }
        SelectGameController.prototype.init = function () {
            var _this = this;
            _super.prototype.init.call(this);
            this.view = this.common.layouts[lobby.SelectGameView.LAYOUT_NAME];
            this.view.create();

            this.view.getNextPageBtn().on("click", function () {
                _this.pageId++;
                _this.view.tweenPage(true);
                _this.view.updatePage(_this.common.config.games, _this.pageId);
            });
            this.view.getPrevPageBtn().on("click", function () {
                _this.pageId--;
                _this.view.tweenPage(false);
                _this.view.updatePage(_this.common.config.games, _this.pageId);
            });
            this.view.getBackBtn().on("click", function () {
                _this.remove();
                _super.prototype.send.call(_this, lobby.NotificationList.LOGOUT);
            });
            var gameIcons = this.view.getIcons();
            for (var i = 0; i < gameIcons.length; i++) {
                var gameIcon = gameIcons[i];
                gameIcon.on("click", function (eventData) {
                    _this.onSelectGame(eventData);
                });
                gameIcon.on("rollover", function () {
                    document.body.style.cursor = "pointer";
                });
                gameIcon.on("rollout", function () {
                    document.body.style.cursor = "default";
                });
            }
            this.view.updatePage(this.common.config.games, this.pageId);
        };

        SelectGameController.prototype.listNotification = function () {
            var notifications = _super.prototype.listNotification.call(this);
            notifications.push(lobby.NotificationList.AUTHORIZED);
            notifications.push(lobby.NotificationList.START_GAME);
            return notifications;
        };

        SelectGameController.prototype.handleNotification = function (message, data) {
            switch (message) {
                case lobby.NotificationList.AUTHORIZED: {
                    this.show();
                    break;
                }
                case lobby.NotificationList.START_GAME: {
                    this.remove();
                    break;
                }
            }
        };

        SelectGameController.prototype.show = function () {
            this.container.addChild(this.view);
        };

        SelectGameController.prototype.remove = function () {
            this.view.dispose();
        };

        SelectGameController.prototype.onSelectGame = function (eventData) {
            var targetGameIcon = eventData.currentTarget;
            var gameIcons = this.view.getIcons();
            var iconId = parseInt(targetGameIcon.name.substr(lobby.SelectGameView.GAME_ICON_PREFIX.length));
            var gameId = this.pageId * gameIcons.length + iconId - 1;
            this.common.currentGame = this.common.config.games[gameId];
            _super.prototype.send.call(this, lobby.NotificationList.START_GAME);
        };
        return SelectGameController;
    })(lobby.BaseController);
    lobby.SelectGameController = SelectGameController;
})(lobby || (lobby = {}));
var lobby;
(function (lobby) {
    var GameController = (function (_super) {
        __extends(GameController, _super);
        function GameController(manager, common, gameContainer) {
            _super.call(this, manager);
            this.common = common;
            this.gameContainer = gameContainer;
        }
        GameController.prototype.init = function () {
            _super.prototype.init.call(this);
        };

        GameController.prototype.listNotification = function () {
            var notifications = _super.prototype.listNotification.call(this);
            notifications.push(lobby.NotificationList.START_GAME);
            return notifications;
        };

        GameController.prototype.handleNotification = function (message, data) {
            switch (message) {
                case lobby.NotificationList.START_GAME: {
                    this.startGame();
                    break;
                }
            }
        };

        GameController.prototype.onEnterFrame = function () {
            if (this.gameController != null) {
                this.gameController.gameUpdate();
            }
        };

        GameController.prototype.startGame = function () {
            var _this = this;
            window.onresize(null);
            this.gameController = new engine.GameController();
            this.gameContainer.on(engine.GameController.EVENT_BACK_TO_LOBBY, function () {
                _this.closeGame();
            });
            this.gameController.start(this.common.currentGame.id, this.common.key, this.gameContainer);
        };

        GameController.prototype.closeGame = function () {
            while (this.gameContainer.getNumChildren() > 0) {
                this.gameContainer.removeChildAt(0);
            }
            this.gameController = null;
            this.common.currentGame = null;
            this.send(lobby.NotificationList.AUTHORIZED);
        };

        GameController.prototype.dispose = function () {
            _super.prototype.dispose.call(this);
            this.gameController.dispose();
        };
        return GameController;
    })(lobby.BaseController);
    lobby.GameController = GameController;
})(lobby || (lobby = {}));
var lobby;
(function (lobby) {
    var ServerController = (function (_super) {
        __extends(ServerController, _super);
        function ServerController(manager, common) {
            _super.call(this, manager);
            this.authorizationUrl = "onlinecasino/common.asmx/Authorization";
            this.closeSessionUrl = "onlinecasino/common.asmx/Close";
            this.getBalanceUrl = "onlinecasino/common.asmx/GetBalance";
            this.common = common;
        }
        ServerController.prototype.init = function () {
            _super.prototype.init.call(this);
        };

        ServerController.prototype.listNotification = function () {
            var notifications = _super.prototype.listNotification.call(this);
            notifications.push(lobby.NotificationList.RES_LOADED);
            notifications.push(lobby.NotificationList.AUTHORIZATION);
            notifications.push(lobby.NotificationList.LOGOUT);
            notifications.push(lobby.NotificationList.DISCONNECT);
            return notifications;
        };

        ServerController.prototype.handleNotification = function (message, data) {
            var _this = this;
            switch (message) {
                case lobby.NotificationList.RES_LOADED: {
                    var baseServerUrl = this.common.config.serverUrl;
                    this.authorizationUrl = (baseServerUrl + this.authorizationUrl).toLowerCase();
                    this.getBalanceUrl = (baseServerUrl + this.getBalanceUrl).toLowerCase();
                    this.closeSessionUrl = (baseServerUrl + this.closeSessionUrl).toLowerCase();
                    break;
                }
                case lobby.NotificationList.AUTHORIZATION: {
                    this.sendLoginRequest(this.common.login, this.common.password, function () {
                        _this.send(lobby.NotificationList.AUTHORIZED);
                    });
                    break;
                }
                case lobby.NotificationList.LOGOUT:
                case lobby.NotificationList.DISCONNECT: {
                    this.sendDisconnectRequest();
                    break;
                }
            }
        };

        ServerController.prototype.sendLoginRequest = function (login, password, onComplete) {
            var _this = this;
            var passwordMD5 = CryptoJS.MD5(password).toString();
            var request = new lobby.AjaxRequest(this.authorizationUrl, true, { "login": login, "password": passwordMD5 });
            request.get(function (responseData) {
                var result = responseData.documentElement.textContent;
                switch (result) {
                    case "1001": {
                        console.log("ERROR: User already authorized!");
                        _super.prototype.send.call(_this, lobby.NotificationList.LOGOUT);
                        return;
                    }
                }
                if (result == null || result.length == 0) {
                    _super.prototype.send.call(_this, lobby.NotificationList.LOGOUT);
                    return;
                }
                _this.common.key = result;
                console.log("Key: " + result);
                onComplete();
            });
        };

        ServerController.prototype.sendDisconnectRequest = function () {
            if (this.common.key != null) {
                var request = new lobby.AjaxRequest(this.closeSessionUrl, false, { key: this.common.key });
                request.get();
            }
        };
        return ServerController;
    })(lobby.BaseController);
    lobby.ServerController = ServerController;
})(lobby || (lobby = {}));
///<reference path="../../dts/createjs.d.ts" />
///<reference path="../../dts/cryptojs.d.ts" />
///<reference path="../../dts/easeljs.d.ts" />
///<reference path="../../dts/layoutjs.d.ts" />
///<reference path="../../dts/preloadjs.d.ts" />
///<reference path="lobby/const/LobbyConst.ts" />
///<reference path="lobby/const/FileConst.ts" />
///<reference path="lobby/const/NotificationList.ts" />
///<reference path="lobby/model/CommonRefs.ts" />
///<reference path="lobby/utils/AjaxRequest.ts" />
///<reference path="lobby/utils/Utils.ts" />
///<reference path="lobby/view/LoginView.ts" />
///<reference path="lobby/view/SelectGameView.ts" />
///<reference path="lobby/controller/base/ControllerManager.ts" />
///<reference path="lobby/controller/base/BaseController.ts" />
///<reference path="lobby/controller/LoaderController.ts" />
///<reference path="lobby/controller/LobbyController.ts" />
///<reference path="lobby/controller/LoginController.ts" />
///<reference path="lobby/controller/SelectGameController.ts" />
///<reference path="lobby/controller/GameController.ts" />
///<reference path="lobby/controller/ServerController.ts" />
//# sourceMappingURL=lobby.js.map
