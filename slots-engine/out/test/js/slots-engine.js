var engine;
(function (engine) {
    var BonusTypes = (function () {
        function BonusTypes() {
        }
        BonusTypes.GAMBLE = "gamble";
        BonusTypes.FREE_SPINS = "free_spins";
        BonusTypes.MINI_GAME = "mini_game";
        BonusTypes.SELECT_GAME = "select_item";
        return BonusTypes;
    })();
    engine.BonusTypes = BonusTypes;
})(engine || (engine = {}));
var engine;
(function (engine) {
    var GameConst = (function () {
        function GameConst() {
        }
        GameConst.DEMO_AUTHORIZATION_URL = "onlinecasino/common.asmx/DemoSession";
        GameConst.AUTHORIZATION_URL = "onlinecasino/common.asmx/Authorization";
        GameConst.AUTHORIZATION_ROOM = "onlinecasino/common.asmx/authorizationUIS";
        GameConst.CLOSE_SESSION_URL = "onlinecasino/common.asmx/Close";
        GameConst.GET_BALANCE_URL = "onlinecasino/common.asmx/GetBalance";
        GameConst.GET_BETS_URL = "onlinecasino/games/%s.asmx/GetBets";
        GameConst.SPIN_URL = "onlinecasino/games/%s.asmx/Spin";
        GameConst.JACKPOT_URL = "onlinecasino/games/%s.asmx/GetJackpot";
        GameConst.BONUS_URL = "onlinecasino/games/%s.asmx/BonusGame";
        GameConst.SET_WHEEL_URL = "onlinecasino/games/%s.asmx/SetWheel";
        GameConst.MAIN_RES_FOLDER = "assets/";
        GameConst.LAYOUT_FOLDER = "layout/";
        GameConst.FONTS_FOLDER = "fonts/";
        GameConst.SOUNDS_FOLDER = "sounds/";
        GameConst.COMMON_LAYOUTS_FOLDER = "common_assets/";
        GameConst.GAME_CONFIG_NAME = "config";
        GameConst.GAME_FPS = 30;
        GameConst.MOBILE_TO_WEB_SCALE = 2;
        return GameConst;
    })();
    engine.GameConst = GameConst;
})(engine || (engine = {}));
var engine;
(function (engine) {
    var NotificationList = (function () {
        function NotificationList() {
        }
        NotificationList.BACK_TO_LOBBY = "back_to_lobby";
        NotificationList.HIDE_PRELOADER = "hide_preloader";
        NotificationList.SERVER_INIT = "server_init";
        NotificationList.SERVER_SEND_SPIN = "send_spin";
        NotificationList.SERVER_GOT_SPIN = "got_spin";
        NotificationList.SERVER_SEND_BONUS = "send_bonus";
        NotificationList.SERVER_GOT_BONUS = "got_bonus";
        NotificationList.SERVER_SET_WHEEL = "set_wheel";
        NotificationList.SERVER_DISCONNECT = "disconnect";
        NotificationList.SERVER_NOT_RESPONSE = "server_not_response";
        NotificationList.SERVER_GET_BALANCE_REQUEST = "server_get_balance";
        NotificationList.RES_LOADED = "res_loaded";
        NotificationList.LOAD_SOUNDS = "load_sounds";
        NotificationList.SOUNDS_LOADED = "sounds_loaded";
        NotificationList.KEYBOARD_CLICK = "keyboard_click";
        NotificationList.ON_SCREEN_CLICK = "on_screen_click";
        NotificationList.TRY_START_AUTO_PLAY = "try_start_auto_play";
        NotificationList.END_AUTO_PLAY = "end_auto_play";
        NotificationList.TRY_START_SPIN = "try_start_spin";
        NotificationList.START_SPIN = "start_spin";
        NotificationList.START_SPIN_AUDIO = "start_spin_audio";
        NotificationList.EXPRESS_STOP = "express_stop";
        NotificationList.STOPPED_REEL_ID = "stopped_reel_id";
        NotificationList.STOPPED_REEL_ID_PREPARE = "stopped_reel_id_prepare";
        NotificationList.STOPPED_ALL_REELS = "stopped_all_reels";
        NotificationList.CHANGED_LINE_COUNT = "changed_line_count";
        NotificationList.OPEN_PAY_TABLE = "open_pay_table";
        NotificationList.CLOSE_PAY_TABLE = "close_pay_table";
        NotificationList.OPEN_AUTO_SPIN_MENU = "open_auto_spin_menu";
        NotificationList.TOGGLE_SETTINGS_MENU = "toggle_settings_menu";
        NotificationList.CLOSE_SETTINGS_MENU = "close_settings_menu";
        NotificationList.SOUND_TOGGLE = "sound_toggle";
        NotificationList.SHOW_WIN_LINES = "show_win_lines";
        NotificationList.WIN_LINES_SHOWED = "win_lines_showed";
        NotificationList.REMOVE_WIN_LINES = "remove_vin_lines";
        NotificationList.SHOW_LINES = "show_line";
        NotificationList.HIDE_LINES = "hide_lines";
        NotificationList.SHOW_HEADER = "show_header";
        NotificationList.REMOVE_HEADER = "remove_header";
        NotificationList.UPDATE_BALANCE_TF = "update_balance";
        NotificationList.SHOW_WIN_TF = "show_win";
        NotificationList.COLLECT_WIN_TF = "collect_balance";
        NotificationList.CREATE_BONUS = "create_bonus";
        NotificationList.START_BONUS = "start_bonus";
        NotificationList.END_BONUS = "end_bonus";
        NotificationList.START_SECOND_FREE_GAME = "second_game";
        NotificationList.HIDE_CARD = "hide_card";
        NotificationList.SHOW_CARD = "show_card";
        NotificationList.SHOW_ALL_SYMBOLS = "show_all_symbols";
        NotificationList.HIDE_SYMBOLS = "hide_symbols";
        NotificationList.GET_ALL_IDS = "get_all_ids";
        NotificationList.LAZY_LOAD = "lazy_load";
        NotificationList.LAZY_LOAD_COMP = "lazy_load_compl";
        NotificationList.AUTO_PLAY_COMP = "auto_play_compl";
        NotificationList.AUTO_PLAY_CONT = "auto_play_cont";
        NotificationList.SHOW_AUTO_MODAL = "show_auto_modal";
        NotificationList.LOAD_PAY_TABLE = "load_pay_tables";
        NotificationList.START_BONUS_AUDIO = "start_bonus_audio";
        NotificationList.TRY_START_AUTO_PLAY_AUDIO = "try_start_auto_play_audio";
        NotificationList.SHOW_WILD_REEL_FADEIN = "show_wild_reel_fadein";
        NotificationList.SHOW_WILD_REEL = "show_wild_reel";
        NotificationList.RECURSION_PROCESS_FALLING = "recursion_process_falling";
        NotificationList.RECURSION_PROCESSED_FALLING = "recursion_processed_falling";
        NotificationList.RECURSION_PROCESSED = "recursion_processed";
        NotificationList.RECURSION_PROCESS_ROTATION = "recursion_process_rotation";
        NotificationList.LOADER_LOADED = "loader_loaded";
        NotificationList.SHOW_ERRORS = "show_errors";
        NotificationList.UPDATE_JACKPOT = "update_jackpot";
        NotificationList.UPDATE_JACKPOT_SERVER = "update_jackpot_server";
        NotificationList.SHOW_JACKPOT_POPUP = "show_jackpot_popup";
        NotificationList.OK_BTN_ERROR_CLICKED = "ok_btn_error_clicked";
        NotificationList.TURBO_OFF_CLICKED = "turbo_off_clicked";
        NotificationList.UPDATE_TURBO_MODE = "update_turbo_mode";
        return NotificationList;
    })();
    engine.NotificationList = NotificationList;
})(engine || (engine = {}));
var engine;
(function (engine) {
    var SoundsList = (function () {
        function SoundsList() {
        }
        //-------------SOUND NAMES---------------------------------
        SoundsList.REGULAR_CLICK_BTN = "regular_click_btn";
        SoundsList.SPIN_CLICK_BTN = "spin_click_btn";
        SoundsList.REGULAR_SPIN_BG = "regular_spin_bg";
        SoundsList.AUTO_SPIN_BG = "auto_spin_bg";
        SoundsList.FREE_SPIN_BG = "free_spin_bg";
        SoundsList.PAY_TABLE_BG = "pay_table_bg";
        SoundsList.COLLECT = "collect";
        SoundsList.REEL_STOP = "reel_stop";
        SoundsList.FREE_SPIN_COLLECT = "free_spin_collect";
        SoundsList.REGULAR_WIN = "regular_win";
        SoundsList.LINE_WIN_ENUM = "line_win_enum";
        SoundsList.GAMBLE_BG = 'gamble_bg';
        SoundsList.GAMBLE_SHUFFLE = 'gamble_shuffle';
        SoundsList.GAMBLE_TURN = 'gamble_turn';
        SoundsList.GAMBLE_AMBIENT = 'gamble_ambient';
        SoundsList.CARD_TURN = 'card_turn';
        SoundsList.CARD_SHUFFLE = 'card_shuffle';
        SoundsList.RECURSION_ROTATION = 'recursion_rotation';
        return SoundsList;
    })();
    engine.SoundsList = SoundsList;
})(engine || (engine = {}));
var engine;
(function (engine) {
    var WinAniVO = (function () {
        function WinAniVO() {
        }
        return WinAniVO;
    })();
    engine.WinAniVO = WinAniVO;
})(engine || (engine = {}));
var engine;
(function (engine) {
    var BonusVO = (function () {
        function BonusVO() {
        }
        return BonusVO;
    })();
    engine.BonusVO = BonusVO;
})(engine || (engine = {}));
var engine;
(function (engine) {
    var WinLineVO = (function () {
        function WinLineVO() {
        }
        return WinLineVO;
    })();
    engine.WinLineVO = WinLineVO;
})(engine || (engine = {}));
var engine;
(function (engine) {
    var ServerData = (function () {
        function ServerData() {
            this.recursion = [];
            this.recursion_len = 0;
            this.win = 0;
        }
        ServerData.prototype.setBalance = function (value) {
            this.balance = value;
        };
        ServerData.prototype.getBalance = function () {
            return this.balance;
        };
        ServerData.prototype.setMaxMultiply = function () {
            this.multiply = this.availableMultiples[this.availableMultiples.length - 1];
        };
        ServerData.prototype.setMaxBet = function () {
            this.bet = this.availableBets[this.availableBets.length - 1];
        };
        ServerData.prototype.setMaxLines = function () {
            this.lineCount = this.maxLineCount;
        };
        ServerData.prototype.setNextMultiply = function () {
            var index = this.availableMultiples.indexOf(this.multiply);
            var nextIndex = (index + 1) % this.availableMultiples.length;
            this.multiply = this.availableMultiples[nextIndex];
        };
        ServerData.prototype.setNextBet = function (isCircle) {
            var index = this.availableBets.indexOf(this.bet);
            if (++index == this.availableBets.length) {
                index = isCircle ? 0 : this.availableBets.length - 1;
            }
            this.bet = this.availableBets[index];
        };
        ServerData.prototype.isHasNextBet = function () {
            return this.availableBets.indexOf(this.bet) != this.availableBets.length - 1;
        };
        ServerData.prototype.setPrevBet = function (isCircle) {
            var index = this.availableBets.indexOf(this.bet);
            if (--index == -1) {
                index = isCircle ? this.availableBets.length - 1 : 0;
            }
            this.bet = this.availableBets[index];
        };
        ServerData.prototype.isHasPrevBet = function () {
            return this.availableBets.indexOf(this.bet) != 0;
        };
        ServerData.prototype.setNextLineCount = function (isCircle) {
            var lines = this.lineCount + 1;
            if (lines > this.maxLineCount) {
                lines = isCircle ? 1 : this.maxLineCount;
            }
            this.lineCount = lines;
        };
        ServerData.prototype.isHasNextLine = function () {
            return this.lineCount != this.maxLineCount;
        };
        ServerData.prototype.setPrevLineCount = function (isCircle) {
            var lines = this.lineCount - 1;
            if (lines < 1) {
                lines = isCircle ? this.maxLineCount : 1;
            }
            this.lineCount = lines;
        };
        ServerData.prototype.isHasPrevLine = function () {
            return this.lineCount != 1;
        };
        ServerData.prototype.getTotalBet = function () {
            return this.lineCount * this.multiply * this.bet;
        };
        ServerData.prototype.getBetPerLine = function () {
            return this.multiply * this.bet;
        };
        return ServerData;
    })();
    engine.ServerData = ServerData;
})(engine || (engine = {}));
var engine;
(function (engine) {
    var CommonRefs = (function () {
        function CommonRefs() {
            this.isLayoutsLoaded = false;
            this.moverType = engine.ReelMoverTypes.DEFAULT;
            this.wildSymbolId = 2;
            this.isTurbo = false;
            this.isAuto = false;
        }
        return CommonRefs;
    })();
    engine.CommonRefs = CommonRefs;
})(engine || (engine = {}));
var engine;
(function (engine) {
    var Utils = (function () {
        function Utils() {
        }
        Utils.float2int = function (value) {
            return value | 0;
        };
        Utils.toIntArray = function (value) {
            var returnVal = new Array(value.length);
            for (var i = 0; i < value.length; i++) {
                returnVal[i] = parseInt(value[i]);
            }
            return returnVal;
        };
        Utils.getClassName = function (obj) {
            var funcNameRegex = /function (.{1,})\(/;
            var results = (funcNameRegex).exec(obj["constructor"].toString());
            return (results && results.length > 1) ? results[1] : "";
        };
        Utils.fillDisplayObject = function (container, prefix) {
            var i = 1;
            var item;
            var result = [];
            while ((item = container.getChildByName(prefix + i)) != null) {
                result.push(item);
                i++;
            }
            return result;
        };
        return Utils;
    })();
    engine.Utils = Utils;
})(engine || (engine = {}));
var engine;
(function (engine) {
    var Keyboard = (function () {
        function Keyboard() {
        }
        Keyboard.SPACE = 32;
        Keyboard.BACK_QUOTE = 192;
        return Keyboard;
    })();
    engine.Keyboard = Keyboard;
})(engine || (engine = {}));
var engine;
(function (engine) {
    var XMLUtils = (function () {
        function XMLUtils() {
        }
        XMLUtils.getElements = function (xml, tagName) {
            var result = [];
            var childNodes = xml.childNodes;
            for (var i = 0; i < childNodes.length; i++) {
                var node = childNodes[i];
                if (node.nodeName == tagName) {
                    result.push(node);
                }
            }
            return result;
        };
        XMLUtils.getElement = function (xml, tagName) {
            var childNodes = xml.childNodes;
            for (var i = 0; i < childNodes.length; i++) {
                var node = childNodes[i];
                if (node.nodeName == tagName) {
                    return node;
                }
            }
            return null;
        };
        XMLUtils.getChildrenElements = function (xml) {
            var childArr = [];
            var childNodes = xml.childNodes;
            for (var i = 0; i < childNodes.length; i++) {
                var node = childNodes[i];
                childArr.push(node);
            }
            return childArr;
        };
        XMLUtils.getSessionRoom = function (xml, tagName) {
            var childNodes = xml.getElementsByTagName(tagName)[0];
            if (childNodes != null) {
                return childNodes.textContent;
            }
            else
                return null;
        };
        XMLUtils.getAttribute = function (xml, attributeName) {
            var attributes = xml.attributes;
            if (attributes.hasOwnProperty(attributeName)) {
                return xml.attributes[attributeName].value;
            }
        };
        XMLUtils.getAttributeInt = function (xml, attributeName) {
            var attribute = XMLUtils.getAttribute(xml, attributeName);
            if (attribute != null) {
                return parseInt(attribute);
            }
            return null;
        };
        XMLUtils.getAttributeFloat = function (xml, attributeName) {
            var attribute = XMLUtils.getAttribute(xml, attributeName);
            if (attribute != null) {
                return parseFloat(attribute);
            }
            return null;
        };
        return XMLUtils;
    })();
    engine.XMLUtils = XMLUtils;
})(engine || (engine = {}));
var engine;
(function (engine) {
    var MoneyFormatter = (function () {
        function MoneyFormatter() {
        }
        MoneyFormatter.format = function (value, isTrimZeros, isShowZero, symbol) {
            if (isShowZero === void 0) { isShowZero = true; }
            if (symbol === void 0) { symbol = ""; }
            if (value == 0) {
                return isShowZero ? symbol + "0" : "";
            }
            var digits = value.toFixed(2).split(".");
            digits[0] = digits[0].split("").reverse().join("").replace(/(\d{3})(?=\d)/g, "$1,").split("").reverse().join("");
            if (isTrimZeros) {
                value = parseInt(digits[1]);
                if (value > 0) {
                    digits[1] = value.toString();
                }
                else {
                    return symbol + digits[0];
                }
            }
            return symbol + digits.join(".");
        };
        return MoneyFormatter;
    })();
    engine.MoneyFormatter = MoneyFormatter;
})(engine || (engine = {}));
var engine;
(function (engine) {
    var FullScreen = (function () {
        function FullScreen() {
        }
        FullScreen.init = function () {
            var element = document.documentElement;
            if (element.requestFullscreen) {
                FullScreen.fullscreenRequest = "requestFullscreen";
            }
            else if (element.mozRequestFullScreen) {
                FullScreen.fullscreenRequest = "mozRequestFullScreen";
            }
            else if (element.webkitRequestFullscreen) {
                FullScreen.fullscreenRequest = "webkitRequestFullscreen";
            }
            else if (element.msRequestFullscreen) {
                FullScreen.fullscreenRequest = "msRequestFullscreen";
            }
            if (document.exitFullscreen) {
                FullScreen.cancelFullScreen = "exitFullscreen";
            }
            else if (document.mozCancelFullScreen) {
                FullScreen.cancelFullScreen = "mozCancelFullScreen";
            }
            else if (document.webkitExitFullscreen) {
                FullScreen.cancelFullScreen = "webkitExitFullscreen";
            }
            else if (document.msExitFullscreen) {
                FullScreen.cancelFullScreen = "msExitFullscreen";
            }
        };
        FullScreen.toggleFullScreen = function () {
            if (FullScreen.isFullScreen()) {
                FullScreen.fullScreen();
            }
            else {
                FullScreen.closeFullScreen();
            }
        };
        FullScreen.isFullScreen = function () {
            return document.fullscreenElement || document.mozFullScreenElement || document.webkitFullscreenElement || document.msFullscreenElement;
        };
        FullScreen.fullScreen = function () {
            var element = document.documentElement;
            if (FullScreen.fullscreenRequest != undefined) {
                element[FullScreen.fullscreenRequest]();
            }
        };
        FullScreen.closeFullScreen = function () {
            if (FullScreen.cancelFullScreen != undefined) {
                document[FullScreen.cancelFullScreen]();
            }
        };
        return FullScreen;
    })();
    engine.FullScreen = FullScreen;
})(engine || (engine = {}));
var engine;
(function (engine) {
    var AssetVO = (function () {
        function AssetVO(id, src, type) {
            if (type === void 0) { type = ""; }
            this.id = id;
            this.src = src;
            this.type = type;
        }
        return AssetVO;
    })();
    engine.AssetVO = AssetVO;
})(engine || (engine = {}));
var engine;
(function (engine) {
    var FileConst = (function () {
        function FileConst() {
        }
        FileConst.LAYOUT_EXTENSION = ".layout";
        FileConst.JSON_EXTENSION = ".json";
        FileConst.WOFF_EXTENSION = ".woff";
        return FileConst;
    })();
    engine.FileConst = FileConst;
})(engine || (engine = {}));
var engine;
(function (engine) {
    var BaseEasing = (function () {
        function BaseEasing() {
        }
        /**
         * Установка параметров
         * @param distance - рассояние
         * @param updateCount - за сколько кадров нужно прйти от distance
         */
        BaseEasing.prototype.setParameters = function (distance, updateCount) {
            this.distance = distance;
            this.dt = 1 / updateCount;
            this.reset();
        };
        BaseEasing.prototype.getPosition = function () {
            this.currentTime += this.dt;
            if (this.currentTime + BaseEasing.EPSILON > 1) {
                this.currentTime = 1;
            }
            var currentPos = this.getRatio(this.currentTime) * this.distance;
            this.speed = currentPos - this.position;
            this.position = currentPos;
            return currentPos;
        };
        BaseEasing.prototype.getTime = function () {
            return this.currentTime;
        };
        BaseEasing.prototype.reset = function () {
            this.currentTime = 0;
            this.position = 0;
            this.speed = 0;
        };
        BaseEasing.prototype.getRatio = function (time) {
            throw new Error("ERROR: Need override method");
        };
        BaseEasing.EPSILON = 0.000000001;
        return BaseEasing;
    })();
    engine.BaseEasing = BaseEasing;
})(engine || (engine = {}));
var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var engine;
(function (engine) {
    var LinearEasing = (function (_super) {
        __extends(LinearEasing, _super);
        function LinearEasing() {
            _super.call(this);
        }
        LinearEasing.prototype.getRatio = function (time) {
            return time;
        };
        return LinearEasing;
    })(engine.BaseEasing);
    engine.LinearEasing = LinearEasing;
})(engine || (engine = {}));
var engine;
(function (engine) {
    var BezierEasing = (function (_super) {
        __extends(BezierEasing, _super);
        function BezierEasing(a, b, c, d, e) {
            _super.call(this);
            this.a = a;
            this.b = b;
            this.c = c;
            this.d = d;
            this.e = e;
        }
        BezierEasing.prototype.getRatio = function (time) {
            var ts = time * time;
            var tc = ts * time;
            return this.a * tc * ts + this.b * ts * ts + this.c * tc + this.d * ts + this.e * time;
        };
        return BezierEasing;
    })(engine.BaseEasing);
    engine.BezierEasing = BezierEasing;
})(engine || (engine = {}));
var engine;
(function (engine) {
    var AjaxRequest = (function () {
        function AjaxRequest(type, url, isAsynchronous, params) {
            this.type = type;
            this.url = url + AjaxRequest.paramsToString(params);
            this.isAsynchronous = isAsynchronous;
        }
        AjaxRequest.paramsToString = function (params) {
            var paramsStr = "";
            for (var key in params) {
                paramsStr += paramsStr.length == 0 ? "?" : "&";
                paramsStr += key + "=" + params[key];
            }
            return paramsStr;
        };
        AjaxRequest.getXmlHttp = function () {
            if (window.hasOwnProperty("XMLHttpRequest")) {
                // code for IE7+, Firefox, Chrome, Opera, Safari
                return new XMLHttpRequest();
            }
            else if (window.hasOwnProperty("ActiveXObject")) {
                // code for IE6, IE5
                return new ActiveXObject("Microsoft.XMLHTTP");
            }
        };
        /**
         * Send GET request
         */
        AjaxRequest.prototype.get = function (handler) {
            var _this = this;
            if (handler === void 0) { handler = null; }
            this.handler = handler;
            this.xmlHttp = AjaxRequest.getXmlHttp();
            this.xmlHttp.onreadystatechange = function () {
                _this.onComplete();
            };
            this.xmlHttp.open("GET", this.url, this.isAsynchronous);
            this.xmlHttp.send(null);
        };
        AjaxRequest.prototype.abort = function () {
            this.xmlHttp.abort();
        };
        /**
         * Request is ready
         */
        AjaxRequest.prototype.onComplete = function () {
            if (this.xmlHttp.readyState == XMLHttpRequest.DONE) {
                // success
                if (this.xmlHttp.status == 200) {
                    // Registration : in the registration process xmlRootNode return current status that user registered or not.
                    var responseData;
                    switch (this.type) {
                        case AjaxRequest.XML:
                            {
                                responseData = this.xmlHttp.responseXML;
                                break;
                            }
                        case AjaxRequest.JSON:
                            {
                                responseData = JSON.parse(this.xmlHttp.response);
                                break;
                            }
                    }
                    // TODO: need remove on server empty response
                    /*if (responseData == null) {
                        throw new Error("ERROR: Server response null");
                    }*/
                    if (this.handler != null) {
                        this.handler(responseData);
                    }
                }
                else {
                    throw new Error("ERROR: There was a problem retrieving the data: " + this.xmlHttp.statusText);
                }
            }
        };
        AjaxRequest.JSON = "json";
        AjaxRequest.XML = "xml";
        return AjaxRequest;
    })();
    engine.AjaxRequest = AjaxRequest;
})(engine || (engine = {}));
var engine;
(function (engine) {
    var ItemVO = (function () {
        function ItemVO(id) {
            this.id = id;
            this.isSelected = false;
            this.winValue = -1;
            this.type = 0;
        }
        return ItemVO;
    })();
    engine.ItemVO = ItemVO;
})(engine || (engine = {}));
var engine;
(function (engine) {
    var AnimationTextField = (function () {
        function AnimationTextField(textField) {
            this.textField = textField;
            this.easing = new engine.LinearEasing();
            this.isUpdate = false;
        }
        AnimationTextField.prototype.setValue = function (value, updateTime) {
            if (updateTime === void 0) { updateTime = 0; }
            if (updateTime == 0) {
                this.value = value;
                this.setText(value);
                this.isUpdate = false;
            }
            else if (this.value != value) {
                this.isUpdate = true;
                this.easing.setParameters(value - this.value, updateTime);
            }
        };
        AnimationTextField.prototype.update = function () {
            if (this.isUpdate) {
                var pos = this.easing.getPosition();
                this.setText(this.value + pos);
                if (this.easing.getTime() >= 1) {
                    this.isUpdate = false;
                    this.value += pos;
                    this.easing.reset();
                }
            }
        };
        AnimationTextField.prototype.setText = function (value) {
            this.textField.text = engine.MoneyFormatter.format(value, false);
        };
        return AnimationTextField;
    })();
    engine.AnimationTextField = AnimationTextField;
})(engine || (engine = {}));
var engine;
(function (engine) {
    var TouchMenu = (function () {
        function TouchMenu(view) {
            this.view = view;
        }
        TouchMenu.prototype.init = function (currentIdx, allValues, onUpdate) {
            var _this = this;
            this.currentIdx = currentIdx;
            this.allValues = allValues;
            this.onUpdate = onUpdate;
            this.initTextFields();
            this.updateValue();
            this.step = this.view.getBounds().width / this.texts.length / 2;
            this.view.on("mousedown", function (eventDownObj) {
                var div;
                var startIdx = _this.currentIdx;
                _this.view.on("pressmove", function (eventMoveObj) {
                    div = engine.Utils.float2int((eventDownObj.stageX - eventMoveObj.stageX) / _this.step);
                    var currentIdx = startIdx + div;
                    if (currentIdx != _this.currentIdx && currentIdx >= 0 && currentIdx < _this.allValues.length) {
                        _this.changeIdx(currentIdx);
                    }
                });
                _this.view.on("pressup", function () {
                    _this.view.removeAllEventListeners("pressmove");
                    _this.view.removeAllEventListeners("pressup");
                });
            });
        };
        TouchMenu.prototype.changeIdx = function (idx) {
            this.currentIdx = idx;
            this.updateValue();
            this.onUpdate(this.currentIdx);
        };
        TouchMenu.prototype.updateValue = function () {
            var idx;
            var div = (this.texts.length - 1) / 2;
            for (var i = 0; i < this.texts.length; i++) {
                idx = this.currentIdx + i - div;
                if (idx >= 0 && idx < this.allValues.length) {
                    this.texts[i].text = this.allValues[idx].toString();
                }
                else {
                    this.texts[i].text = "";
                }
            }
        };
        TouchMenu.prototype.initTextFields = function () {
            var textField;
            var i = 1;
            this.texts = [];
            while ((textField = this.view.getChildByName(TouchMenu.TEXT_PREFIX + i)) != null) {
                this.texts.push(textField);
                i++;
            }
        };
        TouchMenu.TEXT_PREFIX = "text";
        return TouchMenu;
    })();
    engine.TouchMenu = TouchMenu;
})(engine || (engine = {}));
var engine;
(function (engine) {
    var Container = createjs.Container;
    var Sprite = createjs.Sprite;
    var WinSymbolView = (function (_super) {
        __extends(WinSymbolView, _super);
        function WinSymbolView() {
            _super.call(this);
            this._is_wild = false;
        }
        WinSymbolView.prototype.create = function (winSymbolCreator, highlightCreator, isMobile) {
            if (winSymbolCreator != null) {
                var winSymbolContainer = new Container();
                winSymbolCreator.create(winSymbolContainer);
                this.winSymbol = winSymbolContainer.getChildAt(0);
                this.addChild(winSymbolContainer);
            }
            if (highlightCreator != null) {
                var highlightContainer = new Container();
                highlightCreator.create(highlightContainer);
                this.highlight = highlightContainer.getChildAt(0);
                if (isMobile) {
                    this.highlight.scaleX *= engine.GameController.MOBILE_TO_WEB_SCALE;
                    this.highlight.scaleY *= engine.GameController.MOBILE_TO_WEB_SCALE;
                }
                this.addChild(highlightContainer);
            }
        };
        WinSymbolView.prototype.play = function () {
            if (this.winSymbol != null && this.winSymbol instanceof Sprite) {
                var sprite = this.winSymbol;
                sprite.gotoAndPlay(0);
            }
            if (this.highlight != null) {
                this.highlight.gotoAndPlay(0);
            }
        };
        WinSymbolView.prototype.setY = function (value) {
            this.y = value;
        };
        WinSymbolView.prototype.setX = function (value) {
            this.x = value;
        };
        return WinSymbolView;
    })(Container);
    engine.WinSymbolView = WinSymbolView;
})(engine || (engine = {}));
/**
 * Created by Taras on 22.05.2015.
 */
var engine;
(function (engine) {
    var Container = createjs.Container;
    var Sprite = createjs.Sprite;
    var Tween = createjs.Tween;
    var WildSymbolView = (function (_super) {
        __extends(WildSymbolView, _super);
        function WildSymbolView() {
            _super.call(this);
            this._is_wild_started = false;
        }
        WildSymbolView.prototype.create = function (winSymbolCreator, highlightCreator, wildSymbolCreator, isMobile) {
            if (winSymbolCreator != null) {
                var winSymbolContainer = new Container();
                winSymbolCreator.create(winSymbolContainer);
                this.winSymbol = winSymbolContainer.getChildAt(0);
                this.addChild(winSymbolContainer);
            }
            if (highlightCreator != null) {
                var highlightContainer = new Container();
                highlightCreator.create(highlightContainer);
                this.highlight = highlightContainer.getChildAt(0);
                if (isMobile) {
                    this.highlight.scaleX *= engine.GameController.MOBILE_TO_WEB_SCALE;
                    this.highlight.scaleY *= engine.GameController.MOBILE_TO_WEB_SCALE;
                }
                this.addChild(highlightContainer);
            }
            var wildSymbolLoop = new Container();
            wildSymbolCreator.create(wildSymbolLoop);
            this.wildLoop = wildSymbolLoop.getChildAt(0);
            this.wildLoop.visible = false;
            this.addChild(wildSymbolLoop);
        };
        WildSymbolView.prototype.play = function () {
            if (this.winSymbol != null && this.winSymbol instanceof Sprite) {
                var sprite = this.winSymbol;
                this.wildLoop.visible = false;
                var wildLoop = this.wildLoop;
                var frameCount = sprite.spriteSheet.getNumFrames(null);
                sprite.addEventListener("tick", function (eventObj) {
                    var currentAnimation = eventObj.currentTarget;
                    currentAnimation.visible = true;
                    if (currentAnimation.currentFrame == (frameCount - 1 * 2)) {
                        currentAnimation.stop();
                        currentAnimation.visible = false;
                        currentAnimation.removeAllEventListeners("tick");
                        wildLoop.visible = true;
                        wildLoop.gotoAndPlay(0);
                    }
                });
                //setTimeout(function(){ // prevent Tick listening
                //    console.log("setTimeoutsetTimeoutsetTimeout");
                //    var currentAnimation:Sprite = sprite;
                //    currentAnimation.visible = true;
                //    if (currentAnimation.currentFrame == (frameCount - 2)) {
                //        currentAnimation.stop();
                //        currentAnimation.visible = false;
                //        currentAnimation.removeAllEventListeners("tick");
                //        wildLoop.visible = true;
                //        wildLoop.gotoAndPlay(0);
                //    }
                //}, 1000);
                if (!this._is_wild_started) {
                    this._is_wild_started = true;
                    sprite.gotoAndPlay(0);
                }
            }
        };
        WildSymbolView.prototype.playFadeIn = function () {
            if (this.winSymbol != null && this.winSymbol instanceof Sprite) {
                var sprite = this.winSymbol;
                sprite.visible = false;
                var wildLoop = this.wildLoop;
                wildLoop.visible = true;
                wildLoop.alpha = 0;
                this._is_wild_started = true;
                wildLoop.gotoAndPlay(0);
                Tween.get(wildLoop).to({ alpha: 1 }, 900);
            }
        };
        WildSymbolView.prototype.setY = function (value) {
            this.y = value;
        };
        WildSymbolView.prototype.setX = function (value) {
            this.x = value;
        };
        return WildSymbolView;
    })(Container);
    engine.WildSymbolView = WildSymbolView;
})(engine || (engine = {}));
var engine;
(function (engine) {
    var Tween = createjs.Tween;
    var Shape = createjs.Shape;
    var Ease = createjs.Ease;
    var ReelView = (function () {
        function ReelView(view, reelId) {
            this.view = view;
            this.reelId = reelId;
        }
        ReelView.prototype.init = function () {
            this.symbols = [];
            var symbolId = 1;
            var symbol;
            while ((symbol = this.view.getChildByName(ReelView.SYMBOL_PREFIX + symbolId)) != null) {
                this.symbols.push(symbol);
                symbolId++;
            }
        };
        ReelView.prototype.showAllSymbols = function () {
            for (var i = 0; i < this.symbols.length; i++) {
                this.symbols[i].visible = true;
            }
        };
        /////////////////////////////////////////
        //	GETTERS
        /////////////////////////////////////////
        ReelView.prototype.getReelId = function () {
            return this.reelId;
        };
        ReelView.prototype.getSymbol = function (id) {
            return this.symbols[id];
        };
        ReelView.prototype.getSymbolFrame = function (id) {
            return this.symbols[id].currentFrame;
        };
        ReelView.prototype.getSymbolCount = function () {
            return this.symbols.length - 1;
        };
        ReelView.prototype.getX = function () {
            return this.view.x;
        };
        ReelView.prototype.getY = function () {
            return this.view.y;
        };
        /////////////////////////////////////////
        //	SETTERS
        /////////////////////////////////////////
        ReelView.prototype.setX = function (value) {
            this.view.x = value;
        };
        ReelView.prototype.setY = function (value) {
            this.view.y = value;
        };
        ReelView.prototype.setSymbolFrame = function (symbolId, frameId) {
            return this.symbols[symbolId].gotoAndStop(frameId || 0);
        };
        ReelView.prototype.getView = function () {
            return this.view;
        };
        ReelView.prototype.getSymbols = function () {
            return this.symbols;
        };
        ReelView.prototype.symbolsMoveDown = function (delay_reels, delay_symbols, callback) {
            var self = this;
            for (var i = 0; i < this.symbols.length; ++i) {
                //Tween.get(this.symbols[i]).wait(delay).to({y:400+300*(i+1)},1000).call(function(){self.symbolsAppearFromTop();});
                Tween.get(this.symbols[i]).wait(delay_reels + delay_symbols * (this.symbols.length - i)).to({ y: 400 + 600 * (i + 1) }, engine.ReelMoverCascade.MOVE_DOWN_TIME, Ease.getPowIn(5.2)).call(callback);
            }
        };
        ReelView.prototype.symbolsAppearFromTop = function (callback) {
            var symbolHeight = this.symbols[0].getBounds().height;
            for (var i = 0; i < this.symbols.length; ++i) {
                Tween.get(this.symbols[i], { override: true }).to({ y: -1500 + 300 * i }).to({ y: symbolHeight * (i) }, 500).call(callback);
            }
            //Tween.get(this).call(callback);
        };
        ReelView.prototype.recursionProcessFalling = function (common) {
            var symbolHeight = this.symbols[0].getBounds().height;
            for (var symbolid = 0; symbolid < engine.ReelMover.SYMBOLS_IN_REEL; ++symbolid) {
                var start_y = this.symbols[symbolid].y;
                var end_y = this.symbols[symbolid].y;
                var symbolUpdates = this.getSymbolUpdates(symbolid, common);
                if (symbolUpdates["isNeedRefresh"]) {
                    var frameid = this.getUpdatedFrameId(symbolid, common);
                    this.setSymbolFrame(symbolid, frameid);
                    start_y = start_y - parseInt(symbolUpdates["symbolUpdates"]) * symbolHeight;
                    Tween.get(this.symbols[symbolid], { override: true }).to({ y: start_y, rotation: 0 }).to({ y: end_y }, engine.ReelMoverCascade.RECURSION_SYMBOLS_FALLING_TIME);
                    this.symbols[symbolid].mask = null;
                }
            }
        };
        ReelView.prototype.recursionProcessRotation = function (common) {
            var symbolWidth = this.symbols[0].getBounds().width;
            var symbolHeight = this.symbols[0].getBounds().height;
            for (var symbolid = 0; symbolid < engine.ReelMover.SYMBOLS_IN_REEL; ++symbolid) {
                var symbolUpdates = this.getSymbolUpdates(symbolid, common);
                if (symbolUpdates["isWin"]) {
                    var mask = new Shape();
                    mask.graphics.beginFill("0");
                    mask.graphics.drawRect(this.symbols[symbolid].x, this.symbols[symbolid].y, symbolWidth, symbolHeight);
                    this.symbols[symbolid].mask = mask;
                    Tween.get(this.symbols[symbolid], { override: true }).to({ rotation: 90 }, engine.ReelMoverCascade.RECURSION_ROTATION_TIME);
                }
            }
        };
        ReelView.prototype.getSymbolUpdates = function (symbolid, common) {
            var symbolUpdates = [];
            var needRefresh = 0;
            for (var i = symbolid; i < engine.ReelMover.SYMBOLS_IN_REEL; ++i) {
                for (var j = 0; j < common.server.winLines.length; ++j) {
                    if (common.server.winLines[j].winPos[this.reelId] == (symbolid + 1)) {
                        symbolUpdates["isWin"] = true;
                    }
                    if (common.server.winLines[j].winPos[this.reelId] == (i + 1)) {
                        ++needRefresh;
                        break;
                    }
                }
            }
            symbolUpdates["symbolUpdates"] = needRefresh;
            symbolUpdates["isNeedRefresh"] = (needRefresh > 0);
            return symbolUpdates;
        };
        ReelView.prototype.getUpdatedFrameId = function (symbolid, common) {
            var frameid = common.server.recursion[0].wheels[this.reelId * engine.ReelMoverCascade.SYMBOLS_IN_REEL + symbolid] - 1;
            return frameid;
        };
        ReelView.prototype.getNumFrames = function () {
            return this.symbols[0].spriteSheet.getNumFrames();
        };
        ReelView.SYMBOL_PREFIX = "symbol_";
        return ReelView;
    })();
    engine.ReelView = ReelView;
})(engine || (engine = {}));
var engine;
(function (engine) {
    var Shape = createjs.Shape;
    var Layout = layout.Layout;
    var ReelsView = (function (_super) {
        __extends(ReelsView, _super);
        function ReelsView() {
            _super.call(this);
        }
        ReelsView.prototype.setMobileTrue = function (isMobile) {
            this.isMobile = isMobile;
            this.createReelsMask();
        };
        ReelsView.prototype.onInit = function () {
            this.createReelsView();
        };
        ReelsView.prototype.createReelsView = function () {
            this.reels = [];
            var view;
            var reelId = 1;
            while ((view = this.getChildByName(ReelsView.REEL_PREFIX + reelId)) != null) {
                var reelView = new engine.ReelView(view, reelId - 1);
                reelView.init();
                this.reels.push(reelView);
                reelId++;
            }
        };
        ReelsView.prototype.createReelsMask = function () {
            var firstReel = this.reels[0];
            var symbolBounds = firstReel.getSymbol(0).getBounds();
            var lastReel = this.reels[this.reels.length - 1];
            var mask = new Shape();
            var y = firstReel.getY();
            if (this.isMobile) {
                mask.graphics.beginFill("0").drawRect(firstReel.getX(), y * engine.GameController.MOBILE_TO_WEB_SCALE, this.getWidth() * engine.GameController.MOBILE_TO_WEB_SCALE, this.getHeight() * engine.GameController.MOBILE_TO_WEB_SCALE - symbolBounds.height * engine.GameController.MOBILE_TO_WEB_SCALE);
            }
            else {
                mask.graphics.beginFill("0").drawRect(firstReel.getX(), firstReel.getY(), lastReel.getX() + symbolBounds.width, this.getHeight() - symbolBounds.height);
            }
            //mask.graphics.beginFill("0").drawRect(firstReel.getX(), firstReel.getY(), lastReel.getX() + symbolBounds.width, this.getHeight() - symbolBounds.height); //mobiletoweb
            this.mask = mask;
            this.hitArea = mask;
        };
        ReelsView.prototype.showAllSymbols = function () {
            for (var i = 0; i < this.reels.length; i++) {
                this.reels[i].showAllSymbols();
            }
        };
        ReelsView.prototype.hideSymbols = function (positions, offset) {
            if (offset === void 0) { offset = 0; }
            for (var i = 0; i < positions.length; i++) {
                var pos = positions[i];
                this.reels[pos.x].getSymbol(pos.y + offset).visible = false;
            }
        };
        ReelsView.prototype.dispose = function () {
            this.parent.removeChild(this);
            this.reels = null;
        };
        /////////////////////////////////////////
        //	GETTERS
        /////////////////////////////////////////
        ReelsView.prototype.getReel = function (id) {
            return this.reels[id];
        };
        ReelsView.prototype.getReelCount = function () {
            return this.reels.length;
        };
        ReelsView.prototype.getHeight = function () {
            return this.getBounds().height;
        };
        ReelsView.prototype.getWidth = function () {
            return this.getBounds().width;
        };
        ReelsView.prototype.getReelsMask = function () {
            return this.mask;
        };
        ReelsView.LAYOUT_NAME = "ReelsView";
        ReelsView.REEL_PREFIX = "reel_";
        return ReelsView;
    })(Layout);
    engine.ReelsView = ReelsView;
})(engine || (engine = {}));
var engine;
(function (engine) {
    var Layout = layout.Layout;
    var PayTableHudView = (function (_super) {
        __extends(PayTableHudView, _super);
        function PayTableHudView() {
            _super.call(this);
            this.buttonsNames = [
                PayTableHudView.BACK_TO_GAME_BTN,
                PayTableHudView.PREV_PAGE_BTN,
                PayTableHudView.NEXT_PAGE_BTN
            ];
        }
        PayTableHudView.prototype.getBtn = function (btnName) {
            return this.getChildByName(btnName);
        };
        PayTableHudView.prototype.resizeForMobile = function () {
            var stage = this.getStage();
            if (stage == null) {
                return;
            }
            var i;
            var displayObject;
            var canvas = stage.canvas;
            var rightX = (canvas.width - stage.x) / stage.scaleX;
            for (i = 0; i < this.buttonsNames.length; i++) {
                displayObject = this.getChildByName(this.buttonsNames[i]);
                if (displayObject != null) {
                    displayObject.x = rightX - displayObject.getBounds().width;
                }
            }
        };
        PayTableHudView.LAYOUT_NAME = "PayTableHudView";
        PayTableHudView.BACK_TO_GAME_BTN = "backToGameBtn";
        PayTableHudView.PREV_PAGE_BTN = "prevPageBtn";
        PayTableHudView.NEXT_PAGE_BTN = "nextPageBtn";
        return PayTableHudView;
    })(Layout);
    engine.PayTableHudView = PayTableHudView;
})(engine || (engine = {}));
var engine;
(function (engine) {
    var Layout = layout.Layout;
    var HudView = (function (_super) {
        __extends(HudView, _super);
        function HudView() {
            _super.call(this);
            this.buttonsNames = [
                HudView.SETTINGS_BTN,
                HudView.BACK_TO_LOBBY_BTN,
                HudView.START_AUTO_PLAY_BTN,
                HudView.STOP_AUTO_PLAY_BTN,
                HudView.GAMBLE_BTN,
                HudView.INC_SPIN_COUNT_BTN,
                HudView.DEC_SPIN_COUNT_BTN,
                HudView.INC_LINE_COUNT_BTN,
                HudView.DEC_LINE_COUNT_BTN,
                HudView.NEXT_BET_BTN,
                HudView.PREV_BET_BTN,
                HudView.MAX_BET_BTN,
                HudView.MAX_LINES_BTN,
                HudView.START_SPIN_BTN,
                HudView.START_BONUS_BTN,
                HudView.STOP_SPIN_BTN,
                HudView.PAY_TABLE_BTN,
                HudView.BET_BTN,
                HudView.LINE_COUNT_BTN,
                HudView.MULTIPLY_BTN,
                HudView.OPEN_AUTO_SPIN_TABLE_BTN,
                HudView.TURBO_OFF_BTN,
                HudView.TURBO_ON_BTN,
                HudView.AUTO_OFF_BTN,
                HudView.AUTO_ON_BTN
            ];
            // for mobile
            this.leftObj = [
                HudView.BACK_TO_LOBBY_BTN,
                HudView.START_SPIN_BTN,
                HudView.STOP_SPIN_BTN,
                HudView.START_AUTO_PLAY_BTN,
                HudView.STOP_AUTO_PLAY_BTN,
                HudView.GAMBLE_BTN
            ];
            // for mobile
            this.rightObj = [
                HudView.SETTINGS_BTN,
                HudView.PAY_TABLE_BTN
            ];
            this.text_fileds_shadow = [
                HudView.BALANCE_TEXT,
                HudView.TOTAL_BET_TEXT,
                HudView.WIN_TEXT,
                HudView.BET_PER_LINE_TEXT
            ];
            this.linesText = [];
        }
        HudView.prototype.onInit = function () {
            //this.prepareView();
            var textField;
            var i = 1;
            while ((textField = this.getChildByName(HudView.LINES_COUNT_TEXT + i)) != null) {
                this.linesText.push(textField);
                i++;
            }
        };
        HudView.prototype.prepareView = function () {
        };
        HudView.prototype.addFieldsShadows = function () {
            for (var i = 0; i < this.text_fileds_shadow.length; ++i) {
                engine.GameController.setFieldStroke(this, this.text_fileds_shadow[i], 0, engine.GameController.SHADOW_COLOR, 1, 1, 2);
            }
            for (var i = 0; i < this.linesText.length; ++i) {
                engine.GameController.setTextStroke(this.linesText[i], 0, engine.GameController.SHADOW_COLOR, 1, 1, 2);
            }
        };
        HudView.prototype.resizeForMobile = function () {
            var stage = this.getStage();
            if (stage == null) {
                return;
            }
            var i;
            var displayObject;
            var startX;
            var canvas = stage.canvas;
            var rightX = -stage.x / stage.scaleX;
            var leftX = (canvas.width - stage.x) / stage.scaleX;
            startX = this.getBtn(HudView.STOP_AUTO_PLAY_BTN).x;
            for (i = 0; i < this.leftObj.length; i++) {
                displayObject = this.getChildByName(this.leftObj[i]);
                if (displayObject != null) {
                    displayObject.x = leftX - displayObject.getBounds().width;
                }
            }
            this.getText(HudView.AUTO_SPINS_COUNT_TEXT).x += this.getBtn(HudView.STOP_AUTO_PLAY_BTN).x - startX;
            // move settings menu
            var settingsMenu = this.getChildByName(HudView.SETTINGS_MENU);
            if (settingsMenu != null) {
                displayObject = this.getChildByName(this.rightObj[0]);
                settingsMenu.x = rightX + (settingsMenu.x - displayObject.x);
            }
            for (i = 0; i < this.rightObj.length; i++) {
                displayObject = this.getChildByName(this.rightObj[i]);
                if (displayObject != null) {
                    displayObject.x = rightX;
                }
            }
        };
        HudView.prototype.changeButtonState = function (btnName, visible, enable) {
            var button = this.getBtn(btnName);
            if (button) {
                button.visible = visible;
                button.setEnable(enable);
            }
        };
        HudView.prototype.setLineCountText = function (value) {
            for (var i = 0; i < this.linesText.length; i++) {
                this.linesText[i].text = value;
            }
        };
        HudView.prototype.dispose = function () {
            this.parent.removeChild(this);
        };
        /////////////////////////////////////////
        //	GETTERS
        /////////////////////////////////////////
        HudView.prototype.getBtn = function (btnName) {
            return this.getChildByName(btnName);
        };
        HudView.prototype.getText = function (textFieldName) {
            return this.getChildByName(textFieldName);
        };
        HudView.prototype.changeTextState = function (textFieldName, visible) {
            var textField = this.getText(textFieldName);
            if (textField) {
                textField.visible = visible;
            }
        };
        HudView.LAYOUT_NAME = "HudView";
        //================================================================
        //------------------MENU------------------------------------------
        //================================================================
        HudView.AUTO_SPIN_TABLE = "autoSpinTable";
        HudView.AUTO_SPIN_TABLE_MASK = "autoSpinTableMask";
        HudView.OPEN_AUTO_SPIN_TABLE_BTN = "openAutoSpinTableBtn";
        HudView.SETTINGS_MENU = "settingsMenu";
        //================================================================
        //------------------BUTTONS NAMES---------------------------------
        //================================================================
        HudView.BACK_TO_LOBBY_BTN = "backToLobbyBtn";
        HudView.SETTINGS_BTN = "settingsBtn";
        HudView.FULL_SCREEN_BTN = "fullScreenBtn";
        //Запускает автоспины
        HudView.START_AUTO_PLAY_BTN = "autoSpinStartBtn";
        //Начинает спин
        HudView.START_SPIN_BTN = "spinBtn";
        //Останавлевает автоспины
        HudView.STOP_AUTO_PLAY_BTN = "autoStopBtn";
        //Ускоряет остановку барабанов
        HudView.STOP_SPIN_BTN = "stopBtn";
        //Запускает рисковую
        HudView.GAMBLE_BTN = "gambleBtn";
        //Запускает бонус
        HudView.START_BONUS_BTN = "bonusStartBtn";
        //Открывает HELP
        HudView.PAY_TABLE_BTN = "payTableBtn";
        //Устанавлевает следующее значение фриспинов из конфига autoPlayCount
        HudView.INC_SPIN_COUNT_BTN = "incSpinsBtn";
        //Устанавлевает предыдущее значение фриспинов из конфига autoPlayCount
        HudView.DEC_SPIN_COUNT_BTN = "decSpinsBtn";
        //Увеличевает количество линий на 1 до максимального
        HudView.INC_LINE_COUNT_BTN = "incLinesBtn";
        //Уменьшает количество линий на 1 до 1
        HudView.DEC_LINE_COUNT_BTN = "decLinesBtn";
        //Меняет количество линий по кругу
        HudView.LINE_COUNT_BTN = "linesBtn";
        //Устанавлевает максимальное количество линий и начинает спин
        HudView.MAX_LINES_BTN = "maxBetBtn"; //this is max_lines_btn that should be named as maxBetBtn as we don't have time to overload all games
        //Устанавлевает следующую ставку до максимальной
        HudView.NEXT_BET_BTN = "nextBetBtn";
        //Устанавлевает предыдущую ставку до первой
        HudView.PREV_BET_BTN = "prevBetBtn";
        //Устанавлевает следующую ставку по кругу
        HudView.BET_BTN = "betBtn";
        //Устанавлевает максимальный мультиплекатор и начинает спин
        HudView.MAX_BET_BTN = "maxLinesBtn";
        //Устанавлевает следующий мультиплекатор по кругу
        HudView.MULTIPLY_BTN = "multiplyBtn";
        //Ускоренный режим прокрутки барабанов
        HudView.TURBO_ON_BTN = "turboOnBtn";
        HudView.TURBO_OFF_BTN = "turboOffBtn";
        //Постоянные автоспины
        HudView.AUTO_ON_BTN = "autoOnBtn";
        HudView.AUTO_OFF_BTN = "autoOffBtn";
        //================================================================
        //------------------TEXT FIELDS NAMES-----------------------------
        //================================================================
        HudView.WIN_TEXT = "winTf";
        HudView.BET_TEXT = "betTf";
        HudView.TOTAL_BET_TEXT = "totalBetTf";
        HudView.PREV_BET_PER_LINE_TEXT = "prevLineBetTf";
        HudView.BET_PER_LINE_TEXT = "lineBetTf";
        HudView.NEXT_BET_PER_LINE_TEXT = "nextLineBetTf";
        HudView.PREV_LINES_COUNT_TEXT = "prevLinesCountTf";
        HudView.LINES_COUNT_TEXT = "linesCountTf";
        HudView.NEXT_LINES_COUNT_TEXT = "nextLinesCountTf";
        HudView.PREV_AUTO_SPINS_COUNT_TEXT = "prevSpinCountTf";
        HudView.AUTO_SPINS_COUNT_TEXT = "spinCountTf";
        HudView.NEXT_AUTO_SPINS_COUNT_TEXT = "nextSpinCountTf";
        HudView.BALANCE_TEXT = "balanceTf";
        return HudView;
    })(Layout);
    engine.HudView = HudView;
})(engine || (engine = {}));
/**
 * Created by Taras on 22.12.2014.
 */
var engine;
(function (engine) {
    var Layout = layout.Layout;
    var DrawView = (function (_super) {
        __extends(DrawView, _super);
        function DrawView() {
            _super.call(this);
        }
        DrawView.LAYOUT_NAME = "DrawView";
        return DrawView;
    })(Layout);
    engine.DrawView = DrawView;
})(engine || (engine = {}));
var engine;
(function (engine) {
    var Layout = layout.Layout;
    var LinesView = (function (_super) {
        __extends(LinesView, _super);
        function LinesView() {
            _super.call(this);
        }
        LinesView.prototype.onInit = function () {
            var lineId = 1;
            var line;
            this.lines = [];
            while ((line = this.getChildByName(LinesView.LINE_PREFIX + lineId)) != null) {
                this.lines.push(line);
                lineId++;
            }
        };
        LinesView.prototype.showLine = function (id) {
            this.lines[id - 1].visible = true;
        };
        LinesView.prototype.hideLine = function (id) {
            this.lines[id - 1].visible = false;
        };
        LinesView.prototype.showLinesFromTo = function (fromId, toId) {
            this.hideAllLines();
            for (var i = fromId - 1; i < toId; i++) {
                this.lines[i].visible = true;
            }
        };
        LinesView.prototype.showLines = function (linesIds) {
            for (var i = 0; i < linesIds.length; i++) {
                this.lines[linesIds[i] - 1].visible = true;
            }
        };
        LinesView.prototype.hideAllLines = function () {
            for (var i = 0; i < this.lines.length; i++) {
                this.lines[i].visible = false;
            }
        };
        LinesView.LAYOUT_NAME = "LinesView";
        LinesView.LINE_PREFIX = "line_";
        return LinesView;
    })(Layout);
    engine.LinesView = LinesView;
})(engine || (engine = {}));
var engine;
(function (engine) {
    var Shape = createjs.Shape;
    var Layout = layout.Layout;
    var LoaderView = (function (_super) {
        __extends(LoaderView, _super);
        function LoaderView() {
            _super.call(this);
            this.buttonsNames = [
                LoaderView.BUTTON_YES,
                LoaderView.BUTTON_NO
            ];
        }
        LoaderView.prototype.onInit = function () {
            this.initAnimation();
            this.initProgressBar();
        };
        LoaderView.prototype.initAnimation = function () {
            var animation = this.getChildByName(LoaderView.ANIMATION_CONTAINER_NAME);
            if (animation != null) {
                animation.play();
            }
        };
        LoaderView.prototype.initProgressBar = function () {
            this.progressBar = this.getChildByName(LoaderView.PROGRESS_BAR);
            this.progressBar.visible = false;
            this.progress = this.progressBar.getChildByName(LoaderView.PROGRESS);
            var bounds = this.progress.getBounds();
            var mask = new Shape();
            mask.graphics.beginFill("0").drawRect(this.progress.x, this.progress.y, bounds.width, bounds.height);
            this.progress.x -= bounds.width;
            this.progress.mask = mask;
            this.startPos = this.progress.x;
        };
        LoaderView.prototype.hideButtons = function () {
            for (var i = 0; i < this.buttonsNames.length; i++) {
                var buttonName = this.buttonsNames[i];
                var button = this.getChildByName(buttonName);
                button.visible = false;
            }
        };
        LoaderView.prototype.showProgressBar = function () {
            this.progressBar.visible = true;
        };
        /**
         * @param value - loading progress [0;1]
         */
        LoaderView.prototype.setProgress = function (value) {
            if (value > 1 || value < 0) {
                throw new Error("loading progress value is not correct");
            }
            this.progress.x = this.startPos + this.progress.getBounds().width * value;
            //console.log("Loading progress: " + value);
        };
        LoaderView.prototype.dispose = function () {
            this.parent.removeChild(this);
        };
        LoaderView.LAYOUT_NAME = "LoaderView";
        LoaderView.ANIMATION_CONTAINER_NAME = "animation";
        LoaderView.PROGRESS_BAR = "progressBar";
        LoaderView.PROGRESS = "progress";
        LoaderView.BUTTON_YES = "yesBtn";
        LoaderView.BUTTON_NO = "noBtn";
        return LoaderView;
    })(Layout);
    engine.LoaderView = LoaderView;
})(engine || (engine = {}));
var engine;
(function (engine) {
    var Layout = layout.Layout;
    var Tween = createjs.Tween;
    var Ticker = createjs.Ticker;
    var RotateScreenView = (function (_super) {
        __extends(RotateScreenView, _super);
        function RotateScreenView() {
            _super.call(this);
            RotateScreenView.ROTATION_TIME = engine.Utils.float2int(RotateScreenView.ROTATION_TIME * Ticker.getFPS());
            RotateScreenView.DELAY_TIME = engine.Utils.float2int(RotateScreenView.DELAY_TIME * Ticker.getFPS());
        }
        RotateScreenView.prototype.onInit = function () {
            this.art = this.getChildAt(0);
            var bounds = this.art.getBounds();
            this.art.regX = bounds.width / 2;
            this.art.regY = bounds.height / 2;
            this.art.x += this.art.regX;
            this.art.y += this.art.regY;
        };
        RotateScreenView.prototype.play = function () {
            if (this.art != undefined) {
                var _art = this.art;
                tween();
                function tween() {
                    _art.rotation = 0;
                    Tween.get(_art, { useTicks: true }).to({ rotation: -90 }, RotateScreenView.ROTATION_TIME).wait(RotateScreenView.DELAY_TIME).call(tween);
                }
            }
        };
        RotateScreenView.prototype.stop = function () {
            if (this.art != undefined) {
                Tween.removeTweens(this.art);
            }
        };
        RotateScreenView.LAYOUT_NAME = "RotateScreenView";
        RotateScreenView.ROTATION_TIME = 0.7;
        RotateScreenView.DELAY_TIME = 0.7;
        return RotateScreenView;
    })(Layout);
    engine.RotateScreenView = RotateScreenView;
})(engine || (engine = {}));
var engine;
(function (engine) {
    var Layout = layout.Layout;
    var AutoPlayView = (function (_super) {
        __extends(AutoPlayView, _super);
        function AutoPlayView() {
            _super.call(this);
            this.buttonsNames = [
                AutoPlayView.YES_BTN,
                AutoPlayView.NO_BTN
            ];
        }
        AutoPlayView.prototype.onInit = function () {
            var lines = this.getChildByName(AutoPlayView.AUTO_COUNT);
            if (lines) {
                lines.y = lines.y + lines.getBounds().height / 1.5;
            }
        };
        AutoPlayView.prototype.setText = function (count) {
            var lines = this.getChildByName(AutoPlayView.AUTO_COUNT);
            if (lines) {
                lines.text = count.toString();
            }
        };
        AutoPlayView.prototype.getButton = function (name) {
            return this.getChildByName(name);
        };
        AutoPlayView.prototype.changeButtonState = function (btnName, visible, enable) {
            var button = this.getButton(btnName);
            if (button) {
                button.visible = visible;
                button.setEnable(enable);
            }
        };
        AutoPlayView.LAYOUT_NAME = "AutoPlayView";
        AutoPlayView.YES_BTN = "yesBtn";
        AutoPlayView.NO_BTN = "noBtn";
        AutoPlayView.AUTO_COUNT = "counter";
        return AutoPlayView;
    })(Layout);
    engine.AutoPlayView = AutoPlayView;
})(engine || (engine = {}));
var engine;
(function (engine) {
    var Layout = layout.Layout;
    var ConnectionView = (function (_super) {
        __extends(ConnectionView, _super);
        function ConnectionView() {
            _super.call(this);
            this.buttonsNames = [
                ConnectionView.OK_BTN
            ];
        }
        ConnectionView.prototype.onInit = function () {
        };
        ConnectionView.prototype.getButton = function (name) {
            return this.getChildByName(name);
        };
        ConnectionView.prototype.changeButtonState = function (btnName, visible, enable) {
            var button = this.getButton(btnName);
            if (button) {
                button.visible = visible;
                button.setEnable(enable);
            }
        };
        ConnectionView.prototype.setText = function (txt) {
            return;
        };
        ConnectionView.LAYOUT_NAME = "ConnectionView";
        ConnectionView.OK_BTN = "okBtn";
        return ConnectionView;
    })(Layout);
    engine.ConnectionView = ConnectionView;
})(engine || (engine = {}));
var engine;
(function (engine) {
    var Layout = layout.Layout;
    var ErrorView = (function (_super) {
        __extends(ErrorView, _super);
        function ErrorView() {
            _super.call(this);
            this.buttonsNames = [
                ErrorView.OK_BTN
            ];
        }
        ErrorView.prototype.onInit = function () {
        };
        ErrorView.prototype.setText = function (txt) {
            var tf = this.getChildByName(ErrorView.ERROR_MESSAGE);
            if (tf) {
                tf.text = txt.join("\n");
            }
        };
        ErrorView.prototype.getButton = function (name) {
            return this.getChildByName(name);
        };
        ErrorView.prototype.changeButtonState = function (btnName, visible, enable) {
            var button = this.getButton(btnName);
            if (button) {
                button.visible = visible;
                button.setEnable(enable);
            }
        };
        ErrorView.LAYOUT_NAME = "ErrorView";
        ErrorView.OK_BTN = "okBtnError";
        ErrorView.ERROR_MESSAGE = "errorMessage";
        return ErrorView;
    })(Layout);
    engine.ErrorView = ErrorView;
})(engine || (engine = {}));
var engine;
(function (engine) {
    var Layout = layout.Layout;
    var Tween = createjs.Tween;
    var Shape = createjs.Shape;
    var JackpotView = (function (_super) {
        __extends(JackpotView, _super);
        function JackpotView() {
            _super.call(this);
        }
        JackpotView.prototype.onInit = function () {
            this.jackpotBlackline = this.getChildByName(JackpotView.JACKPOT_BLACKLINE_NAME);
            this.jackpotScroll = this.getChildByName(JackpotView.JACKPOT_SCROLL_NAME);
            this.jackpotTF = this.jackpotScroll.getChildByName(JackpotView.JACKPOT_TF_NAME);
            this.startScrolling();
            this.setScrollingMask();
        };
        JackpotView.prototype.setScrollingMask = function () {
            this.mask = new Shape();
            this.mask.graphics.beginFill("0").drawRect(this.jackpotBlackline.x + this.x, this.jackpotBlackline.y + this.y, this.jackpotBlackline.getBounds().width, this.jackpotBlackline.getBounds().height);
        };
        JackpotView.prototype.startScrolling = function () {
            var _this = this;
            Tween.get(this.jackpotScroll, { useTicks: true }).to({ x: -1 * this.jackpotBlackline.getBounds().width }, JackpotView.SCROLL_TIME).call(function () {
                _this.jackpotScroll.x = _this.jackpotBlackline.getBounds().width;
                _this.startScrolling();
            });
        };
        JackpotView.prototype.setJackpot = function (jackpot) {
            this.jackpotTF.text = JackpotView.JACKPOT_TF_TEXT + engine.MoneyFormatter.format(jackpot, false);
        };
        JackpotView.SCROLL_TIME = 300;
        JackpotView.LAYOUT_NAME = "JackpotView";
        JackpotView.JACKPOT_SCROLL_NAME = "jackpot_scroll";
        JackpotView.JACKPOT_BLACKLINE_NAME = "jackpot_blackline";
        JackpotView.JACKPOT_TF_NAME = "jackpotTF";
        JackpotView.JACKPOT_TF_TEXT = "";
        return JackpotView;
    })(Layout);
    engine.JackpotView = JackpotView;
})(engine || (engine = {}));
var engine;
(function (engine) {
    var Layout = layout.Layout;
    var JackpotViewPopup = (function (_super) {
        __extends(JackpotViewPopup, _super);
        function JackpotViewPopup() {
            _super.call(this);
            this.buttonsNames = [
                JackpotViewPopup.JACKPOT_CONTINUE_BTN_NAME
            ];
        }
        JackpotViewPopup.prototype.onInit = function () {
            this.jackpotTF = this.getChildByName(JackpotViewPopup.JACKPOT_TF_NAME);
            this.on("click", function () {
            }); // prevent click on lower layers
        };
        JackpotViewPopup.prototype.setText = function (txt) {
            this.jackpotTF.text = txt;
        };
        JackpotViewPopup.prototype.getButton = function (name) {
            return this.getChildByName(name);
        };
        JackpotViewPopup.prototype.changeButtonState = function (btnName, visible, enable) {
            var button = this.getButton(btnName);
            if (button) {
                button.visible = visible;
            }
        };
        JackpotViewPopup.LAYOUT_NAME = "JackpotViewPopup";
        JackpotViewPopup.JACKPOT_TF_NAME = "TxtJackpot";
        JackpotViewPopup.JACKPOT_CONTINUE_BTN_NAME = "btnContinue";
        return JackpotViewPopup;
    })(Layout);
    engine.JackpotViewPopup = JackpotViewPopup;
})(engine || (engine = {}));
var engine;
(function (engine) {
    var DOMElement = createjs.DOMElement;
    var Container = createjs.Container;
    var SetWheelView = (function (_super) {
        __extends(SetWheelView, _super);
        function SetWheelView(width, height) {
            _super.call(this);
            this.width = width;
            this.height = height;
        }
        SetWheelView.prototype.init = function () {
            var canvasHolder = document.getElementById("canvasHolder");
            this.div = document.createElement("div");
            this.div.style.background = "#666";
            canvasHolder.appendChild(this.div);
            var table = document.createElement("table");
            this.div.appendChild(table);
            this.sendBtn = document.createElement("button");
            this.sendBtn.innerHTML = "SET";
            this.div.appendChild(this.sendBtn);
            var caption = document.createElement("caption");
            caption.innerHTML = "SET WHEEL:";
            table.appendChild(caption);
            var tr = document.createElement("tr");
            table.appendChild(tr);
            this.textFields = new Array(this.width);
            for (var i = 0; i < this.width; i++) {
                var td = document.createElement("td");
                tr.appendChild(td);
                var column = new Array(this.height);
                for (var j = 0; j < this.height; j++) {
                    var input = document.createElement("input");
                    input.type = "text";
                    input.value = "-1";
                    input.style.width = "40px";
                    td.appendChild(input);
                    column[j] = input;
                    var br = document.createElement("br");
                    td.appendChild(br);
                }
                this.textFields[i] = column;
            }
            var element = new DOMElement(this.div);
            this.addChild(element);
            this.setVisible(false);
        };
        SetWheelView.prototype.getSendBtn = function () {
            return this.sendBtn;
        };
        SetWheelView.prototype.setVisible = function (value) {
            if (value) {
                this.div.style.display = "";
            }
            else {
                this.div.style.display = "none";
            }
        };
        SetWheelView.prototype.getVisible = function () {
            return this.div.style.display != "none";
        };
        SetWheelView.prototype.getTextValue = function () {
            var result = new Array(this.textFields.length);
            for (var i = 0; i < this.textFields.length; i++) {
                var columnText = this.textFields[i];
                var column = new Array(columnText.length);
                for (var j = 0; j < columnText.length; j++) {
                    column[j] = parseInt(columnText[j].value);
                    if (isNaN(column[j])) {
                        column[j] = -1;
                    }
                }
                result[i] = column;
            }
            return result;
        };
        return SetWheelView;
    })(Container);
    engine.SetWheelView = SetWheelView;
})(engine || (engine = {}));
var engine;
(function (engine) {
    var Tween = createjs.Tween;
    var Ticker = createjs.Ticker;
    var Layout = layout.Layout;
    var SelectItemView = (function (_super) {
        __extends(SelectItemView, _super);
        function SelectItemView() {
            _super.call(this);
            this.allTitles = [
                SelectItemView.SPINS_COUNT,
                SelectItemView.MULTIPLIER_COUNT
            ];
            SelectItemView.DELAY_FOR_NEXT_STEP_UP = engine.Utils.float2int(SelectItemView.DELAY_FOR_NEXT_STEP * Ticker.getFPS());
            SelectItemView.HIDE_POPUP_TIME_UP = engine.Utils.float2int(SelectItemView.HIDE_POPUP_TIME * Ticker.getFPS());
        }
        SelectItemView.prototype.onInit = function () {
            this.bonus = this.getChildByName(SelectItemView.BONUS);
            this.winPopup = this.getChildByName(SelectItemView.WIN_POPUP);
            this.intro = this.getChildByName(SelectItemView.INTRO_ANIM);
            this.items = engine.Utils.fillDisplayObject(this.bonus, SelectItemView.ITEM_BTN);
            this.itemsCount = this.items.length;
            this.texts = engine.Utils.fillDisplayObject(this.bonus, SelectItemView.ITEM_TEXT);
            this.animations = engine.Utils.fillDisplayObject(this.bonus, SelectItemView.WIN_ANIMATION);
            for (var i = 0; i < this.texts.length; ++i) {
                engine.GameController.setTextStroke(this.texts[i], 2, "rgba(0,0,0,1)", 2, 2, 4);
            }
        };
        SelectItemView.prototype.showWinAnimation = function (vo, callback) {
            this.items[vo.id].setEnable(false);
            var animation = this.animations[vo.id];
            var frameCount = animation.spriteSheet.getNumFrames(null);
            animation.visible = true;
            animation.addEventListener("tick", function (eventObj) {
                var currentAnimation = eventObj.currentTarget;
                if (currentAnimation.currentFrame == frameCount - 1) {
                    currentAnimation.stop();
                }
            });
            animation.gotoAndPlay(0);
            var itemText = this.texts[vo.id];
            itemText.text = (vo.type == 2) ? 'x' + vo.winValue : vo.winValue.toString();
            itemText.visible = true;
            itemText.alpha = 0;
            Tween.get(itemText, { useTicks: true }).to({ alpha: 1 }, frameCount / 2).wait(SelectItemView.DELAY_FOR_NEXT_STEP_UP).call(callback);
        };
        SelectItemView.prototype.changeButtonState = function (btnName, visible, enable) {
            var button = this.getBtn(btnName);
            if (button) {
                button.cursor = "pointer";
                button.visible = visible;
            }
        };
        SelectItemView.prototype.setTextPositions = function () {
            if (!this.textsPos) {
                for (var n = 0; n < this.allTitles.length; n++) {
                    var title = this.bonus.getChildByName(this.allTitles[n]);
                    title.y = title.y + title.lineHeight / 2;
                    engine.GameController.setTextStroke(title, 2, "rgba(0,0,0,1)", 0, 0, 4);
                }
                for (var i = 0; i < this.texts.length; i++) {
                    var animText = this.texts[i];
                    animText.y = animText.y + animText.lineHeight / 2;
                    engine.GameController.setTextStroke(this.texts[i], 0, engine.GameController.SHADOW_COLOR, 2, 2, 0);
                }
                var spinsText = this.winPopup.getChildByName(this.allTitles[0]);
                spinsText.y = spinsText.y + spinsText.lineHeight / 2;
                var multiText = this.winPopup.getChildByName(this.allTitles[1]);
                multiText.y = multiText.y + multiText.lineHeight / 2;
                engine.GameController.setFieldStroke(this.winPopup, this.allTitles[0], 2, "rgba(0,0,0,1)", 2, 2, 4);
                engine.GameController.setFieldStroke(this.winPopup, this.allTitles[1], 2, "rgba(0,0,0,1)", 2, 2, 4);
            }
            this.textsPos = true;
        };
        SelectItemView.prototype.getBtn = function (btnName) {
            return this.getChildByName(btnName);
        };
        SelectItemView.prototype.startBonusGame = function () {
            this.setTextPositions();
            this.changeButtonState(SelectItemView.START_BTN, false, false);
            var bonus = this.bonus;
            bonus.alpha = 1;
            var animation = this.intro;
            animation.alpha = 0;
        };
        SelectItemView.prototype.startButtonAnimation = function (btnName) {
            var btn = this.getChildByName(btnName);
            btn.gotoAndPlay(0);
            return;
        };
        SelectItemView.prototype.updateCounters = function (index, count) {
            var title = this.bonus.getChildByName(this.allTitles[index]);
            title.text = count;
        };
        SelectItemView.prototype.showWinPopup = function () {
            this.winPopup.visible = true;
            this.winPopup.alpha = 1;
            this.bonus.visible = false;
        };
        SelectItemView.prototype.setupMobile = function () {
            var startBtn = this.getBtn(SelectItemView.START_BTN);
            startBtn.x = startBtn.x + startBtn.getBounds().width / 2;
        };
        SelectItemView.prototype.showBonus = function () {
            var bonus = this.bonus;
            bonus.visible = true;
            bonus.alpha = 0;
            var animation = this.intro;
            animation.alpha = 1;
            var frameCount = animation.spriteSheet.getNumFrames(null);
            animation.addEventListener("tick", function (eventObj) {
                var currentAnimation = eventObj.currentTarget;
                if (currentAnimation.currentFrame == frameCount - 1) {
                    currentAnimation.stop();
                }
            });
            animation.gotoAndPlay(0);
            this.winPopup.visible = false;
        };
        SelectItemView.prototype.hideAllWinAnimation = function () {
            for (var i = 0; i < this.animations.length; i++) {
                this.animations[i].visible = false;
                this.texts[i].visible = false;
            }
        };
        SelectItemView.prototype.hideWinPopup = function (callback) {
            Tween.get(this.winPopup, { useTicks: true }).to({ alpha: 0 }, SelectItemView.HIDE_POPUP_TIME_UP).call(callback);
        };
        SelectItemView.prototype.getCollectBtn = function () {
            return this.winPopup.getChildByName(SelectItemView.COLLECT_BTN);
        };
        SelectItemView.prototype.getItems = function () {
            return this.items;
        };
        SelectItemView.prototype.getItemCount = function () {
            return this.itemsCount;
        };
        SelectItemView.prototype.setTotalWin = function (spins, multi) {
            var spinsText = this.winPopup.getChildByName(this.allTitles[0]);
            var multiText = this.winPopup.getChildByName(this.allTitles[1]);
            spinsText.text = spins.toString();
            multiText.text = multi.toString();
            //var totalWin:Text = <Text>this.winPopup.getChildByName(SelectItemView.TOTAL_WIN_TEXT);
            //totalWin.text = MoneyFormatter.format(value, true);
            //totalWin.text = value.toString();
        };
        SelectItemView.getItemIdByName = function (itemName) {
            return parseInt(itemName.substr(SelectItemView.ITEM_TEXT.length + 1));
        };
        SelectItemView.LAYOUT_NAME = "SelectItemView";
        SelectItemView.WIN_POPUP = "winPopup";
        SelectItemView.BONUS = "bonus";
        SelectItemView.WIN_ANIMATION = "winAnimation_";
        SelectItemView.ITEM_TEXT = "itemTf_";
        SelectItemView.ITEM_BTN = "itemBtn_";
        SelectItemView.START_BTN = "ButtonGStart";
        SelectItemView.TOTAL_WIN_TEXT = "totalWin";
        SelectItemView.COLLECT_BTN = "collectBtn";
        SelectItemView.SPINS_COUNT = "TxtRot";
        SelectItemView.MULTIPLIER_COUNT = "TxtMul";
        SelectItemView.INTRO_ANIM = "intro";
        SelectItemView.DELAY_FOR_NEXT_STEP = 2;
        SelectItemView.HIDE_POPUP_TIME = 0.5;
        return SelectItemView;
    })(Layout);
    engine.SelectItemView = SelectItemView;
})(engine || (engine = {}));
var engine;
(function (engine) {
    var Tween = createjs.Tween;
    var Ticker = createjs.Ticker;
    var Layout = layout.Layout;
    var GambleView = (function (_super) {
        __extends(GambleView, _super);
        function GambleView() {
            _super.call(this);
            this.buttonsNames = [
                GambleView.COLLECT_BTN,
                GambleView.RED_BTN,
                GambleView.BLACK_BTN
            ];
            this.allTitles = [
                GambleView.BANK_TEXT,
                GambleView.DOUBLE_TEXT,
                GambleView.MESSAGE_TEXT
            ];
            GambleView.INVISIBLE_TIME_UP = engine.Utils.float2int(GambleView.INVISIBLE_TIME * Ticker.getFPS());
            GambleView.DELAY_REMOVE_UP = engine.Utils.float2int(GambleView.DELAY_REMOVE * Ticker.getFPS());
        }
        GambleView.prototype.getButton = function (name) {
            return this.getChildByName(name);
        };
        GambleView.prototype.setBankValue = function (value) {
            var tf = this.getChildByName(this.allTitles[0]);
            if (tf !== null)
                tf.text = engine.MoneyFormatter.format(value, true);
        };
        GambleView.prototype.setDoubleValue = function (value) {
            var tf = this.getChildByName(this.allTitles[1]);
            if (tf !== null)
                tf.text = engine.MoneyFormatter.format(value, true);
        };
        GambleView.prototype.setTextPositions = function () {
            if (!this.textsPos) {
                for (var n = 0; n < this.allTitles.length; n++) {
                    var title = this.getChildByName(this.allTitles[n]);
                    if (title != null) {
                        title.y = title.y + title.lineHeight / 2;
                    }
                }
            }
            this.textsPos = true;
        };
        GambleView.prototype.setMessageText = function (text) {
            var tf = this.getChildByName(GambleView.MESSAGE_TEXT);
            if (tf !== null)
                tf.text = text;
        };
        GambleView.prototype.showCard = function (cardId) {
            var back = this.getChildByName(GambleView.BACK);
            back.visible = false;
            var card = this.getChildByName(GambleView.CARD);
            if (card !== null) {
                card.gotoAndStop(cardId);
                card.visible = true;
            }
        };
        GambleView.prototype.hideCard = function () {
            var back = this.getChildByName(GambleView.BACK);
            back.visible = true;
            var card = this.getChildByName(GambleView.CARD);
            card.visible = false;
        };
        GambleView.prototype.showHistory = function (historyData) {
            var i = 1;
            var cardContainer;
            while ((cardContainer = this.getChildByName(GambleView.HISTORY_CARD_PREFIX + i)) != null) {
                var back = cardContainer.getChildByName(GambleView.BACK);
                back.scaleX = back.scaleY = 1;
                var cards = cardContainer.getChildByName(GambleView.CARD);
                if (i <= historyData.length) {
                    cards.gotoAndStop(historyData[i - 1]);
                    cards.visible = true;
                    back.visible = false;
                }
                else {
                    cards.visible = false;
                    back.visible = true;
                    var scale = cards.getBounds().width / back.getBounds().width;
                    back.scaleX = back.scaleY = back.scaleX * scale;
                }
                i++;
            }
        };
        GambleView.prototype.hideBonus = function (isDelay, callback) {
            var tween = Tween.get(this, { useTicks: true });
            if (isDelay) {
                tween.wait(GambleView.DELAY_REMOVE_UP);
            }
            tween.to({ alpha: 0 }, GambleView.INVISIBLE_TIME_UP);
            tween.call(callback);
        };
        GambleView.LAYOUT_NAME = "GambleView";
        GambleView.INVISIBLE_TIME = 0.5;
        GambleView.DELAY_REMOVE = 3;
        GambleView.MESSAGE_TEXT = "massageTf";
        GambleView.DOUBLE_TEXT = "doubleTf";
        GambleView.BANK_TEXT = "bankTf";
        GambleView.BACK = "back";
        GambleView.CARD = "card";
        GambleView.HISTORY_CARD_PREFIX = "history_";
        GambleView.COLLECT_BTN = "collectBtn";
        GambleView.RED_BTN = "redBtn";
        GambleView.BLACK_BTN = "blackBtn";
        return GambleView;
    })(Layout);
    engine.GambleView = GambleView;
})(engine || (engine = {}));
var engine;
(function (engine) {
    var Tween = createjs.Tween;
    var Ticker = createjs.Ticker;
    var Layout = layout.Layout;
    var Sound = createjs.Sound;
    var GambleView5 = (function (_super) {
        __extends(GambleView5, _super);
        function GambleView5() {
            _super.call(this);
            this.buttonsNames = [
                GambleView5.COLLECT_BTN,
                GambleView5.HALF_BTN,
                GambleView5.DOUBLE_BTN
            ];
            this.allTitles = [
                GambleView5.MESSAGE_TEXT,
                GambleView5.HALF_TEXT,
                GambleView5.DOUBLE_TEXT,
                GambleView5.BANK_TEXT,
                GambleView5.TOTAL_BET_TEXT
            ];
            GambleView5.INVISIBLE_TIME = engine.Utils.float2int(GambleView5.INVISIBLE_TIME * Ticker.getFPS());
            GambleView5.DELAY_REMOVE = engine.Utils.float2int(GambleView5.DELAY_REMOVE * Ticker.getFPS());
        }
        GambleView5.prototype.setTextPositions = function (isMobile) {
            if (!this.textsPos) {
                for (var n = 0; n < this.allTitles.length; n++) {
                    var title = this.getChildByName(this.allTitles[n]);
                    if (title != null) {
                        title.y = (isMobile) ? title.y + title.lineHeight / 2.5 : title.y + title.lineHeight / 2;
                        title.color = '#fff';
                        title.alpha = 1;
                    }
                }
            }
            this.alphaTextFild(4, 0);
            this.textsPos = true;
        };
        GambleView5.prototype.alphaTextFild = function (index, a) {
            var title = this.getChildByName(this.allTitles[index]);
            title.alpha = a;
        };
        GambleView5.prototype.getButton = function (name) {
            return this.getChildByName(name);
        };
        GambleView5.prototype.setBankValue = function (value) {
            var tf = this.getChildByName(GambleView5.BANK_TEXT);
            tf.text = engine.MoneyFormatter.format(value, true);
        };
        GambleView5.prototype.setTotalValue = function (value) {
            var tf = this.getChildByName(GambleView5.TOTAL_BET_TEXT);
            if (tf != null)
                tf.text = engine.MoneyFormatter.format(value, true);
            tf.alpha = 1;
        };
        GambleView5.prototype.setBetValue = function (value, index) {
            console.log(value, index);
            var tf = this.getChildByName(this.allTitles[index]);
            tf.text = engine.MoneyFormatter.format(value, true);
        };
        GambleView5.prototype.setMessageText = function (text) {
            var tf = this.getChildByName(GambleView5.MESSAGE_TEXT);
            if (tf != null)
                tf.text = text;
        };
        GambleView5.prototype.changeButtonState = function (btnName, visible) {
            var button = this.getButton(btnName);
            if (button) {
                button.visible = visible;
            }
        };
        GambleView5.prototype.showCard = function (cardId) {
            if (cardId === void 0) { cardId = 0; }
            var cardName = 'card_1';
            var back = this.getChildByName(cardName);
            back.visible = false;
            var card = this.getChildByName(cardName);
            card.gotoAndStop(cardId);
            card.visible = true;
        };
        GambleView5.prototype.hideCard = function () {
            for (var i = 1; i < 6; i++) {
                var cardName = 'card_' + i;
                var card = this.getChildByName(cardName);
                this.resetLot(card);
            }
        };
        GambleView5.prototype.resetLot = function (card) {
            Tween.get(card, { useTicks: true }).wait(GambleView5.DELAY_REMOVE).call(function () {
                card.gotoAndStop(0);
                card.alpha = 1;
            });
        };
        GambleView5.prototype.setHistory = function (historyData) {
            this.historyData = [];
            this.historyData = historyData;
        };
        GambleView5.prototype.showHistory = function (i) {
            var cardIndex = i + 1;
            var cardName = 'card_' + cardIndex;
            var back = this.getChildByName(cardName);
            back.visible = false;
            var card = this.getChildByName(cardName);
            card.gotoAndStop(this.historyData[i - 1]);
            card.visible = true;
            var n = 1;
            for (var c = 2; c < 6; c++) {
                if ((i + 1) != c) {
                    this.showItem(c, 0.5, n * GambleView5.DELAY_SHOW_ITEM);
                    n++;
                }
            }
        };
        GambleView5.prototype.showItem = function (index, alpha, sec) {
            var cardName = 'card_' + index;
            var card = this.getChildByName(cardName);
            card.visible = false;
            card.gotoAndStop(0);
            card.alpha = 1;
            card.visible = true;
            var back = this.getChildByName(cardName);
            var self = this;
            Tween.get(card).wait(sec).call(function () {
                back.visible = false;
                card.visible = true;
                card.gotoAndStop(self.historyData[index - 2]);
                card.alpha = alpha;
                Sound.play(engine.SoundsList.CARD_TURN);
            }, { self: self, alpha: alpha });
        };
        GambleView5.prototype.hideBonus = function (isDelay, callback) {
            var tween = Tween.get(this, { useTicks: true });
            if (isDelay) {
                tween.wait(GambleView5.DELAY_REMOVE);
            }
            tween.to({ alpha: 0 }, GambleView5.INVISIBLE_TIME);
            tween.call(callback);
        };
        GambleView5.LAYOUT_NAME = "GambleView5";
        GambleView5.INVISIBLE_TIME = 0.5;
        GambleView5.DELAY_REMOVE = 3;
        GambleView5.DELAY_SHOW_ITEM = 300;
        GambleView5.MESSAGE_TEXT = "massageTf";
        GambleView5.DOUBLE_TEXT = "doubleTf";
        GambleView5.BANK_TEXT = "bankTf";
        GambleView5.HALF_TEXT = "halfTf";
        GambleView5.TOTAL_BET_TEXT = "betTf";
        GambleView5.COLLECT_BTN = "collectBtn";
        GambleView5.HALF_BTN = "halfBtn";
        GambleView5.DOUBLE_BTN = "doubleBtn";
        return GambleView5;
    })(Layout);
    engine.GambleView5 = GambleView5;
})(engine || (engine = {}));
var engine;
(function (engine) {
    var Layout = layout.Layout;
    var FreeSpinsView = (function (_super) {
        __extends(FreeSpinsView, _super);
        function FreeSpinsView() {
            _super.call(this);
            this.isSetStrokes = false;
            this.isSetTextPositions = false;
            this.multiplier = 0;
        }
        FreeSpinsView.prototype.onInit = function () {
            this.win = new engine.AnimationTextField(this.getChildByName(FreeSpinsView.WIN_TEXT));
            this.setStrokes();
        };
        FreeSpinsView.prototype.setStrokes = function () {
            if (!this.isSetStrokes) {
                engine.GameController.setFieldStroke(this, FreeSpinsView.LEFT_SPINS_COUNT_TEXT, 0, engine.GameController.SHADOW_COLOR, 1, 1, 2);
                engine.GameController.setFieldStroke(this, FreeSpinsView.WIN_TEXT, 0, engine.GameController.SHADOW_COLOR, 1, 1, 2);
            }
            this.isSetStrokes = true;
        };
        FreeSpinsView.prototype.setLeftSpins = function (value) {
            console.log("setLeftSpinі=" + value);
            var tf = this.getChildByName(FreeSpinsView.LEFT_SPINS_COUNT_TEXT);
            if (tf != null) {
                tf.text = value.toString();
            }
            //this.setStrokes();
        };
        FreeSpinsView.prototype.setMultiplicator = function (value) {
            var tf = this.getChildByName(FreeSpinsView.MULTIPLICATOR_TEXT);
            if (tf != null) {
                tf.text = "x" + value.toString();
            }
        };
        FreeSpinsView.prototype.setWin = function (value, updateTime) {
            if (updateTime === void 0) { updateTime = 0; }
            this.win.setValue(value, updateTime);
        };
        FreeSpinsView.prototype.update = function () {
            if (this.win != null) {
                this.win.update();
            }
        };
        FreeSpinsView.prototype.showMultiplierAnimation = function (multiplier) {
            if (this.multiplier != multiplier) {
                this.multiplier = multiplier;
                this.stopMultipliersAnimation();
                multiplier = Math.min(FreeSpinsView.MULTIPLIER_MAX_VALUE, multiplier);
                var sprite = this.getChildByName(FreeSpinsView.MULTIPLIER_PREFFIX + multiplier);
                if (sprite) {
                    sprite.gotoAndPlay(0);
                    var frameCount = sprite.spriteSheet.getNumFrames(null);
                    sprite.addEventListener("tick", function (eventObj) {
                        var currentAnimation = eventObj.currentTarget;
                        if (currentAnimation.currentFrame == (frameCount - 1 * 2)) {
                            currentAnimation.stop();
                            currentAnimation.removeAllEventListeners("tick");
                        }
                    });
                }
            }
        };
        FreeSpinsView.prototype.stopMultipliersAnimation = function () {
            var mult;
            mult = 0;
            var sprite;
            while (sprite = this.getChildByName(FreeSpinsView.MULTIPLIER_PREFFIX + (++mult))) {
                sprite.gotoAndStop(0);
            }
        };
        FreeSpinsView.LAYOUT_NAME = "FreeSpinsView";
        FreeSpinsView.LEFT_SPINS_COUNT_TEXT = "leftSpinsTf";
        FreeSpinsView.MULTIPLICATOR_TEXT = "multiplicatorTf";
        FreeSpinsView.WIN_TEXT = "winTf";
        FreeSpinsView.MULTIPLIER_PREFFIX = "multipleX";
        FreeSpinsView.MULTIPLIER_BLOCK = "multipleFeature";
        FreeSpinsView.MULTIPLIER_MAX_VALUE = 5;
        FreeSpinsView.titleFields = [FreeSpinsView.LEFT_SPINS_COUNT_TEXT, FreeSpinsView.MULTIPLICATOR_TEXT, FreeSpinsView.WIN_TEXT];
        return FreeSpinsView;
    })(Layout);
    engine.FreeSpinsView = FreeSpinsView;
})(engine || (engine = {}));
var engine;
(function (engine) {
    var Layout = layout.Layout;
    var Tween = createjs.Tween;
    var Ticker = createjs.Ticker;
    var FreeSpinsStartView = (function (_super) {
        __extends(FreeSpinsStartView, _super);
        function FreeSpinsStartView() {
            _super.call(this);
        }
        FreeSpinsStartView.prototype.onInit = function () {
            FreeSpinsStartView.INVISIBLE_TIME_UP = engine.Utils.float2int(FreeSpinsStartView.INVISIBLE_TIME * Ticker.getFPS());
        };
        FreeSpinsStartView.prototype.setFreeSpinsCount = function (value) {
            var tf = this.getChildByName(FreeSpinsStartView.FREE_SPINS_COUNT);
            if (tf != null) {
                tf.text = value.toString();
            }
        };
        FreeSpinsStartView.prototype.hide = function (callback) {
            var tween = Tween.get(this, { useTicks: true });
            tween.to({ alpha: 0 }, FreeSpinsStartView.INVISIBLE_TIME_UP);
            tween.call(callback);
        };
        FreeSpinsStartView.LAYOUT_NAME = "FreeSpinsStartView";
        FreeSpinsStartView.FREE_SPINS_COUNT = "freeSpinsCountTf";
        FreeSpinsStartView.INVISIBLE_TIME = 0.5;
        return FreeSpinsStartView;
    })(Layout);
    engine.FreeSpinsStartView = FreeSpinsStartView;
})(engine || (engine = {}));
var engine;
(function (engine) {
    var Layout = layout.Layout;
    var Tween = createjs.Tween;
    var Ticker = createjs.Ticker;
    var FreeSpinsMoreView = (function (_super) {
        __extends(FreeSpinsMoreView, _super);
        function FreeSpinsMoreView() {
            _super.call(this);
            this.textsPos = false;
        }
        FreeSpinsMoreView.prototype.onInit = function () {
            FreeSpinsMoreView.INVISIBLE_TIME_UP = engine.Utils.float2int(FreeSpinsMoreView.INVISIBLE_TIME * Ticker.getFPS());
            this.setTextPositions();
        };
        FreeSpinsMoreView.prototype.setTextPositions = function () {
            if (!this.textsPos) {
                var title = this.getChildByName(FreeSpinsMoreView.FREE_SPINS_COUNT);
                if (title != null) {
                    title.y += title.lineHeight / 2;
                    console.log("height=" + title.lineHeight + " measureheight=" + title.getMeasuredLineHeight());
                }
            }
            this.textsPos = true;
        };
        FreeSpinsMoreView.prototype.setFreeSpinsCount = function (value) {
            var tf = this.getChildByName(FreeSpinsMoreView.FREE_SPINS_COUNT);
            if (tf != null) {
                tf.text = value.toString();
            }
        };
        FreeSpinsMoreView.prototype.hide = function (callback, time) {
            if (time === void 0) { time = 0; }
            time = engine.Utils.float2int(time * Ticker.getFPS());
            var tween = Tween.get(this, { useTicks: true });
            tween.wait(time).to({ alpha: 0 }, FreeSpinsMoreView.INVISIBLE_TIME_UP);
            tween.call(callback);
        };
        FreeSpinsMoreView.LAYOUT_NAME = "FreeSpinsMoreView";
        FreeSpinsMoreView.FREE_SPINS_COUNT = "freeSpinsCountTf";
        FreeSpinsMoreView.INVISIBLE_TIME = 0.05;
        return FreeSpinsMoreView;
    })(Layout);
    engine.FreeSpinsMoreView = FreeSpinsMoreView;
})(engine || (engine = {}));
var engine;
(function (engine) {
    var Tween = createjs.Tween;
    var Ticker = createjs.Ticker;
    var Layout = layout.Layout;
    var FreeSpinsResultView = (function (_super) {
        __extends(FreeSpinsResultView, _super);
        function FreeSpinsResultView() {
            _super.call(this);
            this.allTitles = [
                FreeSpinsResultView.GAME_WIN,
                FreeSpinsResultView.FEATURE_WIN,
                FreeSpinsResultView.TOTAL_WIN
            ];
        }
        FreeSpinsResultView.prototype.onInit = function () {
            FreeSpinsResultView.INVISIBLE_TIME = engine.Utils.float2int(FreeSpinsResultView.INVISIBLE_TIME * Ticker.getFPS());
        };
        FreeSpinsResultView.prototype.getCollectBtn = function () {
            return this.getChildByName(FreeSpinsResultView.COLLECT_BTN);
        };
        FreeSpinsResultView.prototype.setTextPositions = function (isMobile) {
            if (isMobile === void 0) { isMobile = false; }
            if (!this.textsPos) {
                for (var n = 0; n < this.allTitles.length; n++) {
                    var title = this.getChildByName(this.allTitles[n]);
                    //if(title)title.y = (isMobile) ? title.y + title.lineHeight / 1.5: title.y + title.lineHeight / 2;
                    title.y += title.lineHeight / 2;
                    engine.GameController.setTextStroke(title, 0, engine.GameController.SHADOW_COLOR, 2, 2, 2);
                }
            }
            this.textsPos = true;
        };
        FreeSpinsResultView.prototype.setTotalWin = function (value) {
            var tf = this.getChildByName(FreeSpinsResultView.TOTAL_WIN);
            if (tf != null) {
                tf.text = engine.MoneyFormatter.format(value, true);
            }
        };
        FreeSpinsResultView.prototype.setGameWin = function (value) {
            var tf = this.getChildByName(FreeSpinsResultView.GAME_WIN);
            if (tf != null) {
                tf.text = engine.MoneyFormatter.format(value, true);
            }
        };
        FreeSpinsResultView.prototype.setFeatureWin = function (value) {
            var tf = this.getChildByName(FreeSpinsResultView.FEATURE_WIN);
            if (tf != null) {
                tf.text = engine.MoneyFormatter.format(value, true);
            }
        };
        FreeSpinsResultView.prototype.hide = function (callback) {
            var tween = Tween.get(this, { useTicks: true });
            tween.to({ alpha: 0 }, FreeSpinsResultView.INVISIBLE_TIME);
            tween.call(callback);
        };
        FreeSpinsResultView.LAYOUT_NAME = "FreeSpinsResultView";
        FreeSpinsResultView.INVISIBLE_TIME = 0.5;
        FreeSpinsResultView.COLLECT_BTN = "collectBtn";
        FreeSpinsResultView.GAME_WIN = "TxtWinBaseGame";
        FreeSpinsResultView.FEATURE_WIN = "TxtWinBonusGame";
        FreeSpinsResultView.TOTAL_WIN = "TxtWin";
        return FreeSpinsResultView;
    })(Layout);
    engine.FreeSpinsResultView = FreeSpinsResultView;
})(engine || (engine = {}));
var engine;
(function (engine) {
    var ControllerManager = (function () {
        function ControllerManager() {
            this.observerMap = {};
            this.controllers = [];
        }
        ControllerManager.prototype.register = function (controller) {
            var notificationsNames = controller.listNotification();
            for (var i = 0; i < notificationsNames.length; i++) {
                this.registerObserver(notificationsNames[i], controller);
            }
            this.controllers.push(controller);
        };
        ControllerManager.prototype.remove = function (controller) {
            var notificationsNames = controller.listNotification();
            for (var i = 0; i < notificationsNames.length; i++) {
                this.removeObserver(notificationsNames[i], controller);
            }
            var index = this.controllers.indexOf(controller);
            this.controllers.splice(index, 1);
        };
        ControllerManager.prototype.registerObserver = function (notificationName, controller) {
            if (this.observerMap[notificationName] != null) {
                this.observerMap[notificationName].push(controller);
            }
            else {
                this.observerMap[notificationName] = [controller];
            }
        };
        ControllerManager.prototype.removeObserver = function (notificationName, controller) {
            var controllers = this.observerMap[notificationName];
            for (var i = 0; i < controllers.length; i++) {
                if (controllers[i] == controller) {
                    controllers.splice(i, 1);
                }
            }
        };
        ControllerManager.prototype.send = function (notificationName, data) {
            //console.log(notificationName, "send")
            var controllers = this.observerMap[notificationName];
            if (controllers != null) {
                for (var i = 0; i < controllers.length; i++) {
                    controllers[i].handleNotification(notificationName, data);
                }
            }
        };
        ControllerManager.prototype.onEnterFrame = function () {
            for (var i = 0; i < this.controllers.length; i++) {
                this.controllers[i].onEnterFrame();
            }
        };
        return ControllerManager;
    })();
    engine.ControllerManager = ControllerManager;
})(engine || (engine = {}));
var engine;
(function (engine) {
    var BaseController = (function () {
        function BaseController(manager) {
            this.manager = manager;
        }
        BaseController.prototype.init = function () {
            this.manager.register(this);
        };
        BaseController.prototype.listNotification = function () {
            return [];
        };
        BaseController.prototype.handleNotification = function (message, data) {
        };
        BaseController.prototype.send = function (message, data) {
            if (data === void 0) { data = null; }
            console.log(data, message);
            this.manager.send(message, data);
        };
        BaseController.prototype.dispose = function () {
            console.log("Dispose controller: " + engine.Utils.getClassName(this));
            this.manager.remove(this);
        };
        BaseController.prototype.onEnterFrame = function () {
        };
        return BaseController;
    })();
    engine.BaseController = BaseController;
})(engine || (engine = {}));
var engine;
(function (engine) {
    var AutoSpinState = (function () {
        function AutoSpinState(controller) {
            this.controller = controller;
            this.common = controller.common;
            this.view = controller.view;
        }
        AutoSpinState.prototype.start = function (data) {
            this.autoSpinCount = data;
            this.lastSpinCount = this.autoSpinCount;
            this.hardStop = false;
            this.view.changeButtonState(engine.HudView.SETTINGS_BTN, true, false);
            this.view.changeButtonState(engine.HudView.OPEN_AUTO_SPIN_TABLE_BTN, false, false);
            this.view.changeButtonState(engine.HudView.START_AUTO_PLAY_BTN, false, false);
            this.view.changeButtonState(engine.HudView.STOP_AUTO_PLAY_BTN, true, true);
            this.view.changeTextState(engine.HudView.AUTO_SPINS_COUNT_TEXT, true);
            this.view.changeButtonState(engine.HudView.GAMBLE_BTN, true, false);
            this.view.changeButtonState(engine.HudView.INC_SPIN_COUNT_BTN, true, false);
            this.view.changeButtonState(engine.HudView.DEC_SPIN_COUNT_BTN, true, false);
            this.view.changeButtonState(engine.HudView.INC_LINE_COUNT_BTN, true, false);
            this.view.changeButtonState(engine.HudView.DEC_LINE_COUNT_BTN, true, false);
            this.view.changeButtonState(engine.HudView.NEXT_BET_BTN, true, false);
            this.view.changeButtonState(engine.HudView.PREV_BET_BTN, true, false);
            this.view.changeButtonState(engine.HudView.MAX_LINES_BTN, true, false);
            this.view.changeButtonState(engine.HudView.MAX_BET_BTN, true, false);
            this.view.changeButtonState(engine.HudView.START_SPIN_BTN, true, false);
            this.view.changeButtonState(engine.HudView.START_BONUS_BTN, false, false);
            this.view.changeButtonState(engine.HudView.STOP_SPIN_BTN, false, false);
            this.view.changeButtonState(engine.HudView.PAY_TABLE_BTN, true, false);
            this.view.changeButtonState(engine.HudView.BET_BTN, true, false);
            this.view.changeButtonState(engine.HudView.LINE_COUNT_BTN, true, false);
            this.view.changeButtonState(engine.HudView.MULTIPLY_BTN, true, false);
            this.controller.send(engine.NotificationList.CLOSE_SETTINGS_MENU);
            this.controller.send(engine.NotificationList.REMOVE_WIN_LINES);
            this.controller.tryStartSpin();
        };
        AutoSpinState.prototype.handleNotification = function (message, data) {
            switch (message) {
                case engine.NotificationList.TRY_START_SPIN:
                    {
                        if (!this.controller.common.isAuto) {
                            this.autoSpinCount--;
                            this.updateAutoSpinCountText();
                        }
                        this.controller.send(engine.NotificationList.COLLECT_WIN_TF);
                        this.controller.send(engine.NotificationList.START_SPIN);
                        this.controller.send(engine.NotificationList.START_SPIN_AUDIO);
                        this.controller.send(engine.NotificationList.SERVER_SEND_SPIN);
                        break;
                    }
                case engine.NotificationList.STOPPED_ALL_REELS:
                    {
                        var bonus = this.common.server.bonus;
                        if (bonus == null || bonus.type == engine.BonusTypes.GAMBLE) {
                            this.controller.send(engine.NotificationList.SHOW_WIN_LINES, true);
                        }
                        else {
                            this.view.changeButtonState(engine.HudView.STOP_AUTO_PLAY_BTN, false, false);
                            this.view.changeButtonState(engine.HudView.START_AUTO_PLAY_BTN, true, false);
                            var autoPlayText = this.view.getText(engine.HudView.AUTO_SPINS_COUNT_TEXT);
                            if (autoPlayText)
                                autoPlayText.visible = this.common.config.showAllTimeSpinCount;
                            this.controller.send(engine.NotificationList.SHOW_WIN_LINES, true);
                        }
                        this.controller.send(engine.NotificationList.SHOW_WIN_TF);
                        break;
                    }
                case engine.NotificationList.RECURSION_PROCESSED:
                    {
                        var bonus = this.common.server.bonus;
                        if (this.common.server.recursion.length) {
                        }
                        else if (bonus != null && bonus.type != engine.BonusTypes.GAMBLE) {
                            this.controller.changeState(engine.BonusState.NAME);
                        }
                        else if (this.autoSpinCount == 0) {
                            if (!this.hardStop) {
                                this.view.changeButtonState(engine.HudView.STOP_AUTO_PLAY_BTN, false, false);
                                var autoPlayText = this.view.getText(engine.HudView.AUTO_SPINS_COUNT_TEXT);
                                autoPlayText.visible = this.common.config.showAllTimeSpinCount;
                                this.controller.send(engine.NotificationList.SHOW_AUTO_MODAL, this.lastSpinCount);
                            }
                            else {
                                this.controller.send(engine.NotificationList.END_AUTO_PLAY);
                                this.controller.changeState(engine.DefaultState.NAME);
                            }
                            return;
                        }
                        else {
                            this.controller.send(engine.NotificationList.REMOVE_WIN_LINES);
                            this.controller.tryStartSpin();
                        }
                        break;
                    }
                case engine.NotificationList.AUTO_PLAY_COMP:
                    {
                        this.controller.send(engine.NotificationList.END_AUTO_PLAY);
                        this.controller.changeState(engine.DefaultState.NAME);
                        return;
                        break;
                    }
                case engine.NotificationList.AUTO_PLAY_CONT:
                    {
                        this.start(this.lastSpinCount);
                        break;
                    }
                case engine.NotificationList.OK_BTN_ERROR_CLICKED:
                    {
                        break;
                    }
            }
        };
        AutoSpinState.prototype.onBtnClick = function (buttonName) {
            switch (buttonName) {
                case engine.HudView.STOP_AUTO_PLAY_BTN:
                    {
                        this.autoSpinCount = 0;
                        this.hardStop = true;
                        var autoPlayText = this.view.getText(engine.HudView.AUTO_SPINS_COUNT_TEXT);
                        autoPlayText.visible = this.common.config.showAllTimeSpinCount;
                        this.view.changeButtonState(engine.HudView.STOP_AUTO_PLAY_BTN, false, false);
                        this.view.changeButtonState(engine.HudView.START_AUTO_PLAY_BTN, true, false);
                        this.view.changeButtonState(engine.HudView.OPEN_AUTO_SPIN_TABLE_BTN, true, false);
                        break;
                    }
            }
        };
        AutoSpinState.prototype.updateAutoSpinCountText = function () {
            var autoPlayText = this.view.getText(engine.HudView.AUTO_SPINS_COUNT_TEXT);
            if (autoPlayText != null) {
                autoPlayText.text = this.autoSpinCount.toString();
            }
        };
        AutoSpinState.prototype.end = function () {
            this.controller.common.isAuto = false;
            this.view.changeButtonState(engine.HudView.AUTO_OFF_BTN, true, true);
        };
        AutoSpinState.NAME = "AutoSpinState";
        return AutoSpinState;
    })();
    engine.AutoSpinState = AutoSpinState;
})(engine || (engine = {}));
var engine;
(function (engine) {
    var BonusState = (function () {
        function BonusState(controller) {
            this.controller = controller;
            this.common = controller.common;
            this.view = controller.view;
        }
        BonusState.prototype.start = function (data) {
            // update buttons
            if (this.common.isMobile) {
                this.view.changeButtonState(engine.HudView.SETTINGS_BTN, false, false);
                this.view.changeButtonState(engine.HudView.GAMBLE_BTN, false, false);
                this.view.changeButtonState(engine.HudView.PAY_TABLE_BTN, false, false);
            }
            else {
                this.view.changeButtonState(engine.HudView.SETTINGS_BTN, true, false);
                this.view.changeButtonState(engine.HudView.GAMBLE_BTN, true, false);
                this.view.changeButtonState(engine.HudView.PAY_TABLE_BTN, true, false);
            }
            this.view.changeButtonState(engine.HudView.OPEN_AUTO_SPIN_TABLE_BTN, true, false);
            this.view.changeButtonState(engine.HudView.STOP_SPIN_BTN, false, false);
            this.view.changeButtonState(engine.HudView.START_AUTO_PLAY_BTN, true, false);
            this.view.changeButtonState(engine.HudView.STOP_AUTO_PLAY_BTN, false, false);
            this.view.changeTextState(engine.HudView.AUTO_SPINS_COUNT_TEXT, false);
            this.view.changeButtonState(engine.HudView.INC_SPIN_COUNT_BTN, true, false);
            this.view.changeButtonState(engine.HudView.DEC_SPIN_COUNT_BTN, true, false);
            this.view.changeButtonState(engine.HudView.INC_LINE_COUNT_BTN, true, false);
            this.view.changeButtonState(engine.HudView.DEC_LINE_COUNT_BTN, true, false);
            this.view.changeButtonState(engine.HudView.MAX_BET_BTN, true, false);
            this.view.changeButtonState(engine.HudView.BET_BTN, true, false);
            this.view.changeButtonState(engine.HudView.LINE_COUNT_BTN, true, false);
            this.view.changeButtonState(engine.HudView.MULTIPLY_BTN, true, false);
            this.view.changeButtonState(engine.HudView.NEXT_BET_BTN, true, false);
            this.view.changeButtonState(engine.HudView.PREV_BET_BTN, true, false);
            this.view.changeButtonState(engine.HudView.MAX_LINES_BTN, true, false);
            if (this.common.server.bonus.type == engine.BonusTypes.GAMBLE) {
                this.view.changeButtonState(engine.HudView.START_SPIN_BTN, true, false);
                this.view.changeButtonState(engine.HudView.START_BONUS_BTN, false, false);
                if (this.common.isMobile)
                    this.containerHudCVisibility(false);
            }
            else {
                var bonusBtn = this.view.getBtn(engine.HudView.START_BONUS_BTN);
                if (bonusBtn != null) {
                    this.view.changeButtonState(engine.HudView.START_SPIN_BTN, false, false);
                    this.view.changeButtonState(engine.HudView.START_BONUS_BTN, true, true);
                }
                else {
                    this.view.changeButtonState(engine.HudView.START_SPIN_BTN, true, true);
                }
            }
            if (this.common.server.bonus.type == engine.BonusTypes.SELECT_GAME) {
                this.view.changeButtonState(engine.HudView.START_BONUS_BTN, false, false);
                this.view.changeButtonState(engine.HudView.START_SPIN_BTN, true, false);
            }
            this.controller.send(engine.NotificationList.COLLECT_WIN_TF);
            this.controller.send(engine.NotificationList.REMOVE_WIN_LINES);
            this.controller.send(engine.NotificationList.CREATE_BONUS);
        };
        BonusState.prototype.containerHudCVisibility = function (state) {
            this.view.visible = state;
        };
        BonusState.prototype.handleNotification = function (message, data) {
            switch (message) {
                case engine.NotificationList.END_BONUS:
                    {
                        // remove bonus VO
                        console.log('end bonus');
                        if (this.common.isMobile) {
                            this.containerHudCVisibility(true);
                        }
                        this.common.server.bonus = null;
                        this.controller.changeState(engine.DefaultState.NAME);
                        break;
                    }
            }
        };
        BonusState.prototype.onBtnClick = function (buttonName) {
            switch (buttonName) {
                case engine.HudView.START_SPIN_BTN:
                case engine.HudView.START_BONUS_BTN:
                    {
                        this.view.changeButtonState(engine.HudView.START_SPIN_BTN, true, false);
                        this.view.changeButtonState(engine.HudView.START_BONUS_BTN, false, false);
                        this.controller.send(engine.NotificationList.START_BONUS, 'spins');
                        break;
                    }
            }
        };
        BonusState.prototype.end = function () {
        };
        BonusState.NAME = "BonusState";
        return BonusState;
    })();
    engine.BonusState = BonusState;
})(engine || (engine = {}));
var engine;
(function (engine) {
    var RegularSpinState = (function () {
        function RegularSpinState(controller) {
            this.controller = controller;
            this.common = controller.common;
            this.view = controller.view;
        }
        RegularSpinState.prototype.start = function (data) {
            this.view.changeButtonState(engine.HudView.SETTINGS_BTN, true, false);
            this.view.changeButtonState(engine.HudView.OPEN_AUTO_SPIN_TABLE_BTN, true, false);
            this.view.changeButtonState(engine.HudView.START_AUTO_PLAY_BTN, true, false);
            this.view.changeButtonState(engine.HudView.STOP_AUTO_PLAY_BTN, false, false);
            this.view.changeButtonState(engine.HudView.GAMBLE_BTN, true, false);
            this.view.changeButtonState(engine.HudView.INC_SPIN_COUNT_BTN, true, false);
            this.view.changeButtonState(engine.HudView.DEC_SPIN_COUNT_BTN, true, false);
            this.view.changeButtonState(engine.HudView.INC_LINE_COUNT_BTN, true, false);
            this.view.changeButtonState(engine.HudView.DEC_LINE_COUNT_BTN, true, false);
            this.view.changeButtonState(engine.HudView.NEXT_BET_BTN, true, false);
            this.view.changeButtonState(engine.HudView.PREV_BET_BTN, true, false);
            this.view.changeButtonState(engine.HudView.MAX_BET_BTN, true, false);
            this.view.changeButtonState(engine.HudView.MAX_LINES_BTN, true, false);
            this.view.changeButtonState(engine.HudView.START_BONUS_BTN, false, false);
            this.view.changeButtonState(engine.HudView.PAY_TABLE_BTN, true, false);
            this.view.changeButtonState(engine.HudView.BET_BTN, true, false);
            this.view.changeButtonState(engine.HudView.LINE_COUNT_BTN, true, false);
            this.view.changeButtonState(engine.HudView.MULTIPLY_BTN, true, false);
            if (this.view.getBtn(engine.HudView.STOP_SPIN_BTN)) {
                this.view.changeButtonState(engine.HudView.START_SPIN_BTN, false, false);
            }
            else {
                this.view.changeButtonState(engine.HudView.START_SPIN_BTN, true, false);
            }
            this.view.changeButtonState(engine.HudView.STOP_SPIN_BTN, true, true);
            this.controller.send(engine.NotificationList.CLOSE_SETTINGS_MENU);
            this.controller.send(engine.NotificationList.COLLECT_WIN_TF);
            this.controller.send(engine.NotificationList.REMOVE_WIN_LINES);
            this.controller.send(engine.NotificationList.START_SPIN);
            this.controller.send(engine.NotificationList.START_SPIN_AUDIO);
            this.controller.send(engine.NotificationList.SERVER_SEND_SPIN);
        };
        RegularSpinState.prototype.handleNotification = function (message, data) {
            switch (message) {
                case engine.NotificationList.KEYBOARD_CLICK:
                    {
                        if (data == engine.Keyboard.SPACE) {
                            this.stopSpin();
                        }
                        break;
                    }
                case engine.NotificationList.ON_SCREEN_CLICK:
                    {
                        this.stopSpin();
                        break;
                    }
                case engine.NotificationList.STOPPED_ALL_REELS:
                    {
                        var bonus = this.common.server.bonus;
                        if (bonus == null || bonus.type == engine.BonusTypes.GAMBLE) {
                            this.controller.changeState(engine.DefaultState.NAME);
                            this.controller.send(engine.NotificationList.SHOW_WIN_LINES, this.common.config.winLinesTogether);
                        }
                        else {
                            //this.view.changeButtonState(HudView.STOP_SPIN_BTN, false, false);
                            //this.view.changeButtonState(HudView.START_SPIN_BTN, true, false);
                            this.controller.send(engine.NotificationList.SHOW_WIN_LINES, true);
                        }
                        this.controller.send(engine.NotificationList.SHOW_WIN_TF);
                        if (!this.common.server.recursion.length) {
                            this.controller.send(engine.NotificationList.UPDATE_BALANCE_TF, this.controller.common.server.getBalance());
                        }
                        break;
                    }
                case engine.NotificationList.RECURSION_PROCESSED:
                    {
                        var bonus = this.common.server.bonus;
                        if (bonus != null && bonus.type != engine.BonusTypes.GAMBLE) {
                            this.controller.changeState(engine.BonusState.NAME);
                        }
                        break;
                    }
                case engine.NotificationList.EXPRESS_STOP:
                    {
                        this.view.changeButtonState(engine.HudView.STOP_SPIN_BTN, false, false);
                        this.view.changeButtonState(engine.HudView.START_SPIN_BTN, true, false);
                        break;
                    }
            }
        };
        RegularSpinState.prototype.onBtnClick = function (buttonName) {
            switch (buttonName) {
                case engine.HudView.STOP_SPIN_BTN:
                    {
                        this.stopSpin();
                        break;
                    }
            }
        };
        RegularSpinState.prototype.stopSpin = function () {
            this.controller.send(engine.NotificationList.EXPRESS_STOP);
        };
        RegularSpinState.prototype.end = function () {
        };
        RegularSpinState.NAME = "RegularSpinState";
        return RegularSpinState;
    })();
    engine.RegularSpinState = RegularSpinState;
})(engine || (engine = {}));
var engine;
(function (engine) {
    var PayTableState = (function () {
        function PayTableState(controller) {
            this.controller = controller;
            this.common = controller.common;
            this.view = controller.view;
        }
        PayTableState.prototype.start = function (data) {
            // update buttons
            this.view.changeButtonState(engine.HudView.START_AUTO_PLAY_BTN, !this.common.isMobile, false);
            this.view.changeButtonState(engine.HudView.GAMBLE_BTN, !this.common.isMobile, false);
            this.view.changeButtonState(engine.HudView.START_SPIN_BTN, !this.common.isMobile, false);
            this.view.changeButtonState(engine.HudView.PAY_TABLE_BTN, !this.common.isMobile, false);
            this.view.changeButtonState(engine.HudView.OPEN_AUTO_SPIN_TABLE_BTN, true, false);
            this.view.changeButtonState(engine.HudView.SETTINGS_BTN, false, false);
            this.view.changeButtonState(engine.HudView.STOP_AUTO_PLAY_BTN, false, false);
            this.view.changeButtonState(engine.HudView.NEXT_BET_BTN, true, false);
            this.view.changeButtonState(engine.HudView.INC_LINE_COUNT_BTN, true, false);
            this.view.changeButtonState(engine.HudView.INC_SPIN_COUNT_BTN, true, false);
            this.view.changeButtonState(engine.HudView.PREV_BET_BTN, true, false);
            this.view.changeButtonState(engine.HudView.DEC_LINE_COUNT_BTN, true, false);
            this.view.changeButtonState(engine.HudView.DEC_SPIN_COUNT_BTN, true, false);
            this.view.changeButtonState(engine.HudView.MAX_LINES_BTN, true, false);
            this.view.changeButtonState(engine.HudView.MAX_BET_BTN, true, false);
            this.view.changeButtonState(engine.HudView.START_BONUS_BTN, false, false);
            this.view.changeButtonState(engine.HudView.STOP_SPIN_BTN, false, false);
            this.view.changeButtonState(engine.HudView.BET_BTN, true, false);
            this.view.changeButtonState(engine.HudView.LINE_COUNT_BTN, true, false);
            this.view.changeButtonState(engine.HudView.MULTIPLY_BTN, true, false);
            if (this.common.config.showAllTimeSpinCount) {
                this.view.getText(engine.HudView.AUTO_SPINS_COUNT_TEXT).visible = false;
            }
            this.controller.send(engine.NotificationList.CLOSE_SETTINGS_MENU);
            this.controller.send(engine.NotificationList.OPEN_PAY_TABLE);
        };
        PayTableState.prototype.handleNotification = function (message, data) {
            switch (message) {
                case engine.NotificationList.CLOSE_PAY_TABLE:
                    {
                        this.controller.changeState(engine.DefaultState.NAME);
                        break;
                    }
            }
        };
        PayTableState.prototype.onBtnClick = function (buttonName) {
        };
        PayTableState.prototype.end = function () {
            if (this.common.config.showAllTimeSpinCount) {
                this.view.getText(engine.HudView.AUTO_SPINS_COUNT_TEXT).visible = true;
            }
        };
        PayTableState.NAME = "PayTableState";
        return PayTableState;
    })();
    engine.PayTableState = PayTableState;
})(engine || (engine = {}));
var engine;
(function (engine) {
    var DefaultState = (function () {
        function DefaultState(controller) {
            this.controller = controller;
            this.common = controller.common;
            this.view = controller.view;
        }
        DefaultState.prototype.start = function (data) {
            this.view.changeButtonState(engine.HudView.AUTO_OFF_BTN, true, true);
            this.controller.send(engine.NotificationList.SHOW_HEADER);
            if (!this.common.server.recursion.length) {
                this.updateState();
            }
        };
        DefaultState.prototype.updateState = function () {
            this.view.changeButtonState(engine.HudView.OPEN_AUTO_SPIN_TABLE_BTN, true, true);
            var autoPlayText = this.view.getText(engine.HudView.AUTO_SPINS_COUNT_TEXT);
            if (autoPlayText)
                autoPlayText.visible = this.common.config.showAllTimeSpinCount;
            this.view.changeButtonState(engine.HudView.SETTINGS_BTN, true, true);
            this.view.changeButtonState(engine.HudView.MAX_LINES_BTN, true, true);
            this.view.changeButtonState(engine.HudView.START_AUTO_PLAY_BTN, true, true);
            this.view.changeButtonState(engine.HudView.STOP_AUTO_PLAY_BTN, false, false);
            this.view.changeButtonState(engine.HudView.GAMBLE_BTN, true, false);
            this.view.changeButtonState(engine.HudView.MAX_BET_BTN, true, true);
            this.view.changeButtonState(engine.HudView.START_SPIN_BTN, true, true);
            this.view.changeButtonState(engine.HudView.STOP_SPIN_BTN, false, false);
            this.view.changeButtonState(engine.HudView.START_BONUS_BTN, false, false);
            this.view.changeButtonState(engine.HudView.PAY_TABLE_BTN, this.common.payTableLoaded, this.common.payTableLoaded);
            this.view.changeButtonState(engine.HudView.BET_BTN, true, true);
            this.view.changeButtonState(engine.HudView.LINE_COUNT_BTN, true, true);
            this.view.changeButtonState(engine.HudView.MULTIPLY_BTN, true, true);
            this.controller.updateAutoPlayBtn();
            this.updateGambleBtn();
            this.controller.updateAutoSpinCountText();
            this.updateBetBtn();
            this.updateLineBtn();
        };
        DefaultState.prototype.handleNotification = function (message, data) {
            switch (message) {
                case engine.NotificationList.KEYBOARD_CLICK:
                    {
                        if (data == engine.Keyboard.SPACE) {
                            this.controller.tryStartSpin();
                        }
                        break;
                    }
                case engine.NotificationList.ON_SCREEN_CLICK:
                    {
                        this.controller.tryStartSpin();
                        break;
                    }
                case engine.NotificationList.TRY_START_SPIN:
                    {
                        this.controller.changeState(engine.RegularSpinState.NAME);
                        break;
                    }
                case engine.NotificationList.LOAD_PAY_TABLE:
                    {
                        this.view.changeButtonState(engine.HudView.PAY_TABLE_BTN, true, true);
                        break;
                    }
                case engine.NotificationList.TRY_START_AUTO_PLAY:
                    {
                        this.controller.send(engine.NotificationList.TRY_START_AUTO_PLAY_AUDIO);
                        var balanceAfterSpin = this.common.server.getBalance() - this.common.server.getTotalBet();
                        if (balanceAfterSpin >= 0) {
                            this.controller.changeState(engine.AutoSpinState.NAME, data);
                        }
                        else {
                            this.controller.send(engine.NotificationList.SHOW_ERRORS, [engine.ErrorController.ERROR_NO_MONEY_STR]);
                        }
                        break;
                    }
                case engine.NotificationList.RECURSION_PROCESSED:
                    {
                        this.updateState();
                        break;
                    }
            }
        };
        DefaultState.prototype.onBtnClick = function (buttonName) {
            switch (buttonName) {
                case engine.HudView.START_SPIN_BTN:
                    {
                        this.controller.tryStartSpin();
                        break;
                    }
                case engine.HudView.PAY_TABLE_BTN:
                    {
                        this.controller.changeState(engine.PayTableState.NAME);
                        break;
                    }
                case engine.HudView.LINE_COUNT_BTN:
                    {
                        this.common.server.setNextLineCount(true);
                        this.controller.updateLineCountText();
                        this.controller.updateTotalBetText();
                        this.controller.send(engine.NotificationList.CHANGED_LINE_COUNT);
                        break;
                    }
                case engine.HudView.INC_LINE_COUNT_BTN:
                    {
                        this.common.server.setNextLineCount(false);
                        this.updateLineBtn();
                        this.view.changeButtonState(engine.HudView.DEC_LINE_COUNT_BTN, true, true);
                        this.controller.updateLineCountText();
                        this.controller.updateTotalBetText();
                        this.controller.send(engine.NotificationList.CHANGED_LINE_COUNT);
                        break;
                    }
                case engine.HudView.DEC_LINE_COUNT_BTN:
                    {
                        this.common.server.setPrevLineCount(false);
                        this.updateLineBtn();
                        this.view.changeButtonState(engine.HudView.INC_LINE_COUNT_BTN, true, true);
                        this.controller.updateLineCountText();
                        this.controller.updateTotalBetText();
                        this.controller.send(engine.NotificationList.CHANGED_LINE_COUNT);
                        break;
                    }
                case engine.HudView.MULTIPLY_BTN:
                    {
                        this.common.server.setNextMultiply();
                        this.controller.updateBetPerLineText();
                        this.controller.updateTotalBetText();
                        break;
                    }
                case engine.HudView.BET_BTN:
                    {
                        this.common.server.setNextBet(true);
                        this.controller.updateBetText();
                        this.controller.updateBetPerLineText();
                        this.controller.updateTotalBetText();
                        break;
                    }
                case engine.HudView.NEXT_BET_BTN:
                    {
                        this.common.server.setNextBet(false);
                        this.updateBetBtn();
                        this.controller.updateBetText();
                        this.controller.updateBetPerLineText();
                        this.controller.updateTotalBetText();
                        break;
                    }
                case engine.HudView.PREV_BET_BTN:
                    {
                        this.common.server.setPrevBet(false);
                        this.updateBetBtn();
                        this.controller.updateBetText();
                        this.controller.updateBetPerLineText();
                        this.controller.updateTotalBetText();
                        break;
                    }
                case engine.HudView.MAX_BET_BTN:
                    {
                        this.common.server.setMaxMultiply();
                        this.common.server.setMaxBet();
                        this.updateBetBtn();
                        this.common.server.setMaxLines();
                        this.updateLineBtn();
                        this.controller.updateBetPerLineText();
                        this.controller.updateLineCountText();
                        this.controller.updateTotalBetText();
                        this.controller.updateBetText();
                        this.controller.updateBetPerLineText();
                        this.controller.tryStartSpin();
                        break;
                    }
                case engine.HudView.MAX_LINES_BTN:
                    {
                        this.common.server.setMaxLines();
                        this.controller.updateLineCountText();
                        this.controller.updateTotalBetText();
                        this.controller.tryStartSpin();
                        break;
                    }
                case engine.HudView.OPEN_AUTO_SPIN_TABLE_BTN:
                    {
                        this.controller.send(engine.NotificationList.OPEN_AUTO_SPIN_MENU);
                        break;
                    }
                case engine.HudView.SETTINGS_BTN:
                    {
                        this.controller.send(engine.NotificationList.TOGGLE_SETTINGS_MENU);
                        break;
                    }
                case engine.HudView.START_AUTO_PLAY_BTN:
                    {
                        var autoPlayCount = this.controller.autoPlayCount[this.controller.autoPlayCountIdx];
                        this.controller.send(engine.NotificationList.TRY_START_AUTO_PLAY, autoPlayCount);
                        break;
                    }
                case engine.HudView.INC_SPIN_COUNT_BTN:
                    {
                        this.controller.autoPlayCountIdx++;
                        this.controller.updateAutoPlayBtn();
                        this.controller.updateAutoSpinCountText();
                        break;
                    }
                case engine.HudView.DEC_SPIN_COUNT_BTN:
                    {
                        this.controller.autoPlayCountIdx--;
                        this.controller.updateAutoPlayBtn();
                        this.controller.updateAutoSpinCountText();
                        break;
                    }
                case engine.HudView.GAMBLE_BTN:
                    {
                        this.controller.changeState(engine.BonusState.NAME);
                        this.controller.send(engine.NotificationList.START_BONUS);
                        break;
                    }
            }
        };
        DefaultState.prototype.updateGambleBtn = function () {
            console.log("Update gamble btn");
            var bonusVO = this.common.server.bonus;
            if (bonusVO != null && bonusVO.type == engine.BonusTypes.GAMBLE) {
                this.view.changeButtonState(engine.HudView.GAMBLE_BTN, true, true);
            }
            //var button:Button = this.view.getBtn(HudView.GAMBLE_BTN);
            //if (button != null){
            //	console.log("gambleBtn="+button.getEnable()+" "+button.id);
            //}
        };
        DefaultState.prototype.updateBetBtn = function () {
            this.view.changeButtonState(engine.HudView.NEXT_BET_BTN, true, this.common.server.isHasNextBet());
            this.view.changeButtonState(engine.HudView.PREV_BET_BTN, true, this.common.server.isHasPrevBet());
        };
        DefaultState.prototype.updateLineBtn = function () {
            this.view.changeButtonState(engine.HudView.INC_LINE_COUNT_BTN, true, this.common.server.isHasNextLine());
            this.view.changeButtonState(engine.HudView.DEC_LINE_COUNT_BTN, true, this.common.server.isHasPrevLine());
        };
        DefaultState.prototype.end = function () {
            this.view.changeButtonState(engine.HudView.AUTO_OFF_BTN, true, false);
        };
        DefaultState.NAME = "DefaultState";
        return DefaultState;
    })();
    engine.DefaultState = DefaultState;
})(engine || (engine = {}));
var engine;
(function (engine) {
    var EventDispatcher = createjs.EventDispatcher;
    var Tween = createjs.Tween;
    var Shape = createjs.Shape;
    var WinLine = (function (_super) {
        __extends(WinLine, _super);
        function WinLine(container, lineId, winVOs) {
            _super.call(this);
            this.wildLineId = -1;
            this.container = container;
            this.lineId = lineId;
            this.winVOs = winVOs;
        }
        WinLine.prototype.create = function () {
            for (var i = 0; i < this.winVOs.length; i++) {
                var vo = this.winVOs[i];
                var winSymbolView = vo.winSymbolView;
                if (winSymbolView.parent != null) {
                    winSymbolView.parent.removeChild(winSymbolView);
                }
                winSymbolView.x = vo.rect.x;
                winSymbolView.y = vo.rect.y;
                if (!winSymbolView._is_wild) {
                    winSymbolView.play();
                    this.container.addChild(winSymbolView);
                }
                else {
                    this.wildLineId = i;
                    this.dispatchEvent(engine.BaseWinLines.SHOW_WILD_REEL);
                }
            }
        };
        WinLine.prototype.remove = function () {
            for (var i = 0; i < this.winVOs.length; i++) {
                var vo = this.winVOs[i];
                var winSymbolView = vo.winSymbolView;
                if (!winSymbolView._is_wild) {
                    this.container.removeChild(winSymbolView);
                }
            }
        };
        WinLine.prototype.getPositions = function () {
            var positions = new Array(this.winVOs.length);
            for (var i = 0; i < this.winVOs.length; i++) {
                positions[i] = this.winVOs[i].posIdx.clone();
            }
            return positions;
        };
        WinLine.prototype.getLineId = function () {
            return this.lineId;
        };
        WinLine.prototype.processRecursionRotation = function (isMobile) {
            for (var i = 0; i < this.winVOs.length; i++) {
                var vo = this.winVOs[i];
                var mask = new Shape();
                mask.graphics.beginFill("0");
                if (isMobile) {
                    mask.graphics.drawRect(vo.rect.x, vo.rect.y, vo.rect.width * engine.GameConst.MOBILE_TO_WEB_SCALE, vo.rect.height * engine.GameConst.MOBILE_TO_WEB_SCALE);
                }
                else {
                    mask.graphics.drawRect(vo.rect.x, vo.rect.y, vo.rect.width, vo.rect.height);
                }
                vo.winSymbolView.mask = mask;
                Tween.get(vo.winSymbolView).to({ rotation: 90 }, engine.ReelMoverCascade.RECURSION_ROTATION_TIME);
            }
            return;
        };
        return WinLine;
    })(EventDispatcher);
    engine.WinLine = WinLine;
})(engine || (engine = {}));
var engine;
(function (engine) {
    var EventDispatcher = createjs.EventDispatcher;
    var BaseWinLines = (function (_super) {
        __extends(BaseWinLines, _super);
        function BaseWinLines(winLines, time) {
            _super.call(this);
            this.winLines = winLines;
            this.time = time;
            this.leftTime = time;
            this.isAllLineShowed = false;
        }
        BaseWinLines.prototype.onEnterFrame = function () {
            if (--this.leftTime == 0) {
                this.onEndTime();
            }
        };
        BaseWinLines.prototype.create = function () {
        };
        BaseWinLines.prototype.onEndTime = function () {
        };
        BaseWinLines.prototype.dispose = function () {
            for (var i = 0; i < this.winLines.length; i++) {
                this.winLines[i].remove();
            }
            this.winLines = null;
        };
        BaseWinLines.prototype.processRecursionRotation = function (isMobile) {
        };
        BaseWinLines.EVENT_CREATE_LINE = "create_line";
        BaseWinLines.EVENT_All_LINES_SHOWED = "lines_showed";
        BaseWinLines.EVENT_LINE_SHOWED = "line_showed";
        BaseWinLines.SHOW_WILD_REEL = "shoow_wild_symbol";
        return BaseWinLines;
    })(EventDispatcher);
    engine.BaseWinLines = BaseWinLines;
})(engine || (engine = {}));
var engine;
(function (engine) {
    var WinLines = (function (_super) {
        __extends(WinLines, _super);
        function WinLines(winLines, time) {
            _super.call(this, winLines, time);
        }
        WinLines.prototype.create = function () {
            this.index = -1;
            this.showNextLine();
        };
        WinLines.prototype.onEndTime = function () {
            if (!this.isAllLineShowed && this.index == this.winLines.length - 1) {
                this.dispatchEvent(engine.BaseWinLines.EVENT_All_LINES_SHOWED);
                this.isAllLineShowed = true;
            }
            if (this.winLines == null || this.winLines.length == 1) {
                return;
            }
            this.winLines[this.index].remove();
            this.showNextLine();
        };
        WinLines.prototype.showNextLine = function () {
            this.leftTime = this.time;
            this.index = (this.index + 1) % this.winLines.length;
            this.showLine();
            this.dispatchEvent(engine.BaseWinLines.EVENT_LINE_SHOWED, this.index);
        };
        WinLines.prototype.showLine = function () {
            var winLine = this.winLines[this.index];
            var winLinesIds = [];
            var lineId = winLine.getLineId();
            if (lineId > 0) {
                winLinesIds.push(lineId);
            }
            this.dispatchEvent(engine.BaseWinLines.EVENT_CREATE_LINE, [winLine.getPositions(), winLinesIds]);
            winLine.create();
        };
        return WinLines;
    })(engine.BaseWinLines);
    engine.WinLines = WinLines;
})(engine || (engine = {}));
var engine;
(function (engine) {
    var WinLinesTogether = (function (_super) {
        __extends(WinLinesTogether, _super);
        function WinLinesTogether(winLines, time) {
            _super.call(this, winLines, time);
        }
        WinLinesTogether.prototype.create = function () {
            var positions = [];
            var winLinesId = [];
            for (var i = 0; i < this.winLines.length; i++) {
                var winLine = this.winLines[i];
                var lineId = winLine.getLineId();
                if (lineId > 0) {
                    winLinesId.push(lineId);
                }
                WinLinesTogether.pushUniquePos(positions, winLine.getPositions());
                winLine.create();
            }
            this.dispatchEvent(engine.BaseWinLines.EVENT_CREATE_LINE, [positions, winLinesId]);
        };
        WinLinesTogether.pushUniquePos = function (allPositions, positions) {
            for (var i = 0; i < positions.length; i++) {
                var pos = positions[i];
                var isFind = false;
                for (var j = 0; j < allPositions.length; j++) {
                    var pos2 = allPositions[j];
                    if (pos.x == pos2.x && pos.y == pos2.y) {
                        isFind = true;
                        break;
                    }
                }
                if (!isFind) {
                    allPositions.push(pos);
                }
            }
        };
        WinLinesTogether.prototype.onEndTime = function () {
            if (!this.isAllLineShowed) {
                this.dispatchEvent(engine.BaseWinLines.EVENT_All_LINES_SHOWED);
                this.isAllLineShowed = true;
            }
        };
        WinLinesTogether.prototype.processRecursionRotation = function (isMobile) {
            for (var i = 0; i < this.winLines.length; i++) {
                this.winLines[i].processRecursionRotation(isMobile);
            }
        };
        return WinLinesTogether;
    })(engine.BaseWinLines);
    engine.WinLinesTogether = WinLinesTogether;
})(engine || (engine = {}));
var engine;
(function (engine) {
    var BonusHolderController = (function (_super) {
        __extends(BonusHolderController, _super);
        function BonusHolderController(manager, common, container) {
            _super.call(this, manager);
            this.common = common;
            this.container = container;
        }
        BonusHolderController.prototype.init = function () {
            _super.prototype.init.call(this);
        };
        BonusHolderController.prototype.listNotification = function () {
            var notifications = _super.prototype.listNotification.call(this);
            notifications.push(engine.NotificationList.CREATE_BONUS);
            notifications.push(engine.NotificationList.END_BONUS);
            notifications.push(engine.NotificationList.START_SECOND_FREE_GAME);
            return notifications;
        };
        BonusHolderController.prototype.handleNotification = function (message, data) {
            switch (message) {
                case engine.NotificationList.CREATE_BONUS:
                    {
                        this.startBonus();
                        break;
                    }
                case engine.NotificationList.END_BONUS:
                    {
                        this.removeBonus();
                        break;
                    }
                case engine.NotificationList.START_SECOND_FREE_GAME:
                    {
                        this.removeBonus();
                        this.common.server.bonus.className = 'FreeSpinsController';
                        this.startBonus();
                        break;
                    }
            }
        };
        BonusHolderController.prototype.startBonus = function () {
            var className = this.common.server.bonus.className;
            var BonusClass = engine[className];
            this.bonusController = new BonusClass(this.manager, this.common, this.container);
            this.bonusController.init();
            console.log(className, "startBonus");
        };
        BonusHolderController.prototype.removeBonus = function () {
            this.bonusController.dispose();
            this.bonusController = null;
        };
        BonusHolderController.prototype.dispose = function () {
            _super.prototype.dispose.call(this);
            this.removeBonus();
        };
        return BonusHolderController;
    })(engine.BaseController);
    engine.BonusHolderController = BonusHolderController;
})(engine || (engine = {}));
var engine;
(function (engine) {
    var Tween = createjs.Tween;
    var Shape = createjs.Shape;
    var Ticker = createjs.Ticker;
    var LinesController = (function (_super) {
        __extends(LinesController, _super);
        function LinesController(manager, common, view) {
            _super.call(this, manager);
            this.common = common;
            this.view = view;
            //this.setViewMask();
            LinesController.DELAY_REMOVE = engine.Utils.float2int(LinesController.DELAY_REMOVE * Ticker.getFPS());
            LinesController.INVISIBLE_TIME = engine.Utils.float2int(LinesController.INVISIBLE_TIME * Ticker.getFPS());
            LinesController.HIGHLIGHT_LINE_HOR = this.common.config.highlight_line_hor || LinesController.HIGHLIGHT_LINE_HOR;
            LinesController.HIGHLIGHT_LINE_VER = this.common.config.highlight_line_ver || LinesController.HIGHLIGHT_LINE_VER;
        }
        LinesController.prototype.setViewMask = function () {
            this.view.mask = new Shape();
            this.view.mask.graphics.beginFill("0").drawRect(50, 0, 700, 600);
            return;
        };
        LinesController.prototype.init = function () {
            _super.prototype.init.call(this);
            this.view.create();
            this.view.hideAllLines();
        };
        LinesController.prototype.listNotification = function () {
            var notifications = _super.prototype.listNotification.call(this);
            notifications.push(engine.NotificationList.SHOW_LINES);
            notifications.push(engine.NotificationList.HIDE_LINES);
            notifications.push(engine.NotificationList.START_SPIN);
            notifications.push(engine.NotificationList.CREATE_BONUS);
            notifications.push(engine.NotificationList.OPEN_PAY_TABLE);
            notifications.push(engine.NotificationList.CLOSE_PAY_TABLE);
            if (!this.common.isMobile) {
                notifications.push(engine.NotificationList.CHANGED_LINE_COUNT);
            }
            return notifications;
        };
        LinesController.prototype.createMask = function (positions) {
            var symbolsRect = this.common.symbolsRect;
            var mask = new Shape();
            mask.graphics.beginFill("0");
            for (var x = 0; x < symbolsRect.length; x++) {
                var reelSymbolsRect = symbolsRect[x];
                for (var y = 0; y < reelSymbolsRect.length; y++) {
                    if (!isExist(x, y, positions)) {
                        var rect = this.common.symbolsRect[x][y];
                        if (this.common.isMobile) {
                            mask.graphics.drawRect(rect.x - LinesController.HIGHLIGHT_LINE_HOR, rect.y - LinesController.HIGHLIGHT_LINE_VER, rect.width * engine.GameController.MOBILE_TO_WEB_SCALE + LinesController.HIGHLIGHT_LINE_HOR * 2, rect.height * engine.GameController.MOBILE_TO_WEB_SCALE + LinesController.HIGHLIGHT_LINE_VER * 2);
                        }
                        else {
                            mask.graphics.drawRect(rect.x - LinesController.HIGHLIGHT_LINE_HOR, rect.y - LinesController.HIGHLIGHT_LINE_VER, rect.width + LinesController.HIGHLIGHT_LINE_HOR * 2, rect.height + LinesController.HIGHLIGHT_LINE_VER * 2);
                        }
                    }
                }
            }
            this.view.mask = mask;
            function isExist(x, y, positions) {
                for (var i = 0; i < positions.length; i++) {
                    var position = positions[i];
                    if (position.x == x && position.y == y) {
                        return true;
                    }
                }
                return false;
            }
        };
        LinesController.prototype.handleNotification = function (message, data) {
            var _this = this;
            switch (message) {
                case engine.NotificationList.SHOW_LINES:
                    {
                        this.createMask(data[0]);
                        this.view.showLines(data[1]);
                        break;
                    }
                case engine.NotificationList.START_SPIN:
                case engine.NotificationList.CREATE_BONUS:
                case engine.NotificationList.HIDE_LINES:
                    {
                        this.view.mask = null;
                        this.view.hideAllLines();
                        break;
                    }
                case engine.NotificationList.CHANGED_LINE_COUNT:
                    {
                        Tween.removeTweens(this.view);
                        this.send(engine.NotificationList.REMOVE_WIN_LINES);
                        //this.view.mask = null;
                        this.setViewMask();
                        this.view.alpha = 1;
                        this.view.showLinesFromTo(1, this.common.server.lineCount);
                        Tween.get(this.view, { useTicks: true }).wait(LinesController.DELAY_REMOVE).to({ alpha: 0 }, LinesController.INVISIBLE_TIME).call(function () {
                            _this.view.hideAllLines();
                            _this.view.alpha = 1;
                        });
                        break;
                    }
                case engine.NotificationList.OPEN_PAY_TABLE:
                    {
                        this.view.visible = false;
                        break;
                    }
                case engine.NotificationList.CLOSE_PAY_TABLE:
                    {
                        this.view.visible = true;
                        break;
                    }
            }
        };
        LinesController.HIGHLIGHT_LINE_HOR = 9;
        LinesController.HIGHLIGHT_LINE_VER = 9;
        LinesController.DELAY_REMOVE = 2;
        LinesController.INVISIBLE_TIME = 1;
        return LinesController;
    })(engine.BaseController);
    engine.LinesController = LinesController;
})(engine || (engine = {}));
var engine;
(function (engine) {
    var Container = createjs.Container;
    var BackgroundController = (function (_super) {
        __extends(BackgroundController, _super);
        function BackgroundController(manager, common, container) {
            _super.call(this, manager);
            this.common = common;
            this.container = container;
        }
        BackgroundController.prototype.init = function () {
            _super.prototype.init.call(this);
            this.changeType();
        };
        BackgroundController.prototype.createView = function (type) {
            var creator = this.common.layouts[type];
            if (creator != null) {
                if (this.view != null) {
                    this.container.removeChild(this.view);
                }
                this.view = new Container();
                creator.create(this.view);
                this.container.addChild(this.view);
            }
        };
        BackgroundController.prototype.changeType = function () {
            this.createView(BackgroundController.REGULAR_LAYOUT_NAME);
            var bonus = this.common.server.bonus;
            if (bonus != null && bonus.type == engine.BonusTypes.FREE_SPINS) {
                this.createView(BackgroundController.FREE_SPINS_LAYOUT_NAME);
            }
            //else{
            //}
        };
        BackgroundController.prototype.listNotification = function () {
            var notifications = _super.prototype.listNotification.call(this);
            notifications.push(engine.NotificationList.CREATE_BONUS);
            notifications.push(engine.NotificationList.END_BONUS);
            return notifications;
        };
        BackgroundController.prototype.handleNotification = function (message, data) {
            switch (message) {
                case engine.NotificationList.CREATE_BONUS:
                case engine.NotificationList.END_BONUS:
                    {
                        this.changeType();
                        break;
                    }
            }
        };
        BackgroundController.prototype.dispose = function () {
            _super.prototype.dispose.call(this);
        };
        BackgroundController.REGULAR_LAYOUT_NAME = "BackgroundView";
        BackgroundController.FREE_SPINS_LAYOUT_NAME = "FreeSpinsBackgroundView";
        return BackgroundController;
    })(engine.BaseController);
    engine.BackgroundController = BackgroundController;
})(engine || (engine = {}));
var engine;
(function (engine) {
    var Ticker = createjs.Ticker;
    var Container = createjs.Container;
    var HeaderController = (function (_super) {
        __extends(HeaderController, _super);
        function HeaderController(manager, common, container) {
            _super.call(this, manager);
            this.common = common;
            this.container = container;
            HeaderController.DELAY = engine.Utils.float2int(HeaderController.DELAY * Ticker.getFPS());
        }
        HeaderController.prototype.init = function () {
            _super.prototype.init.call(this);
        };
        HeaderController.prototype.createView = function (type) {
            var creator = this.common.layouts[type];
            if (creator != null) {
                this.view = new Container();
                creator.create(this.view);
                this.delay = 0;
                this.animation = this.view.getChildByName(HeaderController.ANIMATION_CONTAINER);
                if (this.animation != null) {
                    this.animationFrames = this.animation.spriteSheet.getNumFrames(null) - 1;
                }
            }
        };
        HeaderController.prototype.listNotification = function () {
            var notifications = _super.prototype.listNotification.call(this);
            notifications.push(engine.NotificationList.SHOW_HEADER);
            notifications.push(engine.NotificationList.REMOVE_HEADER);
            return notifications;
        };
        HeaderController.prototype.handleNotification = function (message, data) {
            switch (message) {
                case engine.NotificationList.SHOW_HEADER:
                    {
                        if (this.view == null) {
                            this.createView(HeaderController.REGULAR_LAYOUT_NAME);
                        }
                        if (!this.container.contains(this.view)) {
                            this.container.addChild(this.view);
                        }
                        break;
                    }
                case engine.NotificationList.REMOVE_HEADER:
                    {
                        if (this.view != null && this.container.contains(this.view)) {
                            this.container.removeChild(this.view);
                        }
                        break;
                    }
            }
        };
        HeaderController.prototype.onEnterFrame = function () {
            if (this.animation != null) {
                if (this.animation.paused) {
                    if (this.delay == 0) {
                        this.animation.gotoAndPlay(0);
                    }
                    this.delay -= 1;
                }
                else {
                    if (this.animation.currentFrame == this.animationFrames) {
                        this.animation.stop();
                        this.delay = HeaderController.DELAY;
                    }
                }
            }
        };
        HeaderController.prototype.dispose = function () {
            _super.prototype.dispose.call(this);
        };
        HeaderController.REGULAR_LAYOUT_NAME = "HeaderView";
        HeaderController.FREE_SPINS_LAYOUT_NAME = "FreeSpinsHeaderView";
        HeaderController.ANIMATION_CONTAINER = "animation";
        HeaderController.DELAY = 2;
        return HeaderController;
    })(engine.BaseController);
    engine.HeaderController = HeaderController;
})(engine || (engine = {}));
var engine;
(function (engine) {
    var ServerController = (function (_super) {
        __extends(ServerController, _super);
        function ServerController(manager, common) {
            _super.call(this, manager);
            this.jackpotData = null;
            this.common = common;
        }
        ServerController.prototype.init = function () {
            _super.prototype.init.call(this);
        };
        ServerController.prototype.listNotification = function () {
            var notifications = _super.prototype.listNotification.call(this);
            notifications.push(engine.NotificationList.RES_LOADED);
            notifications.push(engine.NotificationList.SERVER_SEND_SPIN);
            notifications.push(engine.NotificationList.SERVER_SEND_BONUS);
            notifications.push(engine.NotificationList.SERVER_SET_WHEEL);
            notifications.push(engine.NotificationList.SERVER_DISCONNECT);
            notifications.push(engine.NotificationList.UPDATE_JACKPOT_SERVER);
            notifications.push(engine.NotificationList.SERVER_GET_BALANCE_REQUEST);
            notifications.push(engine.NotificationList.RECURSION_PROCESSED);
            return notifications;
        };
        ServerController.prototype.handleNotification = function (message, data) {
            var _this = this;
            switch (message) {
                case engine.NotificationList.RES_LOADED:
                    {
                        var gameName = this.common.config.serverGameName;
                        var baseServerUrl = this.common.getParams['serverUrl'] ? this.common.getParams['serverUrl'] : this.common.config.serverUrl;
                        this.demoAuthorizationUrl = (baseServerUrl + engine.GameConst.DEMO_AUTHORIZATION_URL).toLowerCase();
                        this.authorizationUrl = (baseServerUrl + engine.GameConst.AUTHORIZATION_URL).toLowerCase();
                        this.authorizationRoom = (baseServerUrl + engine.GameConst.AUTHORIZATION_ROOM);
                        this.getBalanceUrl = (baseServerUrl + engine.GameConst.GET_BALANCE_URL).toLowerCase();
                        this.closeSessionUrl = (baseServerUrl + engine.GameConst.CLOSE_SESSION_URL).toLowerCase();
                        this.getBetsUrl = (baseServerUrl + engine.GameConst.GET_BETS_URL.replace("%s", gameName)).toLowerCase();
                        this.spinUrl = (baseServerUrl + engine.GameConst.SPIN_URL.replace("%s", gameName)).toLowerCase();
                        this.jackpotUrl = (baseServerUrl + engine.GameConst.JACKPOT_URL.replace("%s", gameName)).toLowerCase();
                        this.bonusUrl = (baseServerUrl + engine.GameConst.BONUS_URL.replace("%s", gameName)).toLowerCase();
                        this.setWheelUrl = (baseServerUrl + engine.GameConst.SET_WHEEL_URL.replace("%s", gameName)).toLowerCase();
                        this.common.server = new engine.ServerData();
                        if (this.common.key == null) {
                            this.startTimer();
                            if (this.common.isDemo) {
                                this.sendDemoLoginRequest(function () {
                                    _this.onAuthorized();
                                });
                            }
                            else {
                                this.sendLoginRequest(this.common.login, this.common.password, this.common.room, function () {
                                    _this.onAuthorized();
                                });
                            }
                        }
                        else {
                            this.onAuthorized();
                        }
                        break;
                    }
                case engine.NotificationList.SERVER_SEND_SPIN:
                    {
                        //this.send(NotificationList.SHOW_ALL_SYMBOLS);
                        this.startTimer();
                        this.sendSpinRequest(function () {
                            _this.send(engine.NotificationList.SERVER_GOT_SPIN);
                        });
                        break;
                    }
                case engine.NotificationList.UPDATE_JACKPOT_SERVER:
                    {
                        this.sendJackpotRequest(null);
                        break;
                    }
                case engine.NotificationList.SERVER_SEND_BONUS:
                    {
                        //this.send(NotificationList.SHOW_ALL_SYMBOLS);
                        this.startTimer();
                        console.log(data);
                        this.sendBonusRequest(data, function () {
                            _this.send(engine.NotificationList.SERVER_GOT_BONUS);
                        });
                        break;
                    }
                case engine.NotificationList.SERVER_SET_WHEEL:
                    {
                        this.sendSetWheel(data);
                        break;
                    }
                case engine.NotificationList.SERVER_DISCONNECT:
                    {
                        this.sendDisconnectRequest();
                        break;
                    }
                case engine.NotificationList.SERVER_GET_BALANCE_REQUEST:
                    {
                        this.sendGetBalanceRequest(function () {
                            _this.send(engine.NotificationList.UPDATE_BALANCE_TF);
                        });
                        break;
                    }
                case engine.NotificationList.RECURSION_PROCESSED:
                    {
                        if (this.jackpotData && this.jackpotData.textContent) {
                            this.send(engine.NotificationList.SHOW_JACKPOT_POPUP, this.jackpotData.textContent);
                        }
                        break;
                    }
            }
        };
        ServerController.prototype.startTimer = function () {
            var xhr = this.xhr;
            var that = this;
            this.xhrTimeout = setTimeout(function () {
                if (xhr)
                    xhr.abort();
                that.send(engine.NotificationList.SERVER_NOT_RESPONSE);
            }, 15000);
        };
        ServerController.prototype.sendSpinRequest = function (onComplete) {
            var _this = this;
            var serverDelay = this.xhrTimeout;
            var request = new engine.AjaxRequest(engine.AjaxRequest.XML, this.spinUrl, true, {
                "multiply": this.common.server.multiply,
                "key": this.common.key,
                "bet": this.common.server.bet,
                "lineBet": this.common.server.lineCount
            });
            this.xhr = request;
            request.get(function (responseData) {
                //this.send(NotificationList.SHOW_JACKPOT_POPUP, 55555);
                clearTimeout(serverDelay);
                _this.parseErrors(responseData);
                var spinData = engine.XMLUtils.getElement(responseData, "spin");
                _this.parseSpinData(spinData);
                _this.parseBonusData(spinData);
                _this.parseSpinJackpotData(engine.XMLUtils.getElement(spinData, "jackpot"));
                onComplete != null && onComplete();
            });
        };
        ServerController.prototype.parseSpinJackpotData = function (jackpotData) {
            this.jackpotData = jackpotData;
            this.common.server.setBalance(this.common.server.getBalance() - (parseFloat(this.jackpotData.textContent) || 0));
        };
        ServerController.prototype.sendBonusRequest = function (params, onComplete) {
            var _this = this;
            params.key = this.common.key;
            params.bonus = this.common.server.bonus.key;
            var request = new engine.AjaxRequest(engine.AjaxRequest.XML, this.bonusUrl, true, params);
            this.xhr = request;
            var serverDelay = this.xhrTimeout;
            request.get(function (responseData) {
                clearTimeout(serverDelay);
                _this.parseErrors(responseData);
                var error = engine.XMLUtils.getElement(responseData, "error");
                if (error == null) {
                    if (_this.common.server.bonus != null) {
                        _this.common.server.bonus.data = engine.XMLUtils.getElement(responseData, "bonus");
                        onComplete != null && onComplete();
                    }
                }
                else {
                    throw new Error("ERROR: " + error.textContent);
                }
            });
        };
        ServerController.prototype.sendSetWheel = function (params) {
            var XML = document.createElement("data");
            for (var i = 0; i < params.length; i++) {
                var column = document.createElement("i");
                column.setAttribute("id", i.toString());
                column.innerHTML = params[i].join(",");
                XML.appendChild(column);
            }
            var request = new engine.AjaxRequest(engine.AjaxRequest.XML, this.setWheelUrl, true, { "key": this.common.key, "wheel": XML.innerHTML });
            request.get();
        };
        ServerController.prototype.sendDemoLoginRequest = function (onComplete) {
            var _this = this;
            var request = new engine.AjaxRequest(engine.AjaxRequest.JSON, this.demoAuthorizationUrl, true, null);
            this.xhr = request;
            var serverDelay = this.xhrTimeout;
            request.get(function (responseData) {
                _this.parseErrors(responseData);
                clearTimeout(serverDelay);
                _this.common.key = responseData.key;
                console.log("session key: " + _this.common.key);
                onComplete();
            });
        };
        ServerController.prototype.sendLoginRequest = function (login, password, room, onComplete) {
            var _this = this;
            var passwordMD5 = CryptoJS.MD5(password).toString();
            var roomUrl = (typeof (room) != "undefined" && room != null);
            console.log(roomUrl, "roomUrl");
            console.log(room, "room");
            if (roomUrl) {
                if (room != '-1') {
                    passwordMD5 = '*' + CryptoJS.SHA1(CryptoJS.SHA1(password));
                }
                var request = new engine.AjaxRequest(engine.AjaxRequest.XML, this.authorizationRoom, true, { "login": login, "password": passwordMD5, "room": room });
            }
            else {
                var request = new engine.AjaxRequest(engine.AjaxRequest.XML, this.authorizationUrl, true, { "login": login, "password": passwordMD5 });
            }
            this.xhr = request;
            var serverDelay = this.xhrTimeout;
            request.get(function (responseData) {
                _this.parseErrors(responseData);
                clearTimeout(serverDelay);
                var result = responseData.documentElement.textContent;
                switch (result) {
                    case "1001":
                        {
                            alert("User already authorized! Please relogin!");
                            throw new Error("ERROR: User already authorized!");
                        }
                }
                if (roomUrl) {
                    result = engine.XMLUtils.getSessionRoom(responseData, "key");
                }
                //if(login.length > 10) result = login;
                if (result == null || result.length == 0) {
                    alert("Login or password is incorrect! Please relogin!");
                    throw new Error("ERROR: Login or password is incorrect");
                }
                _this.common.key = result;
                console.log("session key: " + _this.common.key);
                onComplete();
            });
        };
        ServerController.prototype.sendGetBalanceRequest = function (onComplete) {
            var _this = this;
            var request = new engine.AjaxRequest(engine.AjaxRequest.XML, this.getBalanceUrl, true, { key: this.common.key });
            this.xhr = request;
            request.get(function (responseData) {
                _this.parseErrors(responseData);
                _this.common.server.setBalance(parseFloat(responseData.documentElement.textContent));
                onComplete != null && onComplete();
            });
        };
        ServerController.prototype.sendGetBetsRequest = function (onComplete) {
            var _this = this;
            var request = new engine.AjaxRequest(engine.AjaxRequest.XML, this.getBetsUrl, true, { key: this.common.key });
            request.get(function (responseData) {
                _this.parseErrors(responseData);
                var bets = engine.XMLUtils.getElement(responseData, "bets");
                _this.common.server.jackpot = engine.XMLUtils.getAttributeFloat(bets, "jackpot");
                _this.common.server.bet = engine.XMLUtils.getAttributeFloat(bets, "bet");
                _this.common.server.multiply = engine.XMLUtils.getAttributeInt(bets, "mp");
                _this.common.server.lineCount = engine.XMLUtils.getAttributeInt(bets, "lines");
                _this.common.server.maxLineCount = engine.XMLUtils.getAttributeInt(bets, "maxlines");
                _this.common.server.wheel = engine.Utils.toIntArray(engine.XMLUtils.getAttribute(bets, "wheel").split(","));
                _this.common.server.availableMultiples = engine.Utils.toIntArray(engine.XMLUtils.getAttribute(bets, "multiply").split(","));
                _this.common.server.availableBets = [];
                var betsData = engine.XMLUtils.getElements(bets, "item");
                for (var i = 0; i < betsData.length; i++) {
                    var bet = betsData[i].childNodes[0].data;
                    _this.common.server.availableBets.push(parseFloat(bet));
                }
                _this.parseBonusData(bets);
                onComplete != null && onComplete();
            });
            this.xhr = request;
        };
        ServerController.prototype.sendDisconnectRequest = function () {
            if (this.common.key != null) {
                var request = new engine.AjaxRequest(engine.AjaxRequest.XML, this.closeSessionUrl, false, { key: this.common.key });
                request.get();
            }
        };
        ServerController.prototype.onAuthorized = function () {
            var _this = this;
            this.sendGetBalanceRequest(function () {
                _this.sendGetBetsRequest(function () {
                    _this.send(engine.NotificationList.SERVER_INIT);
                });
            });
        };
        ServerController.prototype.findBonusConfig = function (bonusId) {
            var bonuses = this.common.config.bonuses;
            for (var i = 0; i < bonuses.length; i++) {
                var bonus = bonuses[i];
                if (bonus.id == bonusId) {
                    return bonus;
                }
            }
            console.log("ERROR: No bonus in config id = " + bonusId);
        };
        ServerController.prototype.parseSpinData = function (data) {
            //console.log("parseSpinData", data);
            var wheelsData = engine.XMLUtils.getElement(data, "wheels").childNodes;
            for (var i = 0; i < wheelsData.length; i++) {
                var wheelData = engine.Utils.toIntArray(wheelsData[i].childNodes[0].data.split(","));
                for (var j = 0; j < wheelData.length; j++) {
                    this.common.server.wheel[i * wheelData.length + j] = wheelData[j];
                }
            }
            var winLinesData = engine.XMLUtils.getElement(data, "winposition").childNodes;
            this.common.server.winLines = [];
            this.common.server.arrLinesIds = [];
            for (var i = 0; i < winLinesData.length; i++) {
                var winLine = winLinesData[i];
                var winLineVO = new engine.WinLineVO();
                winLineVO.lineId = engine.XMLUtils.getAttributeInt(winLine, "line");
                winLineVO.win = engine.XMLUtils.getAttributeFloat(winLine, "win");
                winLineVO.winPos = engine.Utils.toIntArray(winLine.childNodes[0].data.split(","));
                this.common.server.winLines.push(winLineVO);
                this.common.server.arrLinesIds.push(winLineVO.lineId);
            }
            this.common.server.setBalance(parseFloat(engine.XMLUtils.getElement(data, "balance").textContent));
            this.common.server.win = engine.XMLUtils.getAttributeFloat(data, "firstwin") || parseFloat(engine.XMLUtils.getElement(data, "win").textContent);
            //recursion
            this.parseRecursion(data);
        };
        ServerController.prototype.parseBonusData = function (data) {
            this.common.server.bonus = null;
            var bonusData = engine.XMLUtils.getElement(data, "bonus");
            if (bonusData != null && bonusData.attributes.length > 0) {
                var step, restoreBonus;
                step = Math.max(engine.XMLUtils.getAttributeInt(bonusData, "step") - 1, 0);
                var bonusId = engine.XMLUtils.getAttributeInt(bonusData, "id");
                var bonusConfig = this.findBonusConfig(bonusId);
                if (bonusConfig == null) {
                    return;
                }
                var keyData = engine.XMLUtils.getAttribute(bonusData, "key");
                var last = engine.XMLUtils.getElement(bonusData, "last");
                if (last != null) {
                    restoreBonus = engine.XMLUtils.getElement(last, "bonus");
                    step = engine.XMLUtils.getAttributeInt(bonusData, "step") - 1;
                    this.common.restore = true;
                    //recursion
                    if (engine.XMLUtils.getElement(restoreBonus, "data")) {
                        this.parseRecursion(engine.XMLUtils.getElement(engine.XMLUtils.getElement(restoreBonus, "data"), "spin"));
                    }
                }
                else {
                }
                var bonusVO = new engine.BonusVO();
                bonusVO.id = bonusId;
                bonusVO.key = keyData != null ? keyData : bonusData.textContent;
                bonusVO.step = step;
                bonusVO.type = bonusConfig.type;
                bonusVO.className = bonusConfig.className;
                bonusVO.data = restoreBonus != null ? restoreBonus : null;
                //console.log("step", step);
                //console.log("bonusData", bonusData);
                this.common.server.bonus = bonusVO;
            }
        };
        ServerController.prototype.parseRecursion = function (data) {
            this.common.server.recursion = [];
            if (!data)
                return;
            var recursionData = engine.XMLUtils.getElement(data, "recursion");
            if (recursionData) {
                var recursionDataArr = recursionData.childNodes;
                for (var i = 0; i < recursionDataArr.length; i++) {
                    var wheelData = eval(recursionDataArr[i].attributes[0].nodeValue);
                    this.common.server.recursion[i] = [];
                    this.common.server.recursion[i]["wheels"] = wheelData;
                    this.common.server.recursion[i]["win"] = engine.XMLUtils.getAttributeInt(recursionDataArr[i], "win");
                    this.common.server.recursion[i]["winLines"] = [];
                    this.common.server.recursion[i]["arrLinesIds"] = [];
                    var winLinesData = engine.XMLUtils.getElement(recursionDataArr[i], "winposition").childNodes;
                    for (var j = 0; j < winLinesData.length; ++j) {
                        var winLine = winLinesData[j];
                        var winLineVO = new engine.WinLineVO();
                        winLineVO.lineId = engine.XMLUtils.getAttributeInt(winLine, "line");
                        winLineVO.win = engine.XMLUtils.getAttributeFloat(winLine, "win");
                        winLineVO.winPos = engine.Utils.toIntArray(winLine.childNodes[0].data.split(","));
                        this.common.server.recursion[i]["winLines"].push(winLineVO);
                        this.common.server.recursion[i]["arrLinesIds"].push(winLineVO.lineId);
                    }
                }
            }
            this.common.server.recursion_len = this.common.server.recursion.length;
        };
        ServerController.prototype.dispose = function () {
            _super.prototype.dispose.call(this);
            this.common = null;
        };
        ServerController.prototype.parseErrors = function (responseData) {
            var errors = engine.ErrorController.parseXMLErrors(responseData);
            if (errors.length)
                this.send(engine.NotificationList.SHOW_ERRORS, errors);
        };
        ServerController.prototype.sendJackpotRequest = function (onComplete) {
            var _this = this;
            var serverDelay = this.xhrTimeout;
            var request = new engine.AjaxRequest(engine.AjaxRequest.XML, this.jackpotUrl, true, {
                "multiply": this.common.server.multiply,
                "key": this.common.key,
                "bet": this.common.server.bet,
                "lineBet": this.common.server.lineCount
            });
            this.xhr = request;
            request.get(function (responseData) {
                clearTimeout(serverDelay);
                _this.parseErrors(responseData);
                _this.parseJackpotData(responseData);
                onComplete != null && onComplete();
            });
        };
        ServerController.prototype.parseJackpotData = function (responseData) {
            var jackpotText = (engine.XMLUtils.getElement(responseData, "jackpot")) ? engine.XMLUtils.getElement(responseData, "jackpot").textContent : "";
            this.send(engine.NotificationList.UPDATE_JACKPOT, jackpotText);
        };
        return ServerController;
    })(engine.BaseController);
    engine.ServerController = ServerController;
})(engine || (engine = {}));
var engine;
(function (engine) {
    var Container = createjs.Container;
    var Sound = createjs.Sound;
    var PayTableController = (function (_super) {
        __extends(PayTableController, _super);
        function PayTableController(manager, common, gameContainer) {
            _super.call(this, manager);
            this.common = common;
            this.gameContainer = gameContainer;
            this.pageIdx = 1;
        }
        PayTableController.prototype.init = function () {
            _super.prototype.init.call(this);
            this.payTableContainer = new Container();
            this.pageContainer = new Container();
            this.payTableContainer.addChild(this.pageContainer);
            this.hudView = this.common.layouts[engine.PayTableHudView.LAYOUT_NAME];
            this.hudView.create();
            this.createPage(this.pageIdx);
            this.payTableContainer.addChild(this.hudView);
            this.initButtonsHandler();
        };
        PayTableController.prototype.listNotification = function () {
            var notifications = _super.prototype.listNotification.call(this);
            notifications.push(engine.NotificationList.OPEN_PAY_TABLE);
            notifications.push(engine.NotificationList.CLOSE_PAY_TABLE);
            return notifications;
        };
        PayTableController.prototype.handleNotification = function (message, data) {
            switch (message) {
                case engine.NotificationList.OPEN_PAY_TABLE:
                    {
                        this.onOpen();
                        break;
                    }
                case engine.NotificationList.CLOSE_PAY_TABLE:
                    {
                        this.onClose();
                        break;
                    }
            }
        };
        PayTableController.prototype.initButtonsHandler = function () {
            var _this = this;
            var buttonsNames = this.hudView.buttonsNames;
            for (var i = 0; i < buttonsNames.length; i++) {
                this.hudView.getBtn(buttonsNames[i]).on("click", function (eventObj) {
                    if (eventObj.nativeEvent instanceof MouseEvent) {
                        PayTableController.playBtnSound(eventObj.currentTarget.name);
                        _this.onBtnClick(eventObj.currentTarget.name);
                    }
                });
            }
        };
        PayTableController.playBtnSound = function (buttonName) {
            Sound.play(engine.SoundsList.REGULAR_CLICK_BTN);
        };
        PayTableController.prototype.onBtnClick = function (buttonName) {
            switch (buttonName) {
                case engine.PayTableHudView.PREV_PAGE_BTN:
                    {
                        this.createPage(--this.pageIdx);
                        break;
                    }
                case engine.PayTableHudView.NEXT_PAGE_BTN:
                    {
                        this.createPage(++this.pageIdx);
                        break;
                    }
                case engine.PayTableHudView.BACK_TO_GAME_BTN:
                    {
                        this.send(engine.NotificationList.CLOSE_PAY_TABLE);
                        break;
                    }
            }
        };
        PayTableController.prototype.createPage = function (pageIdx) {
            this.clearPageContainer();
            var prevCreator = this.common.layouts[PayTableController.PAY_TABLE_LAYOUT_PREFIX + (pageIdx - 1)];
            this.hudView.getBtn(engine.PayTableHudView.PREV_PAGE_BTN).setEnable(prevCreator != null);
            var nextCreator = this.common.layouts[PayTableController.PAY_TABLE_LAYOUT_PREFIX + (pageIdx + 1)];
            this.hudView.getBtn(engine.PayTableHudView.NEXT_PAGE_BTN).setEnable(nextCreator != null);
            var creator = this.common.layouts[PayTableController.PAY_TABLE_LAYOUT_PREFIX + pageIdx];
            creator.create(this.pageContainer);
        };
        PayTableController.prototype.onClose = function () {
            this.gameContainer.removeChild(this.payTableContainer);
            this.gameContainer.removeChild(this.hudView);
        };
        PayTableController.prototype.onOpen = function () {
            this.gameContainer.addChild(this.payTableContainer);
            if (this.common.isMobile) {
                this.hudView = this.common.layouts[engine.PayTableHudView.LAYOUT_NAME];
                this.hudView.create();
                this.hudView.resizeForMobile();
                this.gameContainer.addChild(this.hudView);
            }
        };
        PayTableController.prototype.clearPageContainer = function () {
            while (this.pageContainer.getNumChildren() > 0) {
                this.pageContainer.removeChildAt(0);
            }
        };
        PayTableController.prototype.dispose = function () {
            _super.prototype.dispose.call(this);
        };
        PayTableController.PAY_TABLE_LAYOUT_PREFIX = "PayTable_";
        return PayTableController;
    })(engine.BaseController);
    engine.PayTableController = PayTableController;
})(engine || (engine = {}));
var engine;
(function (engine) {
    var Sound = createjs.Sound;
    var SoundController = (function (_super) {
        __extends(SoundController, _super);
        function SoundController(manager, common) {
            _super.call(this, manager);
            this.freeSpinBgStarted = false;
            this.common = common;
        }
        SoundController.prototype.init = function () {
            _super.prototype.init.call(this);
        };
        SoundController.prototype.listNotification = function () {
            var notifications = _super.prototype.listNotification.call(this);
            notifications.push(engine.NotificationList.TRY_START_AUTO_PLAY_AUDIO);
            notifications.push(engine.NotificationList.END_AUTO_PLAY);
            notifications.push(engine.NotificationList.START_SPIN);
            notifications.push(engine.NotificationList.START_SPIN_AUDIO);
            notifications.push(engine.NotificationList.STOPPED_ALL_REELS);
            notifications.push(engine.NotificationList.SHOW_WIN_LINES);
            notifications.push(engine.NotificationList.STOPPED_REEL_ID);
            notifications.push(engine.NotificationList.STOPPED_REEL_ID_PREPARE);
            notifications.push(engine.NotificationList.OPEN_PAY_TABLE);
            notifications.push(engine.NotificationList.CLOSE_PAY_TABLE);
            notifications.push(engine.NotificationList.SHOW_LINES);
            notifications.push(engine.NotificationList.SOUND_TOGGLE);
            notifications.push(engine.NotificationList.START_BONUS_AUDIO);
            notifications.push(engine.NotificationList.END_BONUS);
            notifications.push(engine.NotificationList.SHOW_HEADER);
            notifications.push(engine.NotificationList.HIDE_CARD);
            notifications.push(engine.NotificationList.SHOW_CARD);
            notifications.push(engine.NotificationList.CREATE_BONUS);
            notifications.push(engine.NotificationList.RECURSION_PROCESS_ROTATION);
            notifications.push(engine.NotificationList.RECURSION_PROCESSED_FALLING);
            return notifications;
        };
        SoundController.prototype.handleNotification = function (message, data) {
            switch (message) {
                case engine.NotificationList.SOUND_TOGGLE:
                    {
                        this.common.isSoundOn = !this.common.isSoundOn;
                        Sound.setMute(!this.common.isSoundOn);
                        break;
                    }
                case engine.NotificationList.TRY_START_AUTO_PLAY_AUDIO:
                    {
                        this.autoSpinBg = Sound.createInstance(engine.SoundsList.AUTO_SPIN_BG);
                        this.autoSpinBg.play({ loop: -1 });
                        break;
                    }
                case engine.NotificationList.END_AUTO_PLAY:
                    {
                        this.autoSpinBg.stop();
                        this.autoSpinBg = null;
                        break;
                    }
                case engine.NotificationList.START_SPIN:
                    {
                        break;
                    }
                case engine.NotificationList.START_SPIN_AUDIO:
                    {
                        if (this.common.server.win > 0) {
                            Sound.play(engine.SoundsList.COLLECT);
                        }
                        if (this.autoSpinBg == null) {
                            this.regularSpinBg = Sound.createInstance(engine.SoundsList.REGULAR_SPIN_BG);
                            this.regularSpinBg.play();
                        }
                        break;
                    }
                case engine.NotificationList.STOPPED_ALL_REELS:
                    {
                        if (this.autoSpinBg == null) {
                            if (this.regularSpinBg != null)
                                this.regularSpinBg.stop();
                        }
                        break;
                    }
                case engine.NotificationList.SHOW_WIN_LINES:
                    {
                        this.isShowAllLines = data;
                        var winLinesVOs = this.common.server.winLines;
                        if (winLinesVOs.length > 0) {
                            Sound.play(engine.SoundsList.REGULAR_WIN);
                        }
                        break;
                    }
                case engine.NotificationList.RECURSION_PROCESS_ROTATION:
                    {
                        Sound.play(engine.SoundsList.RECURSION_ROTATION);
                        break;
                    }
                case engine.NotificationList.STOPPED_REEL_ID_PREPARE:
                case engine.NotificationList.RECURSION_PROCESSED_FALLING:
                    {
                        Sound.play(engine.SoundsList.REEL_STOP);
                        break;
                    }
                case engine.NotificationList.OPEN_PAY_TABLE:
                    {
                        this.payTable = Sound.createInstance(engine.SoundsList.PAY_TABLE_BG);
                        this.payTable.play({ loop: -1 });
                        break;
                    }
                case engine.NotificationList.CLOSE_PAY_TABLE:
                    {
                        this.payTable.stop();
                        break;
                    }
                case engine.NotificationList.SHOW_LINES:
                    {
                        if (!this.isShowAllLines) {
                            Sound.play(engine.SoundsList.LINE_WIN_ENUM);
                        }
                        break;
                    }
                case engine.NotificationList.START_BONUS_AUDIO:
                case engine.NotificationList.CREATE_BONUS:
                    {
                        //console.log(NotificationList.START_BONUS_AUDIO, "soundcontroller");
                        var bonus = this.common.server.bonus;
                        if (bonus != null) {
                            //console.log("bonus.type="+bonus.type, "soundcontroller");
                            if (this.autoSpinBg != null) {
                                this.autoSpinBg.stop();
                                this.autoSpinBg = null;
                            }
                            switch (bonus.type) {
                                case engine.BonusTypes.FREE_SPINS:
                                case engine.BonusTypes.SELECT_GAME:
                                    {
                                        if (!(this.freeSpinBg != undefined && this.freeSpinBg != null))
                                            this.freeSpinBg = Sound.createInstance(engine.SoundsList.FREE_SPIN_BG);
                                        if (this.freeSpinBg != undefined && this.freeSpinBg != null && !this.freeSpinBgStarted && this.common.isSoundLoaded) {
                                            this.freeSpinBgStarted = true;
                                            this.freeSpinBg.stop();
                                            this.freeSpinBg.play({ loop: -1 });
                                        }
                                        break;
                                    }
                                case engine.BonusTypes.GAMBLE:
                                    {
                                        if (!(this.gambleBg != undefined && this.gambleBg != null))
                                            this.gambleBg = Sound.createInstance(engine.SoundsList.GAMBLE_AMBIENT);
                                        if (this.gambleBg != undefined && this.gambleBg != null) {
                                            this.gambleBg.stop();
                                            this.gambleBg.play({ loop: -1 });
                                        }
                                        break;
                                    }
                            }
                        }
                        break;
                    }
                case engine.NotificationList.END_BONUS:
                    {
                        if (this.freeSpinBg != undefined && this.freeSpinBg != null) {
                            this.freeSpinBg.stop();
                            this.freeSpinBg = null;
                        }
                        this.freeSpinBgStarted = false;
                        break;
                    }
                case engine.NotificationList.SHOW_HEADER:
                    {
                        if (this.gambleBg != undefined && this.gambleBg != null) {
                            this.gambleBg.stop();
                            this.gambleBg = null;
                        }
                        break;
                    }
                case engine.NotificationList.HIDE_CARD:
                    {
                        Sound.play(engine.SoundsList.GAMBLE_SHUFFLE);
                        break;
                    }
                case engine.NotificationList.SHOW_CARD:
                    {
                        Sound.play(engine.SoundsList.GAMBLE_TURN);
                        break;
                    }
            }
        };
        SoundController.prototype.dispose = function () {
            _super.prototype.dispose.call(this);
            this.common = null;
        };
        return SoundController;
    })(engine.BaseController);
    engine.SoundController = SoundController;
})(engine || (engine = {}));
var engine;
(function (engine) {
    var Ticker = createjs.Ticker;
    var Tween = createjs.Tween;
    var Shape = createjs.Shape;
    var AutoSpinTableController = (function (_super) {
        __extends(AutoSpinTableController, _super);
        function AutoSpinTableController(manager, common, view, maskView) {
            _super.call(this, manager);
            this.common = common;
            this.view = view;
            this.maskView = maskView;
            AutoSpinTableController.OPEN_TIME = engine.Utils.float2int(AutoSpinTableController.OPEN_TIME * Ticker.getFPS());
            AutoSpinTableController.CLOSE_TIME = engine.Utils.float2int(AutoSpinTableController.CLOSE_TIME * Ticker.getFPS());
        }
        AutoSpinTableController.prototype.init = function () {
            _super.prototype.init.call(this);
            this.initMask();
            this.initItems();
            this.startY = this.view.y;
            this.isOpen = true;
            this.close(false);
        };
        AutoSpinTableController.prototype.listNotification = function () {
            var notifications = _super.prototype.listNotification.call(this);
            notifications.push(engine.NotificationList.OPEN_AUTO_SPIN_MENU);
            notifications.push(engine.NotificationList.TRY_START_AUTO_PLAY);
            notifications.push(engine.NotificationList.TRY_START_SPIN);
            notifications.push(engine.NotificationList.OPEN_PAY_TABLE);
            return notifications;
        };
        AutoSpinTableController.prototype.handleNotification = function (message, data) {
            switch (message) {
                case engine.NotificationList.OPEN_AUTO_SPIN_MENU:
                    {
                        this.toggle();
                        break;
                    }
                case engine.NotificationList.TRY_START_AUTO_PLAY:
                case engine.NotificationList.TRY_START_SPIN:
                    {
                        this.close(true);
                        break;
                    }
                case engine.NotificationList.OPEN_PAY_TABLE:
                    {
                        this.close(false);
                        break;
                    }
            }
        };
        AutoSpinTableController.prototype.toggle = function () {
            if (this.isOpen) {
                this.close(true);
            }
            else {
                this.open(true);
            }
        };
        AutoSpinTableController.prototype.initItems = function () {
            var _this = this;
            var i;
            var autoPlayCount = this.common.config.autoPlayCount;
            this.itemsBtn = new Array(autoPlayCount.length);
            for (var i = 1; i <= this.itemsBtn.length; i++) {
                var button = this.view.getChildByName(AutoSpinTableController.ITEM_PREFIX + i + AutoSpinTableController.ITEM_POSTFIX);
                var bounds = button.getBounds();
                var hitArea = new Shape();
                hitArea.graphics.beginFill("0").drawRect(bounds.x, bounds.y, bounds.width, bounds.height);
                button.hitArea = hitArea;
                button.on("click", function (eventObj) {
                    if (eventObj.nativeEvent instanceof MouseEvent) {
                        var target = eventObj.currentTarget;
                        var id = parseInt(target.name.substring(AutoSpinTableController.ITEM_PREFIX.length, target.name.length - AutoSpinTableController.ITEM_POSTFIX.length));
                        var count = parseInt(_this.itemsText[id - 1].text);
                        var bonus = _this.common.server.bonus;
                        if (bonus == null || bonus.type == engine.BonusTypes.GAMBLE) {
                            _this.send(engine.NotificationList.TRY_START_AUTO_PLAY, count);
                        }
                    }
                });
                this.itemsBtn[i - 1] = button;
            }
            this.itemsText = new Array(autoPlayCount.length);
            for (i = 1; i <= this.itemsText.length; i++) {
                var itemValue = this.view.getChildByName(AutoSpinTableController.ITEM_TEXT_PREFIX + i);
                itemValue.text = autoPlayCount[autoPlayCount.length - i].toString();
                this.itemsText[i - 1] = itemValue;
            }
        };
        AutoSpinTableController.prototype.initMask = function () {
            this.maskView.parent.removeChild(this.maskView);
            var bounds = this.maskView.getBounds();
            var mask = new Shape();
            mask.graphics.beginFill("0").drawRect(this.maskView.x + bounds.x, this.maskView.y + bounds.y, bounds.width, bounds.height);
            this.view.mask = mask;
        };
        AutoSpinTableController.prototype.close = function (animation) {
            var endPos = this.startY + this.view.getBounds().height;
            if (animation) {
                var tween = Tween.get(this.view, { useTicks: true });
                tween.to({ y: endPos }, AutoSpinTableController.CLOSE_TIME);
            }
            else {
                this.view.y = endPos;
            }
            this.isOpen = false;
            this.setButtonsEnable(false);
        };
        AutoSpinTableController.prototype.open = function (animation) {
            if (animation) {
                var tween = Tween.get(this.view, { useTicks: true });
                tween.to({ y: this.startY }, AutoSpinTableController.OPEN_TIME);
            }
            else {
                this.view.y = this.startY;
            }
            this.isOpen = true;
            this.setButtonsEnable(true);
        };
        AutoSpinTableController.prototype.setButtonsEnable = function (enable) {
            for (var i = 1; i <= this.itemsBtn.length; i++) {
                var button = this.view.getChildByName(AutoSpinTableController.ITEM_PREFIX + i + AutoSpinTableController.ITEM_POSTFIX);
                button.setEnable(enable);
            }
        };
        AutoSpinTableController.prototype.dispose = function () {
            _super.prototype.dispose.call(this);
        };
        AutoSpinTableController.ITEM_PREFIX = "item";
        AutoSpinTableController.ITEM_POSTFIX = "Btn";
        AutoSpinTableController.ITEM_TEXT_PREFIX = "item_value_";
        AutoSpinTableController.OPEN_TIME = 0.3;
        AutoSpinTableController.CLOSE_TIME = 0.3;
        return AutoSpinTableController;
    })(engine.BaseController);
    engine.AutoSpinTableController = AutoSpinTableController;
})(engine || (engine = {}));
var engine;
(function (engine) {
    var Ticker = createjs.Ticker;
    var StatsController = (function (_super) {
        __extends(StatsController, _super);
        function StatsController(manager, common, view) {
            _super.call(this, manager);
            this.common = common;
            this.view = view;
            StatsController.SHOW_TIME = engine.Utils.float2int(StatsController.SHOW_TIME * Ticker.getFPS());
            StatsController.HIDE_TIME = engine.Utils.float2int(StatsController.HIDE_TIME * Ticker.getFPS());
        }
        StatsController.prototype.init = function () {
            _super.prototype.init.call(this);
            this.winTf = new engine.AnimationTextField(this.view.getText(engine.HudView.WIN_TEXT));
            this.winTf.setValue(0);
            this.balanceTf = new engine.AnimationTextField(this.view.getText(engine.HudView.BALANCE_TEXT));
            this.balanceTf.setValue(this.common.server.getBalance());
        };
        StatsController.prototype.listNotification = function () {
            var notifications = _super.prototype.listNotification.call(this);
            notifications.push(engine.NotificationList.SHOW_WIN_TF);
            notifications.push(engine.NotificationList.COLLECT_WIN_TF);
            notifications.push(engine.NotificationList.UPDATE_BALANCE_TF);
            notifications.push(engine.NotificationList.START_SPIN);
            return notifications;
        };
        StatsController.prototype.onEnterFrame = function () {
            this.winTf.update();
            this.balanceTf.update();
        };
        StatsController.prototype.handleNotification = function (message, data) {
            switch (message) {
                case engine.NotificationList.UPDATE_BALANCE_TF:
                    {
                        this.updateBalanceText(data);
                        break;
                    }
                case engine.NotificationList.SHOW_WIN_TF:
                    {
                        this.updateWinText();
                        break;
                    }
                case engine.NotificationList.COLLECT_WIN_TF:
                    {
                        this.collectWinText();
                        break;
                    }
                case engine.NotificationList.START_SPIN:
                    {
                        this.winTf.setValue(0, 0);
                        break;
                    }
            }
        };
        StatsController.prototype.collectWinText = function () {
            this.winTf.setValue(0, StatsController.HIDE_TIME);
        };
        StatsController.prototype.updateBalanceText = function (value, time) {
            if (value === void 0) { value = null; }
            if (time === void 0) { time = StatsController.SHOW_TIME; }
            value = value || this.common.server.getBalance();
            this.balanceTf.setValue(value, time);
        };
        StatsController.prototype.updateWinText = function () {
            this.winTf.setValue(this.common.server.win, StatsController.SHOW_TIME);
        };
        StatsController.SHOW_TIME = 0.5;
        StatsController.HIDE_TIME = 0.3;
        return StatsController;
    })(engine.BaseController);
    engine.StatsController = StatsController;
})(engine || (engine = {}));
var engine;
(function (engine) {
    var Sound = createjs.Sound;
    var Tween = createjs.Tween;
    var Ticker = createjs.Ticker;
    var SettingsController = (function (_super) {
        __extends(SettingsController, _super);
        function SettingsController(manager, common, hudController, view) {
            _super.call(this, manager);
            this.common = common;
            this.hud = hudController;
            this.view = view;
            this.view.visible = false;
            this.autoPlayCountIdx = 0;
            SettingsController.PRELOADER_ROTATION_TIME = engine.Utils.float2int(SettingsController.PRELOADER_ROTATION_TIME * Ticker.getFPS());
        }
        SettingsController.prototype.init = function () {
            _super.prototype.init.call(this);
            this.initLinesMenu();
            this.initBetMenu();
            this.initSpinsCountMenu();
            this.initSoundToggle();
            this.initPreloader();
        };
        SettingsController.prototype.listNotification = function () {
            var notifications = _super.prototype.listNotification.call(this);
            notifications.push(engine.NotificationList.TOGGLE_SETTINGS_MENU);
            notifications.push(engine.NotificationList.CLOSE_SETTINGS_MENU);
            notifications.push(engine.NotificationList.LOAD_SOUNDS);
            notifications.push(engine.NotificationList.SOUNDS_LOADED);
            return notifications;
        };
        SettingsController.prototype.handleNotification = function (message, data) {
            switch (message) {
                case engine.NotificationList.TOGGLE_SETTINGS_MENU:
                    {
                        this.view.visible = !this.view.visible;
                        break;
                    }
                case engine.NotificationList.CLOSE_SETTINGS_MENU:
                    {
                        this.view.visible = false;
                        break;
                    }
                case engine.NotificationList.LOAD_SOUNDS:
                    {
                        this.showPreloader();
                        break;
                    }
                case engine.NotificationList.SOUNDS_LOADED:
                    {
                        this.removePreloader();
                        break;
                    }
            }
        };
        SettingsController.prototype.initPreloader = function () {
            if (this.common.isSoundLoaded) {
                this.removePreloader();
            }
            else {
                var soundPreloader = this.view.getChildByName(SettingsController.SOUND_PRELOADER);
                var bounds = soundPreloader.getBounds();
                soundPreloader.regX = bounds.width / 2;
                soundPreloader.regY = bounds.height / 2;
                soundPreloader.x += bounds.width / 2;
                soundPreloader.y += bounds.height / 2;
                soundPreloader.visible = false;
            }
        };
        SettingsController.prototype.showPreloader = function () {
            var soundPreloader = this.view.getChildByName(SettingsController.SOUND_PRELOADER);
            this.rotatePreloader();
            soundPreloader.visible = true;
        };
        SettingsController.prototype.rotatePreloader = function () {
            var _this = this;
            var soundPreloader = this.view.getChildByName(SettingsController.SOUND_PRELOADER);
            soundPreloader.rotation = 0;
            Tween.get(soundPreloader, { useTicks: true }).to({ "rotation": 360 }, SettingsController.PRELOADER_ROTATION_TIME).call(function () {
                _this.rotatePreloader();
            });
        };
        SettingsController.prototype.removePreloader = function () {
            var soundPreloader = this.view.getChildByName(SettingsController.SOUND_PRELOADER);
            Tween.removeAllTweens();
            this.view.removeChild(soundPreloader);
        };
        SettingsController.prototype.initSoundToggle = function () {
            var _this = this;
            var soundToggle = this.view.getChildByName(SettingsController.SOUND_TOGGLE);
            if (soundToggle != null) {
                soundToggle.setIsOn(this.common.isSoundLoaded);
                soundToggle.on("click", function (eventObj) {
                    if (eventObj.nativeEvent instanceof MouseEvent) {
                        if (!_this.common.isSoundLoaded) {
                            _this.send(engine.NotificationList.LOAD_SOUNDS);
                        }
                        _this.send(engine.NotificationList.SOUND_TOGGLE);
                        Sound.play(engine.SoundsList.REGULAR_CLICK_BTN);
                    }
                });
            }
        };
        SettingsController.prototype.initLinesMenu = function () {
            var _this = this;
            var values = [];
            for (var i = 1; i <= this.common.server.maxLineCount; i++) {
                values.push(i);
            }
            var menu = new engine.TouchMenu(this.view.getChildByName(SettingsController.LINES_MENU));
            menu.init(this.common.server.lineCount - 1, values, function (value) {
                _this.common.server.lineCount = value + 1;
                _this.hud.updateLineCountText();
            });
        };
        SettingsController.prototype.initBetMenu = function () {
            var _this = this;
            var availableBets = this.common.server.availableBets;
            var idx = availableBets.indexOf(this.common.server.bet);
            var menu = new engine.TouchMenu(this.view.getChildByName(SettingsController.BET_MENU));
            menu.init(idx, availableBets, function (value) {
                _this.common.server.bet = availableBets[value];
                _this.hud.updateBetText();
                _this.hud.updateBetPerLineText();
                _this.hud.updateTotalBetText();
            });
        };
        SettingsController.prototype.initSpinsCountMenu = function () {
            var _this = this;
            var menu = new engine.TouchMenu(this.view.getChildByName(SettingsController.SPINS_COUNT_MENU));
            menu.init(0, this.common.config.autoPlayCount, function (value) {
                _this.hud.autoPlayCountIdx = value;
                _this.hud.updateAutoPlayBtn();
                _this.hud.updateAutoSpinCountText();
            });
        };
        SettingsController.prototype.dispose = function () {
            _super.prototype.dispose.call(this);
            this.view = null;
            this.common = null;
        };
        SettingsController.PRELOADER_ROTATION_TIME = 1;
        SettingsController.BET_MENU = "betMenu";
        SettingsController.SOUND_TOGGLE = "soundToggle";
        SettingsController.SOUND_PRELOADER = "soundPreloader";
        SettingsController.LINES_MENU = "linesMenu";
        SettingsController.SPINS_COUNT_MENU = "spinsCountMenu";
        return SettingsController;
    })(engine.BaseController);
    engine.SettingsController = SettingsController;
})(engine || (engine = {}));
var engine;
(function (engine) {
    var Sound = createjs.Sound;
    var HudController = (function (_super) {
        __extends(HudController, _super);
        function HudController(manager, common, view) {
            _super.call(this, manager);
            this.common = common;
            this.view = view;
            this.autoPlayCount = this.common.config.autoPlayCount;
            this.autoPlayCountIdx = 0;
        }
        HudController.prototype.init = function () {
            _super.prototype.init.call(this);
            this.initHudState();
            this.initButtons();
            this.updateLineCountText();
            this.updateTotalBetText();
            this.updateBetText();
            this.updateBetPerLineText();
            this.setTextFields();
        };
        HudController.prototype.listNotification = function () {
            var notifications = _super.prototype.listNotification.call(this);
            notifications.push(engine.NotificationList.KEYBOARD_CLICK);
            notifications.push(engine.NotificationList.ON_SCREEN_CLICK);
            notifications.push(engine.NotificationList.TRY_START_SPIN);
            notifications.push(engine.NotificationList.START_SPIN);
            notifications.push(engine.NotificationList.WIN_LINES_SHOWED);
            notifications.push(engine.NotificationList.STOPPED_ALL_REELS);
            notifications.push(engine.NotificationList.EXPRESS_STOP);
            notifications.push(engine.NotificationList.CLOSE_PAY_TABLE);
            notifications.push(engine.NotificationList.END_BONUS);
            notifications.push(engine.NotificationList.TRY_START_AUTO_PLAY);
            notifications.push(engine.NotificationList.AUTO_PLAY_COMP);
            notifications.push(engine.NotificationList.AUTO_PLAY_CONT);
            notifications.push(engine.NotificationList.LOAD_PAY_TABLE);
            notifications.push(engine.NotificationList.CREATE_BONUS);
            notifications.push(engine.NotificationList.RECURSION_PROCESSED);
            notifications.push(engine.NotificationList.OK_BTN_ERROR_CLICKED);
            return notifications;
        };
        HudController.prototype.handleNotification = function (message, data) {
            this.currentState.handleNotification(message, data);
        };
        HudController.prototype.initHudState = function () {
            this.states = {};
            this.states[engine.DefaultState.NAME] = new engine.DefaultState(this);
            this.states[engine.PayTableState.NAME] = new engine.PayTableState(this);
            this.states[engine.RegularSpinState.NAME] = new engine.RegularSpinState(this);
            this.states[engine.AutoSpinState.NAME] = new engine.AutoSpinState(this);
            this.states[engine.BonusState.NAME] = new engine.BonusState(this);
            if (this.common.server.bonus == null) {
                this.changeState(engine.DefaultState.NAME);
            }
            else {
                this.changeState(engine.BonusState.NAME);
            }
        };
        HudController.prototype.initButtons = function () {
            var _this = this;
            var buttonsNames = this.view.buttonsNames;
            for (var i = 0; i < buttonsNames.length; i++) {
                var buttonName = buttonsNames[i];
                var button = this.view.getBtn(buttonName);
                if (button == null) {
                    continue;
                }
                button.on("click", function (eventObj) {
                    if (eventObj.nativeEvent instanceof MouseEvent) {
                        _this.onBtnClick(eventObj.currentTarget.name);
                    }
                });
            }
        };
        HudController.prototype.onBtnClick = function (buttonName) {
            switch (buttonName) {
                case engine.HudView.BACK_TO_LOBBY_BTN:
                    {
                        this.send(engine.NotificationList.BACK_TO_LOBBY);
                        break;
                    }
                case engine.HudView.FULL_SCREEN_BTN:
                    {
                        engine.FullScreen.toggleFullScreen();
                        break;
                    }
                case engine.HudView.TURBO_OFF_BTN:
                    {
                        this.setTurboMode(true);
                        this.view.changeButtonState(engine.HudView.TURBO_OFF_BTN, false, false);
                        break;
                    }
                case engine.HudView.TURBO_ON_BTN:
                    {
                        this.setTurboMode(false);
                        this.view.changeButtonState(engine.HudView.TURBO_OFF_BTN, true, true);
                        break;
                    }
                case engine.HudView.AUTO_OFF_BTN:
                    {
                        this.setAutoMode(true);
                        this.view.changeButtonState(engine.HudView.AUTO_OFF_BTN, false, false);
                        break;
                    }
                case engine.HudView.AUTO_ON_BTN:
                    {
                        this.setAutoMode(false);
                        this.view.changeButtonState(engine.HudView.AUTO_OFF_BTN, true, true);
                        break;
                    }
            }
            HudController.playBtnSound(buttonName);
            this.currentState.onBtnClick(buttonName);
        };
        HudController.playBtnSound = function (buttonName) {
            if (buttonName == engine.HudView.START_SPIN_BTN) {
                Sound.play(engine.SoundsList.SPIN_CLICK_BTN);
            }
            else {
                Sound.play(engine.SoundsList.REGULAR_CLICK_BTN);
            }
        };
        HudController.prototype.changeState = function (name, data) {
            if (data === void 0) { data = null; }
            console.log("Change state: " + name);
            if (this.currentState != null) {
                this.currentState.end();
            }
            this.currentState = this.states[name];
            this.currentState.start(data);
        };
        HudController.prototype.tryStartSpin = function () {
            console.log("Try start spin");
            var balanceAfterSpin = this.common.server.getBalance() - this.common.server.getTotalBet();
            if (balanceAfterSpin >= 0) {
                if (!this.common.server.recursion.length) {
                    var balance = this.common.server.getBalance() - this.common.server.getTotalBet();
                    this.common.server.setBalance(balance);
                    this.send(engine.NotificationList.UPDATE_BALANCE_TF);
                    this.send(engine.NotificationList.TRY_START_SPIN);
                }
            }
            else {
                this.send(engine.NotificationList.SHOW_ERRORS, [engine.ErrorController.ERROR_NO_MONEY_STR]);
            }
        };
        HudController.prototype.updateTotalBetText = function () {
            var totalBetText = this.view.getText(engine.HudView.TOTAL_BET_TEXT);
            if (totalBetText != null) {
                totalBetText.text = engine.MoneyFormatter.format(this.common.server.getTotalBet(), true);
            }
        };
        HudController.prototype.updateBetText = function () {
            var betText = this.view.getText(engine.HudView.BET_TEXT);
            if (betText != null) {
                betText.text = engine.MoneyFormatter.format(this.common.server.bet, true);
            }
        };
        HudController.prototype.updateBetPerLineText = function () {
            var betPerLineText = this.view.getText(engine.HudView.BET_PER_LINE_TEXT);
            if (betPerLineText != null) {
                betPerLineText.text = engine.MoneyFormatter.format(this.common.server.getBetPerLine(), false);
            }
        };
        HudController.prototype.updateLineCountText = function () {
            this.view.setLineCountText(this.common.server.lineCount.toString());
            if (this.common.isMobile)
                this.updateTotalBetText();
        };
        HudController.prototype.updateAutoPlayBtn = function () {
            //console.log("Update auto play btn");
            this.view.changeButtonState(engine.HudView.DEC_SPIN_COUNT_BTN, true, this.autoPlayCountIdx > 0);
            this.view.changeButtonState(engine.HudView.INC_SPIN_COUNT_BTN, true, this.autoPlayCountIdx < this.autoPlayCount.length - 1);
        };
        HudController.prototype.updateAutoSpinCountText = function () {
            //console.log("Update auto spin count text");
            var autoSpinCountText = this.view.getText(engine.HudView.AUTO_SPINS_COUNT_TEXT);
            if (autoSpinCountText != null && this.autoPlayCountIdx > -1 && this.autoPlayCountIdx < this.autoPlayCount.length) {
                var autoPlayCount = this.autoPlayCount[this.autoPlayCountIdx];
                autoSpinCountText.text = autoPlayCount.toString();
            }
        };
        HudController.prototype.setTextFields = function () {
            if (this.common.config.hud_shadows) {
                this.view.addFieldsShadows();
            }
        };
        HudController.prototype.setTurboMode = function (mode) {
            this.common.isTurbo = mode;
            this.send(engine.NotificationList.UPDATE_TURBO_MODE, null);
        };
        HudController.prototype.setAutoMode = function (mode) {
            //set auto mode
            this.common.isAuto = mode;
            if (this.common.isAuto) {
                this.changeState(engine.AutoSpinState.NAME, 5);
            }
            else {
                this.changeState(engine.DefaultState.NAME);
            }
        };
        HudController.prototype.dispose = function () {
            _super.prototype.dispose.call(this);
            this.view.dispose();
            this.view = null;
            this.common = null;
        };
        return HudController;
    })(engine.BaseController);
    engine.HudController = HudController;
})(engine || (engine = {}));
var engine;
(function (engine) {
    var ReelController = (function (_super) {
        __extends(ReelController, _super);
        function ReelController(manager, common, view) {
            _super.call(this, manager);
            this.common = common;
            this.view = view;
            this.movers = [];
            this.runnerReels = 0;
        }
        ReelController.prototype.init = function () {
            _super.prototype.init.call(this);
            this.view.create();
            //mobiletoweb
            if (this.common.isMobile) {
                this.view.scaleX *= engine.GameController.MOBILE_TO_WEB_SCALE;
                this.view.scaleY *= engine.GameController.MOBILE_TO_WEB_SCALE;
            }
            //mobiletoweb
            this.view.setMobileTrue(this.common.isMobile);
            this.setSymbolsPos();
            this.createMovers();
        };
        ReelController.prototype.listNotification = function () {
            var notifications = _super.prototype.listNotification.call(this);
            notifications.push(engine.NotificationList.START_SPIN);
            notifications.push(engine.NotificationList.SERVER_GOT_SPIN);
            notifications.push(engine.NotificationList.EXPRESS_STOP);
            notifications.push(engine.NotificationList.SHOW_ALL_SYMBOLS);
            notifications.push(engine.NotificationList.HIDE_SYMBOLS);
            notifications.push(engine.NotificationList.OPEN_PAY_TABLE);
            notifications.push(engine.NotificationList.CLOSE_PAY_TABLE);
            notifications.push(engine.NotificationList.CREATE_BONUS);
            notifications.push(engine.NotificationList.END_BONUS);
            notifications.push(engine.NotificationList.UPDATE_TURBO_MODE);
            return notifications;
        };
        ReelController.prototype.handleNotification = function (message, data) {
            switch (message) {
                case engine.NotificationList.START_SPIN:
                    {
                        this.startSpin();
                        break;
                    }
                case engine.NotificationList.SERVER_GOT_SPIN:
                    {
                        this.gotSpin();
                        break;
                    }
                case engine.NotificationList.EXPRESS_STOP:
                    {
                        this.stopSpin();
                        break;
                    }
                case engine.NotificationList.SHOW_ALL_SYMBOLS:
                    {
                        this.showAllSymbols();
                        break;
                    }
                case engine.NotificationList.HIDE_SYMBOLS:
                    {
                        this.hideSymbols(data);
                        break;
                    }
                case engine.NotificationList.CREATE_BONUS:
                case engine.NotificationList.END_BONUS:
                    {
                        this.updateSequenceType();
                        break;
                    }
                case engine.NotificationList.OPEN_PAY_TABLE:
                    {
                        this.view.visible = false;
                        break;
                    }
                case engine.NotificationList.CLOSE_PAY_TABLE:
                    {
                        this.view.visible = true;
                        break;
                    }
                case engine.NotificationList.UPDATE_TURBO_MODE:
                    {
                        this.updateTurboMode();
                        break;
                    }
            }
        };
        ReelController.prototype.onEnterFrame = function () {
            for (var i = 0; i < this.movers.length; i++) {
                this.movers[i].onEnterFrame();
            }
        };
        ReelController.prototype.dispose = function () {
            _super.prototype.dispose.call(this);
            this.view.dispose();
            this.view = null;
            this.common = null;
            this.movers = null;
        };
        ReelController.prototype.getSequenceType = function () {
            var bonus = this.common.server.bonus;
            if (bonus != null && bonus.type == engine.BonusTypes.FREE_SPINS) {
                return engine.ReelMover.TYPE_FREE_SPIN;
            }
            return engine.ReelMover.TYPE_REGULAR;
        };
        ReelController.prototype.updateSequenceType = function () {
            var type = this.getSequenceType();
            for (var i = 0; i < this.movers.length; i++) {
                this.movers[i].changeSequenceType(type);
            }
        };
        ReelController.prototype.showAllSymbols = function () {
            this.view.showAllSymbols();
        };
        ReelController.prototype.hideSymbols = function (positions) {
            console.log("hideSymbols", positions);
            this.view.hideSymbols(positions, 1);
        };
        ReelController.prototype.setSymbolsPos = function () {
            var reelCount = this.view.getReelCount();
            var symbolsRect = new Array(reelCount);
            for (var reelId = 0; reelId < reelCount; reelId++) {
                var reelView = this.view.getReel(reelId);
                var symbolCount = reelView.getSymbolCount();
                var reelSymbolsRect = new Array(symbolCount);
                for (var symbolId = 0; symbolId < symbolCount; symbolId++) {
                    var symbol = reelView.getSymbol(symbolId);
                    var symbolPos = symbol.localToGlobal(0, 0);
                    var symbolRect = symbol.getBounds().clone();
                    symbolRect.x = symbolPos.x;
                    symbolRect.y = symbolPos.y;
                    symbolRect.width = parseFloat(symbolRect.width);
                    symbolRect.height = parseFloat(symbolRect.height);
                    reelSymbolsRect[symbolId] = symbolRect;
                }
                symbolsRect[reelId] = reelSymbolsRect;
            }
            this.common.symbolsRect = symbolsRect;
        };
        ReelController.prototype.createMovers = function () {
            var _this = this;
            var sequenceType = this.getSequenceType();
            for (var reelId = 0; reelId < this.view.getReelCount(); reelId++) {
                var reelView = this.view.getReel(reelId);
                var reelMover = new engine.ReelMover(this.common.config.reelStrips, reelView, reelId);
                reelMover.on(engine.ReelMover.EVENT_REEL_STOPPED, function (stoppedReelId) {
                    _this.send(engine.NotificationList.STOPPED_REEL_ID, _this.movers.length - _this.runnerReels);
                    if (--_this.runnerReels == 0) {
                        _this.send(engine.NotificationList.STOPPED_ALL_REELS);
                    }
                    console.log("this.runnerReels", _this.runnerReels);
                });
                reelMover.on(engine.ReelMover.EVENT_REEL_PREPARE_STOPPED, function (stoppedReelId) {
                    _this.send(engine.NotificationList.STOPPED_REEL_ID_PREPARE, _this.movers.length - _this.runnerReels);
                });
                reelMover.init(this.common.server.wheel, sequenceType);
                this.movers.push(reelMover);
            }
            if (this.common.config.spinOnReelsClick) {
                this.view.addEventListener("click", function (eventObj) {
                    if (eventObj.nativeEvent instanceof MouseEvent) {
                        _this.send(engine.NotificationList.ON_SCREEN_CLICK);
                    }
                });
            }
        };
        ReelController.prototype.startSpin = function () {
            if (this.runnerReels > 0) {
                return;
            }
            for (var reelId = 0; reelId < this.movers.length; reelId++) {
                this.runnerReels++;
                this.movers[reelId].start();
            }
        };
        ReelController.prototype.gotSpin = function () {
            for (var reelId = 0; reelId < this.movers.length; reelId++) {
                this.movers[reelId].onGetSpin(this.common.server.wheel);
            }
        };
        ReelController.prototype.stopSpin = function () {
            for (var reelId = 0; reelId < this.movers.length; reelId++) {
                this.movers[reelId].expressStop();
            }
        };
        ReelController.prototype.updateTurboMode = function () {
            for (var i = 0; i < this.movers.length; ++i) {
                this.movers[i].isTurbo = this.common.isTurbo;
            }
        };
        return ReelController;
    })(engine.BaseController);
    engine.ReelController = ReelController;
})(engine || (engine = {}));
var engine;
(function (engine) {
    var ReelControllerCascade = (function (_super) {
        __extends(ReelControllerCascade, _super);
        function ReelControllerCascade(manager, common, view) {
            _super.call(this, manager);
            this.common = common;
            this.view = view;
            this.movers = [];
            this.runnerReels = 0;
        }
        ReelControllerCascade.prototype.init = function () {
            _super.prototype.init.call(this);
            this.view.create();
            //mobiletoweb
            if (this.common.isMobile) {
                this.view.scaleX *= engine.GameController.MOBILE_TO_WEB_SCALE;
                this.view.scaleY *= engine.GameController.MOBILE_TO_WEB_SCALE;
            }
            //mobiletoweb
            this.view.setMobileTrue(this.common.isMobile);
            this.setSymbolsPos();
            this.createMovers();
        };
        ReelControllerCascade.prototype.listNotification = function () {
            var notifications = _super.prototype.listNotification.call(this);
            notifications.push(engine.NotificationList.START_SPIN);
            notifications.push(engine.NotificationList.SERVER_GOT_SPIN);
            notifications.push(engine.NotificationList.EXPRESS_STOP);
            notifications.push(engine.NotificationList.SHOW_ALL_SYMBOLS);
            notifications.push(engine.NotificationList.HIDE_SYMBOLS);
            notifications.push(engine.NotificationList.OPEN_PAY_TABLE);
            notifications.push(engine.NotificationList.CLOSE_PAY_TABLE);
            notifications.push(engine.NotificationList.CREATE_BONUS);
            notifications.push(engine.NotificationList.END_BONUS);
            notifications.push(engine.NotificationList.RECURSION_PROCESS_FALLING);
            notifications.push(engine.NotificationList.WIN_LINES_SHOWED);
            return notifications;
        };
        ReelControllerCascade.prototype.handleNotification = function (message, data) {
            switch (message) {
                case engine.NotificationList.START_SPIN:
                    {
                        this.startSpin();
                        break;
                    }
                case engine.NotificationList.SERVER_GOT_SPIN:
                    {
                        this.gotSpin();
                        break;
                    }
                case engine.NotificationList.EXPRESS_STOP:
                    {
                        break;
                    }
                case engine.NotificationList.SHOW_ALL_SYMBOLS:
                    {
                        this.showAllSymbols();
                        break;
                    }
                case engine.NotificationList.HIDE_SYMBOLS:
                    {
                        this.hideSymbols(data);
                        break;
                    }
                case engine.NotificationList.CREATE_BONUS:
                case engine.NotificationList.END_BONUS:
                    {
                        break;
                    }
                case engine.NotificationList.OPEN_PAY_TABLE:
                    {
                        this.view.visible = false;
                        break;
                    }
                case engine.NotificationList.CLOSE_PAY_TABLE:
                    {
                        this.view.visible = true;
                        break;
                    }
                case engine.NotificationList.WIN_LINES_SHOWED:
                    {
                        this.processRecursion();
                        break;
                    }
                case engine.NotificationList.RECURSION_PROCESS_FALLING:
                    {
                        this.recursionProcessFalling();
                    }
            }
        };
        ReelControllerCascade.prototype.processRecursion = function () {
            var self = this;
            if (this.common.server.recursion.length) {
                for (var i = 0; i < this.movers.length; ++i) {
                    this.movers[i].processRecursionRotation(this.common);
                }
            }
        };
        ReelControllerCascade.prototype.recursionProcessFalling = function () {
            for (var reelId = 0; reelId < this.movers.length; reelId++) {
                this.movers[reelId].recursionProcessFalling(this.common);
            }
        };
        ReelControllerCascade.prototype.onEnterFrame = function () {
            for (var i = 0; i < this.movers.length; i++) {
                this.movers[i].onEnterFrame();
            }
        };
        ReelControllerCascade.prototype.dispose = function () {
            _super.prototype.dispose.call(this);
            this.view.dispose();
            this.view = null;
            this.common = null;
            this.movers = null;
        };
        ReelControllerCascade.prototype.getSequenceType = function () {
            var bonus = this.common.server.bonus;
            if (bonus != null && bonus.type == engine.BonusTypes.FREE_SPINS) {
                return engine.ReelMoverCascade.TYPE_FREE_SPIN;
            }
            return engine.ReelMoverCascade.TYPE_REGULAR;
        };
        //private updateSequenceType():void {
        //	var type:string = this.getSequenceType();
        //	for (var i:number = 0; i < this.movers.length; i++) {
        //		this.movers[i].changeSequenceType(type);
        //	}
        //}
        ReelControllerCascade.prototype.showAllSymbols = function () {
            this.view.showAllSymbols();
        };
        ReelControllerCascade.prototype.hideSymbols = function (positions) {
            console.log("hideSymbols", positions);
            this.view.hideSymbols(positions);
        };
        ReelControllerCascade.prototype.setSymbolsPos = function () {
            var reelCount = this.view.getReelCount();
            var symbolsRect = new Array(reelCount);
            for (var reelId = 0; reelId < reelCount; reelId++) {
                var reelView = this.view.getReel(reelId);
                var symbolCount = reelView.getSymbolCount();
                var reelSymbolsRect = new Array(symbolCount);
                for (var symbolId = 0; symbolId < symbolCount; symbolId++) {
                    var symbol = reelView.getSymbol(symbolId);
                    var symbolPos = symbol.localToGlobal(0, 0);
                    var symbolRect = symbol.getBounds().clone();
                    symbolRect.x = symbolPos.x;
                    symbolRect.y = symbolPos.y;
                    symbolRect.width = parseFloat(symbolRect.width);
                    symbolRect.height = parseFloat(symbolRect.height);
                    reelSymbolsRect[symbolId] = symbolRect;
                }
                symbolsRect[reelId] = reelSymbolsRect;
            }
            this.common.symbolsRect = symbolsRect;
        };
        ReelControllerCascade.prototype.createMovers = function () {
            var _this = this;
            var sequenceType = this.getSequenceType();
            for (var reelId = 0; reelId < this.view.getReelCount(); reelId++) {
                var reelView = this.view.getReel(reelId);
                var reelMoverCascade = new engine.ReelMoverCascade(this.common.config.reelStrips, reelView, reelId);
                reelMoverCascade.on(engine.ReelMoverCascade.EVENT_REEL_STOPPED, function (stoppedReelId) {
                    _this.send(engine.NotificationList.STOPPED_REEL_ID, _this.movers.length - _this.runnerReels);
                    if (--_this.runnerReels == 0) {
                        _this.send(engine.NotificationList.STOPPED_ALL_REELS);
                    }
                    console.log("this.runnerReels", _this.runnerReels);
                });
                reelMoverCascade.on(engine.ReelMoverCascade.EVENT_REEL_PREPARE_STOPPED, function (stoppedReelId) {
                    _this.send(engine.NotificationList.STOPPED_REEL_ID_PREPARE, _this.movers.length - _this.runnerReels);
                });
                reelMoverCascade.init(this.common.server.wheel, sequenceType);
                this.movers.push(reelMoverCascade);
            }
            if (this.common.config.spinOnReelsClick) {
                this.view.addEventListener("click", function (eventObj) {
                    if (eventObj.nativeEvent instanceof MouseEvent) {
                        _this.send(engine.NotificationList.ON_SCREEN_CLICK);
                    }
                });
            }
        };
        ReelControllerCascade.prototype.startSpin = function () {
            if (this.runnerReels > 0) {
                return;
            }
            for (var reelId = 0; reelId < this.movers.length; reelId++) {
                this.runnerReels++;
                this.movers[reelId].start();
            }
        };
        ReelControllerCascade.prototype.gotSpin = function () {
            for (var reelId = 0; reelId < this.movers.length; reelId++) {
                this.movers[reelId].onGetSpin(this.common.server.wheel);
            }
        };
        return ReelControllerCascade;
    })(engine.BaseController);
    engine.ReelControllerCascade = ReelControllerCascade;
})(engine || (engine = {}));
var engine;
(function (engine) {
    var ReelWildController = (function (_super) {
        __extends(ReelWildController, _super);
        function ReelWildController(manager, common, reelsView, winView) {
            _super.call(this, manager);
            this.common = common;
            this.reelsView = reelsView;
            this.winView = winView;
            for (var i = 0; i < ReelWildController.reelsCnt; ++i) {
                if (this.common.layouts[engine.WinController.WIN_ANIMATION_PREFIX + this.common.config.wildSymbolId] && this.common.layouts[engine.WinController.WIN_ANIMATION_PREFIX + this.common.config.wildSymbolId + 'loop']) {
                    ReelWildController.wildSymbolsViews[i] = new engine.WildSymbolView();
                    ReelWildController.wildSymbolsViews[i].create(this.common.layouts[engine.WinController.WIN_ANIMATION_PREFIX + this.common.config.wildSymbolId], null, this.common.layouts[engine.WinController.WIN_ANIMATION_PREFIX + this.common.config.wildSymbolId + 'loop'], this.common.isMobile);
                }
            }
        }
        ReelWildController.prototype.init = function () {
            _super.prototype.init.call(this);
        };
        ReelWildController.prototype.listNotification = function () {
            var notifications = _super.prototype.listNotification.call(this);
            notifications.push(engine.NotificationList.SHOW_WILD_REEL_FADEIN);
            notifications.push(engine.NotificationList.END_BONUS);
            notifications.push(engine.NotificationList.SHOW_WILD_REEL);
            notifications.push(engine.NotificationList.REMOVE_WIN_LINES);
            return notifications;
        };
        ReelWildController.prototype.handleNotification = function (message, data) {
            switch (message) {
                case engine.NotificationList.END_BONUS:
                    {
                        this.removeWilds();
                        break;
                    }
                case engine.NotificationList.SHOW_WILD_REEL_FADEIN:
                    {
                        this.showWildFadeIn(data);
                        break;
                    }
                case engine.NotificationList.SHOW_WILD_REEL:
                    {
                        this.showWild(data);
                        break;
                    }
                case engine.NotificationList.REMOVE_WIN_LINES:
                    {
                        this.removeWilds();
                        break;
                    }
            }
        };
        ReelWildController.prototype.showWild = function (reelId) {
            if (ReelWildController.wildSymbolsViews[reelId] && !ReelWildController.wildSymbolsViews[reelId]._is_wild_started) {
                if (this.common.isMobile) {
                    ReelWildController.wildSymbolsViews[reelId].x = this.reelsView.getReel(reelId).getView().x * engine.GameController.MOBILE_TO_WEB_SCALE;
                    ReelWildController.wildSymbolsViews[reelId].y = 92;
                }
                else {
                    ReelWildController.wildSymbolsViews[reelId].x = this.reelsView.getReel(reelId).getView().x;
                    ReelWildController.wildSymbolsViews[reelId].y = 68;
                }
                this.winView.addChild(ReelWildController.wildSymbolsViews[reelId]);
                ReelWildController.wildSymbolsViews[reelId].play();
            }
        };
        ReelWildController.prototype.showWildFadeIn = function (reelId) {
            if (ReelWildController.wildSymbolsViews[reelId] && !ReelWildController.wildSymbolsViews[reelId]._is_wild_started) {
                if (this.common.isMobile) {
                    ReelWildController.wildSymbolsViews[reelId].x = this.reelsView.getReel(reelId).getView().x * engine.GameController.MOBILE_TO_WEB_SCALE;
                    ReelWildController.wildSymbolsViews[reelId].y = 92;
                }
                else {
                    ReelWildController.wildSymbolsViews[reelId].x = this.reelsView.getReel(reelId).getView().x;
                    ReelWildController.wildSymbolsViews[reelId].y = 68;
                }
                this.winView.addChild(ReelWildController.wildSymbolsViews[reelId]);
                ReelWildController.wildSymbolsViews[reelId].playFadeIn();
            }
        };
        ReelWildController.prototype.removeWilds = function () {
            console.log("removeWilds");
            for (var reelId = 0; reelId < ReelWildController.reelsCnt; ++reelId) {
                this.removeWild(reelId);
            }
        };
        ReelWildController.prototype.removeWild = function (reelId) {
            if (ReelWildController.wildSymbolsViews[reelId] && ReelWildController.wildSymbolsViews[reelId]._is_wild_started) {
                this.winView.removeChild(ReelWildController.wildSymbolsViews[reelId]);
                ReelWildController.wildSymbolsViews[reelId]._is_wild_started = false;
            }
        };
        ReelWildController.prototype.dispose = function () {
            _super.prototype.dispose.call(this);
            this.reelsView.dispose();
            this.reelsView = null;
            this.common = null;
        };
        ReelWildController.prototype.hideSymbols = function (positions) {
            this.reelsView.hideSymbols(positions);
        };
        ReelWildController.wildSymbolsViews = [];
        ReelWildController.reelsCnt = 5;
        return ReelWildController;
    })(engine.BaseController);
    engine.ReelWildController = ReelWildController;
})(engine || (engine = {}));
var engine;
(function (engine) {
    var EventDispatcher = createjs.EventDispatcher;
    var ReelMover = (function (_super) {
        __extends(ReelMover, _super);
        function ReelMover(reelStrips, reelView, reelId) {
            _super.call(this);
            this.isTurbo = false;
            this.isFirst = true;
            this.reelStrips = reelStrips;
            this.reelView = reelView;
            this.reelId = reelId;
        }
        ReelMover.prototype.init = function (wheel, sequenceType) {
            this.state = ReelMover.STATE_NORMAL;
            this.dh = this.reelView.getSymbol(0).getBounds().height;
            this.startReelPos = this.reelView.getY();
            this.changeSequenceType(sequenceType);
            this.resetData();
            this.setServerResponse(wheel);
        };
        ReelMover.prototype.onEnterFrame = function () {
            if (this.isTurbo && this.state != ReelMover.STATE_NORMAL && this.state != ReelMover.STATE_DOWN && this.state != ReelMover.STATE_MOVING && !this.isFirst) {
                this.state = ReelMover.STATE_MOVING;
            }
            if (this.state == ReelMover.STATE_NORMAL) {
                return;
            }
            if (this.delayStartReel > 0 && this.state == ReelMover.STATE_UP) {
                this.delayStartReel -= 1;
                return;
            }
            if (this.isStarted) {
                this.isStarted = false;
            }
            if (this.state == ReelMover.STATE_UP) {
                this.up();
            }
            if (this.state == ReelMover.STATE_MOVING) {
                this.move();
            }
            if (this.state == ReelMover.STATE_DOWN) {
                this.down();
            }
            this.updatePos(false);
        };
        ReelMover.prototype.start = function () {
            this.isCanStop = false;
            this.state = ReelMover.STATE_UP;
        };
        ReelMover.prototype.expressStop = function () {
            this.isCutSequence = false;
            this.isHardStop = true;
        };
        ReelMover.prototype.onGetSpin = function (wheel) {
            if (!this.isCanStop) {
                this.setServerResponse(wheel);
                this.updatePos(false);
                this.isCanStop = true;
            }
        };
        ReelMover.prototype.up = function () {
            if (this.upEasing.getTime() < 1) {
                this.pos = this.upEasing.getPosition();
            }
            else {
                this.posPrevState = this.pos;
                this.state = ReelMover.STATE_MOVING;
            }
        };
        ReelMover.prototype.move = function () {
            //console.log("this.moveEasing.getTime()="+this.moveEasing.getTime()+" this.isCanStop="+this.isCanStop);
            if (this.moveEasing.getTime() >= 1) {
                if (this.isCanStop) {
                    this.cutSequence();
                    if (this.currentIdx == this.correctIdx(this.targetIdx - ReelMover.DOWN_MOVE_SYMBOLS)) {
                        this.posPrevState = this.pos;
                        this.state = ReelMover.STATE_DOWN;
                        return;
                    }
                }
                this.moveEasing.reset();
                this.posPrevState = this.pos;
            }
            this.pos = this.posPrevState + this.moveEasing.getPosition();
        };
        ReelMover.prototype.down = function () {
            if (this.downEasing.getTime() < 1) {
                this.pos = this.posPrevState + this.downEasing.getPosition();
                if (this.downEasing.getTime() < ReelMover.PREPARE_REEL_SOUND_TIME) {
                    this.dispatchEvent(ReelMover.EVENT_REEL_PREPARE_STOPPED, this.reelView.getReelId());
                }
            }
            else {
                this.stop();
            }
        };
        ReelMover.prototype.changeSequenceType = function (moverType) {
            if (this.moverType != moverType) {
                this.moverType = moverType;
                var reelStrips = this.reelStrips[moverType];
                var reelId = this.reelView.getReelId();
                this.reelSequence = reelStrips[reelId].reverse();
                this.sequenceLength = this.reelSequence.length;
                this.upEasing = new engine.BezierEasing(0, 0, 4, -3, 0);
                this.upEasing.setParameters(this.dh * ReelMover.UP_MOVE_SYMBOLS, 10);
                this.moveEasing = new engine.LinearEasing();
                this.moveEasing.setParameters(this.dh, ReelMover.MOVE_SPEED);
                this.downEasing = new engine.BezierEasing(0, -2, 10, -15, 8);
                this.downEasing.setParameters(this.dh * ReelMover.DOWN_MOVE_SYMBOLS, 15);
                this.resetForChangeSequence();
            }
        };
        ReelMover.prototype.stop = function () {
            this.isFirst = false;
            this.state = ReelMover.STATE_NORMAL;
            this.resetData();
            this.dispatchEvent(ReelMover.EVENT_REEL_STOPPED, this.reelView.getReelId());
        };
        ReelMover.prototype.setServerResponse = function (wheel, is_synchro_stop) {
            if (is_synchro_stop === void 0) { is_synchro_stop = true; }
            var symbolCount = ReelMover.SYMBOLS_IN_REEL;
            var targetWheels = new Array(symbolCount);
            for (var i = 0; i < symbolCount; i++) {
                targetWheels[i] = wheel[symbolCount - i + symbolCount * this.reelView.getReelId() - 1];
            }
            console.log("targetWheels", targetWheels);
            if (is_synchro_stop) {
                this.pasteSequenceReelsAtIdx(targetWheels);
            }
            var idx = this.getSequenceIdx(targetWheels);
            if (idx == -1) {
                var reverseReelSequence = this.reelSequence.slice(0).reverse();
                var reverseTargetWheels = targetWheels.slice(0).reverse();
                console.log("[ERROR]: reel id: " + this.reelView.getReelId() + " sequence type: " + this.moverType + " target wheels: " + reverseTargetWheels + " not found in sequence: " + reverseReelSequence);
                console.log("targetWheels", targetWheels);
                console.log("wheel", wheel);
                this.reelSequence = this.reelSequence.concat(targetWheels);
                this.sequenceLength = this.reelSequence.length;
                idx = this.getSequenceIdx(targetWheels);
            }
            if (this.state == ReelMover.STATE_NORMAL) {
                this.startIdx = idx;
                this.updatePos(true);
            }
            else {
                this.targetIdx = idx;
            }
        };
        ReelMover.prototype.pasteSequenceReelsAtIdx = function (targetWheels) {
            var idx = this.currentIdx + ReelMover.SYMBOLS_IN_REEL * 2;
            switch (this.state) {
                case ReelMover.STATE_NORMAL:
                    {
                        break;
                    }
                case ReelMover.STATE_MOVING:
                    {
                        idx += this.reelId * ReelMover.SYMBOLS_IN_REEL;
                        break;
                    }
                default:
                    {
                        break;
                    }
            }
            console.log(this.reelId, "this.reelid");
            for (var i = 0; i < ReelMover.SYMBOLS_IN_REEL; ++i) {
                this.reelSequence[(i + idx) % this.reelSequence.length] = targetWheels[i];
            }
            return;
        };
        ReelMover.prototype.updatePos = function (isFist) {
            var idx = this.getCurrentIdx();
            var dh = this.pos % this.dh - this.dh;
            this.reelView.setY(this.startReelPos + dh);
            if (this.currentIdx != idx || isFist) {
                var symbolCount = ReelMover.SYMBOLS_IN_REEL + 1;
                for (var i = 0; i < symbolCount; i++) {
                    var symbolId = symbolCount - i - 1;
                    var symbolIdx = this.correctIdx(idx + i);
                    var frame = this.reelSequence[symbolIdx] - 1;
                    this.reelView.setSymbolFrame(symbolId, frame);
                }
                this.currentIdx = idx;
            }
        };
        ReelMover.prototype.getCurrentIdx = function () {
            return engine.Utils.float2int(this.correctIdx(this.startIdx + Math.abs(this.pos + ReelMover.EPSILON) / this.dh));
        };
        ReelMover.prototype.getSequenceIdx = function (value) {
            var i = 0;
            var valueLength = value.length;
            var j = 0;
            while (j < valueLength) {
                if (this.reelSequence.length == i) {
                    // for set wheel
                    return -1;
                }
                if (this.reelSequence[i] == value[j]) {
                    j++;
                    if (i == this.sequenceLength - 1) {
                        i = -1;
                    }
                }
                else if (j > 0) {
                    i = this.correctIdx(i - j);
                    j = 0;
                }
                i++;
            }
            var idx = i - valueLength;
            if (idx < 0) {
                idx = this.sequenceLength + idx;
            }
            return idx;
        };
        ReelMover.prototype.correctIdx = function (value) {
            if (value >= 0 && value < this.sequenceLength) {
                return value;
            }
            return (value + this.sequenceLength) % this.sequenceLength;
        };
        ReelMover.prototype.cutSequence = function () {
            if (this.isCutSequence) {
                return false;
            }
            var maxDiv = this.isTurbo ? ReelMover.TURBO_STOP_SYMBOLS : this.isHardStop ? ReelMover.HARD_STOP_SYMBOLS : ReelMover.REGULAR_STOP_SYMBOLS;
            this.isHardStop = false;
            var realDivIdx = this.correctIdx(this.targetIdx - ReelMover.DOWN_MOVE_SYMBOLS - this.currentIdx);
            if (realDivIdx <= maxDiv) {
                return;
            }
            var symbolId = this.reelSequence[this.correctIdx(this.currentIdx + maxDiv)];
            if (this.isGroupSymbol(symbolId)) {
                return;
            }
            var div = realDivIdx - maxDiv;
            while (this.isGroupSymbol(this.reelSequence[this.correctIdx(this.currentIdx + div + maxDiv)])) {
                div--;
            }
            if (div <= 0) {
                return;
            }
            this.pos += div * this.dh;
            this.currentIdx = this.getCurrentIdx();
            this.isCutSequence = true;
        };
        // TODO: need implemented
        ReelMover.prototype.isGroupSymbol = function (symbolId) {
            //console.log("isGroupSymbol", "symbolId="+symbolId);
            //console.log("isGroupSymbol this.getCurrentIdx()=", this.getCurrentIdx());
            //console.log("isGroupSymbol this.correctIdx()=", this.correctIdx);
            //console.log("isGroupSymbol this.reelSequence", this.reelSequence);
            return false;
        };
        // Сброс данных после остановки барабана
        ReelMover.prototype.resetData = function () {
            this.delayStartReel = ReelMover.DELAY_BETWEEN_START_REEL * this.reelView.getReelId();
            this.startIdx = this.targetIdx;
            this.targetIdx = 0;
            this.isCutSequence = false;
            this.pos = 0;
            this.upEasing.reset();
            this.downEasing.reset();
            this.moveEasing.reset();
        };
        // Сброс данных после изменения ленты
        ReelMover.prototype.resetForChangeSequence = function () {
            this.currentIdx = 0;
            this.startIdx = 0;
        };
        ReelMover.SYMBOLS_IN_REEL = 4;
        // events
        ReelMover.EVENT_REEL_STOPPED = "event_reel_stopped";
        ReelMover.EVENT_REEL_PREPARE_STOPPED = "event_reel_prepare_stopped";
        // reel strips type
        ReelMover.TYPE_REGULAR = "regular";
        ReelMover.TYPE_FREE_SPIN = "freeSpins";
        // mover states
        ReelMover.STATE_NORMAL = "normal";
        ReelMover.STATE_UP = "up";
        ReelMover.STATE_MOVING = "moving";
        ReelMover.STATE_DOWN = "end";
        ReelMover.UP_MOVE_SYMBOLS = 1;
        ReelMover.DOWN_MOVE_SYMBOLS = 1;
        ReelMover.MOVE_SPEED = 2;
        ReelMover.DELAY_BETWEEN_START_REEL = 7;
        ReelMover.REGULAR_STOP_SYMBOLS = 4;
        ReelMover.HARD_STOP_SYMBOLS = 4;
        ReelMover.TURBO_STOP_SYMBOLS = 1;
        ReelMover.EPSILON = 0.001;
        ReelMover.PREPARE_REEL_SOUND_TIME = 0.09;
        return ReelMover;
    })(EventDispatcher);
    engine.ReelMover = ReelMover;
})(engine || (engine = {}));
var engine;
(function (engine) {
    var EventDispatcher = createjs.EventDispatcher;
    var ReelMoverCascade = (function (_super) {
        __extends(ReelMoverCascade, _super);
        function ReelMoverCascade(reelStrips, reelView, reelId) {
            _super.call(this);
            this.isSymbolsMovedDown = false;
            this.isSymbolsAppeared = false;
            this.symbolsAppeared = 0;
            this.reelStrips = reelStrips;
            this.reelView = reelView;
            this.reelId = reelId;
        }
        ReelMoverCascade.prototype.init = function (wheel, sequenceType) {
            this.state2 = ReelMoverCascade.STATE2_NORMAL;
            this.setServerResponse(wheel);
            this.setSymbolFrames();
        };
        ReelMoverCascade.prototype.onEnterFrame = function () {
            switch (this.state2) {
                case ReelMoverCascade.STATE2_NORMAL:
                    {
                        //this.setSymbolFrames();
                        return;
                    }
                case ReelMoverCascade.STATE2_DOWN_START:
                    {
                        this.moveDown();
                        break;
                    }
                case ReelMoverCascade.STATE2_DOWN_STABLE:
                    {
                        if (this.isCanStop && this.isSymbolsMovedDown) {
                            this.state2 = ReelMoverCascade.STATE2_SHOW_NEW_SYMBOLS;
                            this.isCanStop = false;
                            this.isSymbolsMovedDown = false;
                            this.symbolsAppeared = 0;
                            var self = this;
                            this.reelView.symbolsAppearFromTop(function () {
                                ++self.symbolsAppeared;
                            });
                        }
                        break;
                    }
                case ReelMoverCascade.STATE2_SHOW_NEW_SYMBOLS:
                    {
                        if (this.symbolsAppeared >= ReelMoverCascade.SYMBOLS_IN_REEL) {
                            this.stop();
                        }
                        break;
                    }
                case ReelMoverCascade.STATE2_STOPPED_RECURSION:
                    {
                        break;
                    }
            }
        };
        ReelMoverCascade.prototype.moveDown = function () {
            var self = this;
            this.isSymbolsMovedDown = false;
            this.reelView.symbolsMoveDown(this.reelId * ReelMoverCascade.MOVE_DOWN_DELAY_REELS, ReelMoverCascade.MOVE_DOWN_DELAY_SYMBOLS, function () {
                self.isSymbolsMovedDown = true;
                self.setSymbolFrames();
            });
            this.state2 = ReelMoverCascade.STATE2_DOWN_STABLE;
        };
        ReelMoverCascade.prototype.start = function () {
            this.state2 = ReelMoverCascade.STATE2_DOWN_START;
        };
        ReelMoverCascade.prototype.onGetSpin = function (wheel) {
            if (!this.isCanStop) {
                this.setServerResponse(wheel);
                this.isCanStop = true;
            }
        };
        ReelMoverCascade.prototype.stop = function () {
            this.state2 = ReelMoverCascade.STATE2_NORMAL;
            this.dispatchEvent(ReelMoverCascade.EVENT_REEL_STOPPED, this.reelView.getReelId());
            this.dispatchEvent(ReelMoverCascade.EVENT_REEL_PREPARE_STOPPED, this.reelView.getReelId());
        };
        ReelMoverCascade.prototype.setSymbolFrames = function () {
            for (var i = 0; i < ReelMoverCascade.SYMBOLS_IN_REEL; ++i) {
                this.reelView.setSymbolFrame(i, this.wheel[ReelMoverCascade.SYMBOLS_IN_REEL * this.reelId + i] - 1);
            }
            this.setRandSymbolsForLastRow();
        };
        ReelMoverCascade.prototype.setRandSymbolsForLastRow = function () {
            var framesCnt = this.reelView.getNumFrames();
            this.reelView.setSymbolFrame(ReelMoverCascade.SYMBOLS_IN_REEL, Math.random() * (framesCnt - 1));
        };
        ReelMoverCascade.prototype.setServerResponse = function (wheel, is_synchro_stop) {
            if (is_synchro_stop === void 0) { is_synchro_stop = true; }
            this.wheel = wheel;
        };
        ReelMoverCascade.prototype.recursionProcessFalling = function (common) {
            this.reelView.recursionProcessFalling(common);
            //for(var i:number=0; i<ReelMoverCascade.SYMBOLS_IN_REEL; ++i) {
            //	this.reelView.recursionProcessFalling(common);
            //}
        };
        ReelMoverCascade.prototype.processRecursionRotation = function (common) {
            this.reelView.recursionProcessRotation(common);
        };
        ReelMoverCascade.MOVE_DOWN_TIME = 900;
        ReelMoverCascade.MOVE_DOWN_DELAY_REELS = 100;
        ReelMoverCascade.MOVE_DOWN_DELAY_SYMBOLS = 30;
        ReelMoverCascade.RECURSION_ROTATION_TIME = 300;
        ReelMoverCascade.RECURSION_SYMBOLS_FALLING_TIME = 300;
        ReelMoverCascade.STATE2_NORMAL = "state_normal";
        ReelMoverCascade.STATE2_DOWN_START = "state_down_start";
        ReelMoverCascade.STATE2_DOWN_STABLE = "state_down_stable";
        ReelMoverCascade.STATE2_SHOW_NEW_SYMBOLS = "state_show_new_symbols";
        ReelMoverCascade.STATE2_END_ANIMATION = "state_end_animation";
        ReelMoverCascade.STATE2_STOPPED_RECURSION = "state_stopped_recursion";
        ReelMoverCascade.SYMBOLS_IN_REEL = 3;
        // events
        ReelMoverCascade.EVENT_REEL_STOPPED = "event_reel_stopped";
        ReelMoverCascade.EVENT_REEL_PREPARE_STOPPED = "event_reel_prepare_stopped";
        // reel strips type
        ReelMoverCascade.TYPE_REGULAR = "regular";
        ReelMoverCascade.TYPE_FREE_SPIN = "freeSpins";
        return ReelMoverCascade;
    })(EventDispatcher);
    engine.ReelMoverCascade = ReelMoverCascade;
})(engine || (engine = {}));
/**
 * Created by Taras on 17.08.2015.
 */
var engine;
(function (engine) {
    var ReelMoverTypes = (function () {
        function ReelMoverTypes() {
        }
        ReelMoverTypes.TAPE = "ReelMoverTape";
        ReelMoverTypes.CASCADE = "ReelMoverCascade";
        ReelMoverTypes.DEFAULT = ReelMoverTypes.TAPE;
        return ReelMoverTypes;
    })();
    engine.ReelMoverTypes = ReelMoverTypes;
})(engine || (engine = {}));
var engine;
(function (engine) {
    var LoadQueue = createjs.LoadQueue;
    var Sound = createjs.Sound;
    var WebAudioPlugin = createjs.WebAudioPlugin;
    var HTMLAudioPlugin = createjs.HTMLAudioPlugin;
    var LayoutCreator = layout.LayoutCreator;
    var LoaderController = (function (_super) {
        __extends(LoaderController, _super);
        function LoaderController(manager, common, view) {
            _super.call(this, manager);
            this.common = common;
            this.view = view;
            this.isGameStarted = false;
        }
        LoaderController.prototype.init = function () {
            var _this = this;
            _super.prototype.init.call(this);
            this.common.isSoundLoaded = false;
            this.view.on(LayoutCreator.EVENT_LOADED, function () {
                _this.send(engine.NotificationList.HIDE_PRELOADER);
                _this.send(engine.NotificationList.LOADER_LOADED);
                _this.initLoader();
            });
            this.firstUp = 0;
            this.tempLayout = [];
            this.view.load(this.common.dirName + engine.GameConst.MAIN_RES_FOLDER + this.common.gameName + "/" + engine.GameConst.LAYOUT_FOLDER + engine.LoaderView.LAYOUT_NAME + engine.FileConst.LAYOUT_EXTENSION, true);
        };
        LoaderController.prototype.listNotification = function () {
            var notifications = _super.prototype.listNotification.call(this);
            notifications.push(engine.NotificationList.SERVER_INIT);
            notifications.push(engine.NotificationList.LOAD_SOUNDS);
            notifications.push(engine.NotificationList.LAZY_LOAD);
            notifications.push(engine.NotificationList.LAZY_LOAD_COMP);
            return notifications;
        };
        LoaderController.prototype.handleNotification = function (message, data) {
            switch (message) {
                case engine.NotificationList.SERVER_INIT:
                    {
                        break;
                    }
                case engine.NotificationList.LOAD_SOUNDS:
                    {
                        this.loadSounds();
                        break;
                    }
            }
        };
        LoaderController.prototype.initLoader = function () {
            var _this = this;
            for (var i = 0; i < this.view.buttonsNames.length; i++) {
                var buttonName = this.view.buttonsNames[i];
                var button = this.view.getChildByName(buttonName);
                button.on("click", function (eventObj) {
                    if (eventObj.nativeEvent instanceof MouseEvent) {
                        _this.onButtonClick(eventObj.currentTarget);
                    }
                });
            }
        };
        LoaderController.prototype.onButtonClick = function (button) {
            switch (button.name) {
                case engine.LoaderView.BUTTON_YES:
                    {
                        this.common.isSoundOn = true;
                        break;
                    }
                case engine.LoaderView.BUTTON_NO:
                    {
                        this.common.isSoundOn = false;
                        break;
                    }
            }
            if (this.common.isMobile) {
                engine.FullScreen.fullScreen();
            }
            this.loadConfig();
            this.view.hideButtons();
            this.view.showProgressBar();
        };
        LoaderController.prototype.loadSounds = function () {
            var _this = this;
            var totalFiles = this.common.config.sounds.length;
            var leftLoad = totalFiles;
            if (leftLoad > 0) {
                Sound.alternateExtensions = ["mp3"];
                Sound.registerPlugins([WebAudioPlugin, HTMLAudioPlugin]);
                Sound.addEventListener("fileload", function (event) {
                    console.log("loaded sound id = " + event.id + " url = " + event.src);
                    leftLoad--;
                    //if (this.view != null) {
                    //	var progress = LoaderController.ASSETS_PERCENTAGE + (totalFiles - leftLoad) / totalFiles * LoaderController.SOUND_PERCENTAGE;
                    //	this.view.setProgress(progress);
                    //}
                    if (leftLoad == 0) {
                        _this.onSoundsLoaded();
                    }
                });
                Sound.registerManifest(this.common.config.sounds, this.common.dirName + engine.GameConst.MAIN_RES_FOLDER + this.common.gameName + "/" + engine.GameConst.SOUNDS_FOLDER);
            }
            else {
                this.onSoundsLoaded();
            }
        };
        LoaderController.prototype.onSoundsLoaded = function () {
            this.common.isSoundLoaded = true;
            this.onLoaded();
        };
        LoaderController.prototype.loadConfig = function () {
            var _this = this;
            var configUrl = this.common.dirName + engine.GameConst.MAIN_RES_FOLDER + this.common.gameName + "/" + engine.GameConst.GAME_CONFIG_NAME + engine.FileConst.JSON_EXTENSION + "?" + this.common.key;
            var assetVO = new engine.AssetVO(engine.GameConst.GAME_CONFIG_NAME, configUrl, LoadQueue.JSON);
            createjs.LoadQueue.loadTimeout = 1000;
            var configLoader = new LoadQueue(false);
            configLoader.on("complete", function () {
                _this.common.config = configLoader.getResult(engine.GameConst.GAME_CONFIG_NAME);
                engine.GameConst.COMMON_LAYOUTS_FOLDER = _this.common.config.common_layouts_folder ? _this.common.config.common_layouts_folder : engine.GameConst.COMMON_LAYOUTS_FOLDER;
                _this.loadFonts();
            });
            configLoader.setMaxConnections(10);
            configLoader.loadFile(assetVO);
        };
        LoaderController.prototype.loadFonts = function () {
            var _this = this;
            var fonts = this.common.config.fonts;
            if (fonts != null && fonts.length > 0) {
                var style = document.createElement('style');
                var manifest = [];
                for (var i = 0; i < fonts.length; i++) {
                    var fontName = fonts[i].name;
                    var baseFontUrl = (this.common.config["common_fonts"] && this.common.config["common_fonts"].indexOf(fontName) >= 0) ? this.getCommonUrlDownl() : this.getUrlDownl();
                    var fontURL = baseFontUrl + engine.GameConst.FONTS_FOLDER + fonts[i].fileName + engine.FileConst.WOFF_EXTENSION;
                    style.appendChild(document.createTextNode("@font-face {font-family: '" + fontName + "'; src: url('" + fontURL + "');}"));
                    document.head.appendChild(style);
                    manifest.push({ "id": fontName, "src": fontURL });
                }
                var configLoader = new LoadQueue(false);
                configLoader.on("complete", function () {
                    _this.getForFirst();
                });
                configLoader.loadManifest(manifest);
            }
            else {
                this.getForFirst();
            }
        };
        LoaderController.prototype.getUrlDownl = function () {
            return this.common.dirName + engine.GameConst.MAIN_RES_FOLDER + this.common.gameName + "/";
        };
        LoaderController.prototype.getCommonUrlDownl = function () {
            return engine.GameConst.COMMON_LAYOUTS_FOLDER + "game" + ((this.common.isMobile) ? "_mobile" : "") + "/";
        };
        LoaderController.prototype.loadLayouts = function (from, to, onComplete, eachLoaded) {
            if (onComplete === void 0) { onComplete = null; }
            if (eachLoaded === void 0) { eachLoaded = null; }
            if (to > this.maxLeyers)
                return;
            var layoutsName = this.common.config.layouts;
            var layouts = this.tempLayout;
            for (var i = from; i < to; i++) {
                var layoutName = layoutsName[i];
                var LayoutClass = engine[layoutName];
                layouts[layoutName] = LayoutClass ? new LayoutClass() : new LayoutCreator();
            }
            var needLoad = to - from;
            var leftLoad = needLoad;
            //console.log("common_layouts", this.common.config["common_layouts"]);
            var baseLayoutUrl = this.getUrlDownl();
            for (var i = from; i < to; i++) {
                var layoutName = layoutsName[i];
                var layoutObj = layouts[layoutName];
                layoutObj.on(LayoutCreator.EVENT_LOADED, function (eventObj) {
                    eventObj.currentTarget['isLoaded'] = true;
                    eachLoaded != null && eachLoaded();
                    leftLoad -= 1;
                    if (leftLoad == 0) {
                        onComplete != null && onComplete();
                    }
                });
                //console.log("baseLayoutUrl", this.common.config["common_layouts"], "layoutName="+layoutName);
                baseLayoutUrl = (this.common.config["common_layouts"] && this.common.config["common_layouts"].indexOf(layoutName) >= 0) ? this.getCommonUrlDownl() : this.getUrlDownl();
                var url = baseLayoutUrl + engine.GameConst.LAYOUT_FOLDER + layoutName + engine.FileConst.LAYOUT_EXTENSION + "?" + this.common.key;
                layoutObj.load(url);
                layoutObj['url'] = url;
            }
            this.common.layouts = layouts;
        };
        LoaderController.prototype.initGame = function () {
            var _this = this;
            this.send(engine.NotificationList.RES_LOADED);
            if (this.view != null) {
                this.view.dispose();
                this.view = null;
            }
            this.isGameStarted = true;
            this.loadLayouts(this.firstUp, this.common.config.layouts.length, function () {
                //if (this.common.isSoundOn) {
                //	this.send(NotificationList.LOAD_SOUNDS);
                //}
                //else {
                //	this.onLoaded();
                //}
                _this.common.isLayoutsLoaded = true;
                _this.onLoaded();
            });
        };
        LoaderController.prototype.getForFirst = function () {
            var _this = this;
            if (this.common.isSoundOn) {
                this.send(engine.NotificationList.LOAD_SOUNDS);
            }
            //GameConst.COMMON_LAYOUTS_FOLDER = this.common.config.common_layouts_folder ? this.common.config.common_layouts_folder : GameConst.COMMON_LAYOUTS_FOLDER;
            this.breakPoint = this.common.config.priorityLevel;
            this.maxLeyers = this.common.config.layouts.length;
            console.log("getForFirst this.breakPoint=" + this.breakPoint + " this.maxLeyers=" + this.maxLeyers);
            this.loadLayouts(this.firstUp, this.breakPoint, function () {
                _this.firstUp = _this.breakPoint;
                _this.initGame();
            }, function () {
                ++_this.firstUp;
                _this.updateLoader();
            });
        };
        LoaderController.prototype.updateLoader = function () {
            if (this.view) {
                var leftLoad = this.breakPoint - this.firstUp;
                var progress = (this.breakPoint - leftLoad) / this.breakPoint;
                this.view.setProgress(progress);
            }
        };
        LoaderController.prototype.onLoaded = function () {
            if (this.common.isLayoutsLoaded && ((this.common.isSoundOn && this.common.isSoundLoaded) || !this.common.isSoundOn)) {
                this.send(engine.NotificationList.LAZY_LOAD_COMP);
                this.dispose();
            }
        };
        LoaderController.prototype.dispose = function () {
            if (this.common.isSoundLoaded) {
                _super.prototype.dispose.call(this);
                this.common = null;
            }
        };
        LoaderController.RELOAD_TIME = 20000;
        LoaderController.SOUND_PERCENTAGE = 0.4;
        LoaderController.ASSETS_PERCENTAGE = 1 - LoaderController.SOUND_PERCENTAGE;
        return LoaderController;
    })(engine.BaseController);
    engine.LoaderController = LoaderController;
})(engine || (engine = {}));
var engine;
(function (engine) {
    var Point = createjs.Point;
    var Tween = createjs.Tween;
    var WinController = (function (_super) {
        __extends(WinController, _super);
        function WinController(manager, common, container) {
            _super.call(this, manager);
            this.common = common;
            this.container = container;
        }
        WinController.prototype.init = function () {
            _super.prototype.init.call(this);
        };
        WinController.prototype.listNotification = function () {
            var notifications = _super.prototype.listNotification.call(this);
            notifications.push(engine.NotificationList.SHOW_WIN_LINES);
            notifications.push(engine.NotificationList.REMOVE_WIN_LINES);
            notifications.push(engine.NotificationList.OPEN_PAY_TABLE);
            notifications.push(engine.NotificationList.CLOSE_PAY_TABLE);
            notifications.push(engine.NotificationList.WIN_LINES_SHOWED);
            return notifications;
        };
        WinController.prototype.handleNotification = function (message, data) {
            switch (message) {
                case engine.NotificationList.SHOW_WIN_LINES:
                    {
                        this.create(data);
                        break;
                    }
                case engine.NotificationList.REMOVE_WIN_LINES:
                    {
                        this.send(engine.NotificationList.SHOW_ALL_SYMBOLS);
                        this.remove();
                        break;
                    }
                case engine.NotificationList.OPEN_PAY_TABLE:
                    {
                        this.container.visible = false;
                        break;
                    }
                case engine.NotificationList.CLOSE_PAY_TABLE:
                    {
                        this.container.visible = true;
                        break;
                    }
                case engine.NotificationList.WIN_LINES_SHOWED:
                    {
                        this.processRecursion();
                        break;
                    }
            }
        };
        WinController.prototype.processRecursion = function () {
            //console.log("processing recursion", this.common.server.recursion.slice(0));
            var self = this;
            if (this.common.server.recursion.length) {
                this.send(engine.NotificationList.RECURSION_PROCESS_ROTATION);
                this.winLines.processRecursionRotation(this.common.isMobile);
                Tween.get(this).wait(engine.ReelMoverCascade.RECURSION_ROTATION_TIME).call(function () {
                    self.afterRecursionRotation();
                });
            }
            else {
                var delay_time = this.common.server.recursion_len ? engine.ReelMoverCascade.RECURSION_SYMBOLS_FALLING_TIME : 0;
                Tween.get(this).wait(delay_time).call(function () {
                    self.afterProcessRecursion();
                });
            }
        };
        WinController.prototype.afterRecursionRotation = function () {
            this.send(engine.NotificationList.REMOVE_WIN_LINES);
            this.send(engine.NotificationList.HIDE_LINES);
            this.send(engine.NotificationList.RECURSION_PROCESS_FALLING);
            if (this.common.server.recursion[0]) {
                this.common.server.winLines = this.common.server.recursion[0]["winLines"].slice(0);
                this.common.server.arrLinesIds = this.common.server.recursion[0]["arrLinesIds"].slice(0);
                this.common.server.wheel = this.common.server.recursion[0]["wheels"].slice(0);
            }
            var self = this;
            Tween.get(this).wait(engine.ReelMoverCascade.RECURSION_SYMBOLS_FALLING_TIME).call(function () {
                self.afterRecursionFalling();
            });
        };
        WinController.prototype.afterRecursionFalling = function () {
            this.common.server.win += this.common.server.bet * this.common.server.recursion[0]['win'];
            this.send(engine.NotificationList.SHOW_WIN_TF);
            this.common.server.recursion.shift();
            this.send(engine.NotificationList.SHOW_WIN_LINES, true);
            this.send(engine.NotificationList.RECURSION_PROCESSED_FALLING, true);
        };
        WinController.prototype.afterProcessRecursion = function () {
            //console.log("afterProcessRecursion");
            this.send(engine.NotificationList.UPDATE_BALANCE_TF);
            this.send(engine.NotificationList.RECURSION_PROCESSED);
        };
        WinController.prototype.onEnterFrame = function () {
            if (this.winLines != null) {
                this.winLines.onEnterFrame();
            }
        };
        WinController.prototype.dispose = function () {
            _super.prototype.dispose.call(this);
            this.remove();
            this.common = null;
            this.container = null;
        };
        WinController.prototype.create = function (isTogether) {
            var _this = this;
            var winLinesVOs = this.common.server.winLines;
            if (winLinesVOs.length == 0) {
                this.send(engine.NotificationList.WIN_LINES_SHOWED);
                return;
            }
            if (isTogether) {
                this.winLines = new engine.WinLinesTogether(this.createWinLines(), this.common.config.winAnimationTime);
            }
            else {
                this.winLines = new engine.WinLines(this.createWinLines(), this.common.config.winAnimationTime);
            }
            this.winLines.on(engine.BaseWinLines.EVENT_CREATE_LINE, function (eventObj) {
                if (_this.common.config.hide_symbols_on_animation) {
                    var hydeSymbols = eventObj.target[0];
                    _this.send(engine.NotificationList.HIDE_SYMBOLS, hydeSymbols);
                }
                _this.send(engine.NotificationList.HIDE_LINES);
                _this.send(engine.NotificationList.SHOW_LINES, eventObj.target);
            });
            this.winLines.on(engine.BaseWinLines.EVENT_All_LINES_SHOWED, function () {
                _this.send(engine.NotificationList.WIN_LINES_SHOWED);
            });
            this.winLines.on(engine.BaseWinLines.EVENT_LINE_SHOWED, function (eventObj) {
                //this.send(NotificationList.SHOW_ALL_SYMBOLS);
                //var index:number = eventObj.currentTarget.index;
                //var hydeSymbols:Array<Point> = this.symbolsToHide[index];
                //this.send(NotificationList.HIDE_SYMBOLS, hydeSymbols);
            });
            for (var i = 0; i < this.winLines.winLines.length; ++i) {
                this.winLines.winLines[i].on(engine.BaseWinLines.SHOW_WILD_REEL, function (data) {
                    _this.send(engine.NotificationList.SHOW_WILD_REEL, data.currentTarget.wildLineId);
                });
            }
            this.winLines.create();
        };
        WinController.prototype.createWinLines = function () {
            //console.log("this.common.server.wheel", this.common.server.wheel);
            var wheel = this.common.server.wheel;
            var symbolsRect = this.common.symbolsRect;
            var winLinesVOs = this.common.server.winLines;
            var symbolCount = symbolsRect[0].length;
            var allWinAniVOs = [];
            var winLines = [];
            this.symbolsToHide = [];
            for (var i = 0; i < winLinesVOs.length; i++) {
                var maskedSymbols = [];
                var winLinesVO = winLinesVOs[i];
                var winPos = winLinesVO.winPos;
                var winAniVOs = [];
                for (var reelId = 0; reelId < winPos.length; reelId++) {
                    var symbolIdx = winPos[reelId] - 1;
                    if (symbolIdx >= 0) {
                        var rect = symbolsRect[reelId][symbolIdx];
                        var posIdx = new Point(reelId, symbolIdx);
                        var winAniVO = WinController.findWinVO(allWinAniVOs, posIdx);
                        var winSymbolView;
                        if (winAniVO == null) {
                            //console.log("createWinLines symbolid="+wheel[symbolIdx + ReelMover.SYMBOLS_IN_REEL * reelId]+" symbolIdx="+symbolIdx+" symbolCount="+symbolCount+" reelId="+reelId);
                            //console.log("winPos", winPos);
                            //console.log("wheel", wheel);
                            winSymbolView = this.createWinSymbol(wheel[symbolIdx + engine.ReelMover.SYMBOLS_IN_REEL * reelId]);
                        }
                        else {
                            winSymbolView = winAniVO.winSymbolView;
                        }
                        winAniVO = new engine.WinAniVO();
                        winAniVO.winSymbolView = winSymbolView;
                        winAniVO.rect = rect;
                        winAniVO.posIdx = posIdx;
                        var hideSymbol = posIdx;
                        allWinAniVOs.push(winAniVO);
                        winAniVOs.push(winAniVO);
                        maskedSymbols.push(hideSymbol);
                    }
                }
                var winLine = new engine.WinLine(this.container, winLinesVO.lineId, winAniVOs);
                winLines.push(winLine);
            }
            this.symbolsToHide.push(maskedSymbols);
            return winLines;
        };
        WinController.prototype.createWinSymbol = function (symbolId) {
            var regularAni = this.common.layouts[WinController.WIN_ANIMATION_PREFIX + symbolId];
            var highlightAni = this.common.layouts[WinController.WIN_ANIMATION_HIGHLIGHT_PREFIX];
            var winSymbolView = new engine.WinSymbolView();
            winSymbolView.create(regularAni, highlightAni, this.common.isMobile);
            if (typeof (this.common.config.wildSymbolId) != "undefined" && symbolId == this.common.config.wildSymbolId && this.common.layouts[WinController.WIN_ANIMATION_PREFIX + this.common.config.wildSymbolId]) {
                winSymbolView._is_wild = true;
            }
            return winSymbolView;
        };
        WinController.findWinVO = function (winVOs, posIdx) {
            for (var i = 0; i < winVOs.length; i++) {
                var vo = winVOs[i];
                if (vo.posIdx.x == posIdx.x && vo.posIdx.y == posIdx.y) {
                    return vo;
                }
            }
            return null;
        };
        WinController.prototype.remove = function () {
            if (this.winLines != null) {
                this.winLines.dispose();
                this.winLines = null;
            }
        };
        WinController.WIN_ANIMATION_PREFIX = "WinAnimation_";
        WinController.WIN_ANIMATION_HIGHLIGHT_PREFIX = "SymbolHighlight";
        return WinController;
    })(engine.BaseController);
    engine.WinController = WinController;
})(engine || (engine = {}));
var engine;
(function (engine) {
    var Ticker = createjs.Ticker;
    var Touch = createjs.Touch;
    var Container = createjs.Container;
    var GameController = (function (_super) {
        __extends(GameController, _super);
        function GameController() {
            _super.call(this, new engine.ControllerManager());
            this.windowParams = {};
            this.payTablesInited = false;
        }
        /*public start(gameName:string, key:string, container:Container):void {
            super.init();

            this.container = container;
            this.common = new CommonRefs();
            this.common.gameName = gameName;
            this.common.key = key;
            this.common.isMobile = Device.isMobile();

            this.initBaseControllers();
        }*/
        GameController.prototype.startSingle = function (stage, artWidth, artHeight, gameName, mobile, isDemo, login, password, isDebug, room, getParams) {
            var _this = this;
            if (getParams === void 0) { getParams = []; }
            this.getWindowParams();
            console.log(isDemo, "isDemo");
            //alert("startSingle: window.outerWidth="+window.outerWidth+" window.outerHeight="+window.outerHeight+" window.innerWidth="+this.windowParams.innerWidth+" window.innerHeight="+this.windowParams.innerHeight);
            _super.prototype.init.call(this);
            this.stage = stage;
            this.common = new engine.CommonRefs();
            this.common.isMobile = mobile;
            //this.common.isMobile = true; //always load mobile version //mobiletoweb
            this.common.gameName = this.common.isMobile ? gameName + "_mobile" : gameName;
            this.common.dirName = gameName + "/"; //ALLINONEGAMES
            //this.common.dirName = ""; //SEPARETEGAMES
            this.common.login = login;
            this.common.password = password;
            this.common.room = room;
            this.common.isDemo = isDemo;
            this.common.isDebug = isDebug;
            this.common.artWidth = artWidth;
            this.common.artHeight = artHeight;
            this.common.getParams = getParams;
            this.common.key = (getParams['token']) ? getParams['token'] : null;
            Ticker.setFPS(engine.GameConst.GAME_FPS);
            this.container = new Container();
            if (!this.common.isMobile) {
                this.stage.enableMouseOver(Ticker.getFPS());
            }
            //this.stage.enableMouseOver(Ticker.getFPS()); //mobiletoweb
            this.stage.mouseMoveOutside = true;
            if (Touch.isSupported()) {
                Touch.enable(this.stage, true, true);
            }
            this.stage.addChild(this.container);
            Ticker.addEventListener("tick", function () {
                _this.gameUpdate();
            });
            engine.FullScreen.init();
            this.onResize();
            this.initBaseControllers();
        };
        GameController.prototype.gameUpdate = function () {
            this.manager.onEnterFrame();
        };
        GameController.prototype.onEnterFrame = function () {
            if (this.stage != null) {
                this.stage.update();
            }
        };
        GameController.prototype.listNotification = function () {
            var notifications = _super.prototype.listNotification.call(this);
            notifications.push(engine.NotificationList.SERVER_INIT);
            notifications.push(engine.NotificationList.BACK_TO_LOBBY);
            notifications.push(engine.NotificationList.HIDE_PRELOADER);
            notifications.push(engine.NotificationList.LAZY_LOAD_COMP);
            //notifications.push(NotificationList.LOADER_LOADED);
            return notifications;
        };
        GameController.prototype.handleNotification = function (message, data) {
            switch (message) {
                case engine.NotificationList.SERVER_INIT:
                    {
                        this.initGameControllers();
                        this.initKeyboardHandler();
                        break;
                    }
                case engine.NotificationList.BACK_TO_LOBBY:
                    {
                        this.container.dispatchEvent(GameController.EVENT_BACK_TO_LOBBY);
                        break;
                    }
                case engine.NotificationList.HIDE_PRELOADER:
                    {
                        this.stage.dispatchEvent(GameController.EVENT_HIDE_PRELOADER);
                        break;
                    }
                case engine.NotificationList.LAZY_LOAD_COMP:
                    {
                        this.initPayTables();
                        break;
                    }
            }
        };
        GameController.prototype.initBaseControllers = function () {
            var baseController;
            baseController = new engine.ServerController(this.manager, this.common);
            baseController.init();
            this.loaderView = new engine.LoaderView();
            baseController = new engine.LoaderController(this.manager, this.common, this.loaderView);
            baseController.init();
            this.container.addChild(this.loaderView);
        };
        GameController.prototype.initPayTables = function () {
            if (!this.payTablesInited) {
                this.payTablesInited = true;
                var baseController;
                baseController = new engine.PayTableController(this.manager, this.common, this.container);
                baseController.init();
                // create background
                var backgroundContainer = new Container();
                baseController = new engine.BackgroundController(this.manager, this.common, backgroundContainer);
                baseController.init();
                //if(this.bgTemp)this.container.removeChild(this.bgTemp);
                this.container.addChildAt(backgroundContainer, 0);
                this.common.payTableLoaded = true;
                this.send(engine.NotificationList.LOAD_PAY_TABLE);
            }
        };
        GameController.prototype.initGameControllers = function () {
            console.log("this.common.config", this.common.config);
            //Ticker.setFPS(GameController.TICKER_FPS);
            var baseController;
            this.onResize();
            // create header
            var headerContainer = new Container();
            baseController = new engine.HeaderController(this.manager, this.common, headerContainer);
            baseController.init();
            // create reels
            //var reelsView:ReelsView = this.common.layouts[ReelsView.LAYOUT_NAME];
            //baseController = new ReelController(this.manager, this.common, reelsView);
            //baseController.init();
            this.common.moverType = (this.common.config.reelMoverType) ? this.common.config.reelMoverType : engine.ReelMoverTypes.DEFAULT;
            switch (this.common.moverType) {
                case engine.ReelMoverTypes.TAPE:
                    {
                        var reelsView = this.common.layouts[engine.ReelsView.LAYOUT_NAME];
                        baseController = new engine.ReelController(this.manager, this.common, reelsView);
                        break;
                    }
                case engine.ReelMoverTypes.CASCADE:
                    {
                        var reelsView = this.common.layouts[engine.ReelsView.LAYOUT_NAME];
                        baseController = new engine.ReelControllerCascade(this.manager, this.common, reelsView);
                        break;
                    }
            }
            baseController.init();
            //prepare canvas to draw
            //var linesView:Container = new Container();
            //baseController = new DrawController(this.manager, this.common, linesView);
            //baseController.init();
            // create lines
            var linesView = this.common.layouts[engine.LinesView.LAYOUT_NAME];
            baseController = new engine.LinesController(this.manager, this.common, linesView);
            baseController.init();
            // create win animation
            var winView = new Container();
            baseController = new engine.WinController(this.manager, this.common, winView);
            baseController.init();
            //create Wild Views
            baseController = new engine.ReelWildController(this.manager, this.common, reelsView, winView);
            baseController.init();
            // create bonus
            var bonusContainer = new Container();
            baseController = new engine.BonusHolderController(this.manager, this.common, bonusContainer);
            baseController.init();
            // create game hud
            var hudView = this.common.layouts[engine.HudView.LAYOUT_NAME];
            hudView.create();
            //if(this.common.isMobile && this.common.layouts[BorderView.LAYOUT_NAME]){
            //	var borderView:BorderView = this.common.layouts[BorderView.LAYOUT_NAME];
            //	baseController = new BorderController(this.manager, this.common, borderView);
            //	baseController.init();
            //}
            //JACKPOT
            if (this.common.layouts[engine.JackpotView.LAYOUT_NAME]) {
                var jackpotView = this.common.layouts[engine.JackpotView.LAYOUT_NAME];
                jackpotView.create();
                baseController = new engine.JackpotController(this.manager, this.common, jackpotView);
                baseController.init();
                //headerContainer.addChild(jackpotView); //jackpot over bonus holder
                hudView.addChildAt(jackpotView, 0); //jackpot under bonus holder
            }
            if (this.common.layouts[engine.JackpotViewPopup.LAYOUT_NAME]) {
                var jackpotViewPopup = this.common.layouts[engine.JackpotViewPopup.LAYOUT_NAME];
                jackpotViewPopup.create();
            }
            // create bonus
            var modalContainer = new Container();
            baseController = new engine.ModalWindowController(this.manager, this.common, modalContainer);
            baseController.init();
            // error controller
            baseController = new engine.ErrorController(this.manager, this.common, modalContainer);
            baseController.init();
            var autoSpinTableView = hudView.getChildByName(engine.HudView.AUTO_SPIN_TABLE);
            if (autoSpinTableView != null) {
                var autoSpinTableMask = hudView.getChildByName(engine.HudView.AUTO_SPIN_TABLE_MASK);
                baseController = new engine.AutoSpinTableController(this.manager, this.common, autoSpinTableView, autoSpinTableMask);
                baseController.init();
            }
            baseController = new engine.StatsController(this.manager, this.common, hudView);
            baseController.init();
            var hudController = new engine.HudController(this.manager, this.common, hudView);
            hudController.init();
            var settingsView = hudView.getChildByName(engine.HudView.SETTINGS_MENU);
            if (settingsView != null) {
                baseController = new engine.SettingsController(this.manager, this.common, hudController, settingsView);
                baseController.init();
            }
            baseController = new engine.SoundController(this.manager, this.common);
            baseController.init();
            this.container.addChild(reelsView);
            this.container.addChild(linesView);
            //if(borderView)this.container.addChild(borderView); // if exist border view
            this.container.addChild(winView);
            this.container.addChild(hudView);
            if (this.common.isMobile) {
                hudView.resizeForMobile();
            }
            this.container.addChild(bonusContainer);
            this.container.addChild(headerContainer);
            this.container.addChild(modalContainer);
            //this.container.addChild(wheelView);
            //// create set wheel for debug
            //if (this.common.isDebug) {
            //	var wheelView:SetWheelView = new SetWheelView(this.common.symbolsRect.length, this.common.symbolsRect[0].length);
            //	baseController = new SetWheelController(this.manager, wheelView);
            //	baseController.init();
            //}
        };
        GameController.prototype.initKeyboardHandler = function () {
            var _this = this;
            document.onkeydown = function (event) {
                _this.send(engine.NotificationList.KEYBOARD_CLICK, event.keyCode);
            };
        };
        GameController.prototype.showChangeRotation = function () {
            if (this.windowParams.innerHeight > this.windowParams.innerWidth) {
                if (this.orientationView == undefined) {
                    if (this.common.layouts != null) {
                        this.orientationView = this.common.layouts[engine.RotateScreenView.LAYOUT_NAME];
                        if (this.orientationView != null) {
                            this.orientationView.create();
                        }
                    }
                }
                if (this.orientationView != null && !this.stage.contains(this.orientationView)) {
                    this.orientationView.play();
                    this.stage.addChild(this.orientationView);
                    this.container.visible = false;
                }
            }
            else {
                if (this.orientationView != undefined && this.stage.contains(this.orientationView)) {
                    this.orientationView.stop();
                    this.stage.removeChild(this.orientationView);
                    this.container.visible = true;
                }
            }
        };
        GameController.prototype.onResize = function () {
            this.getWindowParams();
            var canvas = this.stage.canvas;
            if (this.common.isMobile) {
                console.log("is full screen = " + engine.FullScreen.isFullScreen());
                this.showChangeRotation();
                window.scrollTo(0, 1);
            }
            var maxWidth = this.windowParams.innerWidth;
            var maxHeight = this.windowParams.innerHeight;
            //alert("onResize: window.outerWidth="+window.outerWidth+" window.outerHeight="+window.outerHeight+" window.innerWidth="+this.windowParams.innerWidth+" window.innerHeight="+this.windowParams.innerHeight);
            // keep aspect ratio
            var scale = Math.min(maxWidth / this.common.artWidth, maxHeight / this.common.artHeight);
            if (scale > 1 && !this.common.isMobile) {
                scale = 1;
            }
            this.stage.scaleX = scale;
            this.stage.scaleY = scale;
            this.stage.x = (maxWidth - this.common.artWidth * scale) / 2;
            this.stage.y = (maxHeight - this.common.artHeight * scale) / 2;
            // adjust canvas size
            canvas.width = maxWidth;
            canvas.height = maxHeight;
            if (this.common.isMobile && this.common.layouts != null) {
                //this.bgTemp = new Shape();
                //var x:number = -this.stage.x / this.stage.scaleX;
                //var color:string = this.common.config.colorBg || "#FFD8D0";
                //this.bgTemp.graphics.beginFill(color).drawRect(x, this.container.y, maxWidth / scale, maxHeight / scale);
                //this.container.addChildAt(this.bgTemp, 0);
                var hudView = this.common.layouts[engine.HudView.LAYOUT_NAME];
                if (hudView)
                    hudView.resizeForMobile();
                var payView = this.common.layouts[engine.PayTableHudView.LAYOUT_NAME];
                if (payView)
                    payView.resizeForMobile();
            }
        };
        GameController.prototype.dispose = function () {
            this.send(engine.NotificationList.SERVER_DISCONNECT);
        };
        GameController.setFieldStroke = function (container, field, outline, color, offsetX, offsetY, blur) {
            var text = container.getChildByName(field);
            GameController.setTextStroke(text, outline, color, offsetX, offsetY, blur);
        };
        GameController.setTextStroke = function (text, outline, color, offsetX, offsetY, blur) {
            if (text) {
                text.outline = outline;
                text.shadow = new createjs.Shadow(color, offsetX, offsetY, blur);
            }
        };
        GameController.prototype.getWindowParams = function () {
            this.windowParams.innerWidth = document.getElementsByTagName('body')[0].clientWidth;
            this.windowParams.zoomLevel = document.documentElement.clientWidth / window.innerWidth;
            this.windowParams.innerHeight = window.innerHeight * this.windowParams.zoomLevel;
            return;
        };
        GameController.MOBILE_TO_WEB_SCALE = 1;
        GameController.SHADOW_COLOR = "rgba(0,0,0,1)";
        GameController.EVENT_BACK_TO_LOBBY = "back_to_lobby";
        GameController.EVENT_HIDE_PRELOADER = "hide_preloader";
        GameController.TICKER_FPS = 30;
        return GameController;
    })(engine.BaseController);
    engine.GameController = GameController;
})(engine || (engine = {}));
/**
 * Created by Taras on 22.12.2014.
 */
var engine;
(function (engine) {
    var DrawLine = (function () {
        function DrawLine(container, common) {
            this.container = container;
            this.common = common;
            this.winLines = [];
        }
        DrawLine.prototype.create = function (id, alpha) {
            var width;
            var yc;
            var height;
            var params = this.common.symbolsRect[0][0];
            var x = params.x;
            var y = params.y;
            height = width = params.width;
            var cords = this.common.config.allLines[id - 1];
            var newLine = new createjs.Shape();
            var color = this.getRandomLineColor();
            yc = cords[0] + 1;
            newLine.graphics.setStrokeStyle(3).beginStroke(color).moveTo(x + (width / 2), y + ((yc * height) - height / 2));
            for (var c = 1; c < cords.length; c++) {
                yc = cords[c] + 1;
                newLine.graphics.lineTo(x + (c * width) + (width / 2), y + ((yc * height) - height / 2));
            }
            newLine.graphics.endStroke();
            newLine.alpha = alpha;
            newLine.id = id;
            this.winLines.push(newLine);
            return newLine;
        };
        DrawLine.prototype.remove = function () {
            for (var i = this.container.children.length; i >= 0; i--) {
                var line = this.container.children[i];
                this.container.removeChild(line);
            }
        };
        DrawLine.prototype.getRandomLineColor = function () {
            return '#41E969';
        };
        DrawLine.prototype.reset = function () {
            this.index = -1;
        };
        DrawLine.prototype.showNextLine = function () {
            this.hideAll();
            this.leftTime = this.time;
            this.index++;
            this.index = this.index < this.common.server.arrLinesIds.length && this.index >= 0 ? this.index : 0;
            this.showLine();
        };
        DrawLine.prototype.hideAll = function () {
            for (var i = 0; i < this.winLines.length; i++) {
                this.winLines[i].alpha = 0;
            }
        };
        DrawLine.prototype.showLine = function () {
            var id = this.common.server.arrLinesIds[this.index];
            var winLine;
            for (var i = 0, len = this.winLines.length; i < len; i++) {
                if (this.winLines[i].id === id) {
                    winLine = this.winLines[i];
                }
            }
            if (!winLine || winLine == null)
                return;
            winLine.alpha = 1;
        };
        return DrawLine;
    })();
    engine.DrawLine = DrawLine;
})(engine || (engine = {}));
var engine;
(function (engine) {
    var SetWheelController = (function (_super) {
        __extends(SetWheelController, _super);
        function SetWheelController(manager, view) {
            _super.call(this, manager);
            this.view = view;
        }
        SetWheelController.prototype.init = function () {
            var _this = this;
            _super.prototype.init.call(this);
            this.view.init();
            this.view.visible = false;
            var sendBtn = this.view.getSendBtn();
            sendBtn.onclick = function () {
                _this.send(engine.NotificationList.SERVER_SET_WHEEL, _this.view.getTextValue());
            };
        };
        SetWheelController.prototype.listNotification = function () {
            var notifications = _super.prototype.listNotification.call(this);
            notifications.push(engine.NotificationList.KEYBOARD_CLICK);
            return notifications;
        };
        SetWheelController.prototype.handleNotification = function (message, data) {
            switch (message) {
                case engine.NotificationList.KEYBOARD_CLICK:
                    {
                        if (data == engine.Keyboard.BACK_QUOTE) {
                            this.view.setVisible(!this.view.getVisible());
                        }
                        break;
                    }
            }
        };
        return SetWheelController;
    })(engine.BaseController);
    engine.SetWheelController = SetWheelController;
})(engine || (engine = {}));
var engine;
(function (engine) {
    var Container = createjs.Container;
    var Shape = createjs.Shape;
    var ModalWindowController = (function (_super) {
        __extends(ModalWindowController, _super);
        function ModalWindowController(manager, common, container) {
            _super.call(this, manager);
            this.common = common;
            this.container = container;
            this.isCreated = false;
        }
        ModalWindowController.prototype.init = function () {
            _super.prototype.init.call(this);
            this.create();
        };
        ModalWindowController.prototype.listNotification = function () {
            var notifications = _super.prototype.listNotification.call(this);
            notifications.push(engine.NotificationList.SHOW_AUTO_MODAL);
            notifications.push(engine.NotificationList.TRY_START_AUTO_PLAY);
            notifications.push(engine.NotificationList.SERVER_NOT_RESPONSE);
            notifications.push(engine.NotificationList.SHOW_ERRORS);
            notifications.push(engine.NotificationList.SHOW_JACKPOT_POPUP);
            return notifications;
        };
        ModalWindowController.prototype.initModal = function (info) {
            this.view = this.common.layouts[this.modalWindowName] || this.common.layouts[engine.ConnectionView.LAYOUT_NAME];
            this.view.create();
            this.overlay = new Container();
            var maskView = new Container();
            this.overlay.addChild(maskView);
            this.overlay.addChild(this.view);
            this.container.addChild(this.overlay);
            var stage = this.container.getStage();
            var canvas = stage.canvas;
            var leftX = -(canvas.width - stage.x) / stage.scaleX;
            var top = -(canvas.height - stage.y) / stage.scaleY;
            var mask = new Shape();
            var bounds = this.view.getBounds();
            mask.graphics.beginFill("0").drawRect(leftX, top, bounds.width * 10, bounds.height * 5);
            mask.alpha = 0.7;
            maskView.addEventListener("click", function () {
            });
            maskView.addChild(mask);
            this.initHandlers();
            if (info)
                this.view.setText(info);
        };
        ModalWindowController.prototype.initHandlers = function () {
            var _this = this;
            var buttonsNames = this.view.buttonsNames;
            var buttonsCount = buttonsNames.length;
            for (var i = 0; i < buttonsCount; i++) {
                this.view.getButton(buttonsNames[i]).on("click", function (eventObj) {
                    if (eventObj.nativeEvent instanceof MouseEvent) {
                        _this.onBtnClick(eventObj.currentTarget.name);
                    }
                });
                this.view.changeButtonState(buttonsNames[i], true, true);
            }
        };
        ModalWindowController.prototype.disposeModal = function () {
            this.removeHandlers();
            this.view = null;
            this.overlay = null;
            this.container.removeAllChildren();
        };
        ModalWindowController.prototype.removeHandlers = function () {
            var buttonsNames = this.view.buttonsNames;
            var buttonsCount = buttonsNames.length;
            for (var i = 0; i < buttonsCount; i++) {
                this.view.getButton(buttonsNames[i]).removeAllEventListeners("click");
            }
        };
        ModalWindowController.prototype.onBtnClick = function (buttonName) {
            switch (buttonName) {
                case engine.AutoPlayView.YES_BTN:
                    {
                        this.send(engine.NotificationList.AUTO_PLAY_CONT);
                        break;
                    }
                case engine.AutoPlayView.NO_BTN:
                    {
                        this.send(engine.NotificationList.AUTO_PLAY_COMP);
                        break;
                    }
                case engine.ConnectionView.OK_BTN:
                    {
                        window.location.reload();
                        break;
                    }
                case engine.ErrorView.OK_BTN:
                    {
                        //process error closing
                        this.send(engine.NotificationList.OK_BTN_ERROR_CLICKED);
                        break;
                    }
                case engine.JackpotViewPopup.JACKPOT_CONTINUE_BTN_NAME:
                    {
                        //process error closing
                        this.send(engine.NotificationList.SERVER_GET_BALANCE_REQUEST, null);
                        break;
                    }
            }
            this.disposeModal();
        };
        ModalWindowController.prototype.handleNotification = function (message, data) {
            switch (message) {
                case engine.NotificationList.SHOW_AUTO_MODAL:
                    {
                        this.modalWindowName = engine.AutoPlayView.LAYOUT_NAME;
                        this.initModal(data);
                        break;
                    }
                case engine.NotificationList.SERVER_NOT_RESPONSE:
                    {
                        this.modalWindowName = engine.ConnectionView.LAYOUT_NAME;
                        this.initModal(data);
                        break;
                    }
                case engine.NotificationList.SHOW_ERRORS:
                    {
                        this.modalWindowName = engine.ErrorView.LAYOUT_NAME;
                        this.initModal(data);
                        break;
                    }
                case engine.NotificationList.SHOW_JACKPOT_POPUP:
                    {
                        if (this.common.layouts[engine.JackpotViewPopup.LAYOUT_NAME]) {
                            this.modalWindowName = engine.JackpotViewPopup.LAYOUT_NAME;
                            this.initModal(data);
                        }
                        break;
                    }
            }
        };
        // create modal window
        ModalWindowController.prototype.create = function () {
            this.isCreated = true;
        };
        ModalWindowController.prototype.onGotResponse = function (data) {
        };
        return ModalWindowController;
    })(engine.BaseController);
    engine.ModalWindowController = ModalWindowController;
})(engine || (engine = {}));
var engine;
(function (engine) {
    var JackpotController = (function (_super) {
        __extends(JackpotController, _super);
        function JackpotController(manager, common, view) {
            _super.call(this, manager);
            this.common = common;
            this.view = view;
            if (this.common.config.JackpotView_X)
                this.view.x = this.common.config.JackpotView_X;
            if (this.common.config.JackpotView_Y)
                this.view.y = this.common.config.JackpotView_Y;
        }
        JackpotController.prototype.init = function () {
            _super.prototype.init.call(this);
            this.update_time = this.common.config.jackpot_update_time || JackpotController.UPDATE_TIME;
            this.view.onInit();
            this.send(engine.NotificationList.UPDATE_JACKPOT_SERVER);
            //if(!this.common.isMobile) JackpotView.JACKPOT_TF_TEXT = JackpotView.JACKPOT_TF_TEXT_OLD;
        };
        JackpotController.prototype.listNotification = function () {
            var notifications = _super.prototype.listNotification.call(this);
            notifications.push(engine.NotificationList.UPDATE_JACKPOT);
            notifications.push(engine.NotificationList.START_BONUS);
            //notifications.push(NotificationList.REMOVE_HEADER);
            notifications.push(engine.NotificationList.CREATE_BONUS);
            notifications.push(engine.NotificationList.END_BONUS);
            return notifications;
        };
        JackpotController.prototype.handleNotification = function (message, data) {
            var _this = this;
            switch (message) {
                case engine.NotificationList.UPDATE_JACKPOT:
                    {
                        this.setJackpot(data);
                        setTimeout(function () {
                            _this.send(engine.NotificationList.UPDATE_JACKPOT_SERVER);
                        }, this.update_time);
                        break;
                    }
                case engine.NotificationList.START_BONUS:
                case engine.NotificationList.REMOVE_HEADER:
                case engine.NotificationList.CREATE_BONUS:
                    {
                        if (this.common.isMobile)
                            this.view.visible = false;
                        break;
                    }
                case engine.NotificationList.END_BONUS:
                    {
                        this.view.visible = true;
                        break;
                    }
            }
        };
        JackpotController.prototype.setJackpot = function (jackpot) {
            this.view.setJackpot(parseFloat(jackpot));
        };
        JackpotController.prototype.dispose = function () {
            _super.prototype.dispose.call(this);
            this.common = null;
            this.container = null;
        };
        JackpotController.UPDATE_TIME = 10000;
        return JackpotController;
    })(engine.BaseController);
    engine.JackpotController = JackpotController;
})(engine || (engine = {}));
var engine;
(function (engine) {
    var BaseBonusController = (function (_super) {
        __extends(BaseBonusController, _super);
        function BaseBonusController(manager, common, container, startStep, isAutoRequest) {
            _super.call(this, manager);
            this.common = common;
            this.container = container;
            if (this.common.server.bonus.data == null && !this.common.server.bonus.step) {
                this.common.server.bonus.step = startStep;
            }
            this.isCreated = false;
            this.isAutoRequest = isAutoRequest;
        }
        BaseBonusController.prototype.init = function () {
            _super.prototype.init.call(this);
            var bonusData = this.common.server.bonus.data;
            //TODO: process bonus with bonusdata==null
            //console.log(this.common.server.bonus, "bonus");
            //console.log(this.common.server.bonus.key, "bonus key");
            if (bonusData == null && this.isAutoRequest) {
                this.sendRequest();
                this.isAutoRequest = false; // to check bonus with bonusData==null if it is double calling of init() (should work properly) or double creation of object (the error will remain)
            }
            else {
                this.create();
                if (bonusData != null) {
                    this.onGotResponse(bonusData);
                }
            }
        };
        BaseBonusController.prototype.listNotification = function () {
            var notifications = _super.prototype.listNotification.call(this);
            notifications.push(engine.NotificationList.SERVER_GOT_BONUS);
            notifications.push(engine.NotificationList.START_BONUS);
            notifications.push(engine.NotificationList.LAZY_LOAD_COMP);
            return notifications;
        };
        BaseBonusController.prototype.handleNotification = function (message, data) {
            switch (message) {
                case engine.NotificationList.SERVER_GOT_BONUS:
                    {
                        if (!this.isCreated) {
                            this.create();
                        }
                        this.onGotResponse(this.common.server.bonus.data);
                        break;
                    }
                case engine.NotificationList.LAZY_LOAD_COMP:
                    {
                        this.send(engine.NotificationList.START_BONUS_AUDIO);
                        break;
                    }
                case engine.NotificationList.START_BONUS:
                    {
                        this.send(engine.NotificationList.START_BONUS_AUDIO);
                        break;
                    }
            }
        };
        // create bonus game
        BaseBonusController.prototype.create = function () {
            this.isCreated = true;
        };
        BaseBonusController.prototype.sendRequest = function (param) {
            if (param === void 0) { param = 0; }
            this.send(engine.NotificationList.SERVER_SEND_BONUS, { "param": param, "step": ++this.common.server.bonus.step });
        };
        BaseBonusController.prototype.onGotResponse = function (data) {
        };
        return BaseBonusController;
    })(engine.BaseController);
    engine.BaseBonusController = BaseBonusController;
})(engine || (engine = {}));
var engine;
(function (engine) {
    var Tween = createjs.Tween;
    var Ticker = createjs.Ticker;
    var GambleController = (function (_super) {
        __extends(GambleController, _super);
        function GambleController(manager, common, container) {
            // TODO: нужно чтобы на сервере был стандартный step=0
            _super.call(this, manager, common, container, -1, true);
            this.serverStep = null;
            this.restore = false;
            this.XMLBalance = 0;
            GambleController.SHOW_MESSAGE_TIME_COMP = engine.Utils.float2int(GambleController.SHOW_MESSAGE_TIME * Ticker.getFPS());
            this.setMessagesLang();
        }
        GambleController.prototype.setMessagesLang = function () {
            GambleController.MESSAGE_DEFAULT = this.common.config.gamble_message_default || GambleController.MESSAGE_DEFAULT;
            GambleController.MESSAGE_WIN = this.common.config.gamble_message_win || GambleController.MESSAGE_WIN;
            GambleController.MESSAGE_LOSE = this.common.config.gamble_message_lose || GambleController.MESSAGE_LOSE;
        };
        GambleController.prototype.init = function () {
            _super.prototype.init.call(this);
            this.isFirst = true;
            this.restore = this.common.restore;
        };
        GambleController.prototype.create = function () {
            _super.prototype.create.call(this);
            this.view = this.common.layouts[engine.GambleView.LAYOUT_NAME];
            this.view.create();
            this.view.alpha = 1;
            this.bank = this.common.server.win;
            this.send(engine.NotificationList.REMOVE_HEADER);
            this.container.addChild(this.view);
            this.initHandlers();
            this.view.setTextPositions();
        };
        GambleController.prototype.processStep = function () {
            var _this = this;
            console.log(this.serverStep, "serverStep");
            switch (this.serverStep) {
                case 0:
                    {
                        if (this.isFirst) {
                            this.view.setMessageText(GambleController.MESSAGE_DEFAULT);
                            this.lockBonus(false);
                        }
                        this.hideCard();
                        break;
                    }
                case 1:
                    {
                        this.send(engine.NotificationList.SHOW_CARD);
                        if (this.win == 0) {
                            this.common.server.setBalance(this.XMLBalance);
                            this.view.setMessageText(GambleController.MESSAGE_LOSE);
                            // remove bonus
                            this.removeHandlers();
                            this.common.server.win = this.bank;
                            this.view.hideBonus(true, function () {
                                _this.send(engine.NotificationList.SHOW_WIN_TF);
                                _this.send(engine.NotificationList.UPDATE_BALANCE_TF);
                                _this.send(engine.NotificationList.END_BONUS);
                            });
                        }
                        else {
                            //this.bank = this.win;
                            this.common.server.setBalance(this.XMLBalance + this.bank);
                            this.view.setMessageText(GambleController.MESSAGE_WIN + this.bank);
                            Tween.get(this.view, { useTicks: true }).wait(GambleController.SHOW_MESSAGE_TIME_COMP).call(function () {
                                _this.hideCard();
                                _this.view.setMessageText(GambleController.MESSAGE_DEFAULT);
                                _this.lockBonus(false);
                            });
                        }
                        if (this.history.length > 0) {
                            this.view.showCard(this.history[this.history.length - 1]);
                        }
                        break;
                    }
            }
            this.view.setBankValue(this.bank);
            this.view.setDoubleValue(this.bank * 2);
            this.view.showHistory(this.history);
            if (this.isFirst) {
                this.isFirst = false;
            }
            if (this.restore) {
                this.restore = false;
            }
        };
        GambleController.prototype.hideCard = function () {
            this.send(engine.NotificationList.HIDE_CARD);
            this.view.hideCard();
        };
        GambleController.prototype.onGotResponse = function (data) {
            this.parseResponse(data);
            this.processStep();
        };
        GambleController.prototype.sendRequest = function (param) {
            if (param === void 0) { param = 0; }
            _super.prototype.sendRequest.call(this, param);
            this.lockBonus(true);
        };
        GambleController.prototype.dispose = function () {
            _super.prototype.dispose.call(this);
            this.container.removeChild(this.view);
            this.view = null;
        };
        GambleController.prototype.parseResponse = function (xml) {
            console.log(xml, "XML server");
            var XMLBalance = engine.XMLUtils.getElement(xml, "balance");
            console.log("XMLBalance", XMLBalance);
            if (XMLBalance.textContent) {
                this.XMLBalance = parseFloat(XMLBalance.textContent);
                if (parseFloat(XMLBalance.textContent) <= 0) {
                    this.XMLBalance = this.common.server.getBalance();
                }
            }
            this.bank = Math.abs(parseFloat(engine.XMLUtils.getElement(xml, "win").textContent));
            var data = engine.XMLUtils.getElement(xml, "data");
            this.history = [];
            if (data != null) {
                var serverStep = engine.XMLUtils.getElement(data, "step");
                //if(this.serverStep == null) this.serverStep = (serverStep) ? Math.abs(((parseInt(serverStep.textContent) == 0) ? 0 : parseInt(serverStep.textContent)-1) % 2) : (!this.bank && this.restore) ? ((this.isFirst) ? 0 : 1) : 1;
                //else this.serverStep = 1;
                if (this.serverStep == null)
                    this.serverStep = 0;
                else
                    this.serverStep = 1;
                var wheels = engine.XMLUtils.getElement(data, "wheels");
                this.win = engine.XMLUtils.getAttributeFloat(wheels, "bet") || 0;
                this.bank = Math.max(this.bank, this.win);
                if (wheels) {
                    var items = engine.XMLUtils.getChildrenElements(wheels);
                    var item;
                    var suit;
                    var cardId;
                    var frameId;
                    for (var i = items.length - 1; i >= 0; i--) {
                        item = items[i];
                        //TODO: нужно чтобы сервер присылал реальный id карты в калоде от 1 - 52
                        // on server suit from 1 to 4
                        suit = parseInt(item.attributes.suit.value) - 1;
                        // on server card id from 2 to 14
                        cardId = parseInt(item.textContent) - 2;
                        frameId = cardId + (GambleController.CARDS_COUNT * suit);
                        this.history.push(frameId);
                        if (this.history.length > 5)
                            this.history.shift();
                    }
                }
            }
        };
        GambleController.prototype.initHandlers = function () {
            var _this = this;
            var buttonsNames = this.view.buttonsNames;
            var buttonsCount = buttonsNames.length;
            for (var i = 0; i < buttonsCount; i++) {
                this.view.getButton(buttonsNames[i]).on("click", function (eventObj) {
                    if (eventObj.nativeEvent instanceof MouseEvent) {
                        _this.onBtnClick(eventObj.currentTarget.name);
                    }
                });
            }
        };
        GambleController.prototype.removeHandlers = function () {
            var buttonsNames = this.view.buttonsNames;
            var buttonsCount = buttonsNames.length;
            for (var i = 0; i < buttonsCount; i++) {
                this.view.getButton(buttonsNames[i]).removeAllEventListeners("click");
            }
        };
        GambleController.prototype.onBtnClick = function (buttonName) {
            var _this = this;
            switch (buttonName) {
                case engine.GambleView.COLLECT_BTN:
                    {
                        // remove bonus
                        //if(this.common.restore){
                        //	this.common.server.setBalance(this.XMLBalance + this.bank);
                        //}else{
                        //	this.common.server.setBalance(this.XMLBalance);
                        //}
                        this.common.server.setBalance(this.XMLBalance + this.bank);
                        this.removeHandlers();
                        this.send(engine.NotificationList.SHOW_HEADER);
                        this.view.hideBonus(false, function () {
                            _this.common.server.win = _this.bank;
                            _this.sendRequest();
                            _this.send(engine.NotificationList.END_BONUS);
                            _this.send(engine.NotificationList.SHOW_WIN_TF);
                            _this.send(engine.NotificationList.UPDATE_BALANCE_TF);
                        });
                        break;
                    }
                case engine.GambleView.RED_BTN:
                    {
                        this.sendRequest(1);
                        break;
                    }
                case engine.GambleView.BLACK_BTN:
                    {
                        this.sendRequest(2);
                        break;
                    }
            }
        };
        GambleController.prototype.lockBonus = function (value) {
            if (this.view != null) {
                var buttonsNames = this.view.buttonsNames;
                var buttonsCount = buttonsNames.length;
                for (var i = 0; i < buttonsCount; i++) {
                    this.view.getButton(buttonsNames[i]).setEnable(!value);
                }
            }
        };
        GambleController.SHOW_MESSAGE_TIME = 2;
        GambleController.CARDS_COUNT = 13;
        GambleController.MESSAGE_DEFAULT = "";
        //private static MESSAGE_DEFAULT:string = "CHOOSE RED OR BLACK";
        GambleController.MESSAGE_WIN = "YOU WIN: ";
        GambleController.MESSAGE_LOSE = "DEALER WINS";
        return GambleController;
    })(engine.BaseBonusController);
    engine.GambleController = GambleController;
})(engine || (engine = {}));
var engine;
(function (engine) {
    var Ticker = createjs.Ticker;
    var MoneyFormatter = engine.MoneyFormatter;
    var GambleController5 = (function (_super) {
        __extends(GambleController5, _super);
        function GambleController5(manager, common, container) {
            _super.call(this, manager, common, container, -1, true);
            this.serverBet = 0;
            this.serverBank = 0;
            this.serverWin = 0;
            this.serverGBet = 0;
            this.currentBank = 0;
            GambleController5.SHOW_MESSAGE_TIME = engine.Utils.float2int(GambleController5.SHOW_MESSAGE_TIME_DEFAULT * Ticker.getFPS());
            this.setMessagesLang();
        }
        GambleController5.prototype.setMessagesLang = function () {
            GambleController5.MESSAGE_DEFAULT = this.common.config.gamble5_message_default || GambleController5.MESSAGE_DEFAULT;
            GambleController5.MESSAGE_DEFAULT_SECOND = this.common.config.gamble5_message_default_second || GambleController5.MESSAGE_DEFAULT_SECOND;
            GambleController5.MESSAGE_WIN = this.common.config.gamble5_message_win || GambleController5.MESSAGE_WIN;
            GambleController5.MESSAGE_LOSE = this.common.config.gamble5_message_lose || GambleController5.MESSAGE_LOSE;
        };
        GambleController5.prototype.init = function () {
            _super.prototype.init.call(this);
            this.isFirst = true;
        };
        GambleController5.prototype.create = function () {
            _super.prototype.create.call(this);
            this.view = this.common.layouts[engine.GambleView5.LAYOUT_NAME];
            this.view.create();
            this.view.alpha = 1;
            this.stepBonus = 1;
            this.container.addChild(this.view);
            this.initHandlers();
            this.send(engine.NotificationList.REMOVE_HEADER);
            this.view.setTextPositions(this.common.isMobile);
        };
        GambleController5.prototype.initCardsCover = function () {
            var _this = this;
            var cardName;
            for (var n = 2; n < 6; n++) {
                cardName = 'card_' + n;
                var targetCard = this.view.getButton(cardName);
                targetCard.cursor = "pointer";
                targetCard.on("click", function (eventObj) {
                    if (eventObj.nativeEvent instanceof MouseEvent) {
                        _this.onCardCoverClick(eventObj.currentTarget.name);
                    }
                });
            }
        };
        GambleController5.prototype.disableCardsCover = function () {
            var cardName;
            for (var n = 1; n < 6; n++) {
                cardName = 'card_' + n;
                var targetCard = this.view.getButton(cardName);
                targetCard.cursor = "default";
            }
        };
        GambleController5.prototype.onGotResponse = function (data) {
            this.parseResponse(data);
            switch (this.stepBonus) {
                case 1:
                    this.resetGame();
                    this.setBankValues();
                    this.disableCardsCover();
                    break;
                case 2:
                    this.view.showCard(this.history.shift());
                    break;
                case 3:
                    this.setBankValues();
                    this.view.showCard(this.history.shift());
                    this.setBankMessage();
                    this.disableCardsCover();
                    break;
            }
            this.common.restore = false;
        };
        GambleController5.prototype.setBankValues = function () {
            this.view.setBankValue(this.currentBank);
            this.view.setBetValue(this.currentBank * 2, 2);
            this.view.setBetValue(this.currentBank * 1.5, 1);
        };
        GambleController5.prototype.setBankMessage = function () {
            var _this = this;
            this.view.setHistory(this.history);
            this.view.showHistory(this.selectCardIndex);
            var self = this;
            if ((this.serverWin + this.serverBet) == 0) {
                this.view.setMessageText(GambleController5.MESSAGE_LOSE);
                this.view.setTotalValue(this.currentBank);
                this.removeHandlers();
                this.view.hideCard();
                this.common.server.win = this.currentBank; //TO FINISH
                this.view.hideBonus(true, function () {
                    _this.send(engine.NotificationList.END_BONUS);
                    _this.send(engine.NotificationList.UPDATE_BALANCE_TF);
                    _this.send(engine.NotificationList.SHOW_WIN_TF);
                    _this.send(engine.NotificationList.SHOW_HEADER);
                });
                this.common.server.setBalance(this.XMLBalance);
            }
            else {
                this.common.server.setBalance(this.XMLBalance + this.currentBank);
                this.view.setMessageText(GambleController5.MESSAGE_WIN + MoneyFormatter.format(this.currentBank, false));
                this.view.hideCard();
                this.common.server.bonus.step = 0;
                this.common.server.win = this.currentBank; //TO FINISH
                setTimeout(function (self) {
                    self.resetGame();
                }, 3300, self); //think
            }
        };
        GambleController5.prototype.sendRequest = function (param) {
            if (param === void 0) { param = 0; }
            _super.prototype.sendRequest.call(this, param);
            this.lockBonus(true);
        };
        GambleController5.prototype.dispose = function () {
            _super.prototype.dispose.call(this);
            this.container.removeChild(this.view);
            //this.view = null;
        };
        GambleController5.prototype.parseResponse = function (xml) {
            this.history = [];
            var XMLBalance = engine.XMLUtils.getElement(xml, "balance");
            //console.log("XMLBalance",XMLBalance);
            if (XMLBalance.textContent) {
                this.XMLBalance = parseFloat(XMLBalance.textContent);
            }
            this.serverBank = Math.abs(parseFloat(engine.XMLUtils.getElement(xml, "win").textContent));
            var data = engine.XMLUtils.getElement(xml, "data");
            if (data != null) {
                var wheels = engine.XMLUtils.getElement(data, "wheels");
                this.serverBet = engine.XMLUtils.getAttributeFloat(wheels, "bet") || 0;
                this.serverBank = engine.XMLUtils.getAttributeFloat(wheels, "bank") || 0;
                this.serverWin = engine.XMLUtils.getAttributeFloat(wheels, "win") || 0;
                this.serverGBet = engine.XMLUtils.getAttributeFloat(wheels, "gbet") || 0;
                this.selectCardIndex = engine.XMLUtils.getAttributeInt(wheels, "selected");
                //var items:any = wheels.children;
                var items = engine.XMLUtils.getChildrenElements(wheels);
                var item;
                for (var i = items.length - 1; i >= 0; i--) {
                    item = items[i];
                    //TODO: нужно чтобы сервер присылал реальный id карты в калоде от 1 - 52
                    // on server suit from 1 to 4
                    var suit = parseInt(item.attributes.suit.value) - 1;
                    // on server card id from 2 to 14
                    var cardId = parseInt(item.textContent) - 1;
                    var frameId = cardId + (GambleController5.CARDS_COUNT * suit);
                    this.history.unshift(frameId);
                }
                if (this.common.restore) {
                    if (this.serverBet > 0 && (this.serverBank == 0 || this.serverBank == this.serverBet) && this.serverWin == 0) {
                        //step2
                        this.stepBonus = 2;
                        this.isFirst = false;
                        var bet = 1;
                        if (this.serverBank == 0)
                            bet = 2;
                        this.showSecondStep(bet, false);
                    }
                    else if (this.serverBank > 0 && this.serverGBet < this.serverBank) {
                        //step1
                        this.stepBonus = 1;
                        this.isFirst = true;
                    }
                    else {
                        //step3
                        this.stepBonus = 1;
                        this.isFirst = false;
                    }
                }
            }
            this.currentBank = Math.max(this.serverBet, this.serverBank + this.serverWin, this.serverBank + this.serverBet);
            this.stepBonus %= 4;
        };
        GambleController5.prototype.resetGame = function () {
            this.view.setMessageText(GambleController5.MESSAGE_DEFAULT);
            this.lockBonus(false);
            this.view.alphaTextFild(1, 1);
            this.view.alphaTextFild(2, 1);
            this.view.alphaTextFild(3, 1);
            this.view.alphaTextFild(4, 0);
            //this.view.setWhite(1);
            this.stepBonus = 1;
            GambleController5.SHOW_MESSAGE_TIME = engine.Utils.float2int(GambleController5.SHOW_MESSAGE_TIME_DEFAULT * Ticker.getFPS());
        };
        GambleController5.prototype.showSecondStep = function (bet, is_send_request) {
            if (is_send_request === void 0) { is_send_request = true; }
            if (is_send_request) {
                this.sendRequest(bet);
            }
            this.lockBonus(true);
            this.view.alphaTextFild(1, 0);
            this.view.alphaTextFild(2, 0);
            if (bet == 2)
                this.view.alphaTextFild(3, 0);
            this.view.alphaTextFild(bet, 1);
            var value = (bet == 1) ? 1.5 : 2;
            var bank = (bet == 1) ? 0.5 : 1;
            var currentbet = Math.max(this.serverBet, this.serverBank + this.serverWin, this.serverBank + this.serverBet);
            //var currentbank = (bet == 1) ? currentbet*bank : 0;
            this.view.setTotalValue(currentbet * bank);
            this.view.setBetValue(currentbet * value, bet);
            this.view.setBankValue(currentbet);
            this.initCardsCover();
            this.view.setMessageText(GambleController5.MESSAGE_DEFAULT_SECOND);
        };
        GambleController5.prototype.initHandlers = function () {
            var _this = this;
            var buttonsNames = this.view.buttonsNames;
            var buttonsCount = buttonsNames.length;
            for (var i = 0; i < buttonsCount; i++) {
                this.view.changeButtonState(buttonsNames[i], true);
                this.view.getButton(buttonsNames[i]).on("click", function (eventObj) {
                    if (eventObj.nativeEvent instanceof MouseEvent) {
                        _this.onBtnClick(eventObj.currentTarget.name);
                    }
                });
            }
        };
        GambleController5.prototype.removeHandlers = function () {
            var buttonsNames = this.view.buttonsNames;
            var buttonsCount = buttonsNames.length;
            for (var i = 0; i < buttonsCount; i++) {
                this.view.getButton(buttonsNames[i]).removeAllEventListeners("click");
            }
        };
        GambleController5.prototype.onCardCoverClick = function (buttonName) {
            if (this.stepBonus == 2) {
                var name = buttonName.split('_');
                var indexCard = parseInt(name[1]);
                this.sendRequest(indexCard - 1);
                this.stepBonus++;
            }
        };
        GambleController5.prototype.onBtnClick = function (buttonName) {
            var _this = this;
            this.stepBonus++;
            switch (buttonName) {
                case engine.GambleView5.COLLECT_BTN:
                    {
                        // remove bonus
                        this.removeHandlers();
                        this.common.server.setBalance(this.XMLBalance + this.currentBank);
                        this.common.server.win = this.currentBank;
                        this.view.hideBonus(false, function () {
                            _this.sendRequest();
                            _this.send(engine.NotificationList.END_BONUS);
                            _this.send(engine.NotificationList.UPDATE_BALANCE_TF);
                            _this.send(engine.NotificationList.SHOW_WIN_TF);
                            _this.send(engine.NotificationList.SHOW_HEADER);
                        });
                        break;
                    }
                case engine.GambleView5.HALF_BTN:
                    {
                        this.showSecondStep(1);
                        break;
                    }
                case engine.GambleView5.DOUBLE_BTN:
                    {
                        this.showSecondStep(2);
                        break;
                    }
            }
        };
        GambleController5.prototype.endGame = function () {
        };
        GambleController5.prototype.lockBonus = function (value) {
            if (this.view != null) {
                var buttonsNames = this.view.buttonsNames;
                var buttonsCount = buttonsNames.length;
                for (var i = 0; i < buttonsCount; i++) {
                    this.view.getButton(buttonsNames[i]).setEnable(!value);
                    this.view.getButton(buttonsNames[i]).visible = !value;
                }
            }
        };
        GambleController5.SHOW_MESSAGE_TIME = 2;
        GambleController5.SHOW_MESSAGE_TIME_DEFAULT = 2;
        GambleController5.CARDS_COUNT = 13;
        GambleController5.MESSAGE_DEFAULT = "CHOOSE DOUBLE OR DOUBLE HALF";
        GambleController5.MESSAGE_DEFAULT_SECOND = "PICK A HIGHER CARD";
        GambleController5.MESSAGE_WIN = "YOU WIN: ";
        GambleController5.MESSAGE_LOSE = "DEALER WINS";
        return GambleController5;
    })(engine.BaseBonusController);
    engine.GambleController5 = GambleController5;
})(engine || (engine = {}));
var engine;
(function (engine) {
    var Ticker = createjs.Ticker;
    var Sound = createjs.Sound;
    var FreeSpinsController = (function (_super) {
        __extends(FreeSpinsController, _super);
        function FreeSpinsController(manager, common, container) {
            _super.call(this, manager, common, container, 0, true);
            this.featureWin = 0;
            this.featureWinPrev = 0;
            this.isSecondGame = false;
            this.addFSCount = 0; //show winpopup if win add spins
            this.currentWildReel = 2;
            this.multiplier = 0;
        }
        FreeSpinsController.prototype.init = function () {
            _super.prototype.init.call(this);
            FreeSpinsController.UPDATE_WIN_TIME_UP = engine.Utils.float2int(FreeSpinsController.UPDATE_WIN_TIME * Ticker.getFPS());
        };
        FreeSpinsController.prototype.create = function () {
            _super.prototype.create.call(this);
            this.send(engine.NotificationList.REMOVE_HEADER);
            this.view = this.common.layouts[engine.FreeSpinsView.LAYOUT_NAME];
            this.view.create();
            this.container.addChild(this.view);
        };
        FreeSpinsController.prototype.listNotification = function () {
            var notifications = _super.prototype.listNotification.call(this);
            notifications.push(engine.NotificationList.START_BONUS);
            notifications.push(engine.NotificationList.STOPPED_ALL_REELS);
            notifications.push(engine.NotificationList.WIN_LINES_SHOWED);
            notifications.push(engine.NotificationList.RECURSION_PROCESSED);
            notifications.push(engine.NotificationList.RECURSION_PROCESS_FALLING);
            return notifications;
        };
        FreeSpinsController.prototype.handleNotification = function (message, data) {
            _super.prototype.handleNotification.call(this, message, data);
            switch (message) {
                case engine.NotificationList.START_BONUS:
                    {
                        this.isBonusStarted = true;
                        this.setLeftSpins();
                        if (this.isSecondGame) {
                            this.isSecondGame = false;
                        }
                        else {
                            if (data == 'spins' || this.common.originBonus == engine.BonusTypes.SELECT_GAME) {
                                this.removeStartPopup();
                            }
                        }
                        console.log(engine.NotificationList.START_BONUS + " this.leftSpins=" + this.leftSpins + " this.count=" + this.count, "FreeSpinsController");
                        if (this.leftSpins < (this.count - 1) || data == 'spins' || this.common.originBonus == engine.BonusTypes.SELECT_GAME || this.common.restore)
                            this.send(engine.NotificationList.SHOW_WILD_REEL_FADEIN, this.currentWildReel);
                        break;
                    }
                case engine.NotificationList.STOPPED_ALL_REELS:
                    {
                        this.tryShowMoreFreeSpinsPopupForTime();
                        if (this.view != null) {
                            this.view.setWin(this.featureWin, FreeSpinsController.UPDATE_WIN_TIME_UP);
                        }
                        this.send(engine.NotificationList.SHOW_WIN_LINES, true);
                        this.send(engine.NotificationList.SHOW_WIN_TF);
                        break;
                    }
                case engine.NotificationList.RECURSION_PROCESSED:
                    {
                        if (this.isBonusStarted) {
                            if (this.leftSpins == 0) {
                                this.container.removeChild(this.view);
                                this.send(engine.NotificationList.SHOW_HEADER);
                                this.showResultPopup();
                            }
                            else {
                                this.setLeftSpins();
                                this.send(engine.NotificationList.REMOVE_WIN_LINES);
                                this.send(engine.NotificationList.START_SPIN);
                                //this.send(NotificationList.START_SPIN_AUDIO);
                                this.sendRequest();
                            }
                        }
                        break;
                    }
                case engine.NotificationList.RECURSION_PROCESS_FALLING:
                    {
                        if (this.common.server.recursion.length > 1) {
                            this.showNextMultiplier();
                        }
                        break;
                    }
            }
        };
        FreeSpinsController.prototype.setFields = function () {
            this.setLeftSpins();
            this.view.setMultiplicator(this.multiplicator);
            this.view.setWin(this.featureWin);
            this.send(engine.NotificationList.SHOW_WIN_TF);
            if (this.leftSpins == (this.count - 1)) {
                this.view.setWin(this.featureWinPrev);
            }
        };
        FreeSpinsController.prototype.onGotResponse = function (data) {
            this.parseResponse(data);
            if (this.isFist && !this.isSecondGame) {
                this.setFields();
                if (!this.common.restore)
                    this.showStartPopup();
            }
            else {
                this.send(engine.NotificationList.SERVER_GOT_SPIN);
            }
            this.common.restore = false;
            this.multiplier = 0;
            this.showNextMultiplier();
        };
        FreeSpinsController.prototype.onEnterFrame = function () {
            if (this.view != null) {
                this.view.update();
            }
        };
        FreeSpinsController.prototype.showStartPopup = function () {
            var startPopup = this.common.layouts[engine.FreeSpinsStartView.LAYOUT_NAME];
            startPopup.create();
            startPopup.alpha = 1;
            startPopup.setFreeSpinsCount(this.count);
            this.container.addChild(startPopup);
        };
        FreeSpinsController.prototype.tryShowMoreFreeSpinsPopupForTime = function () {
            if (this.addFSCount > 0) {
                this.showMorePopupForTime();
                this.addFSCount = 0;
            }
        };
        FreeSpinsController.prototype.showMorePopupForTime = function () {
            var _this = this;
            var morePopup = this.common.layouts[engine.FreeSpinsMoreView.LAYOUT_NAME];
            morePopup.create();
            morePopup.alpha = 1;
            morePopup.setFreeSpinsCount(this.addFSCount);
            this.container.addChild(morePopup);
            morePopup.hide(function () {
                _this.container.removeChild(morePopup);
            }, FreeSpinsController.START_POPUP_DELAY);
        };
        FreeSpinsController.prototype.removeStartPopup = function () {
            var _this = this;
            this.send(engine.NotificationList.START_SPIN);
            //this.send(NotificationList.START_SPIN_AUDIO);
            this.send(engine.NotificationList.SERVER_GOT_SPIN);
            var startPopup = this.common.layouts[engine.FreeSpinsStartView.LAYOUT_NAME];
            startPopup.hide(function () {
                _this.container.removeChild(startPopup);
            });
        };
        FreeSpinsController.prototype.showResultPopup = function () {
            var _this = this;
            var resultPopup = this.common.layouts[engine.FreeSpinsResultView.LAYOUT_NAME];
            resultPopup.create();
            resultPopup.alpha = 1;
            resultPopup.setTextPositions(this.common.isMobile);
            resultPopup.setGameWin(this.gameWin);
            resultPopup.setFeatureWin(this.featureWin);
            var totalWin = this.gameWin + this.featureWin;
            resultPopup.setTotalWin(totalWin);
            var collectBtn = resultPopup.getCollectBtn();
            collectBtn.removeAllEventListeners("click");
            collectBtn.on("click", function (eventObj) {
                if (eventObj.nativeEvent instanceof MouseEvent) {
                    collectBtn.removeAllEventListeners("click");
                    _this.hideResultPopup();
                }
            });
            this.container.addChild(resultPopup);
        };
        FreeSpinsController.prototype.hideResultPopup = function () {
            var _this = this;
            var resultView = this.common.layouts[engine.FreeSpinsResultView.LAYOUT_NAME];
            resultView.hide(function () {
                Sound.play(engine.SoundsList.COLLECT);
                _this.send(engine.NotificationList.END_BONUS);
                _this.send(engine.NotificationList.UPDATE_BALANCE_TF);
                _this.container.removeChild(resultView);
            });
        };
        FreeSpinsController.prototype.parseResponse = function (xml) {
            console.log(this.common.restore, 'restore');
            try {
                var XMLbalance = parseFloat(engine.XMLUtils.getElement(xml, "balance").textContent);
                if (XMLbalance) {
                    this.common.server.setBalance(XMLbalance);
                }
                var bonusnew = engine.XMLUtils.getElement(engine.XMLUtils.getElement(engine.XMLUtils.getElement(xml, "data"), "spin"), "bonus");
            }
            catch (ex) {
            }
            if (bonusnew && engine.XMLUtils.getAttributeInt(bonusnew, "id")) {
                this.addFSCount = engine.XMLUtils.getAttributeInt(bonusnew, "addFSCount");
                if (this.common.restore)
                    this.tryShowMoreFreeSpinsPopupForTime();
            }
            var data = engine.XMLUtils.getElement(xml, "data");
            var items = engine.XMLUtils.getElement(data, "items");
            if (items) {
                var finishBonus = engine.XMLUtils.getAttributeInt(items, "finishBonus");
                if (finishBonus && finishBonus == 1) {
                    var items = engine.XMLUtils.getElement(data, "items");
                    this.count = engine.XMLUtils.getAttributeInt(items, "spins");
                    this.leftSpins = this.count;
                    this.multiplicator = engine.XMLUtils.getAttributeInt(items, "spinsMultiplier");
                    this.isFist = true;
                    this.gameWin = engine.XMLUtils.getAttributeFloat(xml, "gamewin") || 0;
                    this.featureWin = 0;
                    this.common.server.bonus.step = 0;
                    this.isSecondGame = true;
                    this.setFields();
                    this.sendRequest(1);
                }
            }
            else {
                this.count = engine.XMLUtils.getAttributeInt(xml, "all");
                this.leftSpins = engine.XMLUtils.getAttributeInt(xml, "counter");
                this.multiplicator = engine.XMLUtils.getAttributeInt(xml, "mp");
                this.gameWin = engine.XMLUtils.getAttributeFloat(xml, "gamewin") || 0;
                this.isFist = (this.common.restore) ? true : (this.isSecondGame) ? this.leftSpins == this.count : this.leftSpins == this.count - 1;
                this.featureWin = engine.XMLUtils.getAttributeFloat(data, "summ") || 0;
                var serverWin = engine.XMLUtils.getElement(xml, "win");
                this.featureWinPrev = this.featureWin - parseFloat(serverWin.textContent);
                this.parseSpinData(data);
                var data = null;
                if (!this.common.restore && this.leftSpins < (this.count - 1))
                    data = 'spins';
                this.send(engine.NotificationList.START_BONUS, data);
            }
        };
        FreeSpinsController.prototype.parseSpinData = function (xml) {
            var spinData = engine.XMLUtils.getElement(xml, "spin");
            this.parseRecursion(spinData);
            var wheelsData = engine.XMLUtils.getElement(spinData, "wheels").childNodes;
            for (var i = 0; i < wheelsData.length; i++) {
                var wheelData = engine.Utils.toIntArray(wheelsData[i].childNodes[0].data.split(","));
                for (var j = 0; j < wheelData.length; j++) {
                    this.common.server.wheel[i * wheelData.length + j] = wheelData[j];
                }
                if (wheelData[0] == this.common.wildSymbolId)
                    this.currentWildReel = i;
            }
            var winLinesData = engine.XMLUtils.getElement(spinData, "winposition").childNodes;
            this.common.server.winLines = [];
            for (var i = 0; i < winLinesData.length; i++) {
                var winLine = winLinesData[i];
                var winLineVO = new engine.WinLineVO();
                winLineVO.lineId = engine.XMLUtils.getAttributeInt(winLine, "line");
                winLineVO.win = engine.XMLUtils.getAttributeFloat(winLine, "win");
                winLineVO.winPos = engine.Utils.toIntArray(winLine.childNodes[0].data.split(","));
                this.common.server.winLines.push(winLineVO);
            }
            //TODO: server return 0;
            //this.common.server.setBalance(parseFloat(XMLUtils.getElement(spinData, "balance").textContent));
            this.common.server.win = parseFloat(engine.XMLUtils.getElement(spinData, "win").textContent);
        };
        FreeSpinsController.prototype.parseRecursion = function (data) {
            this.common.server.recursion = [];
            var recursionData = engine.XMLUtils.getElement(data, "recursion");
            if (recursionData) {
                var recursionDataArr = recursionData.childNodes;
                for (var i = 0; i < recursionDataArr.length; i++) {
                    var wheelData = eval(recursionDataArr[i].attributes[0].nodeValue);
                    this.common.server.recursion[i] = [];
                    this.common.server.recursion[i]["wheels"] = wheelData;
                    this.common.server.recursion[i]["win"] = engine.XMLUtils.getAttributeInt(recursionDataArr[i], "win");
                    this.common.server.recursion[i]["winLines"] = [];
                    this.common.server.recursion[i]["arrLinesIds"] = [];
                    var winLinesData = engine.XMLUtils.getElement(recursionDataArr[i], "winposition").childNodes;
                    for (var j = 0; j < winLinesData.length; ++j) {
                        var winLine = winLinesData[j];
                        var winLineVO = new engine.WinLineVO();
                        winLineVO.lineId = engine.XMLUtils.getAttributeInt(winLine, "line");
                        winLineVO.win = engine.XMLUtils.getAttributeFloat(winLine, "win");
                        winLineVO.winPos = engine.Utils.toIntArray(winLine.childNodes[0].data.split(","));
                        this.common.server.recursion[i]["winLines"].push(winLineVO);
                        this.common.server.recursion[i]["arrLinesIds"].push(winLineVO.lineId);
                    }
                }
            }
            this.common.server.recursion_len = this.common.server.recursion.length;
            //console.log("this.common.server.recursion parseRecursion FreeSpinsController", this.common.server.recursion);
        };
        FreeSpinsController.prototype.setLeftSpins = function () {
            if (!this.addFSCount) {
                console.log("this.addFSCount=" + this.addFSCount + " START_BONUS");
                this.view.setLeftSpins(this.leftSpins);
            }
        };
        FreeSpinsController.prototype.showNextMultiplier = function () {
            this.view.showMultiplierAnimation(++this.multiplier);
        };
        FreeSpinsController.UPDATE_WIN_TIME = 0.5;
        FreeSpinsController.START_POPUP_DELAY = 2;
        return FreeSpinsController;
    })(engine.BaseBonusController);
    engine.FreeSpinsController = FreeSpinsController;
})(engine || (engine = {}));
var engine;
(function (engine) {
    var SelectItemController = (function (_super) {
        __extends(SelectItemController, _super);
        function SelectItemController(manager, common, container) {
            _super.call(this, manager, common, container, 0, false);
            this.serverCountStep = 0;
            common.originBonus = common.server.bonus.type;
        }
        SelectItemController.prototype.init = function () {
            _super.prototype.init.call(this);
            if (this.common.isMobile) {
                this.setHudButtons(false, false);
            }
        };
        SelectItemController.prototype.create = function () {
            console.log("create");
            _super.prototype.create.call(this);
            this.send(engine.NotificationList.REMOVE_HEADER);
            this.view = this.common.layouts[engine.SelectItemView.LAYOUT_NAME];
            this.view.create();
            this.view.showBonus();
            this.view.hideAllWinAnimation();
            this.container.addChild(this.view);
            this.spinCount = 8;
            this.multpCount = 2;
            var itemCount = this.view.getItemCount();
            this.itemsVO = new Array(itemCount);
            for (var i = 0; i < itemCount; i++) {
                this.itemsVO[i] = new engine.ItemVO(i);
            }
            this.setEnableAllItem(true);
            this.initStartButton();
            this.view.updateCounters(0, this.spinCount.toString());
            this.view.updateCounters(1, this.multpCount.toString());
        };
        SelectItemController.prototype.initHandlers = function () {
            var _this = this;
            console.log("initHandlers");
            var items = this.view.getItems();
            for (var i = 0; i < items.length; i++) {
                var item = items[i];
                item.on("click", function (eventObj) {
                    if (eventObj.nativeEvent instanceof MouseEvent) {
                        console.log("item click");
                        _this.setEnableAllItem(false);
                        var currentItem = eventObj.currentTarget;
                        var itemId = engine.SelectItemView.getItemIdByName(currentItem.name);
                        _this.sendRequest(itemId);
                    }
                });
            }
        };
        SelectItemController.prototype.startBonusGame = function () {
            console.log("startBonusGame");
            this.view.startBonusGame();
            this.initHandlers();
        };
        SelectItemController.prototype.initStartButton = function () {
            var _this = this;
            this.view.changeButtonState(engine.SelectItemView.START_BTN, true, true);
            //if(this.common.isMobile)this.view.setupMobile();
            var startBtn = this.view.getBtn(engine.SelectItemView.START_BTN);
            if (startBtn) {
                startBtn.on("click", function (eventObj) {
                    if (eventObj.nativeEvent instanceof MouseEvent) {
                        _this.startBonusGame();
                    }
                });
            }
            else {
                this.startBonusGame();
            }
        };
        SelectItemController.prototype.removeHandlers = function () {
            console.log("removeHandlers");
            var items = this.view.getItems();
            for (var i = 0; i < items.length; i++) {
                items[i].removeAllEventListeners("click");
            }
            this.view.getCollectBtn().removeAllEventListeners("click");
            this.view.getBtn(engine.SelectItemView.START_BTN).removeAllEventListeners("click");
            this.send(engine.NotificationList.SHOW_HEADER);
        };
        SelectItemController.prototype.showWinAnimation = function (vo) {
            var _this = this;
            this.view.showWinAnimation(vo, function () {
                if (_this.isFinish) {
                    //this.view.setTotalWin(this.calculateTotalWin());
                    _this.view.setTotalWin(_this.spinCount, _this.multpCount);
                    _this.view.showWinPopup();
                    _this.view.getCollectBtn().on("click", function (eventObj) {
                        if (eventObj.nativeEvent instanceof MouseEvent) {
                            _this.view.hideWinPopup(function () {
                                _this.send(engine.NotificationList.START_SECOND_FREE_GAME);
                            });
                        }
                    });
                }
                else {
                    _this.setEnableAllItem(true);
                }
            });
        };
        SelectItemController.prototype.setEnableAllItem = function (value) {
            var items = this.view.getItems();
            for (var i = 0; i < this.itemsVO.length; i++) {
                if (!this.itemsVO[i].isSelected) {
                    items[i].setEnable(value);
                }
            }
        };
        SelectItemController.prototype.calculateTotalWin = function () {
            var result = 0;
            for (var i = 0; i < this.itemsVO.length; i++) {
                var itemVO = this.itemsVO[i];
                if (itemVO.isSelected) {
                    result += itemVO.winValue;
                }
            }
            return result;
        };
        SelectItemController.prototype.onGotResponse = function (xml) {
            var data = engine.XMLUtils.getElement(xml, "data");
            var counter = engine.XMLUtils.getAttributeInt(xml, "counter");
            this.serverCountStep = engine.XMLUtils.getAttributeInt(data, "step");
            if (counter && counter != 0) {
                var all = engine.XMLUtils.getAttributeInt(xml, "all");
                this.common.server.bonus.step = all - counter;
                this.send(engine.NotificationList.START_SECOND_FREE_GAME);
            }
            else {
                //var wheels:any = XMLUtils.getElement(xml, "wheels");
                var items = engine.XMLUtils.getElement(data, "items");
                var item = engine.XMLUtils.getElements(items, "item");
                for (var i = 0; i < item.length; i++) {
                    var it = item[i];
                    var id = engine.XMLUtils.getAttributeInt(it, "id") - 1;
                    var vo = this.itemsVO[id];
                    vo.type = engine.XMLUtils.getAttributeInt(it, "type");
                    if (!vo.isSelected) {
                        vo.isSelected = engine.XMLUtils.getAttributeInt(it, "selected") == 1;
                        vo.winValue = parseFloat(it.textContent);
                        if (vo.isSelected) {
                            this.showWinAnimation(vo);
                            if (vo.type == 4) {
                                this.spinCount += vo.winValue;
                                this.view.updateCounters(0, this.spinCount.toString());
                            }
                            else {
                                this.multpCount += vo.winValue;
                                this.view.updateCounters(1, this.multpCount.toString());
                            }
                        }
                    }
                }
                this.isFinish = engine.XMLUtils.getAttributeInt(items, "finishLevel") == 1;
                //this.serverVersion = XMLUtils.getAttributeInt(items, "version");
                //this.serverFinishBonus = XMLUtils.getAttributeInt(items, "finishBonus");
                //this.serverSpins = XMLUtils.getAttributeInt(items, "spins");
                //this.serverSpinsMultiplier = XMLUtils.getAttributeInt(items, "spinsMultiplier");
                //this.serverCoinsMultiplierLevel = XMLUtils.getAttributeInt(items, "coinsMultiplierLevel");
                //this.serverCoinsMultiplierTotal = XMLUtils.getAttributeInt(items, "coinsMultiplierTotal");
                this.serverCountStep = engine.XMLUtils.getAttributeInt(items, "countStep");
            }
            console.log(this.serverCountStep, "serverCountStep");
            if (this.serverCountStep > 0 && this.common.restore) {
                switch (this.serverCountStep) {
                    case 1:
                        this.common.server.bonus.step = this.serverCountStep;
                        this.startBonusGame();
                        break;
                    case 2:
                        //this.view.startBonusGame();
                        //this.removeHandlers();
                        //this.isFinish = true;
                        this.send(engine.NotificationList.START_SECOND_FREE_GAME);
                        break;
                }
            }
        };
        SelectItemController.prototype.sendRequest = function (param) {
            if (param === void 0) { param = 0; }
            _super.prototype.sendRequest.call(this, param);
            this.setEnableAllItem(false);
        };
        SelectItemController.prototype.dispose = function () {
            _super.prototype.dispose.call(this);
            this.container.removeChild(this.view);
            this.removeHandlers();
            //this.view = null;
            //this.itemsVO = null;
            if (this.common.isMobile) {
                this.setHudButtons(true, false);
            }
        };
        SelectItemController.prototype.setHudButtons = function (visible, enable) {
            this.common.layouts[engine.HudView.LAYOUT_NAME].changeButtonState(engine.HudView.START_SPIN_BTN, visible, enable);
            //<HudView>this.common.layouts[HudView.LAYOUT_NAME].changeButtonState(HudView.GAMBLE_BTN, visible, enable);
            this.common.layouts[engine.HudView.LAYOUT_NAME].changeButtonState(engine.HudView.START_AUTO_PLAY_BTN, visible, enable);
        };
        return SelectItemController;
    })(engine.BaseBonusController);
    engine.SelectItemController = SelectItemController;
})(engine || (engine = {}));
var engine;
(function (engine) {
    var ErrorController = (function (_super) {
        __extends(ErrorController, _super);
        function ErrorController(manager, common, container) {
            _super.call(this, manager);
            this.common = common;
            this.container = container;
        }
        ErrorController.prototype.init = function () {
            _super.prototype.init.call(this);
        };
        ErrorController.prototype.listNotification = function () {
            var notifications = _super.prototype.listNotification.call(this);
            notifications.push(engine.NotificationList.SHOW_ERRORS);
            return notifications;
        };
        ErrorController.prototype.handleNotification = function (message, data) {
            switch (message) {
                case engine.NotificationList.SHOW_ERRORS:
                    {
                        break;
                    }
            }
        };
        ErrorController.prototype.dispose = function () {
            _super.prototype.dispose.call(this);
            this.common = null;
            this.container = null;
        };
        ErrorController.parseXMLErrors = function (xml) {
            var errors = [];
            var xml_error = engine.XMLUtils.getElement(xml, "error");
            if (xml_error) {
                errors.push(xml_error.textContent);
            }
            return errors;
        };
        ErrorController.ERROR_NO_MONEY_STR = "NO MONEY. PLEASE, CHECK YOUR BALANCE.";
        return ErrorController;
    })(engine.BaseController);
    engine.ErrorController = ErrorController;
})(engine || (engine = {}));
///<reference path="../../dts/createjs.d.ts" />
///<reference path="../../dts/cryptojs.d.ts" />
///<reference path="../../dts/easeljs.d.ts" />
///<reference path="../../dts/layoutjs.d.ts" />
///<reference path="../../dts/preloadjs.d.ts" />
///<reference path="../../dts/soundjs.d.ts" />
//# sourceMappingURL=slots-engine.js.map