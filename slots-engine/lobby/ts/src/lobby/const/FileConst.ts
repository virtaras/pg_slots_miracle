module lobby {
	export class FileConst {
		public static JSON_EXTENSION:string = ".json";
		public static LAYOUT_EXTENSION:string = ".layout";
		public static WOFF_EXTENSION:string = ".woff";
	}
}
