module lobby {
	export class LobbyConst {
		public static MAIN_RES_FOLDER:string = "assets/";
		public static LOBBY_RES_FOLDER:string = "lobby/";
		public static ICONS_FOLDER:string = "icons/";
		public static LAYOUT_FOLDER:string = "layout/";
		public static FONTS_FOLDER:string = "fonts/";

		public static GAME_CONFIG_NAME:string = "config";

		public static FPS:number = 30;
	}
}
