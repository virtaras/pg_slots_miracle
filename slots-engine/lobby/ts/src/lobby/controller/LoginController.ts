module lobby {
	import Button = layout.Button;
	import Container = createjs.Container;
	import Text = createjs.Text;

	export class LoginController extends BaseController {
		private static LOGIN_STATE:number = 1;
		private static PASSWORD_STATE:number = 2;

		private static LOGIN_STATE_TEXT:string = "login";
		private static PASSWORD_STATE_TEXT:string = "password";

		private common:CommonRefs;
		private container:Container;
		private view:LoginView;
		private state:number;

		constructor(manager:ControllerManager, common:CommonRefs, container:Container) {
			super(manager);
			this.common = common;
			this.container = container;
		}

		public init():void {
			super.init();
			this.view = this.common.layouts[LoginView.LAYOUT_NAME];
			this.view.create();
			this.initHandlers();
			this.show();
		}

		public listNotification():Array<string> {
			var notifications:Array<string> = super.listNotification();
			notifications.push(NotificationList.LOGOUT);
			notifications.push(NotificationList.AUTHORIZATION);
			return notifications;
		}

		public handleNotification(message:string, data:any):void {
			switch (message) {
				case NotificationList.LOGOUT:
				{
					this.show();
					break;
				}
				case NotificationList.AUTHORIZATION:
				{
					this.remove();
					break;
				}
			}
		}

		public show():void {
			this.state = LoginController.LOGIN_STATE;
			this.view.getInputTf().text = LoginController.LOGIN_STATE_TEXT;
			this.container.addChild(this.view);
		}

		public remove():void {
			this.view.dispose();
		}

		private initHandlers():void {
			for (var i:number = 0; i < LoginView.NUM_BUTTONS.length; i++) {
				var button:Button = <Button>this.view.getKeyboard().getChildByName(LoginView.NUM_BUTTONS[i]);
				button.on("click", (eventObj:any)=> {
					var value:string = this.view.getInputTf().text;
					if (value == LoginController.LOGIN_STATE_TEXT ||
						value == LoginController.PASSWORD_STATE_TEXT) {
						value = "";
					}
					var currentBtn:Button = eventObj.currentTarget;
					var buttonName:string = currentBtn.name;
					this.view.getInputTf().text = value + LoginView.NUM_BUTTONS.indexOf(buttonName);
				});
			}

			var okBtn:Button = <Button>this.view.getKeyboard().getChildByName(LoginView.OK_BTN);
			okBtn.on("click", (eventData:any)=> {
				var value:string = this.view.getInputTf().text;
				if (value != LoginController.LOGIN_STATE_TEXT &&
					value != LoginController.PASSWORD_STATE_TEXT &&
					value.length > 0) {
					this.onEnterClick();
				}
			});

			var cancelBtn:Button = <Button>this.view.getKeyboard().getChildByName(LoginView.CANCEL_BTN);
			cancelBtn.on("click", (eventData:any)=> {
				var value:string = this.view.getInputTf().text;
				if (value != LoginController.LOGIN_STATE_TEXT &&
					value != LoginController.PASSWORD_STATE_TEXT) {
					this.view.getInputTf().text = value.substr(0, value.length - 1);
				}
			});
		}

		private onEnterClick():void {
			switch (this.state) {
				case LoginController.LOGIN_STATE:
				{
					this.common.login = this.view.getInputTf().text;
					this.view.getInputTf().text = LoginController.PASSWORD_STATE_TEXT;
					this.state = LoginController.PASSWORD_STATE;
					break;
				}
				case LoginController.PASSWORD_STATE:
				{
					this.common.password = this.view.getInputTf().text;
					super.send(NotificationList.AUTHORIZATION);
					break;
				}
			}
		}
	}
}