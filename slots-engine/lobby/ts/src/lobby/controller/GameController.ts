module lobby {
	import Button = layout.Button;
	import Container = createjs.Container;
	import Stage = createjs.Stage;
	import Text = createjs.Text;

	export class GameController extends BaseController {
		private gameContainer:Container;
		private gameController:any;
		private common:CommonRefs;

		constructor(manager:ControllerManager, common:CommonRefs, gameContainer:Container) {
			super(manager);
			this.common = common;
			this.gameContainer = gameContainer;
		}

		public init():void {
			super.init();
		}

		public listNotification():Array<string> {
			var notifications:Array<string> = super.listNotification();
			notifications.push(NotificationList.START_GAME);
			return notifications;
		}

		public handleNotification(message:string, data:any):void {
			switch (message) {
				case NotificationList.START_GAME:
				{
					this.startGame();
					break;
				}
			}
		}

		public onEnterFrame():void {
			if (this.gameController != null) {
				this.gameController.gameUpdate();
			}
		}

		private startGame():void {
			window.onresize(null);
			this.gameController = new engine.GameController();
			this.gameContainer.on(engine.GameController.EVENT_BACK_TO_LOBBY, ()=> {
				this.closeGame();
			});
			this.gameController.start(this.common.currentGame.id, this.common.key, this.gameContainer);
		}

		private closeGame():void {
			while (this.gameContainer.getNumChildren() > 0) {
				this.gameContainer.removeChildAt(0);
			}
			this.gameController = null;
			this.common.currentGame = null;
			this.send(NotificationList.AUTHORIZED);
		}

		public dispose():void {
			super.dispose();
			this.gameController.dispose();
		}
	}
}